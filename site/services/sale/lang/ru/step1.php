<?
$MESS["SALE_WIZARD_PERSON_1"] = "Физическое лицо";

$MESS["SALE_WIZARD_PROP_FIO"] = 'Фамилия, Имя';
$MESS["SALE_WIZARD_PROP_EMAIL"] = 'Email';
$MESS["SALE_WIZARD_PROP_TEL"] = 'Телефон';
$MESS["SALE_WIZARD_PROP_INDEX"] = 'Индекс';
$MESS["SALE_WIZARD_PROP_ADDRESS"] = 'Адрес доставки';
$MESS["SALE_WIZARD_PROP_CITY"] = 'Город';
$MESS["SALE_WIZARD_PROP_GROUP_FIZ1"] = 'Личные данные';
$MESS["SALE_WIZARD_PROP_GROUP_FIZ2"] = 'Данные для доставки';

$MESS["SALE_WIZARD_YMoney"] = "Яндекс.Деньги";

$MESS["SALE_WIZARD_PS_CASH"] = "Наличные курьеру";
$MESS["SALE_WIZARD_PS_CASH_DESCR"] = "Оплата наличными при получении заказа курьеру.";

$MESS["SALE_WIZARD_PS_SB_NCB"] = "Номер кор./сч. банка получателя платежа";
$MESS["SALE_WIZARD_PS_BILL_"] = " ";
$MESS["SALE_WIZARD_PS_OS_DESCR"] = "Вы можете оплатить заказ в любом отделении Ощадбанка. За услугу по переводу денег с Вас возьмут от 3 до 7% от стоимости заказа, в зависимости от региона. Перечисление денег займет порядка 10 дней.";

$MESS["WIZ_VAT_2"] = "НДС 18%";
$MESS["WIZ_VAT_2_VALUE"] = "18";
$MESS["WIZ_PRICE_NAME"] = "Розничная цена";
$MESS["WIZ_VAT_1"] = "Без НДС";
$MESS["WIZ_SALE_STATUS_P"] = "Оплачен, формируется к отправке";
$MESS["WIZ_SALE_STATUS_DESCRIPTION_P"] = "Заказ оплачен, формируется к отправке клиенту.";

$MESS["WIZ_SALE_STATUS_N"] = "Принят, ожидается оплата";
$MESS["WIZ_SALE_STATUS_DESCRIPTION_N"] = "Заказ принят, но пока не обрабатывается.";
$MESS["WIZ_SALE_STATUS_P"] = "Оплачен, формируется к отправке";
$MESS["WIZ_SALE_STATUS_DESCRIPTION_P"] = "Заказ оплачен, формируется к отправке клиенту.";
$MESS["WIZ_SALE_STATUS_F"] = "Выполнен";
$MESS["WIZ_SALE_STATUS_DESCRIPTION_F"] = "Заказ отправлен клиенту";

$MESS["SALE_WIZARD_PS_COLLECT"] = "Наложенный платеж";
$MESS["SALE_WIZARD_PS_COLLECT_DESCR"] = "Оплата с помощью наложенного платежа";

$MESS["SALE_WIZARD_YCards"] = "Банковские карты";
$MESS["SALE_WIZARD_YTerminals"] = "Терминалы";

$MESS["SALE_WIZARD_PS_WM"] = "Оплата в платежной системе Web Money";
$MESS["SALE_WIZARD_PS_WM_DESCR"] = "Авторизация будет производится через процессинговый центр Assist.";

$MESS["SALE_WIZARD_PS_SB"] = "Сбербанк";
$MESS["SALE_WIZARD_PS_SB_DESCR"] = "Вы можете оплатить заказ в любом отделении Сбербанка. За услугу по переводу денег с Вас возьмут от 3 до 7% от стоимости заказа, в зависимости от региона. Перечисление денег займет порядка 10 дней.";
?>