<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("catalog"))
    return;

$iblockXMLFile = WIZARD_SERVICE_RELATIVE_PATH."/xml/".LANGUAGE_ID."/catalog.xml";

$iblockType = "catalog";

$permissions = Array(
	"1" => "X",
	"2" => "R"
);
$dbGroup = CGroup::GetList($by = "", $order = "", Array("STRING_ID" => "sale_administrator"));
if($arGroup = $dbGroup -> Fetch())
{
	$permissions[$arGroup["ID"]] = 'W';
}
$dbGroup = CGroup::GetList($by = "", $order = "", Array("STRING_ID" => "content_editor"));
if($arGroup = $dbGroup -> Fetch())
{
	$permissions[$arGroup["ID"]] = 'W';
}
$IBLOCK_CATALOG_ID = WizardServices::ImportIBlockFromXML(
	$iblockXMLFile,
	"goods".WIZARD_SITE_ID,
	$iblockType,
	WIZARD_SITE_ID,
	$permissions
);
/*echo $IBLOCK_CATALOG_ID.' ';
var_dump($IBLOCK_CATALOG_ID);
echo WIZARD_SITE_ID;*/


$arSites = array();
$db_res = CIBlock::GetSite($IBLOCK_CATALOG_ID);
while ($res = $db_res->Fetch())
	$arSites[] = $res["LID"];
if (!in_array(WIZARD_SITE_ID, $arSites))
{
	$arSites[] = WIZARD_SITE_ID;
	$iblock = new CIBlock;
	$iblock->Update($IBLOCK_CATALOG_ID, array("LID" => $arSites));
}

if ($IBLOCK_CATALOG_ID < 1)
	return;

CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/_index.php", array("CATALOG_IBLOCK_ID" => $IBLOCK_CATALOG_ID));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/catalog/index.php", array("CATALOG_IBLOCK_ID" => $IBLOCK_CATALOG_ID));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/catalog/index.php", array("SITE_DIR" => WIZARD_SITE_DIR));
CWizardUtil::ReplaceMacros($_SERVER['DOCUMENT_ROOT']."/bitrix/templates/adaptiv_shop/header.php", array("CATALOG_IBLOCK_ID" => $IBLOCK_CATALOG_ID));

#CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/catalog/index.php", array("SITE_DIR" => '/site_'.WIZARD_SITE_ID.'/'));

CWizardUtil::ReplaceMacros($_SERVER['DOCUMENT_ROOT'].'/local/php_interface/'.WIZARD_SITE_ID.'/init.php', array("CATALOG_IBLOCK_ID" => $IBLOCK_CATALOG_ID));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/ajax/favoriteHandler.php", array("CATALOG_IBLOCK_ID" => $IBLOCK_CATALOG_ID));
?>