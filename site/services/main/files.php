<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
if (!defined("WIZARD_SITE_ID"))
    return;
if (!defined("WIZARD_SITE_DIR"))
    return;
$path = str_replace("//", "/", WIZARD_ABSOLUTE_PATH . "/site/public/" . LANGUAGE_ID . "/");
$handle = @opendir($path);
if ($handle) {
    while ($file = readdir($handle)) {
        if (in_array($file, array(
            ".",
            "..",
            "bitrix_messages",
            "bitrix_admin",
            "bitrix_php_interface",
            "bitrix_js",
            "bitrix_images",
            "bitrix_themes"
        ))
        ) continue;
        if ($file == 'bitrix_php_interface_init')
            $to = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/' . WIZARD_SITE_ID;
        elseif ($file == 'upload')
            $to = $_SERVER['DOCUMENT_ROOT'] . '/upload/';
        else
            $to = WIZARD_SITE_PATH . "/" . $file;
        CopyDirFiles(
            $path . $file,
            $to,
            $rewrite = true,
            $recursive = true,
            $delete_after_copy = false
        );
    }
}

if(file_exists(WIZARD_ABSOLUTE_PATH."/site/local/"))
{
	CopyDirFiles(
		WIZARD_ABSOLUTE_PATH."/site/local/",
		$_SERVER['DOCUMENT_ROOT'].'/',
		$rewrite = true,
		$recursive = true,
		$delete_after_copy = false
	);
}

mkdir($_SERVER['DOCUMENT_ROOT']."/local/php_interface/".WIZARD_SITE_ID, 0777);

CopyDirFiles(   
	WIZARD_ABSOLUTE_PATH."/site/init/",
	$_SERVER['DOCUMENT_ROOT'].'/local/php_interface/'.WIZARD_SITE_ID.'/',
	$rewrite = true,
	$recursive = true,
	$delete_after_copy = false
);

copy(WIZARD_THEME_ABSOLUTE_PATH . "/favicon.ico", WIZARD_SITE_PATH . "favicon.ico");

CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/js/custom.js", array("SITE_DIR" => WIZARD_SITE_DIR));

/*CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/sect_slide_1.php", array("SITE_TITLE" => htmlspecialcharsbx($wizard->GetVar("siteName"))));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/local/templates/".WIZARD_TEMPLATE_ID."/include/logo.php", array("SITE_TITLE" => htmlspecialcharsbx($wizard->GetVar("siteName"))));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/sect_slide_1.php", array("SITE_SLOGAN" => htmlspecialcharsbx($wizard->GetVar("siteDescription"))));*/

$arUrlRewrite = array(); 
if (file_exists(WIZARD_SITE_ROOT_PATH."/urlrewrite.php"))
{
	include(WIZARD_SITE_ROOT_PATH."/urlrewrite.php");
}

$arNewUrlRewrite = array(
	array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."personal/profile/#",
		"RULE" => "",
		"ID" => "bitrix:sale.personal.profile",
		"PATH" => WIZARD_SITE_DIR."personal/profile/index.php",
	),
	array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."company/actions/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => WIZARD_SITE_DIR."company/actions/index.php",
	),
	array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."personal/order/#",
		"RULE" => "",
		"ID" => "bitrix:sale.personal.order",
		"PATH" => WIZARD_SITE_DIR."personal/order/index.php",
	),
	array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."company/news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => WIZARD_SITE_DIR."company/news/index.php",
	),
	array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."catalog/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => WIZARD_SITE_DIR."catalog/index.php",
	),
	array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."store/#",
		"RULE" => "",
		"ID" => "bitrix:catalog.store",
		"PATH" => WIZARD_SITE_DIR."store/index.php",
	),
	array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => WIZARD_SITE_DIR."news/index.php",
	),
);

foreach ($arNewUrlRewrite as $arUrl)
{
	if (!in_array($arUrl, $arUrlRewrite))
	{
		CUrlRewriter::Add($arUrl);
	}
}

?>