<?
$MESS["FORM"] = "Формы";

$MESS['CALL_BACK_NAME'] = 'Заказать обратный звонок';
$MESS['CALL_BACK_BUTTON'] = 'Сохранить';
$MESS['CALL_BACK_DESCRIPTION'] = 'Заказать обратный звонок';

$MESS['FIELDS_NAME_TITLE'] = 'Имя';
$MESS['FIELDS_PHONE_TITLE'] = 'Телефон';

$MESS['STATUS_TITLE'] = 'Опубликовано';
$MESS['STATUS_DESCRIPTION'] = 'Окончательный статус';

?>