﻿<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

if(!CModule::IncludeModule("iblock"))
    return;

if(!CModule::IncludeModule("form"))
    return;

function randomStr($length = 4){
	$chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
	$numChars = strlen($chars);
	$string = '';
	for ($i = 0; $i < $length; $i++) {
		$string .= substr($chars, rand(1, $numChars) - 1, 1);
	}
	return $string;
}

$idForm = "CALL_BACK_FORM_".randomStr().'_'.WIZARD_SITE_ID;
$arFields = array(
    "NAME"              => GetMessage('CALL_BACK_NAME'),
    "SID"               => $idForm,
    "C_SORT"            => 100,
    "BUTTON"            => GetMessage('CALL_BACK_BUTTON'),
    "DESCRIPTION"       => GetMessage('CALL_BACK_DESCRIPTION'),
    "DESCRIPTION_TYPE"  => "text",
    "arSITE"            => array(WIZARD_SITE_ID),
    "arMENU"            => array("ru" => GetMessage('CALL_BACK_NAME'), "en" => ""),
    //"arGROUP"           => array("2" => "15", "3" => "20"),
    );

// add web form
$NEW_ID = CForm::Set($arFields);

//fields
$nameId = "name_".WIZARD_SITE_ID;
$arFields = array( 
    "FORM_ID"             => $NEW_ID,
    "ACTIVE"              => "Y",
    "TITLE"               => GetMessage('FIELDS_NAME_TITLE'),
    "SID"                 => $nameId,
    "C_SORT"              => 100,
    "ADDITIONAL"          => "N",
    "IN_RESULTS_TABLE"    => "Y",
    "IN_EXCEL_TABLE"      => "Y",
    "FIELD_TYPE"          => "text",
    "FILTER_TITLE"        => GetMessage('FIELDS_NAME_TITLE'),
    "RESULTS_TABLE_TITLE" => GetMessage('FIELDS_NAME_TITLE'),
    "arFILTER_FIELD"      => array("text")
    );
//CFormAnswer::Set($arFields);
$fieldID = CFormField::Set($arFields);

$arFields = array(
    "QUESTION_ID"   => $fieldID,
    "MESSAGE"       => " ",
    "C_SORT"        => 100,
    "ACTIVE"        => "Y",
    "FIELD_TYPE"    => "text",
    "FIELD_WIDTH"   => "40"
    );

$nameId = CFormAnswer::Set($arFields);

$telephoneId = "telephone_".WIZARD_SITE_ID;
$arFields = array( 
    "FORM_ID"             => $NEW_ID,
    "ACTIVE"              => "Y",
    "TITLE"               => GetMessage('FIELDS_PHONE_TITLE'),
    "SID"                 => $telephoneId,
    "C_SORT"              => 200,
    "ADDITIONAL"          => "N",
    "IN_RESULTS_TABLE"    => "Y",
    "IN_EXCEL_TABLE"      => "Y",
    "FIELD_TYPE"          => "text",
    "FILTER_TITLE"        => GetMessage('FIELDS_PHONE_TITLE'),
    "RESULTS_TABLE_TITLE" => GetMessage('FIELDS_PHONE_TITLE'),
    "arFILTER_FIELD"      => array("text")
    );
	
//CFormAnswer::Set($arFields);
$fieldID = CFormField::Set($arFields);
$arFields = array(
    "QUESTION_ID"   => $fieldID,
    "MESSAGE"       => " ",
    "C_SORT"        => 100,
    "ACTIVE"        => "Y",
    "FIELD_TYPE"    => "text",
    "FIELD_WIDTH"   => "40"
    );

$telephoneId = CFormAnswer::Set($arFields);

/*status*/

$arFields = array(
    "FORM_ID"             => $NEW_ID,               // ID веб-формы
    "C_SORT"              => 100,                    // порядок сортировки
    "ACTIVE"              => "Y",                    // статус активен
    "TITLE"               => GetMessage('STATUS_TITLE'),       // заголовок статуса
    "DESCRIPTION"         => GetMessage('STATUS_DESCRIPTION'),// описание статуса
    "CSS"                 => "statusgreen",          // CSS класс
    "HANDLER_OUT"         => "",                     // обработчик
    "HANDLER_IN"          => "",                     // обработчик
    "DEFAULT_VALUE"       => "Y",                    // не по умолчанию
    "arPERMISSION_VIEW"   => array(2),               // право просмотра для всех
    "arPERMISSION_MOVE"   => array(),                // право перевода только админам
    "arPERMISSION_EDIT"   => array(),                // право редактирование для админам
    "arPERMISSION_DELETE" => array(),                // право удаления только админам
);

CFormStatus::Set($arFields);

CWizardUtil::ReplaceMacros($_SERVER['DOCUMENT_ROOT'].'/local/php_interface/'.WIZARD_SITE_ID.'/init.php', array("REQUEST_CALL_FORM_ID" => $NEW_ID));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/ajax/handleRequestCall.php", array("NAME" => 'form_text_'.$nameId));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/ajax/handleRequestCall.php", array("TELEPHONE" => 'form_text_'.$telephoneId));
?>