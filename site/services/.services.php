<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arServices = Array(
    'main' => Array(
        'NAME' => GetMessage("SERVICE_MAIN_SETTINGS"),
        'STAGES' => Array(
            "files.php",
            "template.php",
			"theme.php"
        ),
    ),
    "form" => Array(
        "NAME" => GetMessage("FORM"),
        "STAGES" => Array(
            "form.php",
        ),
    ),
    "iblock" => Array(
        "NAME" => GetMessage("SERVICE_IBLOCK_DEMO_DATA"),
        "STAGES" => Array(
            "types.php",        //типы
            "references.php",   //импорт производители
            "news.php",         //импорт новости
            "actions.php",      //импорт акции
            "slider.php",       //импорт слайдер
            "catalog.php",      //импорт товаров
        ),
    ),
    "sale" => Array(
        "NAME" => GetMessage("SALE"),
        "STAGES" => Array(
			"locations.php",
            "step1.php",
            "step2.php",
            "step3.php",
        ),
    ),
);
?>