<?
class CIBlockElementHandler
{
	public static function OnAfterIBlockElementAdd($arFields)
	{
		if ($arFields["RESULT"]) {
			switch ($arFields["IBLOCK_ID"]) {
				case IBLOCK_ID_CATALOG:
					/** @noinspection PhpDynamicAsStaticMethodCallInspection */
					$dbResult = CIBlockElement::GetList(
						array('ID' => 'ASC'),
						array('ID' => $arFields['ID'], 'IBLOCK_ID' => $arFields["IBLOCK_ID"]),
						false,
						false,
						array('ID', 'NAME', 'PROPERTY_DEFAULT_RATING')
					);

					if ($arItem = $dbResult->Fetch()) {
						$arItem['PROPERTY_DEFAULT_RATING_VALUE'] = intval($arItem['PROPERTY_DEFAULT_RATING_VALUE']);

						if ($arItem['PROPERTY_DEFAULT_RATING_VALUE'] <= 0) {
							CIBlockElement::SetPropertyValuesEx($arFields['ID'], $arFields['IBLOCK_ID'], array(
								'DEFAULT_RATING' => PRODUCT_DEFAULT_RATING
							));
						}
					}
					break;
			}
		}
	}

	public function OnAfterIBlockElementAddUpdateHandler(&$arFields) {
		if ($arFields["RESULT"])
		{
            switch ($arFields["IBLOCK_ID"]) {
                case IBLOCK_ID_CATALOG:
                    $ID = $arFields['ID'];
                    break;
                case IBLOCK_ID_OFFERS:
                    $dbProp = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>IBLOCK_ID_OFFERS, "CODE" => 'CML2_LINK'));
                    if ($arProp = $dbProp->Fetch()) {
                        $arr_ID = current($arFields['PROPERTY_VALUES'][$arProp['ID']]);
						$ID = $arr_ID['VALUE'];
                    }
                    break;
            }

            if ($ID) {

                $minPrice = 0;
                $maxPrice = 0;
                \Bitrix\Main\Loader::includeModule('catalog');

                $arPrice = CIBlockPriceTools::GetCatalogPrices(IBLOCK_ID_CATALOG, array('BASE'));

                $priceRes = CPrice::GetList(array(), array("PRODUCT_ID" => $ID, "CATALOG_GROUP_ID" => $arPrice['BASE']['ID']));
                if ($arProductPrice = $priceRes->Fetch()) {
                    $minPrice = intval($arProductPrice['PRICE']);
                    $maxPrice = intval($arProductPrice['PRICE']);
                }

                $arOffers = CIBlockPriceTools::GetOffersArray(
                    array(
                        'IBLOCK_ID' => IBLOCK_ID_CATALOG,
                    )
                    ,array($ID)
                    ,array(//sort
                    )
                    ,array( //sel_fields
                    )
                    ,array( //sel_props
                    )
                    ,0
                    ,$arPrice
                );

                if (!empty($arOffers) && is_array($arOffers)) {
                    foreach ($arOffers as $arOffer) {
                        if (intval($arOffer['PRICES']['BASE']['VALUE']) < $minPrice || !$minPrice)
                            $minPrice = intval($arOffer['PRICES']['BASE']['VALUE']);

                        if (intval($arOffer['PRICES']['BASE']['VALUE']) > $maxPrice || !$maxPrice)
                            $maxPrice = intval($arOffer['PRICES']['BASE']['VALUE']);
                    }
                }

                if ($minPrice > 0 && $maxPrice > 0)
                {
                    CIBlockElement::SetPropertyValuesEx($ID, IBLOCK_ID_CATALOG, array('MINIMUM_PRICE' => $minPrice, 'MAXIMUM_PRICE' => $maxPrice));
                }
            }
		}
	}
}