<?
//require_once(__DIR__."/include/class/CCatalogData.php");

//Подключим  полезные утилиты
require(__DIR__.'/include/utils.php');

AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("CIBlockElementHandler", "OnAfterIBlockElementAdd"));
AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("CIBlockElementHandler", "OnAfterIBlockElementAddUpdateHandler"));
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("CIBlockElementHandler", "OnAfterIBlockElementAddUpdateHandler"));

AddEventHandler("blog", "OnCommentAdd", Array("CBlogCommentHandler", "OnCommentAdd"));
AddEventHandler("blog", "OnCommentUpdate", Array("CBlogCommentHandler", "OnCommentUpdate"));
AddEventHandler("blog", "OnCommentDelete", Array("CBlogCommentHandler", "OnCommentDelete"));
AddEventHandler("main", "OnBeforeUserRegister", Array("MainEventHandler", "OnBeforeUserRegisterHandler"));

include(__DIR__.'/event_handlers/IBlockElementAddUpdateHandler.php');
include(__DIR__.'/event_handlers/BlogCommentHandler.php');
include(__DIR__.'/event_handlers/MainHandler.php');