<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult['VARIANTS']) && is_array($arResult['VARIANTS'])):?>
<div class="sorting">
	<span class="sort-text">Сортировать по:</span>
	<form method="post">
		<select class="select-class" onchange="makeSort(this)">
		<?foreach ($arResult['VARIANTS'] as $code => $arSort):?>
			<option <?=$arSort['SELECTED'] == 'Y' ? 'selected' : ''?> value="<?=$APPLICATION->GetCurPageParam("sort=".$code, array("sort", "PAGEN_1"))?>"><?=$arSort['title']?></option>
		<?endforeach;?>	
		</select>
	</form>
</div>
<script>
	function makeSort(elem) {
		if ($(elem).val() != '')
			window.location.href = $(elem).val();
	}
</script>
<?endif;?>