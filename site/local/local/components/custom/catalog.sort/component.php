<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if (!isset($arParams['SORT_FIELDS']) && empty($arParams['SORT_FIELDS'])) {
	$arParams['SORT_FIELDS'] = array(
		'price_asc' => array(
			'title' => GetMessage('SORT_TITLE_PRICE_ASC'),
			'sort' => array(
				array(
					'FIELD' => 'PROPERTY_MINIMUM_PRICE',
					'ORDER'	=> 'asc,nulls',
				)	
			)
		),	
		'price_desc' => array(
			'title' => GetMessage('SORT_TITLE_PRICE_DESC'),
			'sort' => array(
				array(
					'FIELD' => 'PROPERTY_MINIMUM_PRICE',
					'ORDER'	=> 'desc,nulls',
				)
			)
		),	
		'manufacture' => array(
			'title' => GetMessage('SORT_TITLE_MANUFACTURE'),
			'sort' => array(
				array(
					'FIELD' => 'PROPERTY_MANUFACTURER.NAME',
					'ORDER'	=> 'asc',
				)
			)	
		),
		'manufacture_price_asc' => array(
			'title' => GetMessage('SORT_TITLE_MANUFACTURE_PRICE_ASC'),
			'sort' => array(
				array(
					'FIELD' => 'PROPERTY_MANUFACTURER.NAME',
					'ORDER'	=> 'asc',
				),
				array(
					'FIELD' => 'PROPERTY_MINIMUM_PRICE',
					'ORDER'	=> 'asc,nulls',
				),
			)	
		),
	);
}

$arResult['VARIANTS'] = $arParams['SORT_FIELDS'];

$arResult['SORT'] = array(
	array(
		'FIELD' => 'PROPERTY_MINIMUM_PRICE',
		'ORDER' => 'asc,nulls'
	),
	array(
		'FIELD' => 'NAME',
		'ORDER' => 'asc'
	),
	
);
if (isset($_GET['sort']) && !empty($_GET['sort']) && isset($arParams['SORT_FIELDS'][$_GET['sort']])) {
	$arResult['SORT']['SORTED'] = 'Y';
	$arResult['VARIANTS'][$_GET['sort']]['SELECTED'] = 'Y';
	if (!empty($arParams['SORT_FIELDS'][$_GET['sort']]['sort'][0]))
		$arResult['SORT'][0] = $arParams['SORT_FIELDS'][$_GET['sort']]['sort'][0];
	
	if (!empty($arParams['SORT_FIELDS'][$_GET['sort']]['sort'][1]))
		$arResult['SORT'][1] = $arParams['SORT_FIELDS'][$_GET['sort']]['sort'][1];
}

$this->IncludeComponentTemplate();
return $arResult['SORT'];
?>
