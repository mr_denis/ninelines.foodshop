<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class CCatalogSectionInfoComponent extends \CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
    {
		$arParams['ADD_SECTIONS_CHAIN'] = $arParams['ADD_SECTIONS_CHAIN'] != "N" ? true : false;
		$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);

        return $arParams;
    }
	
	public function executeComponent()
    {
		if (!\Bitrix\Main\Loader::includeModule("iblock"))
			$this->arResult['ERRORS'][] = GetMessage("IBLOCK_MODULE_NOT_FOUND");
		else 
		{
			$obCache = new CPHPCache();
			if ($obCache->InitCache($this->arParams['CACHE_TIME'], serialize($this->arParams), "/iblock/catalog.section.info"))
			{
				$this->arResult = $obCache->GetVars();
			}
			elseif ($obCache->StartDataCache())
			{
				if($this->arParams["SECTION_ID"] > 0)
				{
					$arFilter["ID"]=$this->arParams["SECTION_ID"];
					$rsSection = CIBlockSection::GetList(array(), $arFilter, false, $arSelect);
					$rsSection->SetUrlTemplates("", $this->arParams["SECTION_URL"]);
					$this->arResult = $rsSection->GetNext();
					if($this->arResult)
						$bSectionFound = true;
				}
				elseif(strlen($this->arParams["SECTION_CODE"]) > 0)
				{
					$arFilter["=CODE"]=$this->arParams["SECTION_CODE"];
					
					$rsSection = CIBlockSection::GetList(array(), $arFilter, false, $arSelect);
					$rsSection->SetUrlTemplates("", $this->arParams["SECTION_URL"]);
					$this->arResult = $rsSection->GetNext();
					
					if($this->arResult)
						$bSectionFound = true;
				}
				else
				{
					//Root section (no section filter)
					$this->arResult = array(
						"ID" => 0,
						"IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
					);
					$bSectionFound = true;
				}

				if(!$bSectionFound)
				{
					$this->AbortResultCache();
					ShowError(GetMessage("CATALOG_SECTION_NOT_FOUND"));
					@define("ERROR_404", "Y");
					if($this->arParams["SET_STATUS_404"]==="Y")
						CHTTP::SetStatus("404 Not Found");
					return;
				}
				elseif($this->arResult["ID"] > 0 && $this->arParams["ADD_SECTIONS_CHAIN"])
				{
					global $APPLICATION;
					$this->arResult["PATH"] = array();
					$rsPath = CIBlockSection::GetNavChain($this->arResult["IBLOCK_ID"], $this->arResult["ID"]);
					$rsPath->SetUrlTemplates("", $this->arParams["SECTION_URL"]);
					while($arPath = $rsPath->GetNext())
					{
						$APPLICATION->AddChainItem($arPath['NAME'], $arPath['SECTION_PAGE_URL']);
						$this->arResult["PATH"][]=$arPath;
					}
					
					if(defined("BX_COMP_MANAGED_CACHE"))
					{
						global $CACHE_MANAGER;
						$CACHE_MANAGER->StartTagCache("/iblock/catalog/section.info");

						$CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);
						
						$CACHE_MANAGER->EndTagCache();
					}
				}
				
				$obCache->EndDataCache($this->arResult);
			}	
		}	
		$this->includeComponentTemplate();
		
		return intval($this->arResult['ID']);
	}
}
?>