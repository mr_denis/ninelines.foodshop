<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<p class="address profile-address" id="address-<?=$arResult['ID']?>">
    <span class="address-r1"><?=implode(', ', $arResult['ADDRESS'])?></span>
    <span class="address-r2"><a class="edit-address-link change" href="javascript:void(0)" data-id="<?=$arResult['ID']?>" title="">Изменить</a><a href="javascript:void(0)" class="del-address-link change"  data-id="<?=$arResult['ID']?>" title="">Удалить адрес</a></span>
</p>
