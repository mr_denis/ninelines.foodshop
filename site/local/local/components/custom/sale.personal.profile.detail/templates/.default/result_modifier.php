<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 25.12.14
 * Time: 16:00
 */
foreach ($arResult['ORDER_PROPS'] as $arPropType)
    foreach ($arPropType['PROPS'] as $arProp)
        $arResult['ORDER_PROPS_BY_CODE'][$arProp['CODE']] = $arProp;

$dbRes = CSaleLocation::GetList(
    array(
        "SORT" => "ASC",
        "COUNTRY_NAME_LANG" => "ASC",
        "CITY_NAME_LANG" => "ASC"
    ),
    array(
        "LID" => LANGUAGE_ID,
        "ID" => $arResult['ORDER_PROPS_VALUES']['ORDER_PROP_'.$arResult['ORDER_PROPS_BY_CODE']['CITY']['ID']]
    ),
    false,
    false,
    array()
);
$arRes = $dbRes->Fetch();
$arResult['ADDRESS'] = array();
if (!empty($arRes))
    $arResult['ADDRESS'][] = 'г. '.$arRes['CITY_NAME'];

if (!empty($arResult['ORDER_PROPS_VALUES']['ORDER_PROP_'.$arResult['ORDER_PROPS_BY_CODE']['ADDRESS']['ID']]))
    $arResult['ADDRESS'][] = $arResult['ORDER_PROPS_VALUES']['ORDER_PROP_'.$arResult['ORDER_PROPS_BY_CODE']['ADDRESS']['ID']];