<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (isset($_POST['save']) || isset($_POST['add'])):?>
    <?if (!empty($arResult["ERROR_MESSAGE"])):?>
        <p><?=$arResult["ERROR_MESSAGE"]?></p>
    <?else:?>
        <p>Данные успешно сохранены</p>
    <?endif;?>
<?else:?>
<div class="caption">Адрес доставки</div>
<div class="slide-div">
    <div id="edit-address-message"></div>
    <form  method="post" id="edit-address-form" action="<?=SITE_DIR?>ajax/orderHandler.php">
        <div class="input-box">
            <input name="ORDER_PROP_<?=$arResult['ORDER_PROPS_BY_CODE']['NAME']['ID']?>" type="text" placeholder="Фамилия, Имя" value="<?=$arResult['ORDER_PROPS_VALUES']['ORDER_PROP_'.$arResult['ORDER_PROPS_BY_CODE']['NAME']['ID']]?>" />
        </div>
        <div class="input-box">
            <input name="ORDER_PROP_<?=$arResult['ORDER_PROPS_BY_CODE']['EMAIL']['ID']?>" type="text" placeholder="Email" value="<?=$arResult['ORDER_PROPS_VALUES']['ORDER_PROP_'.$arResult['ORDER_PROPS_BY_CODE']['EMAIL']['ID']]?>" />
        </div>
        <div class="input-box">
            <input name="ORDER_PROP_<?=$arResult['ORDER_PROPS_BY_CODE']['PHONE']['ID']?>" type="text" placeholder="Телефон" value="<?=$arResult['ORDER_PROPS_VALUES']['ORDER_PROP_'.$arResult['ORDER_PROPS_BY_CODE']['PHONE']['ID']]?>" />
        </div>
        <div class="input-box">
            <?$APPLICATION->IncludeComponent('bitrix:sale.ajax.locations', 'popup', array(
                "AJAX_CALL" => "Y",
                'CITY_OUT_LOCATION' => 'Y',
                'COUNTRY_INPUT_NAME' => '',
                'CITY_INPUT_NAME' => 'ORDER_PROP_'.$arResult['ORDER_PROPS_BY_CODE']['CITY']['ID'],
                'LOCATION_VALUE' => isset($arResult['ORDER_PROPS_VALUES']['ORDER_PROP_'.$arResult['ORDER_PROPS_BY_CODE']['CITY']['ID']]) ? $arResult['ORDER_PROPS_VALUES']['ORDER_PROP_'.$arResult['ORDER_PROPS_BY_CODE']['CITY']['ID']] : $arResult['ORDER_PROPS_BY_CODE']['CITY']["DEFAULT_VALUE"],
                'ONCHANGE' => '',
                ),
                null,
                array('HIDE_ICONS' => 'Y')
            );?>
        </div>
        <div class="input-box">
            <input onchange="setProfileName()" id="edit-address-address" name="ORDER_PROP_<?=$arResult['ORDER_PROPS_BY_CODE']['ADDRESS']['ID']?>" type="text" placeholder="Адрес доставки" value="<?=$arResult['ORDER_PROPS_VALUES']['ORDER_PROP_'.$arResult['ORDER_PROPS_BY_CODE']['ADDRESS']['ID']]?>" />
        </div>
        <div class="input-box">
            <input name="ORDER_PROP_<?=$arResult['ORDER_PROPS_BY_CODE']['INDEX']['ID']?>" type="text" placeholder="Индекс" value="<?=$arResult['ORDER_PROPS_VALUES']['ORDER_PROP_'.$arResult['ORDER_PROPS_BY_CODE']['INDEX']['ID']]?>" />
        </div>
        <?=bitrix_sessid_post()?>
        <input type="hidden" name="ID" value="<?=$arResult["ID"]?>">
        <input type="hidden" name="action" value="edit_address">
        <input type="hidden" name="NAME" id="edit-address-name" value=" ">
        <input type="hidden" name="<?=$arParams['ACTION']?>" value="<?echo GetMessage("SALE_SAVE") ?>">
    </form>
    <a href="javascript:void(0)" id="edit-address-form-btn" class="button" title="">сохранить</a>
</div>
<script>
    $('#edit-address-form-btn').click(function(){
        $.post($('#edit-address-form').attr('action'), $('#edit-address-form').serializeArray(), function(response){
            $('#edit-address-message').html(response);
        }, 'html');
    });

    function setProfileName() {
        var address = $('#edit-address-address').val(),
            city = $('#<?='ORDER_PROP_'.$arResult['ORDER_PROPS_BY_CODE']['CITY']['ID']?>_val').val(),
            name = '';
        if (city != '')
            name += 'г. '+city;
        if (address != '')
            name += ', '+address;
        if (name.charAt(0) == ',')
            name = name.substring(2, name.length);

        $('#edit-address-name').val(name);
    }
    $(function(){
        setProfileName();
    })
</script>
<?
CUtil::InitJSCore(array('core', 'ajax'));
?>
<?endif;?>