<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult['ERROR']))
	return false;
		
if (!empty($arResult['rows'])):
?>

<div class="preim">
	<h2><?=GetMessage('TITLE')?></h2>
	<ul>
<?
	foreach ($arResult['rows'] as $arItem):
		if ($arItem['UF_ACTIVE'] != 'нет'):
?>	
		<li class="preim<?=$arItem['UF_SORT']?>"><?=$arItem['UF_TEXT']?></li>
<?
		endif;
	endforeach;
?>	
	</ul>
</div>
<?
endif;
?>