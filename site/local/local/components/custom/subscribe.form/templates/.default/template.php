<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<?
if (empty($arResult['ERRORS'])):
?>
<div class="podp">
<?
$frame = $this->createFrame("subscribe-form", false)->begin();
?>
	Подписаться на рассылку
	<form action="<?=$arParams['AJAX_HANDLER']?>" id="main-subscription-form">
		<?=bitrix_sessid_post()?>
		<input type="hidden" name="action" value="subscribe" />
		<input type="text" name="email" value="<?if (!intval($arResult['ID'])):?>Ваша электронная почта<?elseif ($arResult['SUBSCRIPTION']['CONFIRMED'] != 'Y'):?>Вам необходимо подтвердить рассылку<?else:?>Вы подписаны на рассылку<?endif;?>" />
		<input type="button" class="red-button" value="Отправить" onclick="doSubscribe('#main-subscription-form')"/>
	</form>
<?
$frame->beginStub();
?>	
	Подписаться на рассылку
	<form action="<?=$arParams['AJAX_HANDLER']?>" id="main-subscription-form">
		<?=bitrix_sessid_post()?>
		<input type="hidden" name="action" value="subscribe" />
		<input type="text" name="email" value="Ваша электронная почта" />
		<input type="button" class="red-button" value="Отправить" onclick="doSubscribe('#main-subscription-form')"/>
	</form>
<?
$frame->end();
?>
</div>
<?
endif;	
?>
