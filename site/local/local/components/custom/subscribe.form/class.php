<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class CSubscribeFormComponent extends \CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
    {
		$arParams['AJAX'] = $arParams['AJAX'] == "Y" ? "Y" : "N";
		$arParams['AJAX_HANDLER'] = strlen($arParams['AJAX_HANDLER']) ? $arParams['AJAX_HANDLER'] : SITE_DIR."ajax/handleSubscribe.php";
		$arParams['RUBRICS'] = !empty($arParams['RUBRICS']) ? $arParams['RUBRICS'] : array(1);
        
        return $arParams;
    }
	
	public function executeComponent()
    {
		if (!CModule::IncludeModule("subscribe"))
			$this->arResult['ERRORS'][] = "Модуль подписки не найден";
		else 
		{
			global $USER;
			$arSubscriptionUser = CSubscription::GetUserSubscription();
			if($arSubscriptionUser['ID'] > 0 || $USER->IsAuthorized())
			{
				if($USER->IsAuthorized())
				{
					$rsSubscription = CSubscription::GetList(array(), array("USER_ID" => $USER->GetID(), 'RUBRIC' => $arParams['RUBRICS']));
				}	
				elseif($arSubscriptionUser['ID'] > 0)
					$rsSubscription = CSubscription::GetByID($ID);
				
				if($arRes = $rsSubscription->GetNext())
				{
					$this->arResult['SUBSCRIPTION'] = $arRes;
					$this->arResult['ID'] = intval($arRes["ID"]);
				}
				else
					$this->arResult['ID'] = 0;
				
			}
			else
				$this->arResult['ID'] = 0;
		}	
		$this->includeComponentTemplate();
	}
}
?>