<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?dump($arResult);
?>
<?if ($arParams['POPUP'] != 'Y'):?>
<div class="box element history">
    <div class="box-title">
        <h3>История заказов</h3>
    </div>
    <div class="box-content">
<?endif;?>
    <?if (!empty($arResult['ORDERS']) && is_array($arResult['ORDERS'])):?>
        <?foreach ($arResult['ORDERS'] as $arData):?>
        <p class="order">
            <span class="order-r1">
                <a href="javascript:void(0);" class="history-order-link" data-id="<?=$arData['ORDER']['ID']?>">
                    <span class="number">Заказ №<?=$arData['ORDER']['ACCOUNT_NUMBER']?></span>
                </a>
                <span class="date">от <?=$arData['ORDER']['DATE_INSERT_FORMATED']?></span>
            </span>
            <span class="order-r2"><span class="price"><?=$arData['ORDER']['FORMATED_PRICE']?></span> <span class="status"><?=$arResult['INFO']['STATUS'][$arData['ORDER']['STATUS_ID']]['NAME']?></span>
        </p>
        <?endforeach;?>
    <?if ($arParams['POPUP'] != 'Y'):?>
        <a href="javascript:void(0)" class="history-link edit" title="">показать все заказы</a>
    <?endif;?>
    <?else:?>
        <p class="order">У Вас еще нет заказов</p>
    <?endif;?>
<?if ($arParams['POPUP'] != 'Y'):?>
    </div>
</div>
<?endif;?>