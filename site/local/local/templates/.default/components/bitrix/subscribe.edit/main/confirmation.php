<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
//*************************************
//show confirmation form
//*************************************
?>
<form action="<?=$arResult["FORM_ACTION"]?>" method="get">
    <?echo GetMessage("subscr_title_confirm")?><br>
    <p><?echo GetMessage("subscr_conf_code")?><span class="starrequired">*</span><br />
    <div class="input-box">
        <input type="text" name="CONFIRM_CODE" value="<?echo $arResult["REQUEST"]["CONFIRM_CODE"];?>"/></p>
    </div>
    <p><?echo GetMessage("subscr_conf_date")?></p>
    <p><?echo $arResult["SUBSCRIPTION"]["DATE_CONFIRM"];?></p>
    <a title="<?echo GetMessage("adm_send_code")?>"  class="button" href="<?echo $arResult["FORM_ACTION"]?>?ID=<?echo $arResult["ID"]?>&amp;action=sendcode&amp;<?echo bitrix_sessid_get()?>"><?echo GetMessage("subscr_conf_note2")?></a>
    <a href="#" onclick="$('#forgot_password_form').submit(); return false;" class="button" title=""><?=GetMessage("AUTH_CHANGE")?></a>
    <input type="submit" style="display:none" name="confirm" value="<?echo GetMessage("subscr_conf_button")?>" />
    <input type="hidden" name="ID" value="<?echo $arResult["ID"];?>" />
    <?echo bitrix_sessid_post();?>
</form>
<br />

