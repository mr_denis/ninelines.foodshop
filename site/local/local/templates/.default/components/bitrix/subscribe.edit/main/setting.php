<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
//***********************************
//setting section
//***********************************
?>
<form action="<?=$arResult["FORM_ACTION"]?>" method="post">
    <?echo bitrix_sessid_post();?>
    <div class="input-box">
        <input type="text" name="EMAIL" placeholder="<?echo GetMessage("subscr_email")?>" value="<?=$arResult["SUBSCRIPTION"]["EMAIL"]!=""?$arResult["SUBSCRIPTION"]["EMAIL"]:$arResult["REQUEST"]["EMAIL"];?>" size="30" maxlength="255" /></p>
    </div>

    <?foreach($arResult["RUBRICS"] as $itemID => $itemValue):?>
    <div class="check">
        <div class="squaredThree">
            <input id="squaredThree<?=$itemID?>" type="checkbox" name="RUB_ID[]" value="<?=$itemValue["ID"]?>" <?if($itemValue["CHECKED"]) echo " checked"?>>
            <label for="squaredThree<?=$itemID?>"><?echo $itemValue["NAME"]?></label>
        </div>
    </div>
    <?endforeach;?>

    <p>
        <?echo GetMessage("subscr_fmt")?><br />
        <label><input type="radio" name="FORMAT" value="text"<?if($arResult["SUBSCRIPTION"]["FORMAT"] == "text") echo " checked"?> /><?echo GetMessage("subscr_text")?></label>&nbsp;/&nbsp;<label><input type="radio" name="FORMAT" value="html"<?if($arResult["SUBSCRIPTION"]["FORMAT"] == "html") echo " checked"?> />HTML</label>
    </p>

    <p><?echo GetMessage("subscr_settings_note1")?></p>
    <p><?echo GetMessage("subscr_settings_note2")?></p>

    <input id="subscribe_settings_submit" style="display:none" type="submit" name="Save" value="<?echo ($arResult["ID"] > 0? GetMessage("subscr_upd"):GetMessage("subscr_add"))?>" />
    <input id="subscribe_settings_reset" style="display:none" type="reset" value="<?echo GetMessage("subscr_reset")?>" name="reset" />

    <input type="hidden" name="PostAction" value="<?echo ($arResult["ID"]>0? "Update":"Add")?>" />
    <input type="hidden" name="ID" value="<?echo $arResult["SUBSCRIPTION"]["ID"];?>" />
<?if($_REQUEST["register"] == "YES"):?>
	<input type="hidden" name="register" value="YES" />
<?endif;?>
<?if($_REQUEST["authorize"]=="YES"):?>
	<input type="hidden" name="authorize" value="YES" />
<?endif;?>
    <a href="JavaScript:void(0)" onclick="$('#subscribe_settings_submit').click(); return false;" class="button" title=""><?echo ($arResult["ID"] > 0? GetMessage("subscr_upd"):GetMessage("subscr_add"))?></a>
    <br>
    <a href="JavaScript:void(0)" onclick="$('#subscribe_settings_reset').click(); return false;" class="button" title=""><?echo GetMessage("subscr_reset")?></a>
</form>
<br />