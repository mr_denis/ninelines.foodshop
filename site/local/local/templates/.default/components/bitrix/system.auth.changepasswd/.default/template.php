<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetDirProperty("section_class", "inner-catalog forms-page");
$APPLICATION->SetDirProperty("content_class", "decription  auth-forms");
$APPLICATION->SetDirProperty("header_style", "display:none");
?>
<div class="windows-form windows-register">
	<a class="close" title="Закрыть" href="#"></a>
	<div class="caption"><?=GetMessage("AUTH_CHANGE_PASSWORD")?></div>
	<div class="slide-div">
		<?
		ShowMessage($arParams["~AUTH_RESULT"]);
		ShowMessage($arResult['ERROR_MESSAGE']);
		?>
		<form name="bform" method="post" id="forgot_password_form" target="_top" action="<?=$arResult["AUTH_URL"]?>">
			<?if (strlen($arResult["BACKURL"]) > 0):?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
			<?endif;?>
			<input type="hidden" name="AUTH_FORM" value="Y">
			<input type="hidden" name="TYPE" value="CHANGE_PWD">
			<input type="hidden" name="change_pwd" value="Y" />

			<div class="input-box">
				<input type="text" class="first" placeholder="E-Mail" name="USER_LOGIN" maxlength="50" value="<?=$arResult["LAST_LOGIN"]?>" />
			</div>
			<div class="input-box">
				<input type="text" placeholder="<?=GetMessage("AUTH_CHECKWORD")?>" name="USER_CHECKWORD" maxlength="50"  value="<?=$arResult["USER_CHECKWORD"]?>" />
			</div>
			<div class="input-box">
				<input type="password" placeholder="<?=GetMessage("AUTH_NEW_PASSWORD_REQ")?>" name="USER_PASSWORD" maxlength="50"  value="<?=$arResult["USER_PASSWORD"]?>" />
			</div>
			<div class="input-box">
				<input type="password"  class="last" placeholder="<?=GetMessage("AUTH_NEW_PASSWORD_CONFIRM")?>" name="USER_CONFIRM_PASSWORD" maxlength="50"  value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" />
			</div>
			<a href="#" onclick="$('#forgot_password_form').submit(); return false;" class="button" title=""><?=GetMessage("AUTH_CHANGE")?></a>
		</form>
	</div>
</div>