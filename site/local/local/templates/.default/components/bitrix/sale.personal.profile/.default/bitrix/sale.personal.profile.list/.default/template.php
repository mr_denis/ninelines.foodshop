<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="box element shipping">
    <div class="box-title">
        <h3>Профили доставки</h3>
    </div>
    <div class="box-content">
        <a href="javascript:void(0)" id="address-default-block" style="display:none" class="default" title="">по умолчанию</a>
    <?foreach($arResult["PROFILES"] as $val):?>
        <?$APPLICATION->IncludeComponent(
            "custom:sale.personal.profile.detail",
            "",
            array(
            "PATH_TO_LIST" => $arParams["PATH_TO_LIST"],
            "PATH_TO_DETAIL" => $arParams["PATH_TO_DETAIL"],
            "SET_TITLE" =>$arParams["SET_TITLE"],
            "USE_AJAX_LOCATIONS" => $arParams['USE_AJAX_LOCATIONS'],
            "ID" => $val['ID'],
            ),
            $component
        );?>

        <?endforeach;?>
        <a href="/personal/profile/edit/" class="edit-address-link edit" title="">Добавить адрес</a>
        <script>
            $(function(){
                if ($('.profile-address').length > 0)
                    $('#address-default-block').show();
            });
        </script>
    </div>
</div>