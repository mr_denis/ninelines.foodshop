<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$APPLICATION->SetTitle('Поиск товаров');

$APPLICATION->SetDirProperty("section_class", "inner-catalog inner_search");
$APPLICATION->SetDirProperty("content_class", "inner");
?>
<?$this->SetViewTarget('add_content');?>
<div class="inner search-container">
    <p>Поиск по каталогу</p>
    <form action="<?=$APPLICATION->GetCurPage()?>" method="get">
        <input type="text" name="q" value="<?=(isset($_GET['q']) ? htmlspecialchars(trim(strip_tags($_GET['q']))) : '')?>" />
    </form>
</div>
<?$this->EndViewTarget();?>
<?
$APPLICATION->IncludeComponent(
	"custom:catalog.search",
	"",
	$arParams,
	false,
	array('HIDE_ICONS' => 'Y')
);
?>