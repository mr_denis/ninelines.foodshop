<?
$arResult['ORDER_FIELDS'] = array();
foreach ($arResult['PRINT_PROPS_FORM']['USER_PROPS_Y'] as $arProp) {
	$arResult['ORDER_FIELDS'][$arProp['CODE']] = $arProp;
}
foreach ($arResult['PRINT_PROPS_FORM']['USER_PROPS_N'] as $arProp) {
	$arResult['ORDER_FIELDS'][$arProp['CODE']] = $arProp;
}

$rsLocations = CSaleLocation::GetList(array("SORT"=>"ASC", "CITY_NAME"=>"ASC"), array("COUNTRY_LID"=>LANGUAGE_ID, "CITY_LID"=>LANGUAGE_ID));
while ($arCity = $rsLocations->GetNext()) {
	$arResult['ORDER_FIELDS']["CITY"]["VARIANTS"][] = $arCity;
}