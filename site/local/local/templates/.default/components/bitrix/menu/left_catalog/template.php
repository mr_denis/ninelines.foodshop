<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if (empty($arResult))
	return;
	
	$arParent1 = array();
	$arParent2 = array();
	$index = -1;
	$subIndex = -1;
	$arMenu = array();

	foreach($arResult as $itemIdex => $arItem)
	{
		switch ($arItem['DEPTH_LEVEL'])
		{
			case 1:
				$index++;
				$arItem['MENU_INDEX'] = $index;
				$arParent1 = $arItem;
				$arParent2 = array();
				$arMenu[$arItem['DEPTH_LEVEL']][$index] = $arItem;
				$subIndex = -1;
				break;
			case 2:
				$subIndex++;
				$arItem['PARENT_INDEX'] = $index;
				$arItem['MENU_INDEX'] = $subIndex;
				$arParent2 = $arItem;
				$arMenu[$arItem['DEPTH_LEVEL']][$index]['PARENT'] = $arParent1;
				$arMenu[$arItem['DEPTH_LEVEL']][$index]['ITEMS'][$subIndex] = $arItem;
				break;
			case 3:
				$arItem['PARENT_INDEX'] = $subIndex;
				$arMenu[$arItem['DEPTH_LEVEL']][$index.'-'.$subIndex]['PARENT'] = $arParent2;
				$arMenu[$arItem['DEPTH_LEVEL']][$index.'-'.$subIndex]['ITEMS'][] = $arItem;
				break;	
		}
	}
?>
<?
if ($arParams['MAX_LEVEL'] == 2):
?>
	<ul class="main-menu menu-cat">
<?
	foreach($arMenu[1] as $index => $arItem):
	
?>
		<li><a href="<?=$arItem["LINK"]?>" class="<?=($arItem['IS_PARENT']) ? 'menu2-'.$index : ''?>" title="<?=$arItem["TEXT"]?>"><?=$arItem["TEXT"]?></a></li>
<?
	endforeach;
?>
	</ul>
<?
elseif ($arParams['MAX_LEVEL'] > 2):
	if (!empty($arMenu[2]) && is_array($arMenu[2])):
		foreach($arMenu[2] as $index => $arMenuData):
?>
			<div class="right-slider-block right-slider-block-main menu2 zindex2 opened submenu2-<?=$index?>">
				<a href="<?=$arMenuData['PARENT']['LINK']?>" class="back2cat" title="Вернуться назад">Вернуться назад</a>
				<ul class="main-menu menu-cat">
<?		
				foreach ($arMenuData['ITEMS'] as $subIndex => $arItem):
?>
					<li><a href="<?=$arItem['LINK']?>" class="<?=($arItem['IS_PARENT'])?'menu2-'.$index.'-'.$subIndex:''?>" title="<?=$arItem['TEXT']?>"><?=$arItem['TEXT']?></a></li>

<?
				endforeach;
?>
				</ul>
			</div>
<?			
		endforeach;
	endif;
	
	if (!empty($arMenu[3]) && is_array($arMenu[3])):
		foreach($arMenu[3] as $index => $arMenuData):
?>
			<div class="right-slider-block right-slider-block-main menu2 zindex3 opened submenu2-<?=$index?>">
				<a href="<?=$arMenuData['PARENT']['LINK']?>" class="back2cat" title="Вернуться назад">Вернуться назад</a>
				<ul class="main-menu menu-cat">
<?		
				foreach ($arMenuData['ITEMS'] as $subIndex => $arItem):
?>
					<li><a href="<?=$arItem['LINK']?>" title="<?=$arItem['TEXT']?>"><?=$arItem['TEXT']?></a></li>

<?
				endforeach;
?>
				</ul>
			</div>
<?			
		endforeach;
	endif;	
endif;
?>	