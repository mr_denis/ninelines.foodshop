<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters['MENU_SHOWED_ITEMS_COUNT'] = array(
	'PARENT' => 'VISUAL',
	'NAME' => GetMessage('MENU_SHOWED_ITEMS_COUNT'),
	'TYPE' => 'STRING',
	'DEFAULT' => 7
);