<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? 
if (count($arResult) < 1)
	return;
?>
<nav>
	<ul>
<?	
	foreach ($arResult as $arMenu):
?>
		<li><a href="<?=$arMenu['LINK']?>"><?=$arMenu['TEXT']?></a></li>
<?
	endforeach;
?>
	</ul>
</nav>
