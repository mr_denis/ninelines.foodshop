<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if (empty($arResult))
	return;

	$index = -1;
	$subIndex = -1;
	$arMenu = array();

	foreach($arResult as $itemIdex => $arItem)
	{
		if ($arItem['DEPTH_LEVEL'] == 1)
		{
			$index++;
			$arMenu[$index] = $arItem;
			$subIndex = -1;
		}
		elseif ($arItem['DEPTH_LEVEL'] == 2)
		{
			$subIndex++;
			$arMenu[$index]['ITEMS'][$subIndex] = $arItem;
		}
		elseif ($arItem['DEPTH_LEVEL'] == 3)
		{
			$arMenu[$index]['ITEMS'][$subIndex]['ITEMS'][] = $arItem;
		}
	}
?>

<nav>
	<ul>
<?
	foreach ($arMenu as $arItem):
?>	
		<li class="li">
			<a href="<?=$arItem['LINK']?>" class="href-li" title="<?=$arItem['TEXT']?>"><?=$arItem['TEXT']?></a>
<?
		if (isset($arItem['ITEMS']) && is_array($arItem['ITEMS'])):
?>			
			<ul class="inner">
<?
			foreach ($arItem['ITEMS'] as $arSubItem):
?>			
				<li><a href="<?=$arSubItem['LINK']?>" title="<?=$arSubItem['TEXT']?>"><?=$arSubItem['TEXT']?></a></li>
<?
			endforeach;
?>				
			</ul>
		</li>
<?
		endif;
	endforeach;
?>		
	</ul>
</nav>