<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if (empty($arResult))
	return;

	$arParams['MENU_SHOWED_ITEMS_COUNT'] = intval($arParams['MENU_SHOWED_ITEMS_COUNT']) ? intval($arParams['MENU_SHOWED_ITEMS_COUNT']) : 7;
	
	$index = -1;
	$subIndex = -1;
	$etc = false;
	$arMenu = array();
	$menuWords = array();
	$menuFixed = false;

	foreach($arResult as $itemIdex => $arItem)
	{
		if (!$menuFixed && ($arItem['DEPTH_LEVEL'] == 1))
		{
			if (($index < $arParams['MENU_SHOWED_ITEMS_COUNT'] - 1))
				$menuWordsLength[] = $arItem['TEXT'];
			
			if (strlen(implode(' ', $menuWordsLength)) >= 132)	
			{
				$arParams['MENU_SHOWED_ITEMS_COUNT'] = $index +1;
				$menuFixed = true;
			}	
		}
		
		
		
		
		if (($index < $arParams['MENU_SHOWED_ITEMS_COUNT'] - 1) || (($index == $arParams['MENU_SHOWED_ITEMS_COUNT'] - 1) && ($arItem['DEPTH_LEVEL'] > 1)))
		{
			if ($arItem['DEPTH_LEVEL'] == 1)
			{
				$index++;
				$arMenu[$index] = $arItem;
				$subIndex = -1;
			}
			elseif ($arItem['DEPTH_LEVEL'] == 2)
			{
				$subIndex++;
				$arMenu[$index]['ITEMS'][$subIndex] = $arItem;
			}
			elseif ($arItem['DEPTH_LEVEL'] == 3)
			{
				$arMenu[$index]['ITEMS'][$subIndex]['ITEMS'][] = $arItem;
			}
		}
		else
		{
			if (!$etc) 
			{
				$etc = true;
				$index++;
				$arMenu[$index] = array(
					'TEXT' => '',
					'LINK' => SITE_DIR."catalog/",
					'PERMISSION' => "X",
					'DEPTH_LEVEL' => 1,
					'IS_PARENT' => 1,
					'PARAMS' => array(
						'FROM_IBLOCK' => 1,
						'IS_PARENT' => 1,
						'DEPTH_LEVEL' => 1,
						'ADD_CLASS' => 'etc',
					),
				);
				$subIndex = -1;
			}
			
			if ($arItem['DEPTH_LEVEL'] == 1)
			{
				$arItem['DEPTH_LEVEL'] = 2;
				$subIndex++;
				$arMenu[$index]['ITEMS'][$subIndex] = $arItem;
			}
			elseif ($arItem['DEPTH_LEVEL'] == 2)
			{
				$arItem['DEPTH_LEVEL'] = 3;
				$arMenu[$index]['ITEMS'][$subIndex]['ITEMS'][] = $arItem;
			}
		}
	}
	//dump($arMenu);
?>
<nav class="main-catalog">
	<ul>
<?
	foreach ($arMenu as $index => $arItem):
?>
		<li class="<?=isset($arItem['PARAMS']['ADD_CLASS'])?$arItem['PARAMS']['ADD_CLASS']:''?>"><a href="<?=$arItem['LINK']?>" class="nav-dop<?=$index?>" title="<?=$arItem['TEXT']?>"><?=$arItem['TEXT']?></a></li>
<?
	endforeach;
?>		
	</ul>
</nav>
<?
foreach ($arMenu as $index => $arItem):
	if (!empty($arItem['ITEMS'])):
	
?>
<div class="nav-dop" id="nav-dop<?=$index?>">
	<ul class="outer">
<?
	foreach ($arItem['ITEMS'] as $arSubItem):
?>
		<li class="li">
			<a href="<?=$arSubItem['LINK']?>" class="href-li" title="<?=$arSubItem['TEXT']?>"><?=$arSubItem['TEXT']?></a>
<?
		if (!empty($arSubItem['ITEMS'])):
?>			
			<ul class="inner">
<?
			foreach ($arSubItem['ITEMS'] as $arSubSubItem):
?>
				<li><a href="<?=$arSubSubItem['LINK']?>" title="<?=$arSubSubItem['TEXT']?>"><?=$arSubSubItem['TEXT']?></a></li>
<?
			endforeach;
?>				
			</ul>
<?
		endif;	
?>			
		</li>
<?
	endforeach;
?>		
	</ul>
</div>
<?
	endif;
endforeach;
?>