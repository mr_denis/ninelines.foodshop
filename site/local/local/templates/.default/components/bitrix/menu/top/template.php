<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if (empty($arResult))
	return;
?>
<ul class="menu-top">
<?foreach($arResult as $itemIdex => $arItem):?>
	<li><a href="<?=$arItem["LINK"]?>" title="<?=$arItem["TEXT"]?>"><?=$arItem["TEXT"]?></a></li>
<?endforeach;?>
</ul>