<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if (empty($arResult))
	return;
	
	$arParent1 = array();
	$arParent2 = array();
	$index = -1;
	$subIndex = -1;
	$arMenu = array();

	foreach($arResult as $itemIdex => $arItem)
	{
		switch ($arItem['DEPTH_LEVEL'])
		{
			case 1:
				$index++;
				$arItem['MENU_INDEX'] = $index;
				$arParent1 = $arItem;
				$arParent2 = array();
				$arMenu[$arItem['DEPTH_LEVEL']][$index] = $arItem;
				$subIndex = -1;
				break;
			case 2:
				$subIndex++;
				$arItem['PARENT_INDEX'] = $index;
				$arItem['MENU_INDEX'] = $subIndex;
				$arParent2 = $arItem;
				$arMenu[$arItem['DEPTH_LEVEL']][$index]['PARENT'] = $arParent1;
				$arMenu[$arItem['DEPTH_LEVEL']][$index]['ITEMS'][$subIndex] = $arItem;
				break;
		}
	}
?>


<ul class="main-menu menu-dop">
<?
foreach($arMenu[1] as $index => $arItem):

?>
	<li><a href="<?=$arItem["LINK"]?>" class="<?=($arItem['IS_PARENT']) ? 'menu3-'.$index : ''?>" title="<?=$arItem["TEXT"]?>"><?=$arItem["TEXT"]?></a></li>
<?
endforeach;
?>
</ul>