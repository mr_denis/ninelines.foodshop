<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetDirProperty("section_class", "inner-catalog forms-page");
$APPLICATION->SetDirProperty("content_class", "decription auth-forms");
$APPLICATION->SetDirProperty("header_style", "display:none");
?>
<div class="windows-form windows-login">
    <a class="close" title="Закрыть" href="#"></a>
    <div class="caption">Вход</div>
    <div class="slide-div">
        <?
        ShowMessage($arParams["~AUTH_RESULT"]);
        ShowMessage($arResult['ERROR_MESSAGE']);
        ?>
        <form name="form_auth" id="form_auth" method="post" target="_top" action="/personal/">
            <input type="hidden" name="AUTH_FORM" value="Y" />
            <input type="hidden" name="TYPE" value="AUTH" />
            <input type="hidden" name="Login" value="Login" />
            <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
            <?foreach ($arResult["POST"] as $key => $value):?>
                <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
            <?endforeach?>
            <div class="input-box">
                <input type="text" class="first" placeholder="Email" name="USER_LOGIN" maxlength="255" value="<?=$arResult["LAST_LOGIN"]?>" />
            </div>
            <div class="input-box">
                <input class="last" placeholder="Пароль" type="password" name="USER_PASSWORD" maxlength="255" value="" /><a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" class="password" title="">Забыли?</a>
            </div>
            <a href="#" class="button" title="" onclick="$('#form_auth').submit(); return false;">Войти</a>
        </form>
    </div>
    <div class="slide-div slide-div-last">
        <p>У вас нет аккаунта?</p>
        <a href="<?=$arResult["AUTH_REGISTER_URL"]?>" class="button button-grey" title="">Зарегистрироваться</a>
    </div>
</div>