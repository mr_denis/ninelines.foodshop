<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(!empty($arResult["CATEGORIES"])):?>
		<?
		foreach($arResult["CATEGORIES"] as $category_id => $arCategory):
			$item = current($arCategory['ITEMS']);

			if ($item['PARAM2'] == IBLOCK_ID_NEWS)
				$type = 'news';
			elseif ($item['PARAM2'] == IBLOCK_ID_ACTIONS)
				$type = 'actions';
			elseif ($item['PARAM2'] == IBLOCK_ID_CATALOG)
				$type = 'catalog';
			else
				continue;
		?>
		<div class="bx_searche">
			<h4><?echo $arCategory["TITLE"]?></h4>
			<?
			foreach ($arCategory['ITEMS'] as $arItem):
				if (!isset($arItem['PARAM2']))
					continue;
					
				if ($type != 'catalog'):
			?>
			<div class="bx_item_block others_result">
				<div class="bx_img_element">
					<div class="bx_image"></div>
				</div>
				<div class="bx_item_element">
					<a href="<?=$arItem['URL']?>" title="<?=htmlspecialchars(strip_tags($arItem['NAME']))?>"><?=$arItem['NAME']?></a>
				</div>
				<div style="clear:both;"></div>
			</div>
			<?
				else:
					$arElement = $arResult["ELEMENTS"][$arItem["ITEM_ID"]];
			?>
			<div class="bx_item_block">
				<div class="bx_img_element">
					<? if (is_array($arElement["PICTURE"])):?>
					<div style="background-image: url('<?=$arElement["PICTURE"]["src"]?>'); background-position: center top; background-repeat: no-repeat;" class="bx_image"></div>
					<? endif;?>
				</div>
				<div class="bx_item_element">
					<a href="<?=$arItem['URL']?>" title="<?=htmlspecialchars(strip_tags($arItem['NAME']))?>"><?=$arItem["NAME"]?><span><?=$arElement["OFFER"]['MIN_PRICE']['PRINT_VALUE']?></span></a>
				</div>
				<div style="clear:both;"></div>
			</div>
			<?
				endif;
			endforeach;
			?>
		</div>
		<?endforeach;?>
<?endif;?>