<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetDirProperty("section_class", "inner-catalog forms-page");
$APPLICATION->SetDirProperty("content_class", "decription auth-forms");
?>
        <div class="windows-form windows-forgot">
            <a class="close" title="Закрыть" href="#"></a>
            <div class="caption">Восстановить пароль</div>
            <div class="slide-div">
                <?
                ShowMessage($arParams["~AUTH_RESULT"]);
                ShowMessage($arResult['ERROR_MESSAGE']);
                ?>
                <form name="bform" method="post" id="forgot_password_form" target="_top" action="<?=$arResult["AUTH_URL"]?>">
                    <?if (strlen($arResult["BACKURL"]) > 0):?>
                        <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
                    <?endif;?>
                    <input type="hidden" name="AUTH_FORM" value="Y">
                    <input type="hidden" name="TYPE" value="SEND_PWD">
                    <input type="hidden" name="send_account_info" value="Y" />

                    <div class="input-box">
                        <input type="text" class="first" placeholder="E-Mail" name="USER_EMAIL" maxlength="255" />
                    </div>
                    <a href="#" onclick="$('#forgot_password_form').submit(); return false;" class="button" title="">Отправить новый пароль</a>
                </form>
            </div>
        </div>