<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<ul class="" id="action-items">
<?
if (($_POST['action'] == 'get_items') && ($_POST['container'] == 'action-items'))
    $APPLICATION->RestartBuffer();
foreach($arResult["ITEMS"] as $arItem)
{
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    $pic = array('src' => '');
    if (!empty($arItem['PREVIEW_PICTURE']))
        $pic = CFile::resizeImageGet($arItem['PREVIEW_PICTURE']['ID'], array('width' => 385, 'height' => 210), BX_RESIZE_IMAGE_EXACT, true);
?>
    <li class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
            <img title="<?=$arItem['NAME']?>" alt="" src="<?=$pic['src']?>">
        </a>
    </li>
<?
}
if (($_POST['action'] == 'get_items') && ($_POST['container'] == 'action-items'))
    die();
?>			
</ul>
<?
if (isset($arResult['NAV_RESULT']->NavPageCount) && ($arResult['NAV_RESULT']->NavPageCount > 1))
{
?>		
<a href="JavaScript:void(0);" class="more" title="Показать еще" onclick="getNewsItems(this, 'action-items')" class="more" title="Показать еще" data-cur-page="<?=$APPLICATION->GetCurPage()?>" data-page-count="<?=$arResult['NAV_RESULT']->NavPageCount?>"  data-paging-var="PAGEN_<?=$arResult['NAV_RESULT']->NavNum?>" data-paging-number="2">Показать еще</a>
<?
}
?>