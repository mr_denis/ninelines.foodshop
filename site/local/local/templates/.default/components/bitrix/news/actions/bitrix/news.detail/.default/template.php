<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$img = 0;
if (intval($arResult["DETAIL_PICTURE"]['ID']))
    $img = $arResult["DETAIL_PICTURE"]['ID'];
elseif (intval($arResult["PREVIEW_PICTURE"]['ID']))
    $img = $arResult["PREVIEW_PICTURE"]['ID'];

if ($img)
    $pic = CFile::ResizeImageGet($img, array('width' => 283, 'height' => 220), BX_RESIZE_IMAGE_PROPORTIONAL, true);

?>
<div class="action-news">
    <?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
        <p class="date grey-text"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></p>
    <?endif;?>

    <?if (!empty($pic['src'])):?>
        <img title="<?=$arResult["NAME"]?>" alt="<?=$arResult["NAME"]?>" class="news-action-photo-inner" src="<?=$pic['src']?>">
    <?endif;?>
    <?if(strlen($arResult["DETAIL_TEXT"])>0):?>
        <?echo $arResult["DETAIL_TEXT"];?>
    <?else:?>
        <?echo $arResult["PREVIEW_TEXT"];?>
    <?endif?>
</div>