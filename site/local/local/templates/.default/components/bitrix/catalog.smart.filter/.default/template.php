<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

CJSCore::Init(array("fx"));
?>
<article class="filter">
	<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter">
		<?foreach($arResult["HIDDEN"] as $arItem):?>
			<input
				type="hidden"
				name="<?echo $arItem["CONTROL_NAME"]?>"
				id="<?echo $arItem["CONTROL_ID"]?>"
				value="<?echo $arItem["HTML_VALUE"]?>"
			/>
		<?endforeach;?>
		<?if (!empty($arResult['CATALOG_SECTIONS'])):?>
		<dl class="open">
			<dt><a href="JavaScript:void(0);" title="">Каталог</a></dt>
			<dd>
				<ul class="cat">
				<?foreach ($arResult['CATALOG_SECTIONS'] as $sID => $arSection):?>			
					<li><a href="<?=$arSection['SECTION_PAGE_URL']?>" title="<?=$arSection['NAME']?>"><?=$arSection['NAME']?></a>
					<?if (isset($arSection['SUBSECTIONS']) && !empty($arSection['SUBSECTIONS'])):?>				
						<ul class="cat-inner">
						<?foreach ($arSection['SUBSECTIONS'] as $ssID => $arSubSection):?>					
							<li><a href="<?=$arSubSection['SECTION_PAGE_URL']?>" title="<?=$arSubSection['NAME']?>"><?=$arSubSection['NAME']?></a></li>
						<?endforeach;?>						
						</ul>
					<?endif;?>					
					</li>
				<?endforeach;?>				
				</ul>
			</dd>
		</dl>
		<?endif;?>
		
		<?foreach($arResult["ITEMS"] as $key=>$arItem):
			$key = md5($key);
			?>
			<?if(isset($arItem["PRICE"])):?>
				<?
				if (!$arItem["VALUES"]["MIN"]["VALUE"] || !$arItem["VALUES"]["MAX"]["VALUE"] || $arItem["VALUES"]["MIN"]["VALUE"] == $arItem["VALUES"]["MAX"]["VALUE"])
					continue;
					
				?>
				<dl class="<?=$arItem['OPEN'] ? 'open' : 'closed'?>">
					<dt><a href="JavaScript:void(0);" title="">Стоимость, руб.</a><span id="bx_filter_container_modef_<?=$arItem['ID']?>" class="bx_filter_container_modef"></span></dt>
					<dd>
						<div class="value">
							<label for="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>">от</label>
							<input
								class="first min-price"
								type="text"
                                data-property_id="<?=$arItem['ID']?>"
								name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
								id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
								value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
								onkeyup="smartFilter.keyup(this)"
							/>
							<label for="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>" class="second">до</label>
							<input
								class="max-price"
								type="text"
                                data-property_id="<?=$arItem['ID']?>"
								name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
								id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
								value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
								onkeyup="smartFilter.keyup(this)"
							/>
						</div>
						<div id="slider-range-<?=$key?>" class="slider-range"></div> 
					</dd>
				</dl>

				<script type="text/javascript">
					$(function(){
						if ($("#slider-range-<?=$key?>").length) {
							$("#slider-range-<?=$key?>").slider({
								range: true,
								min: parseInt(<?=intval($arItem["VALUES"]["MIN"]["VALUE"])?>),
								max: parseInt(<?=intval($arItem["VALUES"]["MAX"]["VALUE"])?>),
								values: [<?echo strlen($arItem["VALUES"]["MIN"]["HTML_VALUE"]) ? intval($arItem["VALUES"]["MIN"]["HTML_VALUE"]) : intval($arItem["VALUES"]["MIN"]["VALUE"])?>, <?echo strlen($arItem["VALUES"]["MAX"]["HTML_VALUE"]) ? intval($arItem["VALUES"]["MAX"]["HTML_VALUE"]) : intval($arItem["VALUES"]["MAX"]["VALUE"])?>]
								, create: function( event, ui ) {
									$(this).find("a:first").addClass("first-a");
									$(this).find("a:last").addClass("last-a");
								}
								, slide: function( event, ui ) {
									if ($(ui.handle).hasClass("first-a")) {
										$("#<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>").val(ui.value);
									}
									if ($(ui.handle).hasClass("last-a")) {
										$("#<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>").val(ui.value);
									}
								}
								, stop: function( event, ui ) {
									if ($(ui.handle).hasClass("first-a")) {
										smartFilter.keyup(BX('<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>'));
									}
									if ($(ui.handle).hasClass("last-a")) {
										smartFilter.keyup(BX('<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>'));
									}
								}
							});
						}
					});
				</script>
			<?endif?>
		<?endforeach?>

		<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
			<?if($arItem["PROPERTY_TYPE"] == "N" ):?>
				<?
				if (!$arItem["VALUES"]["MIN"]["VALUE"] || !$arItem["VALUES"]["MAX"]["VALUE"] || $arItem["VALUES"]["MIN"]["VALUE"] == $arItem["VALUES"]["MAX"]["VALUE"])
					continue;
				?>
				<dl class="<?=$arItem['OPEN'] ? 'open' : 'closed'?>">
					<dt><a href="JavaScript:void(0);" title="<?=$arItem["NAME"]?>"><?=$arItem["NAME"]?></a><span id="bx_filter_container_modef_<?=$arItem['ID']?>" class="bx_filter_container_modef"></span></dt>
					<dd>
						<div class="value">
							<label for="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>">от</label>
							<input
								class="first min-price"
								type="text"
                                data-property_id="<?=$arItem['ID']?>"
								name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
								id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
								value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
								onkeyup="smartFilter.keyup(this)"
							/>
							<label for="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>" class="second">до</label>
							<input
								class="max-price"
								type="text"
                                data-property_id="<?=$arItem['ID']?>"
								name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
								id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
								value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
								onkeyup="smartFilter.keyup(this)"
							/>
						</div>
						<div id="slider-range-<?=$key?>" class="slider-range"></div> 
					</dd>
				</dl>
				<script type="text/javascript">
					$(function(){
						if ($("#slider-range-<?=$key?>").length) {
							$("#slider-range-<?=$key?>").slider({
								range: true,
								min: parseInt(<?=intval($arItem["VALUES"]["MIN"]["VALUE"])?>),
								max: parseInt(<?=intval($arItem["VALUES"]["MAX"]["VALUE"])?>),
								values: [<?echo strlen($arItem["VALUES"]["MIN"]["HTML_VALUE"]) ? intval($arItem["VALUES"]["MIN"]["HTML_VALUE"]) : intval($arItem["VALUES"]["MIN"]["VALUE"])?>, <?echo strlen($arItem["VALUES"]["MAX"]["HTML_VALUE"]) ? intval($arItem["VALUES"]["MAX"]["HTML_VALUE"]) : intval($arItem["VALUES"]["MAX"]["VALUE"])?>]
								, create: function( event, ui ) {
									$(this).find("a:first").addClass("first-a");
									$(this).find("a:last").addClass("last-a");
									//$("#<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>").val(<?=intval($arItem["VALUES"]["MIN"]["VALUE"])?>);
									//$("#<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>").val(<?=intval($arItem["VALUES"]["MAX"]["VALUE"])?>);
								}
								, slide: function( event, ui ) {
									if ($(ui.handle).hasClass("first-a")) {
										$("#<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>").val(ui.value);
									}
									if ($(ui.handle).hasClass("last-a")) {
										$("#<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>").val(ui.value);
									}
								}
								, stop: function( event, ui ) {
									if ($(ui.handle).hasClass("first-a")) {
										smartFilter.keyup(BX('<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>'));
									}
									if ($(ui.handle).hasClass("last-a")) {
										smartFilter.keyup(BX('<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>'));
									}
								}
							});
						}
					});
				</script>
			<?elseif(!empty($arItem["VALUES"]) && !isset($arItem["PRICE"]) && ($arItem['CODE'] == 'COLOR_REF')):?>
			
			<dl class="<?=$arItem['OPEN'] ? 'open' : 'closed'?>">
				<dt><a href="JavaScript:void(0);" title="<?=$arItem["NAME"]?>"><?=$arItem["NAME"]?></a><span id="bx_filter_container_modef_<?=$arItem['ID']?>" class="bx_filter_container_modef"></span></dt>
				<dd>
					<ul class="colors">
					<?foreach($arItem["VALUES"] as $val => $ar):
						if (!empty($ar['VALUE'])):
					?>
						<li class="<?echo $ar["CHECKED"]? 'selected': ''?>">
							<input
								type="checkbox"
                                data-property_id="<?=$arItem['ID']?>"
								value="<?echo $ar["HTML_VALUE"]?>"
								name="<?echo $ar["CONTROL_NAME"]?>"
								id="<?echo $ar["CONTROL_ID"]?>"
								<?echo $ar["CHECKED"]? 'checked="checked"': ''?>
								onclick="smartFilter.click(this)"
							/>
							<label title="<?=$ar['VALUE']?>" style="background-color: #<?=$val?>;" for="<?echo $ar["CONTROL_ID"]?>"></label>
						</li>
					<?
						endif;
					endforeach;?>	
					</ul>
				</dd>
			</dl>
			<?elseif(!empty($arItem["VALUES"]) && !isset($arItem["PRICE"])):?>
			
			<dl class="<?=$arItem['OPEN'] ? 'open' : 'closed'?>">
				<dt><a href="JavaScript:void(0);" title="<?=$arItem["NAME"]?>"><?=$arItem["NAME"]?></a><span id="bx_filter_container_modef_<?=$arItem['ID']?>" class="bx_filter_container_modef"></span></dt>
				<dd>
				<?foreach($arItem["VALUES"] as $val => $ar):?>
					<div class="check">
						<div class="squaredThree">
							<input
								type="checkbox"
                                data-property_id="<?=$arItem['ID']?>"
								value="<?echo $ar["HTML_VALUE"]?>"
								name="<?echo $ar["CONTROL_NAME"]?>"
								id="<?echo $ar["CONTROL_ID"]?>"
								<?echo $ar["CHECKED"]? 'checked="checked"': ''?>
								onclick="smartFilter.click(this)"
							/>
							<label for="<?echo $ar["CONTROL_ID"]?>"><?echo $ar["VALUE"];?><?if (isset($ar["PRODUCT_COUNT"])):?> (<?=$ar["PRODUCT_COUNT"]?>)<?endif;?></label>
						</div>
					</div>
				<?endforeach;?>
				</dd>
			</dl>
			<?endif;?>
		<?endforeach;?>
		<input type="submit" class="red-button" id="set_filter" name="set_filter" value="<?=GetMessage("CT_BCSF_SET_FILTER")?>" />
		<input type="submit" class="red-button grey-button" id="del_filter" name="del_filter" value="<?=GetMessage("CT_BCSF_DEL_FILTER")?>" />
        <div class="bx_filter_popup_result left" id="modef" <?if(!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"';?> style="display: inline-block;">
            <?echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>'));?>
            <a class="bx_filter_popup_result_link"  href="<?echo $arResult["FILTER_URL"]?>"><?echo GetMessage("CT_BCSF_FILTER_SHOW")?></a>
        </div>
	</form>
</article>
<script>
	var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>');
</script>