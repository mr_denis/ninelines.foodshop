<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//Разделы каталога
if($arParams["SECTION_ID"] > 0)
{
	$arCatalogSections = array();
	$arFilter["ID"] = $arParams["SECTION_ID"];
	$rsCurSection = CIBlockSection::GetList(array(), $arFilter, false, array());
	$rsCurSection->SetUrlTemplates("", $this->arParams["SECTION_URL"]);
	if ($arCurSection = $rsCurSection->Fetch())
	{
		$arPath = array();
		$rsPath = CIBlockSection::GetNavChain($arParams["IBLOCK_ID"], $arCurSection["ID"]);
		$rsPath->SetUrlTemplates("", $arParams["SECTION_URL"]);
		while ($arSection = $rsPath->GetNext()) {
			$arPath[] = $arSection;
		}
		
		if ($arPath[0]['ID'])
		{
			$arBaseSection = $arPath[0];
			
			$arFilter = array(
				'IBLOCK_ID' => $arParams['IBLOCK_ID'], 
				'>LEFT_MARGIN' => $arBaseSection['LEFT_MARGIN'], 
				'<RIGHT_MARGIN' => $arBaseSection['RIGHT_MARGIN'],
				">DEPTH_LEVEL" => $arBaseSection["DEPTH_LEVEL"],
			);
			$rsSection = CIBlockSection::GetList(array('DEPTH_LEVEL' => 'ASC'), $arFilter, false, array());
			$rsSection->SetUrlTemplates("", $arParams["SECTION_URL"]);
			
			while ($arSection = $rsSection->GetNext()) {
				if ($arSection['DEPTH_LEVEL'] == 2)
					$arCatalogSections[$arSection['ID']] = $arSection;
				else {
					if ($arSection['IBLOCK_SECTION_ID'] == $arPath[1]['ID'])
						$arCatalogSections[$arSection['IBLOCK_SECTION_ID']]['SUBSECTIONS'][$arSection['ID']] = $arSection;
				}	
			}
		}
	}
}
$arResult['CATALOG_SECTIONS'] = $arCatalogSections;

foreach($arResult["ITEMS"] as $key => $arItem)
{
	if ($arItem['CODE'] == 'MANUFACTURER')
	{
		$arFilter = array(
			'ACTIVE' => 'Y', 
			'IBLOCK_ID' => $arParams['IBLOCK_ID'], 
			'SECTION_ID' => $arParams["SECTION_ID"], 
			'INCLUDE_SUBSECTIONS' => 'Y', 
			'PROPERTY_MANUFACTURER' => array_keys($arItem['VALUES']),
		);
		
		$dbRes = CIBlockElement::GetList(array('PROPERTY_MANUFACTURER' => 'ASC'), $arFilter, array('PROPERTY_MANUFACTURER'), false);
		while ($arRes = $dbRes->GetNext())
			$arResult["ITEMS"][$key]['VALUES'][$arRes['PROPERTY_MANUFACTURER_VALUE']]['PRODUCT_COUNT'] = $arRes['CNT'];
	}
	
	foreach ($arItem['VALUES'] as $arValue)	
	{
		if ($arItem['PRICE'] || ($arItem['PROPERTY_TYPE'] == 'N'))
		{
			if ((trim($arValue['HTML_VALUE']) != '') && (intval($arValue['HTML_VALUE']) > 0)) 
			{
				$arResult["ITEMS"][$key]['OPEN'] = 1;
				break;
			}
		}
		else
		{
			if ($arValue['CHECKED']) 
			{
				$arResult["ITEMS"][$key]['OPEN'] = 1;
				break;
			}	
		}	
	}	
}