<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<script>
	var obEshopBasket = new JSEshopBasket("/ajax/basketHandler.php", "<?=SITE_ID?>");
</script>
<div class="cart-block">
	<span><?=GetMessage('CART_TITLE')?></span>
<?
	$frame = $this->createFrame("sale-basket-basket-line-container", false)->begin();
	if (intval($arResult["NUM_PRODUCTS"])>0)
	{
?>
		<a class="cart_top_inline_link" title="<?=GetMessage('YOUR_CART_TITLE', array('#NUM#' => intval($arResult["NUM_PRODUCTS"]), '#PROD_TITLE#' => sklonenie(intval($arResult["NUM_PRODUCTS"]), 'товар', 'товара', 'товаров'), '#TOTAL#' => $arResult['TOTAL_PRICE']))?>" href="<?=$arParams["PATH_TO_BASKET"]?>"><?=GetMessage('YOUR_CART', array('#NUM#' => intval($arResult["NUM_PRODUCTS"]), '#PROD_TITLE#' => sklonenie(intval($arResult["NUM_PRODUCTS"]), 'товар', 'товара', 'товаров'), '#TOTAL#' => $arResult['TOTAL_PRICE']))?></a>
<?
	}
	else
	{
?>
        <a class="cart_top_inline_link" title="<?=GetMessage('YOUR_CART_EMPTY_TITLE', array('#NUM#' => intval($arResult["NUM_PRODUCTS"]), '#PROD_TITLE#' => sklonenie(intval($arResult["NUM_PRODUCTS"]), 'товар', 'товара', 'товаров'), '#TOTAL#' => $arResult['TOTAL_PRICE']))?>" href="<?=$arParams["PATH_TO_BASKET"]?>"><?=GetMessage('YOUR_CART_EMPTY', array('#NUM#' => intval($arResult["NUM_PRODUCTS"]), '#PROD_TITLE#' => sklonenie(intval($arResult["NUM_PRODUCTS"]), 'товар', 'товара', 'товаров'), '#TOTAL#' => $arResult['TOTAL_PRICE']))?></a>
		<?/*?><span><?=GetMessage('YOUR_CART_EMPTY')?></span><?*/?>
<?
	}
	$frame->beginStub();
?>
        <a class="cart_top_inline_link" title="<?=GetMessage('YOUR_CART_EMPTY_TITLE', array('#NUM#' => intval($arResult["NUM_PRODUCTS"]), '#PROD_TITLE#' => sklonenie(intval($arResult["NUM_PRODUCTS"]), 'товар', 'товара', 'товаров'), '#TOTAL#' => $arResult['TOTAL_PRICE']))?>" href="<?=$arParams["PATH_TO_BASKET"]?>"><?=GetMessage('YOUR_CART_EMPTY', array('#NUM#' => intval($arResult["NUM_PRODUCTS"]), '#PROD_TITLE#' => sklonenie(intval($arResult["NUM_PRODUCTS"]), 'товар', 'товара', 'товаров'), '#TOTAL#' => $arResult['TOTAL_PRICE']))?></a>
<?
	$frame->end();
?>
</div>