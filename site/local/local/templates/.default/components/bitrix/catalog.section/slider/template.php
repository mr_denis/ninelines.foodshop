<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?
if (!empty($arResult['ITEMS']))
{
	$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
	$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
	$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
?>
	<div class="carousel-outer catalog-inner">
		<ul class="carousel-inner">
<?	
	foreach ($arResult['ITEMS'] as $key => $arItem)
	{
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
		$strMainID = $this->GetEditAreaId($arItem['ID']);

		$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);

		$arItemInfo = array();
		if (!empty($arItem['OFFERS']))
		{
			reset($arItem['OFFERS']);
			$offer = current($arItem['OFFERS']);
			$arItemInfo['CURRENT_OFFER'] = $offer;
			$arItemInfo['ITEM_ID'] = $offer['ID'];
		}
		else
		{
			$arItemInfo['ITEM_ID'] = $arItem['ID'];
		}
		$arItemInfo['PRICE'] = $arItem['MIN_PRICE'];
		
		$itemType = array();
		$addClass = '';	
		$discount = false;	
		
		if (!empty($arItem['PROPERTIES']['NEWPRODUCT']['VALUE']))	
			$itemType[] = 'new';
			
		if (!empty($arItem['PROPERTIES']['DISCOUNT']['VALUE']))	
		{
			$itemType[] = 'discount';
			if (intval($arItem['PROPERTIES']['OLD_PRICE']['VALUE']) && 
				intval($arItem['MIN_PRICE']['VALUE']) && 
				($arItem['PROPERTIES']['OLD_PRICE']['VALUE'] > 0) && 
				($arItem['MIN_PRICE']['VALUE'] < $arItem['PROPERTIES']['OLD_PRICE']['VALUE']))
				$discount = $arItem['PROPERTIES']['OLD_PRICE']['VALUE'] - $arItem['MIN_PRICE']['VALUE'];	
		}	
		
		if (!empty($arItem['PROPERTIES']['SALELEADER']['VALUE']))	
		{
			$itemType[] = 'hit';
		}	
		
		if (
			!empty($arItem['PROPERTIES']['PRODUCT_ACTION']['VALUE']) && 
			(strtotime($arItem['PROPERTIES']['PRODUCT_ACTION']['VALUE']) > time())
		)	
		{
			$itemType = 'action';
			$addClass = 'item-medium';
			$time = strtotime($arItem['PROPERTIES']['PRODUCT_ACTION']['VALUE']) - time();
		}	
		
		if (!empty($arItem['PROPERTIES']['PRODUCT_OF_DAY']['VALUE']))	
		{
			$itemType = 'prodday';
			$addClass = 'item-big';
		}	
		
		$pic = array();
		
		if (!empty($arItem['DETAIL_PICTURE']['ID']))
		{
			$arSize = array('width' => 170, 'height' => 167);
				
			$pic = CFile::ResizeImageGet($arItem['DETAIL_PICTURE']['ID'], $arSize, BX_RESIZE_IMAGE_PROPORTIONAL, true);	
		}	
		
?>
			<li class="item"  id="<?=$strMainID;?>">
				<div class="item element">
					<a href="<?=$arItem['DETAIL_PAGE_URL'];?>" title="<?=$arItem['NAME'];?>">
						<span class="img-inside"><img src="<?=$pic['src']?>" alt="<?=$arItem['NAME'];?>" title="<?=$arItem['NAME'];?>" /></span>
						<span class="name"><?=$arItem['NAME'];?></span>
						<span class="price"><?=$arItem['MIN_PRICE']['PRINT_VALUE'];?></span>
<?
					if (is_array($itemType) && in_array('new', $itemType)):
?>					
						<span class="dop-thing new">Новинка</span>
<?
					endif;
					if (is_array($itemType) && in_array('discount', $itemType)):
?>
						<span class="dop-thing discount">Скидка</span>
<?
					endif;
					if (is_array($itemType) && in_array('hit', $itemType)):
?>					
						<span class="dop-thing hit">Хит продаж</span>
<?
					endif;	
?>						
					</a>
					<a href="<?=SITE_DIR?>personal/cart/" data-is_offer="<?=isset($arItemInfo['CURRENT_OFFER'])? 'Y' : 'N'?>" data-item_id="<?=$arItemInfo['ITEM_ID']?>" data-in_cart="N" class="add2cart red-button add2cart_btn_<?=$arItemInfo['ITEM_ID']?>" title="<?=$arItem['NAME']?>">Добавить в корзину</a>
				</div>
			</li>				
<?	
	}
?>			
		</ul>
	</div>		
<?
}
?>