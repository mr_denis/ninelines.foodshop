<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);//dump($arParams['USER_FAVORITES']);dump($arResult['ITEMS']);
?>

<?if (!empty($arResult['ITEMS'])):?>
    <div class="favorite-items cart-items">

    <?foreach ($arResult['ITEMS'] as $key => $arItem):
        $arFavItem = array();
        $arFavItem['IS_OFFER'] = 'N';
        $arFavItem['NAME'] = $arItem['NAME'];
        $arFavItem['DETAIL_PAGE_URL'] = $arItem['DETAIL_PAGE_URL'];
        $arFavItem['PRICE'] = $arItem['MIN_PRICE']['PRINT_VALUE'];

        if (isset($arItem['PREVIEW_PICTURE']['ID']))
            $pic = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'], array('width' => 125, 'height' => 110), BX_RESIZE_IMAGE_PROPORTIONAL, true);
        elseif (isset($arItem['DETAIL_PICTURE']['ID']))
            $pic = CFile::ResizeImageGet($arItem['DETAIL_PICTURE']['ID'], array('width' => 125, 'height' => 110), BX_RESIZE_IMAGE_PROPORTIONAL, true);
        else
            $pic['src'] = $this->__folder.'/images/no_photo.png';
        $arFavItem['PICTURE'] = $pic['src'];

        if (!empty($arItem['PROPERTIES']['DISCOUNT']['VALUE']))
        {
            $itemType[] = 'discount';
            if (intval($arItem['PROPERTIES']['OLD_PRICE']['VALUE']) &&
                intval($arItem['MIN_PRICE']['VALUE']) &&
                ($arItem['PROPERTIES']['OLD_PRICE']['VALUE'] > 0) &&
                ($arItem['MIN_PRICE']['VALUE'] < $arItem['PROPERTIES']['OLD_PRICE']['VALUE']))
                $arFavItem['OLD_PRICE'] = $arItem['PROPERTIES']['OLD_PRICE']['VALUE'];
        }


        if (in_array($arItem['ID'], $arParams['USER_FAVORITES']) && !isset($arParams['USER_FAVORITES'][$arItem['ID']]) && !empty($arItem['OFFERS'])) {
            $arFavItem['ITEM_ID'] = $arItem['ID'];
            $arFavItem['ID'] = $arItem['ID'];
            foreach ($arItem['OFFERS'] as $arOffer) {
                if (isset($arParams['USER_FAVORITES'][$arOffer['ID']])) {
                    $arFavItem['ID'] = $arOffer['ID'];
                    $arFavItem['IS_OFFER'] = 'Y';
                    $arFavItem['NAME'] = $arOffer['NAME'];
                    $arFavItem['PRICE'] = $arOffer['MIN_PRICE']['PRINT_VALUE'];

                    if (isset($arOffer['PREVIEW_PICTURE']['ID']))
                        $pic = CFile::ResizeImageGet($arOffer['PREVIEW_PICTURE']['ID'], array('width' => 125, 'height' => 110), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                    elseif (isset($arOffer['DETAIL_PICTURE']['ID']))
                        $pic = CFile::ResizeImageGet($arOffer['DETAIL_PICTURE']['ID'], array('width' => 125, 'height' => 110), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                    elseif (!isset($pic['src']))
                        $pic['src'] = $this->__folder.'/images/no_photo.png';

                    if (intval($arItem['PROPERTIES']['OLD_PRICE']['VALUE']) &&
                        intval($arItem['MIN_PRICE']['VALUE']) &&
                        ($arItem['PROPERTIES']['OLD_PRICE']['VALUE'] > 0) &&
                        ($arOffer['MIN_PRICE']['VALUE'] < $arItem['PROPERTIES']['OLD_PRICE']['VALUE']))
                        $arFavItem['OLD_PRICE'] = $arItem['PROPERTIES']['OLD_PRICE']['VALUE'];
                    else
                        unset($arFavItem['OLD_PRICE']);
                    $arFavItem['PROPERTIES'] = array();
                    foreach ($arOffer['DISPLAY_PROPERTIES'] as $propCode => $arProp) {
                        if ($propCode != 'MORE_PHOTO') {
                            $arFavItem['PROPERTIES'][$arProp['NAME']] = $arProp['DISPLAY_VALUE'];
                        }
                    }
                    break;
                }
            }
        }
?>
        <div class="cart-item">
            <a href="JavaScript:void(0)" class="favorite-item-remove close-item" data-item_id="<?=$arFavItem['ITEM_ID']?>" data-offer_id="<?=$arFavItem['ID']?>" title="Закрыть"></a>
            <div class="product-image-container">
                <a href="<?=$arFavItem['DETAIL_PAGE_URL']?>" title="<?=$arFavItem['NAME']?>"><img src="<?=$arFavItem['PICTURE']?>" alt="<?=$arFavItem['NAME']?>" title="<?=$arFavItem['NAME']?>" /></a>
            </div>
            <div class="product-info-container">
                <div class="c-name">
                    <span class="name"><?=$arFavItem['NAME']?></span>
                    <?foreach ($arFavItem['PROPERTIES'] as $pName => $pValue):?>
                    <span class="options"><?=$pName?>: <?=$pValue?></span>
                    <?endforeach;?>
                </div>
                <div class="c-price">
                    <span class="n-price">Цена за штуку</span>
                    <span class="price"><?=$arFavItem['PRICE']?></span>
                    <?if (!empty($arFavItem['OLD_PRICE'])):?><span class="o-price"><?=number_format($arFavItem['OLD_PRICE'], 0, ',', ' ');?> руб.</span><?endif;?>
                </div>
                <div class="c-buy">
                    <a href="<?=SITE_DIR?>personal/cart/" title="В корзину" data-type="card" data-is_favorite="Y" data-is_offer="<?=$arFavItem['IS_OFFER']?>" data-item_id="<?=$arFavItem['ID']?>" data-in_cart="N" class="add2cart-favorite add2cart_btn_<?=$arFavItem['ID']?>">В корзину</a>
                </div>
            </div>
        </div>
    <?endforeach;?>
        <script>
        $(function(){
            $('.add2cart-favorite').catalog();
            $('.favorite-item-remove').favorite('init_remove');
        });
        </script>

    </div>
    <div class="cart-items is-empty-fav" style="display:none">
        <p class="cart-is-empty fav-is-empty ">У Вас нет товаров, добавленных в избранное</p>
    </div>
<?else:?>
    <div class="cart-items is-empty-fav">
        <p class="cart-is-empty fav-is-empty ">У Вас нет товаров, добавленных в избранное</p>
    </div>
<?endif;?>