<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?
if (!empty($arResult['ITEMS']))
{
	$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
	$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
	$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
?>
	<div class="catalog-inner container" id="<?=$arParams['TAB_TYPE']?>_container">
<?	//$APPLICATION->RestartBuffer();dump($arResult); die();
	if (($arParams['AJAX_LOAD'] == 'Y') && ($_POST['action'] == 'get_catalog_items') && ($_POST['container'] == $arParams['TAB_TYPE'].'_container'))
		$APPLICATION->RestartBuffer();
    $index = 0;
	foreach ($arResult['ITEMS'] as $key => $arItem)
	{
        $index++;
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
		$strMainID = $this->GetEditAreaId($arItem['ID']);

		$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);

		$arItemInfo = array();
		if (!empty($arItem['OFFERS']))
		{
			reset($arItem['OFFERS']);
			$offer = current($arItem['OFFERS']);
			$arItemInfo['CURRENT_OFFER'] = $offer;
			$arItemInfo['ITEM_ID'] = $offer['ID'];
		}
		else
		{
			$arItemInfo['ITEM_ID'] = $arItem['ID'];
		}
		$arItemInfo['PRICE'] = $arItem['MIN_PRICE'];
		
		$itemType = array();
		$addClass = '';	
		$discount = false;	
		$payed = 0;

		if (!empty($arItem['PROPERTIES']['NEWPRODUCT']['VALUE']))	
			$itemType[] = 'new';
			
		if (!empty($arItem['PROPERTIES']['DISCOUNT']['VALUE']))	
		{
			$itemType[] = 'discount';
			if (intval($arItem['PROPERTIES']['OLD_PRICE']['VALUE']) && 
				intval($arItem['MIN_PRICE']['VALUE']) && 
				($arItem['PROPERTIES']['OLD_PRICE']['VALUE'] > 0) && 
				($arItem['MIN_PRICE']['VALUE'] < $arItem['PROPERTIES']['OLD_PRICE']['VALUE']))
				$discount = $arItem['PROPERTIES']['OLD_PRICE']['VALUE'] - $arItem['MIN_PRICE']['VALUE'];

            if (intval($arItem['PROPERTIES']['ALREADY_PAYED']['VALUE']) && (intval($arItem['PROPERTIES']['ALREADY_PAYED']['VALUE']) > 0) && (intval($arItem['PROPERTIES']['ALREADY_PAYED']['VALUE']) < 100))
                $payed = intval($arItem['PROPERTIES']['ALREADY_PAYED']['VALUE']);

		}	
		
		if (!empty($arItem['PROPERTIES']['SALELEADER']['VALUE']))	
		{
			$itemType[] = 'hit';
		}	
		
		if (intval($arItem['PROPERTIES']['PRODUCT_ACTION']['VALUE']))
		{
			$itemType = 'action';
			$addClass = 'item-medium';
			$quantity = intval($arItem['PROPERTIES']['PRODUCT_ACTION']['VALUE']);
		}	
		
		if (!empty($arItem['PROPERTIES']['PRODUCT_OF_DAY']['VALUE']))	
		{
			$itemType = 'prodday';
			$addClass = 'item-big';
		}	
		
		$pic = array();
		
		if (!empty($arItem['DETAIL_PICTURE']['ID']))
		{
			$arSize = array('width' => 239, 'height' => 202);
			if ($itemType == 'prodday')
				$arSize = array('width' => 479, 'height' => 402);
				
			$pic = CFile::ResizeImageGet($arItem['DETAIL_PICTURE']['ID'], $arSize, BX_RESIZE_IMAGE_PROPORTIONAL, true);	
		}	
		
?>
			<div class="item element <?=$addClass?>" id="<?=$strMainID;?>" >
				<a href="<?=$arItem['DETAIL_PAGE_URL'];?>" title="<?=$arItem['NAME'];?>">
					<span  class="img-inside">
						<img src="<?=$pic['src']?>" alt="<?=$arItem['NAME'];?>" title="<?=$arItem['NAME'];?>" />
					</span>	
					<span class="name"><?=$arItem['NAME'];?></span>
					<span class="price"><?=$arItem['MIN_PRICE']['PRINT_VALUE'];?><?if ($discount):?><span class="old-price"><?=number_format($arItem['PROPERTIES']['OLD_PRICE']['VALUE'], 0, ',', ' ');?> руб.</span><?endif;?></span>
<?	
				if ($itemType == 'prodday'):
?>
					<span class="dop-thing day">Товар <span>дня</span></span>
<?
				elseif ($itemType == 'action'):
?>
					<span class="dop-thing hurry-up">Успей <span>купить</span></span>
<?
				else:
					if (is_array($itemType) && in_array('new', $itemType)):
?>					
					<span class="dop-thing new">Новинка</span>
<?
					endif;
					if (is_array($itemType) && in_array('discount', $itemType)):
?>
					<span class="dop-thing discount">Скидка</span>
<?
					endif;
					if (is_array($itemType) && in_array('hit', $itemType)):
?>					
					<span class="dop-thing hit">Хит продаж</span>
<?
					endif;
				endif;
?>					
				</a>
<?	
?>				
				<a href="<?=SITE_DIR?>personal/cart/" data-is_offer="<?=isset($arItemInfo['CURRENT_OFFER'])? 'Y' : 'N'?>" data-item_id="<?=$arItemInfo['ITEM_ID']?>" data-in_cart="N" class="add2cart red-button add2cart_btn_<?=$arItemInfo['ITEM_ID']?>" title="<?=$arItem['NAME']?>">Добавить в корзину</a>
<?
				if ($discount && ($itemType == 'prodday')):
?>
				<div class="economy">
					<span class="price"><span><?=number_format($discount, 0, ',', ' ');?> руб.</span>экономия</span>
					<div class="percent">
						<span class="back"><span style="width:<?=$payed?>%;"></span></span>
						уже куплено <span><?=$payed?></span>% товаров
					</div>
				</div>
<?
				endif;
				if ($itemType == 'action'):
?>				
				<div class="product-tile-sale">
					<div class="count-down-timer" data-init="countDown">
                        <span class="count-down-timer-content"></span>
						<div class="count-down-timer-amount">
							<strong class="count-down-timer-qty"><?=$quantity?></strong>
							<small class="count-down-timer-abbr">шт</small>
						</div>
					</div>
				</div>
<?
				endif;
?>				
			</div>			
<?	
	}
	if (($arParams['AJAX_LOAD'] == 'Y') && ($_POST['action'] == 'get_catalog_items') && ($_POST['container'] == $arParams['TAB_TYPE'].'_container'))
		die();
?>
				
	</div>		
	<div class="clear-all"></div>
<?
	if ($arParams['AJAX_LOAD'] == 'Y')
	{
		if (isset($arResult['NAV_RESULT']->NavPageCount) && ($arResult['NAV_RESULT']->NavPageCount > 1))
		{
?>
	<a href="JavaScript:void(0)" onclick="getCatalogItems(this, '<?=$arParams['TAB_TYPE']?>_container')" class="more" title="Показать еще" data-cur-page="<?=$APPLICATION->GetCurPage()?>" data-page-count="<?=$arResult['NAV_RESULT']->NavPageCount?>"  data-paging-var="PAGEN_<?=$arResult['NAV_RESULT']->NavNum?>" data-paging-number="2">Показать еще</a>
<?
		}	
	}
	else
	{
		echo $arResult['NAV_STRING'];
	}
?>	
<script>
    $(function(){
        <?if (!empty($arParams['TAB_TYPE'])):?>
            $('#<?=$arParams['TAB_TYPE']?>_container').makeIso({'catalog':false});
        <?else:?>
            $('#_container').makeIso({'catalog':true});
        <?endif;?>
    });
</script>
<?
}
?>