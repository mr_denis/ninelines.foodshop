<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//dump($arResult);
if($USER->IsAuthorized() || $arParams["ALLOW_AUTO_REGISTER"] == "Y")
{
	if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
	{
		if(strlen($arResult["REDIRECT_URL"]) > 0)
		{
			$APPLICATION->RestartBuffer();
			?>
			<script type="text/javascript">
				window.top.location.href='<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';
			</script>
			<?
			die();
		}

	}
}
?>
<div class="cart">
<script type="text/javascript">
	function submitForm(val)
	{
		if(val != 'Y')
			BX('confirmorder').value = 'N';

		var orderForm = BX('ORDER_FORM');
		BX.showWait();
		BX.ajax.submit(orderForm, ajaxResult);

		return true;
	}

	function ajaxResult(res)
	{
		try
		{
			var json = JSON.parse(res);
			BX.closeWait();

			if (json.error)
			{
				return;
			}
			else if (json.redirect)
			{
				window.top.location.href = json.redirect;
			}
		}
		catch (e)
		{
			BX('order_form_content').innerHTML = res;
		}

		BX.closeWait();
	}

	function SetContact(profileId)
	{
		BX("profile_change").value = "Y";
		submitForm();
	}
	</script>
	<h1>Контакты и доставка</h1>
	<span class="total">3 товара на 51 229 руб.</span>
<?
	if($_POST["is_ajax_post"] != "Y")
	{
?>
	<form action="<?=$APPLICATION->GetCurPage();?>" method="POST" name="ORDER_FORM" id="ORDER_FORM" enctype="multipart/form-data">
		<?=bitrix_sessid_post()?>
		<div class="cart-items" id="order_form_content">
<?
	}
	else
	{
		$APPLICATION->RestartBuffer();
	}
?>			
	
		<div class="cart-item">
			<span class="num">1</span>

			<div class="sorting">
				<input type="hidden" value="1" name="PERSON_TYPE" id="PERSON_TYPE_1">
				<span class="sort-text">Выберите профиль доставки</span>
				
				<select class="select-class" name="PROFILE_ID" id="ID_PROFILE_ID" onChange="SetProfile(this.value)">
					<option>Новый профиль</option>
<?
			if (is_array($arResult["ORDER_PROP"]["USER_PROFILES"]) && !empty($arResult["ORDER_PROP"]["USER_PROFILES"])):
				foreach ($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles):
?>				
					<option value="<?= $arUserProfiles["ID"] ?>"<?if ($arUserProfiles["CHECKED"]=="Y") echo " selected";?>><?=$arUserProfiles["NAME"]?></option>
<?
				endforeach;
			endif; 	
?>					
				</select>			
			</div>
			<div class="clear-all">&nbsp;</div>
		</div>
		<div class="cart-item">
			<span class="num">2</span>
			<div class="sorting">
<?
			foreach ($arResult["ORDER_PROP"]["USER_PROPS_Y"] as $arProp):
				if ($arProp["TYPE"] == "LOCATION")
				{
					$value = 0;
					if (is_array($arProperties["VARIANTS"]) && count($arProperties["VARIANTS"]) > 0)
					{
						foreach ($arProperties["VARIANTS"] as $arVariant)
						{
							if ($arVariant["SELECTED"] == "Y")
							{
								$value = $arVariant["ID"];
								break;
							}
						}
					}
					$GLOBALS["APPLICATION"]->IncludeComponent(
						"bitrix:sale.ajax.locations",
						$locationTemplate,
						array(
							"AJAX_CALL" => "N",
							"COUNTRY_INPUT_NAME" => "COUNTRY",
							"REGION_INPUT_NAME" => "REGION",
							"CITY_INPUT_NAME" => $arProperties["FIELD_NAME"],
							"CITY_OUT_LOCATION" => "Y",
							"LOCATION_VALUE" => $value,
							"ORDER_PROPS_ID" => $arProperties["ID"],
							"ONCITYCHANGE" => ($arProperties["IS_LOCATION"] == "Y" || $arProperties["IS_LOCATION4TAX"] == "Y") ? "submitForm()" : "",
							"SIZE1" => $arProperties["SIZE1"],
						),
						null,
						array('HIDE_ICONS' => 'Y')
					);
					?>
					<?
				}
				else
				{
?>			
				<input type="text" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" class="name <?=strtolower($arProp['CODE']);?>" value="<?=!empty($arProperties["VALUE"]) ? $arProperties["VALUE"] : $arProp['NAME']?>" />
<?
				}	
			endforeach;	
?>				
			</div>
			<div class="clear-all"></div>
		</div>
		<div class="cart-item l-cart-item">
			<span class="num">3</span>
			<div class="sorting">
				<span class="sort-text">Способ доставки</span>
				<div class="check-container">
<?
				if(!empty($arResult["DELIVERY"]))
				{
					if ($delivery_id !== 0 && intval($delivery_id) <= 0)
					{
						
					}
					else // stores and courier
					{
						if (count($arDelivery["STORE"]) > 0)
							$clickHandler = "onClick = \"fShowStore('".$arDelivery["ID"]."','".$arParams["SHOW_STORES_IMAGES"]."','".$width."','".SITE_ID."')\";";
						else
							$clickHandler = "onClick = \"BX('ID_DELIVERY_ID_".$arDelivery["ID"]."').checked=true;submitForm();\"";
?>
					<div class="check">
						<div class="squaredThree">
							<input type="radio"
								id="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>"
								name="<?=htmlspecialcharsbx($arDelivery["FIELD_NAME"])?>"
								value="<?= $arDelivery["ID"] ?>"<?if ($arDelivery["CHECKED"]=="Y") echo " checked";?>
								onclick="submitForm();"
								/>
							<label  for="ID_DELIVERY_ID_<?=$arDelivery["ID"]?>" <?=$clickHandler?>><?= htmlspecialcharsbx($arDelivery["NAME"])?> — <span class="price"><?=$arDelivery["PRICE_FORMATED"]?></span> <?if (strlen($arDelivery["PERIOD_TEXT"])):?><span class="date">(<?=$arDelivery["PERIOD_TEXT"]?>)</span><?endif;?></label>
						</div>
					</div>
<?							
					}
				}	
?>					
				</div>
			</div>
			<div class="clear-all">&nbsp;</div>
		</div>
<?
	if($_POST["is_ajax_post"] != "Y")
	{
?>
		<input type="hidden" name="confirmorder" id="confirmorder" value="Y">
		<input type="hidden" name="profile_change" id="profile_change" value="N">
		<input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
		<input type="hidden" name="json" value="Y">
		<a href="javascript:void();" onclick="submitForm('Y'); return false;" class="payment" title="">ПЕРЕЙТИ К ОПЛАТЕ</a>
		</div>
	
	</form>
<?
		if($arParams["DELIVERY_NO_AJAX"] == "N")
		{
?>
			<div style="display:none;"><?$APPLICATION->IncludeComponent("bitrix:sale.ajax.delivery.calculator", "", array(), null, array('HIDE_ICONS' => 'Y')); ?></div>
<?
		}
	}
	else
	{
?>
		<script type="text/javascript">
			top.BX('confirmorder').value = 'Y';
			top.BX('profile_change').value = 'N';
		</script>
<?
		die();
	}		
?>
</div>