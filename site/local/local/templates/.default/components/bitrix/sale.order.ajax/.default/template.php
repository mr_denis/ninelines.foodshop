<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if($USER->IsAuthorized() || $arParams["ALLOW_AUTO_REGISTER"] == "Y")
{
	if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
	{
		if(strlen($arResult["REDIRECT_URL"]) > 0)
		{
			$APPLICATION->RestartBuffer();
			?>
			<script type="text/javascript">
				window.top.location.href='<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';
			</script>
			<?
			die();
		}

	}
}

$APPLICATION->SetAdditionalCSS($templateFolder."/style_cart.css");
$APPLICATION->SetAdditionalCSS($templateFolder."/style.css");

CJSCore::Init(array('fx', 'popup', 'window', 'ajax'));
?>

<a name="order_form"></a>

<div id="order_form_div"  class="cart order-checkout">
	<h1 id="order_form_title" ><?if (!empty($arResult["ORDER"])):?>Заказ оформлен<?else:?>Контакты и доставка<?endif;?></h1>
	<?if (empty($arResult["ORDER"])):?><span class="total"><?=count($arResult['BASKET_ITEMS'])?> <?=sklonenie(count($arResult['BASKET_ITEMS']), 'товар', 'товара', 'товаров');?> на <?=$arResult['ORDER_PRICE_FORMATED']?></span><?endif;?>

<?
if (!function_exists("getColumnName"))
{
	function getColumnName($arHeader)
	{
		return (strlen($arHeader["name"]) > 0) ? $arHeader["name"] : GetMessage("SALE_".$arHeader["id"]);
	}
}

if (!function_exists("cmpBySort"))
{
	function cmpBySort($array1, $array2)
	{
		if (!isset($array1["SORT"]) || !isset($array2["SORT"]))
			return -1;

		if ($array1["SORT"] > $array2["SORT"])
			return 1;

		if ($array1["SORT"] < $array2["SORT"])
			return -1;

		if ($array1["SORT"] == $array2["SORT"])
			return 0;
	}
}
?>

	<?
	if(!$USER->IsAuthorized() && $arParams["ALLOW_AUTO_REGISTER"] == "N")
	{
		if(!empty($arResult["ERROR"]))
		{
			foreach($arResult["ERROR"] as $v)
				echo ShowError($v);
		}
		elseif(!empty($arResult["OK_MESSAGE"]))
		{
			foreach($arResult["OK_MESSAGE"] as $v)
				echo ShowNote($v);
		}

		include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/auth.php");
	}
	else
	{
		if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
		{
			if(strlen($arResult["REDIRECT_URL"]) == 0)
			{
				include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/confirm.php");
			}
		}
		else
		{
			?>
			<script type="text/javascript">
			function submitForm(val)
			{
				if(val != 'Y')
					BX('confirmorder').value = 'N';

				var orderForm = BX('ORDER_FORM');
				BX.showWait();
				BX.ajax.submit(orderForm, ajaxResult);

				return true;
			}
			
			function showOrderStep()
			{
				$('.payment-items').hide();
				$('.order-items').show();
				
				$('#order_form_title').html('Контакты и доставка');
				$('#cart-payment-step').removeClass('select');
				
				$('#order_step_is_payment').val('');
			}
			
			function showPaymentStep()
			{
                var haveErrors = false;
                $('.errors-line').empty().hide();
                if ($('input[data-required="Y"]').length > 0) {
                    $('input[data-required="Y"]').each(function(index, ob){
                        if ($(ob).val() == '') {
                            $('.errors-line').append('<p>Заполните поле "'+$(ob).attr('data-name')+'"</p>');
                            haveErrors = true;
                        }
                    });
                }
                if (haveErrors) {
                    $('.errors-line').show();
                    top.BX.scrollToNode(top.BX('checkout-progress'));
                } else {
                    $('.order-items').hide();
                    $('.errors-line').empty().hide();
                    $('.payment-items').show();
                    $('#order_form_title').html('Оплата');
                    $('#cart-payment-step').addClass('select');
                    $('#order_step_is_payment').val('y');
                    $('#order_step_show_error').val('Y');
                    submitForm();
                    top.BX.scrollToNode(top.BX('checkout-progress'));
                }
				/*
				
				$('#order_form_title').html('Оплата');
				$('#cart-payment-step').addClass('select');
				
				$('#order_step_is_payment').val('y');
				//submitForm();
                submitForm('Y');*/
			}

			function ajaxResult(res)
			{
				try
				{
					var json = JSON.parse(res);
					BX.closeWait();

					if (json.error)
					{
						return;
					}
					else if (json.redirect)
					{
						window.top.location.href = json.redirect;
					}
				}
				catch (e)
				{
					BX('order_form_content').innerHTML = res;
				}

				BX.closeWait();
				
				if ($('select.select-class').length) {
					$('select.select-class').styler();
				}
			}

			function SetContact(profileId)
			{
				BX("profile_change").value = "Y";
				submitForm();
			}
			</script>
			<?if($_POST["is_ajax_post"] != "Y")
			{
				?><form action="<?=$APPLICATION->GetCurPage();?>" method="POST" name="ORDER_FORM" id="ORDER_FORM" enctype="multipart/form-data">
				<?=bitrix_sessid_post()?>
					<div class="cart-items" >
						<div id="order_form_content">
				<?
			}
			else
			{
				$APPLICATION->RestartBuffer();
			}

			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/person_type.php");
			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/props.php");
			//include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/delivery.php");
			
			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/paysystem.php");
			

			//include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/related_props.php");

			//include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/summary.php");
			/*if(strlen($arResult["PREPAY_ADIT_FIELDS"]) > 0)
				echo $arResult["PREPAY_ADIT_FIELDS"];*/
			?>

			<?if($_POST["is_ajax_post"] != "Y")
			{
				?>
						</div>
						<a href="javascript:void(0)" onclick="showPaymentStep()" class="payment order-items" title="">ПЕРЕЙТИ К ОПЛАТЕ</a>
						<a title="" class="payment payment-items" href="javascript:void();" style="display:none;"  onclick="submitForm('Y'); return false;">Оформить заказ</a>
					</div>
					<input type="hidden" name="confirmorder" id="confirmorder" value="Y">
					<input type="hidden" name="profile_change" id="profile_change" value="N">
					<input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
					<input type="hidden" name="json" value="Y">
					<input type="hidden" name="payment" id="order_step_is_payment" value="">
					<input type="hidden" name="show_error" id="order_step_show_error" value="N">
				</form>
				<?
				if($arParams["DELIVERY_NO_AJAX"] == "N")
				{
					?>
					<div style="display:none;"><?$APPLICATION->IncludeComponent("bitrix:sale.ajax.delivery.calculator", "", array(), null, array('HIDE_ICONS' => 'Y')); ?></div>
					<?
				}
			}
			else
			{
				?>
				<script type="text/javascript">
					top.BX('confirmorder').value = 'Y';
					top.BX('profile_change').value = 'N';
				</script>
				<?
				die();
			}
		}
	}
	?>
</div>