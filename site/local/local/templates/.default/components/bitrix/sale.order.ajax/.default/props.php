<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/props_format.php");
?>
<?
if(!empty($arResult["ERROR"]) && $_REQUEST['show_error'] == 'Y'/* && $arResult["USER_VALS"]["FINAL_STEP"] == "Y"*/) {
    foreach($arResult["ERROR"] as $v){?>
    <div class="errors-line" id="errors-line">
        <p><?=$v?></p>
    </div>
    <?}?>
    <script type="text/javascript">
        top.BX.scrollToNode(top.BX('checkout-progress'));
        top.showOrderStep();
    </script>
<?
}
?>
<div class="errors-line" id="errors-line" style="display:none">
</div>
<div class="cart-item order-items" <?if (empty($arResult["ERROR"]) && isset($_REQUEST['payment']) && !empty($_REQUEST['payment'])) echo 'style="display: none"';?>  >
    <span class="num">1</span>
<?
if ($arParams["ALLOW_NEW_PROFILE"] == "Y"):
?>
	<div class="sorting bx_block r3x1">
		<span class="sort-text">Выберите профиль доставки</span>
		
		<select class="select-class" name="PROFILE_ID" id="ID_PROFILE_ID" onChange="SetContact(this.value)">
			<option value="0"><?=GetMessage("SOA_TEMPL_PROP_NEW_PROFILE")?></option>
<?
		foreach($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles)
		{
?>
			<option value="<?= $arUserProfiles["ID"] ?>"<?if ($arUserProfiles["CHECKED"]=="Y") echo " selected";?>><?=$arUserProfiles["NAME"]?></option>
<?
		}
?>					
		</select>			
	</div>
<?
endif;
?>	
	<div class="clear-all">&nbsp;</div>
</div>

<div class="cart-item order-items" <?if (empty($arResult["ERROR"]) && isset($_REQUEST['payment']) && !empty($_REQUEST['payment'])) echo 'style="display: none"';?>>
	<span class="num">2</span>
	<div id="sale_order_props" class="sorting" <?=($bHideProps && $_POST["showProps"] != "Y")?"style='display:none;'":''?>>
		<?
		PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_Y"], $arParams["TEMPLATE_LOCATION"]);
		?>
	</div>

<script type="text/javascript">
	function fGetBuyerProps(el)
	{
		var show = '<?=GetMessageJS('SOA_TEMPL_BUYER_SHOW')?>';
		var hide = '<?=GetMessageJS('SOA_TEMPL_BUYER_HIDE')?>';
		var status = BX('sale_order_props').style.display;
		var startVal = 0;
		var startHeight = 0;
		var endVal = 0;
		var endHeight = 0;
		var pFormCont = BX('sale_order_props');
		pFormCont.style.display = "block";
		pFormCont.style.overflow = "hidden";
		pFormCont.style.height = 0;
		var display = "";

		if (status == 'none')
		{
			el.text = '<?=GetMessageJS('SOA_TEMPL_BUYER_HIDE');?>';

			startVal = 0;
			startHeight = 0;
			endVal = 100;
			endHeight = pFormCont.scrollHeight;
			display = 'block';
			BX('showProps').value = "Y";
			el.innerHTML = hide;
		}
		else
		{
			el.text = '<?=GetMessageJS('SOA_TEMPL_BUYER_SHOW');?>';

			startVal = 100;
			startHeight = pFormCont.scrollHeight;
			endVal = 0;
			endHeight = 0;
			display = 'none';
			BX('showProps').value = "N";
			pFormCont.style.height = startHeight+'px';
			el.innerHTML = show;
		}

		(new BX.easing({
			duration : 700,
			start : { opacity : startVal, height : startHeight},
			finish : { opacity: endVal, height : endHeight},
			transition : BX.easing.makeEaseOut(BX.easing.transitions.quart),
			step : function(state){
				pFormCont.style.height = state.height + "px";
				pFormCont.style.opacity = state.opacity / 100;
			},
			complete : function(){
					BX('sale_order_props').style.display = display;
					BX('sale_order_props').style.height = '';
			}
		})).animate();
	}
</script>

<div style="display:none;">
<?
	$APPLICATION->IncludeComponent(
		"bitrix:sale.ajax.locations",
		$arParams["TEMPLATE_LOCATION"],
		array(
			"AJAX_CALL" => "N",
			"COUNTRY_INPUT_NAME" => "COUNTRY_tmp",
			"REGION_INPUT_NAME" => "REGION_tmp",
			"CITY_INPUT_NAME" => "tmp",
			"CITY_OUT_LOCATION" => "Y",
			"LOCATION_VALUE" => "",
			"ONCITYCHANGE" => "submitForm()",
		),
		null,
		array('HIDE_ICONS' => 'Y')
	);
?>
</div>
	<div class="clear-all"></div>
</div>