<?
$MESS["DETAIL_LINK_TITLE"] = "Перейти на <br/>страницу товара";
$MESS["FAST_LINK_TITLE"] = "Быстрый просмотр";
$MESS["ITEM_ID_TITLE"] = "ID товара";
$MESS["ITEM_BRAND_TITLE"] = "Бренд";
$MESS["QUERE_PRICE_TITLE"] = "Цена по запросу";
$MESS["RUB_TITLE"] = "руб";
$MESS["COLORS_TITLE"] = "Цвета";
$MESS["ITEMS_NOT_FOUND_TITLE"] = "Ничего не найдено";
$MESS["CT_BCS_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["T_ZOOM_TITLE"] = "Увеличить";
$MESS["T_ARTICLE_TITLE"] = "Арт.";
?>