<?
$itemIDS = array();

foreach ($arResult['SEARCH'] as $arSearch) {
	$itemIDS[$arSearch['ITEM_ID']] = $arSearch['ITEM_ID'];
}
if (!empty($itemIDS)) {
	if (!CModule::IncludeModule('iblock'))
		return false;
		
	global $fancyboxItems;
	if (!is_array($fancyboxItems))
		$fancyboxItems = array();
	
	
	$arSelect = array(
		"ID",
		"NAME",
		"CODE",
		"DATE_CREATE",
		"ACTIVE_FROM",
		"ACTIVE_TO",
		"CREATED_BY",
		"IBLOCK_ID",
		"IBLOCK_SECTION_ID",
		"DETAIL_PAGE_URL",
		"DETAIL_TEXT",
		"DETAIL_TEXT_TYPE",
		"DETAIL_PICTURE",
		"PREVIEW_TEXT",
		"PREVIEW_TEXT_TYPE",
		"PREVIEW_PICTURE",
		"TAGS",
		"CATALOG_GROUP_1",
		"PROPERTY_*",
	);
	$dbRes = CIBlockElement::GetList(array(), array('ID' => $itemIDS, 'IBLOCK_ID' => IBLOCK_ID_CATALOG), false, false, $arSelect);
		while ($obRes = $dbRes->GetNextElement()) {
		$arRes = $obRes->GetFields();
		$arRes['PROPERTIES'] = $obRes->GetProperties();
		$arResult['ITEMS'][$arRes['ID']] = $arRes;
		
		$fancyboxItems['search_'.$arRes['ID']] = $arRes;
	}
}	