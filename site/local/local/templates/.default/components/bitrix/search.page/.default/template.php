<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="bc-filter clearfix">
	<div class="pull-right">
		<?=$arResult["NAV_STRING"];?>
	</div>
</div>
<div class="bc-list">
<?
$arItemIds = array();
if (!empty($arResult['ITEMS'])):
	foreach ($arResult['ITEMS'] as $arItem)
		$arItemIds[] = $arItem['ID'];
	$i = 0;
	foreach ($arResult['ITEMS'] as $key => $arItem):
		
		$pic = array('src' =>  SITE_TEMPLATE_PATH . "/img/no_pic.gif");
		if (intval($arItem['PREVIEW_PICTURE']))
			$pic = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 260, 'height' => 260), BX_RESIZE_IMAGE_EXACT, true);
?>
	<div class="bc-item <?if ($i % 3 === 0):?>first<?endif;?>">
		<div class="bc-box">
			<div class="bc-img in-cart">
				<a class="img" href="<?=$arItem['DETAIL_PAGE_URL']?>">
					<img src="<?=$pic['src']?>" alt="<?=$arItem['NAME']?>"/>
					<span style="display:none" class="icon inbookmark inbookmark_<?=$arItem['ID']?>" title="Этот товар добавлен у вас в закладках"></span>
				</a>
				<div class="bc-increase">
					<a class="add-icon fancybox" href="#popup-photo" data-item_id="search_<?=$arItem['ID']?>">
						<span><i><?=GetMessage('T_ZOOM_TITLE')?></i></span>
					</a>
				</div>
			</div>
			<div class="bc-text">
				<div class="bc-text-top clearfix">
					<div class="pull-left">
						<ul class="rating">
<?
						for ($i = 1; $i < 6; $i++):
?>
							<li <?if (intval($arItem['PROPERTIES']['RATING']['VALUE']) >= $i):?> class="active"<?endif;?>></li>
<?
						endfor;
?>
						</ul>
					</div>
					<div class="pull-right bc-art"><?=GetMessage('T_ARTICLE_TITLE')?> <?=$arItem['PROPERTIES']['ARTICLE']['VALUE']?></div>
				</div>
				<div class="bc-caption">
					<a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a>
				</div>
				<div class="bc-text-bottom clearfix">
					<div class="bc-price pull-left">
<?
					if (!empty($arItem['PROPERTIES']['OLD_PRICE']['VALUE'])):
?>
						<div class="old"><?=number_format($arItem['PROPERTIES']['OLD_PRICE']['VALUE'], 0, '', ' ' )?> <em class="rubl">c</em><span class="line"></span></div>
						<div class="new"><strong><?=number_format($arItem['CATALOG_PRICE_1'], 0, '', ' ' )?></strong> <em class="rubl">c</em></div>
<?
					else:
?>
						<strong><?=number_format($arItem['CATALOG_PRICE_1'], 0, '', ' ' )?></strong> <em class="rubl">c</em>
<?
					endif;
?>					
					</div>
					<div class="pull-right"><a class="bc-cart fancybox cart-button-<?=$arItem['ID']?>" onclick="addToCartInList(<?=$arItem['ID']?>, 1, this)" href="#popup-cart"></a></div>
				</div>
			</div>
		</div>
	</div>
<?
		$i++;
	endforeach;
endif;	
?>	
</div>