<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if ($arResult["NavRecordCount"] == 0 || $arResult["NavPageCount"] == 1)
	return;

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>
<ol class="nums">
<?

if ($arResult["NavPageCount"] > 7):
	if ($arResult["NavPageNomer"] <= 4)://Находимся в начале пагинатора
		if ($arResult["NavPageNomer"] > 1):
?>
	<li class="left"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>">&nbsp;</a></li>
<?
		endif;
		$endPage = $arResult["NavPageNomer"] > 1 ? $arResult["NavPageNomer"] + 1 : 3;
		for ($i = 1; $i <= $endPage; $i++):
?>	
	<li <?if ($arResult["NavPageNomer"] == $i):?>class="selected"<?endif;?>><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$i?>" title="<?=$i?>"><?=$i?></a></li>
<?
		endfor;
		$middlePage = $endPage + intval(($arResult["NavPageCount"] - $endPage)/2);
?>
	<li><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$middlePage?>">&hellip;</a></li>
	<li><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>"><?=$arResult["NavPageCount"]?></a></li>
	<li class="right"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">&nbsp;</a></li>
<?
	elseif ($arResult["NavPageNomer"] <= $arResult["NavPageCount"] - 4 ): //Находимся в середине пагинатора
?>
	<li class="left"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>">&nbsp;</a></li>
	<li><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1">1</a></li>
<?
		$middleStartPage = $arResult["NavPageNomer"] - 1 - intval($arResult["NavPageNomer"]/2 - 1);
?>	
	<li><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$middleStartPage?>">&hellip;</a></li>	
<?
		for ($i = $arResult["NavPageNomer"] - 1; $i <= $arResult["NavPageNomer"] + 1; $i++):
?>	
	<li <?if ($arResult["NavPageNomer"] == $i):?>class="selected"<?endif;?>><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$i?>"><?=$i?></a></li>
<?
		endfor;
		$middleEndPage = $arResult["NavPageNomer"] + 1 + intval(($arResult["NavPageCount"] - $arResult["NavPageNomer"] + 1)/2);
?>	
	<li><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$middleEndPage?>">&hellip;</a></li>
	<li><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>"><?=$arResult["NavPageCount"]?></a></li>
	<li class="right"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">&nbsp;</a></li>
<?
	elseif ($arResult["NavPageNomer"] > $arResult["NavPageCount"] - 4 ): //Находимся в конце пагинатора
		$startPage = $arResult["NavPageNomer"] < $arResult["NavPageCount"] ? $arResult["NavPageNomer"] - 1 : $arResult["NavPageCount"] - 2;
		$middlePage = $startPage - intval(($startPage - 1)/2);
?>
		<li class="left"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>">&nbsp;</a></li>
		<li><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1">1</a></li>	
		<li><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$middlePage?>">&hellip;</a></li>
<?
		
		for ($i = $startPage; $i <= $arResult["NavPageCount"]; $i++):
?>	
	<li <?if ($arResult["NavPageNomer"] == $i):?>class="selected"<?endif;?>><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$i?>"><?=$i?></a></li>
<?
		endfor;
		
		if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
?>
	<li class="right"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>">&nbsp;</a></li>
<?
		endif;	
	endif;
else:
	if ($arResult["NavPageNomer"] > 1):
?>
	<li class="left"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>">&nbsp;</a></li>
<?
	endif;
	for ($i = 1; $i < $arResult['NavPageCount'] + 1; $i++):
?>	
	<li <?if ($arResult["NavPageNomer"] == $i):?>class="selected"<?endif;?>><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$i?>"><?=$i?></a></li>
<?
	endfor;
	if ($arResult["NavPageNomer"] < $arResult['NavPageCount']):
?>
	<li class="right"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">&nbsp;</a></li>
<?
	endif;
endif;	
?>
</ol> 