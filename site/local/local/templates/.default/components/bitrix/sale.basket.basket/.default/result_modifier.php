<?
$arItemIds = array();
foreach ($arResult['ITEMS']['AnDelCanBuy'] as $key => $arItem)
{
	$arItemIds[] = $arItem['PRODUCT_ID'];
}
\Bitrix\Main\Loader::includeModule("iblock"); 
$dbRes = CIBlockElement::GetList(array(), array('ID' => $arItemIds), false, false, array('IBLOCK_ID', 'IBLOCK_CODE', 'ID', 'PROPERTY_CML2_LINK'));
while ($arRes = $dbRes->Fetch())
{
	if (($arRes['IBLOCK_CODE'] == 'catalog_offers') && intval($arRes['PROPERTY_CML2_LINK_VALUE']))
		$arResult['PRODUCT_ITEMS'][$arRes['ID']] = $arRes['PROPERTY_CML2_LINK_VALUE'];
	else
		$arResult['PRODUCT_ITEMS'][$arRes['ID']] = $arRes['ID'];
}
?>