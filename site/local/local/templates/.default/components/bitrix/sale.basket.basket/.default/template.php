<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arUrls = Array(
	"delete" => $APPLICATION->GetCurPage()."?".$arParams["ACTION_VARIABLE"]."=delete&id=#ID#",
	"delay" => $APPLICATION->GetCurPage()."?".$arParams["ACTION_VARIABLE"]."=delay&id=#ID#",
	"add" => $APPLICATION->GetCurPage()."?".$arParams["ACTION_VARIABLE"]."=add&id=#ID#",
);

$arBasketJSParams = array(
	'SALE_DELETE' => GetMessage("SALE_DELETE"),
	'SALE_DELAY' => GetMessage("SALE_DELAY"),
	'SALE_TYPE' => GetMessage("SALE_TYPE"),
	'TEMPLATE_FOLDER' => $templateFolder,
	'DELETE_URL' => $arUrls["delete"],
	'DELAY_URL' => $arUrls["delay"],
	'ADD_URL' => $arUrls["add"]
);
?>
<script type="text/javascript">
	var basketJSParams = <?=CUtil::PhpToJSObject($arBasketJSParams);?>
</script>
<?
$APPLICATION->AddHeadScript($templateFolder."/script.js");
//dump($arResult);
include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/functions.php");
?>
<div class="checkout-progress">
	<a href="javascript:void(0)" class="select" title="">
		<span class="num">1</span>
		<span class="no-num">Корзина</span>
	</a>
	
	<a href="javascript:void(0)" title="">
		<span class="num">2</span>
		<span class="no-num">Контакты и доставка</span>
	</a>
	<a href="javascript:void(0)" title="">
		<span class="num">3</span>
		<span class="no-num">Оплата</span>
	</a>
	<span class="line line2"></span>
	<span class="line line1"></span>
</div>
<div class="cart">
	<h1>Корзина</h1>
<?
	if (count($arResult['ITEMS']['AnDelCanBuy']))
	{
?>	
	<span class="total" id="header-total"><?=count($arResult['ITEMS']['AnDelCanBuy']);?> <?=sklonenie(count($arResult['ITEMS']['AnDelCanBuy']), 'товар', 'товара', 'товаров')?> на <?=$arResult['allSum_FORMATED']?></span>
	<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="basket_form" id="basket_form">
	<div class="cart-items"  id="basket_items">
<?
	foreach ($arResult['ITEMS']['AnDelCanBuy'] as $arItem)
	{
?>	
		<div class="cart-item" id="<?=$arItem["ID"]?>">
			<a href="JavaScript:void()" class="close-item"  data-id ="<?=$arItem["ID"]?>" id="delete_item_<?=$arItem["ID"]?>" title="Закрыть"></a>
			<div class="product-image-container">
<?
				$pic = array('src' => $templateFolder."/images/no_photo.png");
				if (intval($arItem["DETAIL_PICTURE"]) > 0):
					$pic = CFile::resizeImageGet($arItem["DETAIL_PICTURE"], array('width' => 125, 'height' => 125), BX_RESIZE_IMAGE_PROPORTIONAL ,true);
				elseif (intval($arItem["PREVIEW_PICTURE"]) > 0):
					$pic = CFile::resizeImageGet($arItem["PREVIEW_PICTURE"], array('width' => 125, 'height' => 125), BX_RESIZE_IMAGE_PROPORTIONAL ,true);
				endif;
?>			
				<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?endif;?>
					<img src="<?=$pic['src']?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" />
				<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
			</div>
			<div class="product-info-container">
				<div class="c-name">
					<span class="name"><?=$arItem["NAME"]?></span>
					
<?
				if (!empty($arItem["PROPS"]) && is_array($arItem["PROPS"]))
				{				
					foreach ($arItem["PROPS"] as $val)
					{

						if (in_array($val['CODE'], $arParams['OFFERS_PROPS']))
						{
?>
							<span class="options"><?=$val["NAME"]?>: <?=$val["VALUE"]?></span>
<?
						}
					}
				}
?>									
					<a href="JavaScript:void(0);" data-offer_id="<?=$arItem['PRODUCT_ID']?>" data-item_id="<?=$arResult['PRODUCT_ITEMS'][$arItem['PRODUCT_ID']]?>" class="favorites favorite_<?=$arResult['PRODUCT_ITEMS'][$arItem['PRODUCT_ID']]?>" title=""><span class="favorites-icon"></span>В избранное</a>
				</div>
				<div class="c-qty">
					<span class="n-qty">Количество</span>
					<span class="qty-control">				
						<a href="javascript:void(0);" class="quantity_down" data-id ="<?=$arItem["ID"]?>" id="quantity_down_<?=$arItem["ID"]?>"></a>
						<input type="text" id="QUANTITY_INPUT_<?=$arItem["ID"]?>" data-id ="<?=$arItem["ID"]?>" name="QUANTITY_INPUT_<?=$arItem["ID"]?>" value="<?=$arItem["QUANTITY"]?>">
						<a href="javascript:void(0);" class="quantity_up" data-id ="<?=$arItem["ID"]?>" id="quantity_up_<?=$arItem["ID"]?>"></a>
					</span>
					<input type="hidden" id="QUANTITY_<?=$arItem['ID']?>" name="QUANTITY_<?=$arItem['ID']?>" value="<?=$arItem["QUANTITY"]?>" />
				</div>
				<div class="c-price">
					<span class="n-price">Цена за штуку</span>
					<span class="price" id="current_price_<?=$arItem["ID"]?>"><?=$arItem["PRICE_FORMATED"]?></span>
					<span class="o-price" id="old_price_<?=$arItem["ID"]?>">
						<?if (floatval($arItem["DISCOUNT_PRICE_PERCENT"]) > 0):?>
							<?=$arItem["FULL_PRICE_FORMATED"]?>
						<?endif;?>
					</span>
				</div>
				<div class="c-tprice">
					<span class="n-tprice">Итого:</span>
					<span class="price" id="sum_<?=$arItem["ID"]?>"><?=$arItem['SUM'];?></span>
				</div>
			</div>
		</div>
<?
	}
?>		
		<div class="price-container">
<?
			if ($arParams["HIDE_COUPON"] != "Y"):

				$couponClass = "";
				if (array_key_exists('COUPON_VALID', $arResult))
					$couponClass = ($arResult["COUPON_VALID"] == "Y") ? "good" : "bad";
				elseif (array_key_exists('COUPON', $arResult) && strlen($arResult["COUPON"]) > 0)
					$couponClass = "good";

?>	
			<span class="left-container">
				<input type="text" id="coupon" name="COUPON" value="<?=$arResult["COUPON"]?>" class="<?=$couponClass?>"/>
                <a href="javascript:void(0)" onclick="setBasketCoupon(this)" class="button submit" title="">Применить</a>
                <script>
                    function setBasketCoupon(ob) {
                        var coupon = $('#coupon').val();
                        if (coupon != '') {
                            $.post('<?=SITE_DIR?>ajax/basketHandler.php', {action: 'set_coupon', coupon: coupon, sessid: BX.bitrix_sessid()}, function(response){
                                if (response.success != 'Y')
                                    $('#coupon').addClass('error');
                                else {

                                }
                            }, 'json')
                        }
                    }
                </script>
			</span>
<?
			endif;
?>			
			<span class="right-container">Итого: <span class="t-price" id="total-price"><?=$arResult['allSum_FORMATED']?></span> Без учета стоимости доставки</span>
		</div>
		<input type="hidden" id="offers_props" value="<?=CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ","))?>" />
		<input type="hidden" id="action_var" value="<?=CUtil::JSEscape($arParams["ACTION_VARIABLE"])?>" />
		<input type="hidden" id="quantity_float" value="<?=$arParams["QUANTITY_FLOAT"]?>" />
		<input type="hidden" id="count_discount_4_all_quantity" value="<?=($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N"?>" />
		<input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
		<input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
		<input type="hidden" id="coupon_approved" value="N" />
		<input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />
		<input type="hidden" name="BasketOrder" value="BasketOrder" />
	</div>
	</form>
	<div class="checkout-container">
		<a href="<?=SITE_DIR?>personal/order/make/" class="checkout" title="">Оформить заказ</a>
        <a title="" class="buy click-buy-cart-link" href="javascript:void(0)">Купить в 1 клик</a>
	</div>
    <?} else {?>
    <div class="cart-items is-empty">
        <p class="cart-is-empty"><?=$arResult["ERROR_MESSAGE"];?></p>
    </div>
    <?}?>
</div>
<?/*
if (strlen($arResult["ERROR_MESSAGE"]) <= 0)
{
	?>
	<div id="warning_message">
		<?
		if (is_array($arResult["WARNING_MESSAGE"]) && !empty($arResult["WARNING_MESSAGE"]))
		{
			foreach ($arResult["WARNING_MESSAGE"] as $v)
				echo ShowError($v);
		}
		?>
	</div>
	<?

	$normalCount = count($arResult["ITEMS"]["AnDelCanBuy"]);
	$normalHidden = ($normalCount == 0) ? "style=\"display:none\"" : "";

	$delayCount = count($arResult["ITEMS"]["DelDelCanBuy"]);
	$delayHidden = ($delayCount == 0) ? "style=\"display:none\"" : "";

	$subscribeCount = count($arResult["ITEMS"]["ProdSubscribe"]);
	$subscribeHidden = ($subscribeCount == 0) ? "style=\"display:none\"" : "";

	$naCount = count($arResult["ITEMS"]["nAnCanBuy"]);
	$naHidden = ($naCount == 0) ? "style=\"display:none\"" : "";

	?>
		<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="basket_form" id="basket_form">
			<div id="basket_form_container">
				<div class="bx_ordercart">
					<div class="bx_sort_container">
						<span><?=GetMessage("SALE_ITEMS")?></span>
						<a href="javascript:void(0)" id="basket_toolbar_button" class="current" onclick="showBasketItemsList()"><?=GetMessage("SALE_BASKET_ITEMS")?><div id="normal_count" class="flat" style="display:none">&nbsp;(<?=$normalCount?>)</div></a>
						<a href="javascript:void(0)" id="basket_toolbar_button_delayed" onclick="showBasketItemsList(2)" <?=$delayHidden?>><?=GetMessage("SALE_BASKET_ITEMS_DELAYED")?><div id="delay_count" class="flat">&nbsp;(<?=$delayCount?>)</div></a>
						<a href="javascript:void(0)" id="basket_toolbar_button_subscribed" onclick="showBasketItemsList(3)" <?=$subscribeHidden?>><?=GetMessage("SALE_BASKET_ITEMS_SUBSCRIBED")?><div id="subscribe_count" class="flat">&nbsp;(<?=$subscribeCount?>)</div></a>
						<a href="javascript:void(0)" id="basket_toolbar_button_not_available" onclick="showBasketItemsList(4)" <?=$naHidden?>><?=GetMessage("SALE_BASKET_ITEMS_NOT_AVAILABLE")?><div id="not_available_count" class="flat">&nbsp;(<?=$naCount?>)</div></a>
					</div>
					<?
					include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items.php");
					include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items_delayed.php");
					include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items_subscribed.php");
					include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items_not_available.php");
					?>
				</div>
			</div>
			<input type="hidden" name="BasketOrder" value="BasketOrder" />
			<!-- <input type="hidden" name="ajax_post" id="ajax_post" value="Y"> -->
		</form>
	<?
}
else
{
	ShowError($arResult["ERROR_MESSAGE"]);
}*/
?>