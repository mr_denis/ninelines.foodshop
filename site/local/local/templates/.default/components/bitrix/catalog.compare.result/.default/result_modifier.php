<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'delete_all') {
	$_SESSION['CATALOG_COMPARE_LIST'][IBLOCK_ID_CATALOG] = array();
	LocalRedirect('/catalog/compare/');
}

$arResult['PROPERTIES'] = array();
$arResult['SECTIONS'] = array();
$arResult['OFFERS'] = array();
$arResult['FIRST_SECTION'] = array();

$section = $sectionIds = array();
$itemIds = $propertyIds = array();

if (!empty($arResult['ITEMS'])) {
	foreach ($arResult['ITEMS'] as $arItem) {
		$itemIds[] = $arItem['ID'];
		$sectionId = intval($arItem['IBLOCK_SECTION_ID']);

		if ($sectionId > 0)
			$sectionIds[$sectionId] = $sectionId;
	}

	if (!empty($sectionIds)) {
		$dbResult = CIBlockSection::GetList(array('ID' => 'ASC'), array('ID' => $sectionIds));

		while ($section = $dbResult->GetNext()) {
			$arResult['SECTIONS'][$section['ID']] = $section;
			$propLinks = CIBlockSectionPropertyLink::GetArray($section['IBLOCK_ID'], $section['ID']);

			if (is_array($propLinks))
				$propertyIds = array_merge($propertyIds, array_keys($propLinks));
		}

		foreach ($sectionIds as $sectionId) {
			if (isset($arResult['SECTIONS'][$sectionId])) {
				$arResult['FIRST_SECTION'] = $arResult['SECTIONS'][$sectionId];
			}
		}
	}

	if (!empty($propertyIds)) {
		$dbResult = CIBlockProperty::GetList(
			array('SORT' => 'ASC'),
			array('IBLOCK_ID' => IBLOCK_ID_CATALOG)
		);

		while ($item = $dbResult->Fetch()) {
			if (!in_array($item['ID'], $propertyIds))
				continue;

			$arResult['PROPERTIES'][$item['ID']] = $item;
		}
	}

	$prices = CIBlockPriceTools::GetCatalogPrices(IBLOCK_ID_CATALOG, array('BASE'));

	$arOffers = CIBlockPriceTools::GetOffersArray(
		array(
			'IBLOCK_ID' => IBLOCK_ID_CATALOG,
		),
		$itemIds,
		array(),
		array(),
		array(),
		1,
		$prices
	);

	foreach ($arOffers as $arOffer) {
		$arResult['OFFERS'][$arOffer['LINK_ELEMENT_ID']] = $arOffer;
	}

	foreach ($arResult['ITEMS'] as &$arItem) {
		if (isset($arResult['OFFERS'][$arItem['ID']]))
			$arItem['OFFER'] = $arResult['OFFERS'][$arItem['ID']];
	}

	unset($arItem);
}

