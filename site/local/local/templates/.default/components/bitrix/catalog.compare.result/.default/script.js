jQuery.fn.carouselCompare = function(previous, next, options){
    var sliderList4 = jQuery(this).children()[0];
    
    if (sliderList4) {
        var increment4 = '240',
        count = 4,
        elmnts4 = jQuery(sliderList4).children(),
        numElmts4 = elmnts4.length,
        sizeFirstElmnt4 = increment4,
        shownInViewport4 = Math.round(jQuery(this).width() / sizeFirstElmnt4),
        firstElementOnViewPort4 = 1,
        curentIndex = 0;
        isAnimating4 = false;

        if (elmnts4.length <= count) {
            jQuery(previous).hide();
            jQuery(next).hide();
        } else {
            jQuery(previous).click(function(event){
                if (!isAnimating4) {
                    if (firstElementOnViewPort4 == 1) {
                        jQuery(sliderList4).css('left', "-" + numElmts4 * sizeFirstElmnt4 + "px");
                        firstElementOnViewPort4 = numElmts4;
                    } else {
                        firstElementOnViewPort4--;
                    }

                    jQuery(sliderList4).animate({
                        left: "+=" + increment4,
                        y: 0,
                        queue: true
                    }, "swing", function(){isAnimating4 = false; curentIndex--; updateVisible();});
                    isAnimating4 = true;
                }
                return false;
            });

            jQuery(next).click(function(event){
                if (!isAnimating4) {
                    if (firstElementOnViewPort4 > numElmts4) {
                        firstElementOnViewPort4 = 2;
                        jQuery(sliderList4).css('left', "0px");
                    } else {
                        firstElementOnViewPort4++;
                    }
                    jQuery(sliderList4).animate({
                        left: "-=" + increment4,
                        y: 0,
                        queue: true
                    }, "swing", function(){isAnimating4 = false; curentIndex++; updateVisible();});
                    isAnimating4 = true;
                }
                return false;
            });

            function updateVisible()
            {
                if (curentIndex + count < numElmts4) {
                    jQuery(next).show();
                } else {
                    jQuery(next).hide();
                }

                if (curentIndex > 0) {
                    jQuery(previous).show();
                } else {
                    jQuery(previous).hide();
                }

                $('body').trigger('compareSlider', [curentIndex, curentIndex + count])
            }

            updateVisible();
        }
    }
};