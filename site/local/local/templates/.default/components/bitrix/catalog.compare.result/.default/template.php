<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

\Bitrix\Main\Loader::includeModule('currency');

?>
<? if (!empty($arResult["ITEMS"])):?>
        <h1><?=$APPLICATION->ShowTitle(false)?></h1>
		<a href="<?=$arResult['FIRST_SECTION']['SECTION_PAGE_URL']?>" class="red-button add-button" title="">Добавить товар</a>
		<a href="<?=$APPLICATION->GetCurPageParam('action=delete_all', array('action', 'ID'))?>" class="red-button grey-button del-button" title="">Удалить все</a>
		<div class="clear-all"></div>
		<div id="carouselCompare" class="carousel acsesorise compare" data-ride="carousel">
			<div class="carousel-outer catalog-inner">
				<ul class="carousel-inner">
					<?
					foreach ($arResult["ITEMS"] as $arItem):
						$pic = array();

						if (!empty($arItem['DETAIL_PICTURE']['ID'])) {
							$pic = CFile::ResizeImageGet(
								$arItem['DETAIL_PICTURE'],
								array('width' => 239, 'height' => 202),
								BX_RESIZE_IMAGE_PROPORTIONAL,
								true
							);
						}
					?>
					<li class="item active">
						<div class="item element">
							<a href="<?=$arItem['DETAIL_PAGE_URL']?>" title="<?=htmlspecialchars($arItem['NAME'])?>">
								<span class="img-inside">
									<img src="<?=$pic['src']?>" alt="<?=htmlspecialchars($arItem['NAME'])?>" title="<?=htmlspecialchars($arItem['NAME'])?>" />
								</span>
								<span class="name"><?=$arItem['NAME']?></span>
								<? if (isset($arItem['OFFER'])):?>
								<span class="price"><?=$arItem['OFFER']['MIN_PRICE']['PRINT_VALUE']?></span>
								<? else:?>
								<span class="price"><?=CurrencyFormat($arItem['PROPERTIES']['MINIMUM_PRICE']['VALUE'], 'RUB')?></span>
								<? endif;?>
							</a>
							<? if (isset($arItem['OFFER'])):?>
							<a  href="<?=SITE_DIR?>personal/cart/" data-item_id="<?=$arItem['OFFER']['ID']?>" data-in_cart="N" data-is_offer="Y" class="add2cart red-button add2cart_btn_<?=$arItem['OFFER']['ID']?>" title="">В корзину</a>
							<? endif;?>
							<a href="javascript:void(0);" class="add2fav-link favorites favorite_<?=$arItem['ID']?>" data-item_id="<?=$arItem['ID']?>" data-sessid="<?=bitrix_sessid();?>" title="">В избранное</a>
							<a href="<?=$APPLICATION->GetCurPageParam('action=DELETE_FROM_COMPARE_RESULT&ID='.$arItem['ID'], array('action', 'ID'))?>" class="close-item" title=""></a>
						</div>
					</li>
					<?
					endforeach;
					?>
				</ul>
			</div>
			<a class="left carousel-control" href="#carouselCompare" role="button" data-slide="prev"><span></span></a>
			<a class="right carousel-control" href="#carouselCompare" role="button" data-slide="next"><span></span></a>
		</div>
		<a href="#" class="red-button grey-button characters-button" id="all-property-link" title="" onclick="showAllProperty(); return false;">Все характеристики</a>
		<a href="#" class="red-button white-button compare-button" id="different-property-link" title="" onclick="showDifferentProperty(); return false;">Показать различия</a>
		<div class="clear-all"></div>
		<div class="compare-table-block" id="compare-table-block">
			<div class="table-block">
				<?
				foreach ($arResult['PROPERTIES'] as $arProp):
					if (!in_array($arProp['CODE'], $arParams['~PROPERTY_CODE']))
						continue;

					$itemsPropValue = array();
					$itemsPropDisplayValue = array();

					foreach ($arResult['ITEMS'] as $arItem) {
						$itemsPropValue[] = $arItem['DISPLAY_PROPERTIES'][$arProp['CODE']]['VALUE'];
						$itemsPropDisplayValue[] = $arItem['DISPLAY_PROPERTIES'][$arProp['CODE']]['DISPLAY_VALUE'];
					}
					$count = 0;
					$isDifferent = count(array_unique($itemsPropValue)) > 1;
				?>
					<div class="table-row <?=($isDifferent ? 'different' : 'not-different')?>">
						<div class="table-cell table-cell-header"><?=$arProp['NAME']?></div>
						<?
						foreach ($itemsPropDisplayValue as $value):
							if (empty($value))
								$value = ' - ';
						?>
							<div class="table-cell table-cell-value-<?=$count?> <?=($isDifferent ? 'selected' : '')?>" <?=($count >= 4 ? 'style="display: none;"' : '')?>><?=$value?></div>
						<?
							$count++;
						endforeach;
						?>
					</div>
				<? endforeach;?>
			</div>
		</div>
<? else: ?>
		<div>Нет товаров для сравнения</div>
<? endif;?>
	<script type="text/javascript">
		function showAllProperty()
		{
			$('#compare-table-block').find('.table-row').show();
			$('#all-property-link').removeClass().addClass('red-button grey-button characters-button');
			$('#different-property-link').removeClass().addClass('red-button white-button compare-button');
		}

		function showDifferentProperty()
		{
			$('#compare-table-block').find('.table-row.not-different').hide();
			$('#different-property-link').removeClass().addClass('red-button grey-button characters-button');
			$('#all-property-link').removeClass().addClass('red-button white-button compare-button');
		}

		$(function(){
			$('#carouselCompare .carousel-outer').carouselCompare("#carouselCompare .left", "#carouselCompare .right");

			$("#carouselCompare .carousel-outer").bind("swiperight", function () {
				$("#carouselCompare .left").click();

				return false;
			});

			$("#carouselCompare .carousel-outer").bind("swipeleft", function () {
				$("#carouselCompare .right").click();

				return false;
			});

			$('body').on('compareSlider', function(e, startIndex, endIndex){
				for (var i = 0; i <= startIndex; i++) {
					$('.table-cell-value-'+i).hide();
				}
				for (var i = startIndex; i <= endIndex; i++) {
					$('.table-cell-value-'+i).show();
				}
				for (var i = endIndex; i <= <?=count($arResult['ITEMS'])?>; i++) {
					$('.table-cell-value-'+i).hide();
				}
			});
		});
        $('.add_desc_section').hide();
	</script>