<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<section class="form-block ask-question">
    <h2>Задать вопрос</h2>
    <?if(!empty($arResult["ERROR_MESSAGE"]))
    {
        foreach($arResult["ERROR_MESSAGE"] as $v)
            ShowError($v);
    }
    if(strlen($arResult["OK_MESSAGE"]) > 0)
    {
        ?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
    }
    ?>
    <form action="<?=POST_FORM_ACTION_URI?>" method="POST">
        <?=bitrix_sessid_post()?>
        <div class="left-part">
            <input type="text" placeholder="Имя" class="input" name="user_name" value="<?=$arResult["AUTHOR_NAME"]?>">
            <input type="text" placeholder="E-mail" class="input"  name="user_email" value="<?=$arResult["AUTHOR_EMAIL"]?>">
            <?if($arParams["USE_CAPTCHA"] == "Y"):?>
                <input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
                <div class="code-img">
                    <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
                </div>
                <input type="text" placeholder="Код с картнки" class="input code"  name="captcha_word">
            <?endif;?>
        </div>
        <div class="right-part">
            <textarea name="MESSAGE" class="textarea"><?=$arResult["MESSAGE"]?></textarea>
            <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
            <input type="submit" name="submit" class="blue-button red-button" value="<?=GetMessage("MFT_SUBMIT")?>">
        </div>
    </form>
</section>