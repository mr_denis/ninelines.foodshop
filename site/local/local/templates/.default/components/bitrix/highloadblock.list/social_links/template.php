<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult['ERROR']))
	return false;
		
if (!empty($arResult['rows'])):
?>
<div class="soc">
	<?=GetMessage('TITLE')?>
	<ul>
<?
	foreach ($arResult['rows'] as $arItem):
?>	
		<li class="<?=$arItem['UF_CLASS']?>"><a href="<?=$arItem['UF_LINK']?>" title="<?=$arItem['UF_NAME']?>"></a></li>
<?
	endforeach;
?>		
	</ul>
</div>
<?
endif;
?>