<?
if (!empty($arResult['ITEMS'])):
?>
<div class="pc-item clearfix">
	<h3>С этим товаром также покупают</h3>
	<div class="flexslider carousel" id="flexslider-carousel">
		<ul class="slides">
			<li>
				<div class="fc-img">
					<a href="#"><img width="160" height="160" src="<?=SITE_TEMPLATE_PATH?>/img/content/img6.jpg" alt=""/></a>
				</div>
				<div class="fc-caption">
					<a href="#">Lightstar 759 серия подвесных светильников</a>
				</div>
				<div class="fc-bottom clearfix">
					<div class="pull-left bc-price">
						<strong>17 200</strong> <em class="rubl">c</em>
					</div>
					<div class="pull-right">
						<a class="bc-cart" href="#"></a>
					</div>
				</div>
			</li>
			<li>
				<div class="fc-img">
					<a href="#"><img width="160" height="160" src="<?=SITE_TEMPLATE_PATH?>/img/content/img6.jpg" alt=""/></a>
				</div>
				<div class="fc-caption">
					<a href="#">Lightstar 759 серия подвесных светильников</a>
				</div>
				<div class="fc-bottom clearfix">
					<div class="pull-left bc-price">
						<strong>11 000</strong> <em class="rubl">c</em>
					</div>
					<div class="pull-right">
						<a class="bc-cart" href="#"></a>
					</div>
				</div>
			</li>
			<li>
				<div class="fc-img">
					<a href="#"><img width="160" height="160" src="<?=SITE_TEMPLATE_PATH?>/img/content/img7.jpg" alt=""/></a>
				</div>
				<div class="fc-caption">
					<a href="#">Lightstar 759 серия подвесных светильников</a>
				</div>
				<div class="fc-bottom clearfix">
					<div class="pull-left bc-price">
						<div class="old">24 150 <em class="rubl">c</em></div>
						<div class="new"><strong>24 150</strong> <em class="rubl">c</em></div>
					</div>
					<div class="pull-right">
						<a class="bc-cart" href="#"></a>
					</div>
				</div>
			</li>
			<li>
				<div class="fc-img">
					<a href="#"><img width="160" height="160" src="<?=SITE_TEMPLATE_PATH?>/img/content/img6.jpg" alt=""/></a>
				</div>
				<div class="fc-caption">
					<a href="#">Lightstar 759 серия подвесных светильников</a>
				</div>
				<div class="fc-bottom clearfix">
					<div class="pull-left bc-price">
						<strong>17 200</strong> <em class="rubl">c</em>
					</div>
					<div class="pull-right">
						<a class="bc-cart" href="#"></a>
					</div>
				</div>
			</li>
			<li>
				<div class="fc-img">
					<a href="#"><img width="160" height="160" src="<?=SITE_TEMPLATE_PATH?>/img/content/img6.jpg" alt=""/></a>
				</div>
				<div class="fc-caption">
					<a href="#">Lightstar 759 серия подвесных светильников</a>
				</div>
				<div class="fc-bottom clearfix">
					<div class="pull-left bc-price">
						<strong>11 000</strong> <em class="rubl">c</em>
					</div>
					<div class="pull-right">
						<a class="bc-cart" href="#"></a>
					</div>
				</div>
			</li>
			<li>
				<div class="fc-img">
					<a href="#"><img width="160" height="160" src="<?=SITE_TEMPLATE_PATH?>/img/content/img7.jpg" alt=""/></a>
				</div>
				<div class="fc-caption">
					<a href="#">Lightstar 759 серия подвесных светильников</a>
				</div>
				<div class="fc-bottom clearfix">
					<div class="pull-left bc-price">
						<div class="old">24 150 <em class="rubl">c</em></div>
						<div class="new"><strong>24 150</strong> <em class="rubl">c</em></div>
					</div>
					<div class="pull-right">
						<a class="bc-cart" href="#"></a>
					</div>
				</div>
			</li>
		</ul>
	</div>
</div>
<?
endif;
?>