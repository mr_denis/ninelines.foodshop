<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
//dump($arResult);
$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME']
);

$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
	'ID' => $strMainID,
	'PICT' => $strMainID.'_pict',
	'DISCOUNT_PICT_ID' => $strMainID.'_dsc_pict',
	'STICKER_ID' => $strMainID.'_sticker',
	'BIG_SLIDER_ID' => $strMainID.'_big_slider',
	'BIG_IMG_CONT_ID' => $strMainID.'_bigimg_cont',
	'SLIDER_CONT_ID' => $strMainID.'_slider_cont',
	'SLIDER_LIST' => $strMainID.'_slider_list',
	'SLIDER_LEFT' => $strMainID.'_slider_left',
	'SLIDER_RIGHT' => $strMainID.'_slider_right',
	'OLD_PRICE' => $strMainID.'_old_price',
	'PRICE' => $strMainID.'_price',
	'DISCOUNT_PRICE' => $strMainID.'_price_discount',
	'SLIDER_CONT_OF_ID' => $strMainID.'_slider_cont_',
	'SLIDER_LIST_OF_ID' => $strMainID.'_slider_list_',
	'SLIDER_LEFT_OF_ID' => $strMainID.'_slider_left_',
	'SLIDER_RIGHT_OF_ID' => $strMainID.'_slider_right_',
	'QUANTITY' => $strMainID.'_quantity',
	'QUANTITY_DOWN' => $strMainID.'_quant_down',
	'QUANTITY_UP' => $strMainID.'_quant_up',
	'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
	'QUANTITY_LIMIT' => $strMainID.'_quant_limit',
	'BUY_LINK' => $strMainID.'_buy_link',
	'ADD_BASKET_LINK' => $strMainID.'_add_basket_link',
	'COMPARE_LINK' => $strMainID.'_compare_link',
	'PROP' => $strMainID.'_prop_',
	'PROP_DIV' => $strMainID.'_skudiv',
	'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
	'OFFER_GROUP' => $strMainID.'_set_group_',
	'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
);
$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
$templateData['JS_OBJ'] = $strObName;

$strTitle = (
	isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]) && '' != $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]
	? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]
	: $arResult['NAME']
);
$strAlt = (
	isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]) && '' != $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]
	? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]
	: $arResult['NAME']
);

reset($arResult['OFFERS']);
$firstOffer = current($arResult['OFFERS']);
?>
<div class="product-common" id="<? echo $arItemIDs['ID']; ?>">
	<div class="photo-part">
<?
	reset($arResult['MORE_PHOTO']);
	$arFirstPhoto = current($arResult['MORE_PHOTO']);
?>
		<div class="big-img">
			<a href="JavaScript:void(0);" title="">
				<img
					class="main_picture"
					id="<? echo $arItemIDs['PICT']; ?>"
					src="<? echo $arFirstPhoto['SRC']; ?>"
					alt="<? echo $strAlt; ?>"
					title="<? echo $strTitle; ?>"
				>
				<span class="lupa" id="big-photo" data-id="<?=$arResult['ID']?>">Увеличить</span>
            <?if (!empty($arResult['PROPERTIES']['PRODUCT_OF_DAY']['VALUE'])):?>
                <span class="dop-thing day">Товар <span>дня</span></span>
            <?elseif (intval($arResult['PROPERTIES']['PRODUCT_ACTION']['VALUE'])):?>
                <span class="dop-thing hurry-up">Успей <span>купить</span></span>
            <?endif;?>
			</a>
        <?if (!empty($arResult['PROPERTIES']['PRODUCT_OF_DAY']['VALUE'])):
            if (intval($arResult['PROPERTIES']['OLD_PRICE']['VALUE']) &&
                intval($arResult['PROPERTIES']['MINIMUM_PRICE']['VALUE']) &&
                ($arResult['PROPERTIES']['OLD_PRICE']['VALUE'] > 0) &&
                ($arResult['PROPERTIES']['MINIMUM_PRICE']['VALUE'] < $arResult['PROPERTIES']['OLD_PRICE']['VALUE']))
                $discount = $arResult['PROPERTIES']['OLD_PRICE']['VALUE'] - $arResult['PROPERTIES']['MINIMUM_PRICE']['VALUE'];

                if ($discount && intval($arResult['PROPERTIES']['ALREADY_PAYED']['VALUE'])):
        ?>
            <div class="economy">
                <span class="price"><span><?=number_format($discount, 0, ',', ' ');?> руб.</span>экономия</span>
                <div class="percent">
                    <span class="back"><span style="width:<?=intval($arResult['PROPERTIES']['ALREADY_PAYED']['VALUE'])?>%;"></span></span>
                    уже куплено <span><?=intval($arResult['PROPERTIES']['ALREADY_PAYED']['VALUE'])?></span>% товаров
                </div>
            </div>
        <?      endif;
            elseif (intval($arResult['PROPERTIES']['PRODUCT_ACTION']['VALUE'])):?>
            <div class="product-tile-sale">
                <div class="count-down-timer" data-init="countDown">
                    <span class="count-down-timer-content"></span>
                    <div class="count-down-timer-amount">
                        <strong class="count-down-timer-qty"><?=$arResult['PROPERTIES']['PRODUCT_ACTION']['VALUE']?></strong>
                        <small class="count-down-timer-abbr">шт</small>
                    </div>
                </div>
            </div>
        <?endif;?>
		</div>
		<?
		if (isset($arResult['MORE_PHOTO']) && !empty($arResult['MORE_PHOTO'])) {
		$display_content = '';
		$showCarousel = true;
		if ($arResult['MORE_PHOTO_COUNT'] <= 1)
		{
			$display_content = 'style="display: none"';
			$showCarousel = false;
		}
		?>		
		<div id="carousel_<?=$arResult['ID']?>" class="carousel product carousel-box" <?=$display_content?>>
			<div class="carousel-outer">
				<ul class="carousel-inner" id="<? echo $arItemIDs['SLIDER_LIST']; ?>">
				<?
				foreach ($arResult['MORE_PHOTO'] as &$arOnePhoto)
				{
					$pic = CFile::ResizeImageGet($arOnePhoto['ID'], array('width' => 61, 'height' => 61), BX_RESIZE_IMAGE_EXACT, true);
					$arOnePhoto['ICON_SRC'] = $pic['src'];
					$arOnePhoto['WIDTH'] = $pic['width'];
					$arOnePhoto['HEIGHT'] = $pic['height'];
				?>
					<li class="item" data-value="<? echo $arOnePhoto['ID']; ?>">
						<a href="<? echo $arOnePhoto['SRC']; ?>" title="">
							<img src="<? echo $arOnePhoto['ICON_SRC']; ?>" alt="<? echo $strAlt; ?>" title="<? echo $strTitle; ?>" />
						</a>
					</li>
				<?
				}
				unset($arOnePhoto);
				?>						
				</ul>
			</div>
			<a class="left carousel-control" href="carousel_<?=$arResult['ID']?>" role="button" data-slide="prev"><span></span></a>
			<a class="right carousel-control" href="carousel_<?=$arResult['ID']?>" role="button" data-slide="next"><span></span></a>
		</div>
        <div class="window-inside2 window-inside-bigphoto-<?=$arResult['ID']?>" style="display: none;">
            <div class="windows-form windows-photo" style="display: none; opacity: 0; top: 50px;">
                <a href="#" title="Закрыть" class="close"></a>
                <div class="slide-div">
                    <div class="photo-part">
                        <div class="big-img">
                            <a title="" href="JavaScript:void(0)">
                                <?
                                $firstPhoto = $arResult['MORE_PHOTO'][0];
                                ?>
                                <img class="main-picture-<?=$arResult['ID']?>" title="<?=$arResult['NAME']?>" alt="<?=$arResult['NAME']?>" src="<? echo $firstPhoto['SRC']; ?>">
                            </a>
                        </div>
						<?if ($showCarousel) {?>
                        <div data-ride="carousel" class="carousel product carousel-bigphoto" data-id="<?=$arResult['ID']?>" id="carousel_big_<?=$arResult['ID']?>" >
                            <div class="carousel-outer">
                                <ul class="carousel-inner">
                                    <?
                                    foreach ($arResult['MORE_PHOTO'] as $arOnePhoto)
                                    {
                                        $pic = CFile::ResizeImageGet($arOnePhoto['ID'], array('width' => 61, 'height' => 61), BX_RESIZE_IMAGE_EXACT, true);
                                        $arOnePhoto['ICON_SRC'] = $pic['src'];
                                        $arOnePhoto['WIDTH'] = $pic['width'];
                                        $arOnePhoto['HEIGHT'] = $pic['height'];
                                        ?>
                                        <li class="item" data-value="<? echo $arResult['ID'].'_'.$arOnePhoto['ID']; ?>">
                                            <a href="<? echo $arOnePhoto['SRC']; ?>" title="">
                                                <img src="<? echo $arOnePhoto['ICON_SRC']; ?>" alt="<? echo $arResult['NAME']; ?>" title="<? echo $arResult['NAME']; ?>" />
                                            </a>
                                        </li>
                                    <?
                                    }
                                    unset($arOnePhoto);
                                    ?>
                                </ul>
                            </div>
                            <a data-slide="prev" role="button" href="#carousel_big_<?=$arResult['ID']?>" class="left carousel-control carousel-left-<?=$arResult['ID']?>"><span></span></a>
                            <a data-slide="next" role="button" href="#carousel_big_<?=$arResult['ID']?>" class="right carousel-control carousel-right-<?=$arResult['ID']?>"><span></span></a>
                        </div>
						<?}?>
                    </div>
                </div>
            </div>
        </div>
	<?
	}
	
	if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
	{
		foreach ($arResult['OFFERS'] as $key => $arOneOffer)
		{
			if (!isset($arOneOffer['MORE_PHOTO_COUNT']) || 0 >= $arOneOffer['MORE_PHOTO_COUNT'])
				continue;
?>
		<div id="carousel_<?=$arOneOffer['ID']?>" class="carousel product carousel-box" <?if ($firstOffer['ID'] != $arOneOffer['ID']):?>style="display:none;"<?endif;?>>
			<div class="carousel-outer">
				<ul class="carousel-inner" id="<? echo $arItemIDs['SLIDER_CONT_OF_ID'].$arOneOffer['ID']; ?>">
<?
				foreach ($arOneOffer['MORE_PHOTO'] as $arOnePhoto)
				{
					$pic = CFile::ResizeImageGet($arOnePhoto['ID'], array('width' => 61, 'height' => 61), BX_RESIZE_IMAGE_EXACT, true);
					$arOnePhoto['ICON_SRC'] = $pic['src'];
					$arOnePhoto['WIDTH'] = $pic['width'];
					$arOnePhoto['HEIGHT'] = $pic['height'];
?>
					<li class="item" data-value="<? echo $arOneOffer['ID'].'_'.$arOnePhoto['ID']; ?>">
						<a href="<? echo $arOnePhoto['SRC']; ?>" title="">
							<img src="<? echo $arOnePhoto['ICON_SRC']; ?>" alt="<? echo $strAlt; ?>" title="<? echo $strTitle; ?>" />
						</a>
					</li>
<?
				}
				unset($arOnePhoto);
?>						
				</ul>
			</div>
			<a class="left carousel-control" href="carousel_<?=$arOneOffer['ID']?>" role="button" data-slide="prev"><span></span></a>
			<a class="right carousel-control" href="carousel_<?=$arOneOffer['ID']?>" role="button" data-slide="next"><span></span></a>
		</div>
        <div class="window-inside2 window-inside-bigphoto-<?=$arOneOffer['ID']?>" style="display: none;">
            <div class="windows-form windows-photo" style="display: none; opacity: 0; top: 50px;">
                <a href="#" title="Закрыть" class="close"></a>
                <div class="slide-div">
                    <div class="photo-part">
                        <div class="big-img">
                            <a title="" href="JavaScript:void(0)">
                            <?
                            $firstPhoto = current($arOneOffer['MORE_PHOTO']);
                            ?>
                                <img class="main-picture-<?=$arOneOffer['ID']?>" title="<?=$arOneOffer['NAME']?>" alt="<?=$arOneOffer['NAME']?>" src="<? echo $firstPhoto['SRC']; ?>">
                            </a>
                        </div>
                        <div data-ride="carousel" class="carousel product carousel-bigphoto" data-id="<?=$arOneOffer['ID']?>" id="carousel_big_<?=$arOneOffer['ID']?>" >
                            <div class="carousel-outer">
                                <ul class="carousel-inner">
                                    <?
                                    foreach ($arOneOffer['MORE_PHOTO'] as $arOnePhoto)
                                    {
                                        $pic = CFile::ResizeImageGet($arOnePhoto['ID'], array('width' => 61, 'height' => 61), BX_RESIZE_IMAGE_EXACT, true);
                                        $arOnePhoto['ICON_SRC'] = $pic['src'];
                                        $arOnePhoto['WIDTH'] = $pic['width'];
                                        $arOnePhoto['HEIGHT'] = $pic['height'];
                                        ?>
                                        <li class="item" data-value="<? echo $arOneOffer['ID'].'_'.$arOnePhoto['ID']; ?>">
                                            <a href="<? echo $arOnePhoto['SRC']; ?>" title="">
                                                <img src="<? echo $arOnePhoto['ICON_SRC']; ?>" alt="<? echo $strAlt; ?>" title="<? echo $strTitle; ?>" />
                                            </a>
                                        </li>
                                    <?
                                    }
                                    unset($arOnePhoto);
                                    ?>
                                </ul>
                            </div>
                            <a data-slide="prev" role="button" href="#carousel_big_<?=$arOneOffer['ID']?>" class="left carousel-control carousel-left-<?=$arOneOffer['ID']?>"><span></span></a>
                            <a data-slide="next" role="button" href="#carousel_big_<?=$arOneOffer['ID']?>" class="right carousel-control carousel-right-<?=$arOneOffer['ID']?>"><span></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?
			if ($firstOffer['ID'] == $arOneOffer['ID'])	
				echo "<script>$('#carousel_".$arResult['ID']."').hide();</script>";
		}
	}
?>		
	</div>
	<div class="info-part">
		<h1><?=$arResult['NAME']?></h1>
		<p class="art">Артикул: <?=$firstOffer['PROPERTIES']['ARTNUMBER']['VALUE']?></p>
		<!-- <span class="logo"><img src="images/apple.png" alt="apple" title="apple" /></span> -->
		<div class="rating">
			<div class="rating-value" style="width: <?=($arResult['RATING']/5 * 100)?>%;"></div>
            <?for ($i = 5; $i >0; $i--):?>
                <a href="#" class="rate<?=$i?>" title="<?=$i?>"></a>
            <?endfor;?>
        </div>
<?		
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']) && !empty($arResult['OFFERS_PROP']))
{
	$arSkuProps = array();
?>
		<dl class="params" id="<? echo $arItemIDs['PROP_DIV']; ?>">
<?
	foreach ($arResult['SKU_PROPS'] as &$arProp)
	{
		if (!isset($arResult['OFFERS_PROP'][$arProp['CODE']]))
			continue;
		$arSkuProps[] = array(
			'ID' => $arProp['ID'],
			'SHOW_MODE' => $arProp['SHOW_MODE'],
			'VALUES_COUNT' => $arProp['VALUES_COUNT']
		);
?>
			<dt><? echo htmlspecialcharsex($arProp['NAME']); ?>:</dt>
			<dd id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_cont">
				<ul class="<?if ($arProp['CODE'] == 'COLOR_REF'):?>colors<?else:?>size<?endif;?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_list">
<?
			foreach ($arProp['VALUES'] as $arOneValue)
			{
?>		
					<li style="display: none;"
						data-treevalue="<? echo $arProp['ID'].'_'.$arOneValue['ID']; ?>" 
						data-onevalue="<? echo $arOneValue['ID']; ?>"
					><a href="JavaScript:void(0);" onclick="$(this).parent().click();" style="<?if ($arProp['CODE'] == 'COLOR_REF'):?>background-color: #<?=$arOneValue['XML_ID']?>;<?endif;?>" title="<?=$arOneValue['NAME']?>"><?if ($arProp['CODE'] != 'COLOR_REF') echo $arOneValue['NAME'];?></a></li>
			
<?				
				$firstScu = false;
			}
?>			
				</ul>
			</dd>
<?
	}
	unset($arProp);
?>
		</dl>
<?
}
?>		
		
		<div class="cart-thing">
			<div class="col1">
				<span class="new" id="<? echo $arItemIDs['PRICE']; ?>"><? echo $arResult['MIN_PRICE']['PRINT_DISCOUNT_VALUE']; ?></span>
<?	
			if (!empty($arResult['PROPERTIES']['DISCOUNT']['VALUE']) && intval($arResult['PROPERTIES']['OLD_PRICE']['VALUE']) && ($arResult['PROPERTIES']['OLD_PRICE']['VALUE'] > $arResult['MIN_PRICE']['VALUE']))
			{
?>				
				<span class="old" id="<? echo $arItemIDs['OLD_PRICE']; ?>"><?=FormatCurrency(intval($arResult['PROPERTIES']['OLD_PRICE']['VALUE']), $arResult['MIN_PRICE']['CURRENCY']);?></span>
				<span class="eco" id="<? echo $arItemIDs['DISCOUNT_PRICE']; ?>">Экономия: <span ><?=FormatCurrency(intval($arResult['PROPERTIES']['OLD_PRICE']['VALUE'] - $arResult['MIN_PRICE']['VALUE']), $arResult['MIN_PRICE']['CURRENCY']);?></span></span>
<?
			}
?>				
			</div>
			<div class="col2">
				<a href="<?=SITE_DIR?>personal/cart/" id="add2cart-button" class="red-button add2cart add2cart_btn_<?=$firstOffer['ID']?>" data-in_cart="N" data-item_id="<?=isset($firstOffer['ID'])?$firstOffer['ID']:$arResult['ID'];?>" data-is_offer="N" data-type="card" title="В корзину">В корзину</a>
				<span class="eco">Наличие: <span>Много</span></span>
			</div>
			<div class="col3">
				<a href="javascript:void(0)" class="red-button click-buy-link grey-button" data-id="<?=$arResult['ID']?>" data-offer_id="" data-price="<? echo $arResult['MIN_PRICE']['PRINT_DISCOUNT_VALUE']; ?>" title="">Купить в 1 клик</a>
				<a href="#" class="buy-credit" title="">Купить в кредит</a>
			</div>
		</div>
		<a href="javascript:void(0);" id="add2fav-link" class="add2fav-link favorites favorite_<?=$arResult['ID']?>" data-offer_id="" data-item_id="<?=$arResult['ID']?>" data-sessid="<?=bitrix_sessid();?>" title="">В избранное</a>
		<a href="javascript:void(0);" class="compare-link" data-item_id="<?=$arResult['ID']?>" data-message-add-compare="Сравнить"  data-message-delete-compare="Перейти в сравнение" title="">Сравнить</a>
		<div class="soc">
            <script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script>
            <script>
                var YaShareInstance = new Ya.share({
                    element: 'yashare-init',
                    l10n: 'ru',
                    description: <?=json_encode(strip_tags($arResult['~PREVIEW_TEXT']))?>,
	                image: <?=(!empty($arResult['DETAIL_PICTURE']) ? json_encode('http://'.$_SERVER['HTTP_HOST'].$arResult['DETAIL_PICTURE']['SRC']) : '""')?>,
                    elementStyle: {
                        type: 'none',
                        quickServices: ['facebook','vkontakte','odnoklassniki','twitter','gplus']
                    }
                });
            </script>
            <div id="yashare-init" class="yashare-init"></div>
		</div>
	</div>
	<a href="<?=$arResult['SECTION']['SECTION_PAGE_URL']?>" class="back2cat-link" title="<?=$arResult['SECTION']['NAME']?>"></a>
	<div class="clear-all"></div>
</div>
<div class="clear-all"></div>
<?
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
{
	foreach ($arResult['JS_OFFERS'] as &$arOneJS)
	{
		if ($arOneJS['PRICE']['DISCOUNT_VALUE'] != $arOneJS['PRICE']['VALUE'])
		{
			$arOneJS['PRICE']['PRINT_DISCOUNT_DIFF'] = GetMessage('ECONOMY_INFO', array('#ECONOMY#' => $arOneJS['PRICE']['PRINT_DISCOUNT_DIFF']));
			$arOneJS['PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arOneJS['PRICE']['DISCOUNT_DIFF_PERCENT'];
		}
		
		if (floatval($arResult['PROPERTIES']['OLD_PRICE']['VALUE']) && $arOneJS['PRICE']['VALUE'] < intval($arResult['PROPERTIES']['OLD_PRICE']['VALUE']))
		{
			$arOneJS['PRICE']['PRINT_DISCOUNT_DIFF_EX'] = FormatCurrency(intval($arResult['PROPERTIES']['OLD_PRICE']['VALUE']) - $arOneJS['PRICE']['VALUE'], $arResult['MIN_PRICE']['CURRENCY']);
			$arOneJS['PRICE']['OLD_PRICE'] = !empty($arResult['PROPERTIES']['OLD_PRICE']['VALUE']) ? FormatCurrency(intval($arResult['PROPERTIES']['OLD_PRICE']['VALUE']), $arResult['MIN_PRICE']['CURRENCY']) : '';
		}
		$strProps = '';
		if ($arResult['SHOW_OFFERS_PROPS'])
		{
			if (!empty($arOneJS['DISPLAY_PROPERTIES']))
			{
				foreach ($arOneJS['DISPLAY_PROPERTIES'] as $arOneProp)
				{
					$strProps .= '<dt>'.$arOneProp['NAME'].'</dt><dd>'.(
						is_array($arOneProp['VALUE'])
						? implode(' / ', $arOneProp['VALUE'])
						: $arOneProp['VALUE']
					).'</dd>';
				}
			}
		}
		$arOneJS['DISPLAY_PROPERTIES'] = $strProps;
	}
	if (isset($arOneJS))
		unset($arOneJS);
		
	$arJSParams = array(
		'CONFIG' => array(
			'USE_CATALOG' => $arResult['CATALOG'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_PRICE' => true,
			'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
			'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
			'DISPLAY_COMPARE' => ('Y' == $arParams['DISPLAY_COMPARE']),
			'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
			'OFFER_GROUP' => $arResult['OFFER_GROUP'],
			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE']
		),
		'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
		'VISUAL' => array(
			'ID' => $arItemIDs['ID'],
		),
		'DEFAULT_PICTURE' => array(
			'PREVIEW_PICTURE' => $arResult['DEFAULT_PICTURE'],
			'DETAIL_PICTURE' => $arResult['DEFAULT_PICTURE']
		),
		'PRODUCT' => array(
			'ID' => $arResult['ID'],
			'NAME' => $arResult['~NAME'],
			'OLD_PRICE' => FormatCurrency(floatval($arResult['PROPERTIES']['OLD_PRICE']['VALUE']), $arResult['MIN_PRICE']['CURRENCY']),
		),
		'BASKET' => array(
			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'BASKET_URL' => $arParams['BASKET_URL'],
			'SKU_PROPS' => $arResult['OFFERS_PROP_CODES']
		),
		'OFFERS' => $arResult['JS_OFFERS'],
		'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
		'TREE_PROPS' => $arSkuProps
	);
}
else
{
	$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
	$arJSParams = array(
		'CONFIG' => array(
			'USE_CATALOG' => $arResult['CATALOG'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_PRICE' => (isset($arResult['MIN_PRICE']) && !empty($arResult['MIN_PRICE']) && is_array($arResult['MIN_PRICE'])),
			'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
			'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
			'DISPLAY_COMPARE' => ('Y' == $arParams['DISPLAY_COMPARE']),
			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE']
		),
		'VISUAL' => array(
			'ID' => $arItemIDs['ID'],
		),
		'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
		'PRODUCT' => array(
			'ID' => $arResult['ID'],
			'PICT' => $arFirstPhoto,
			'NAME' => $arResult['~NAME'],
			'SUBSCRIPTION' => true,
			'PRICE' => $arResult['MIN_PRICE'],
			'OLD_PRICE' => !empty($arResult['PROPERTIES']['OLD_PRICE']['VALUE']) ? FormatCurrency(floatval($arResult['PROPERTIES']['OLD_PRICE']['VALUE']), $arResult['MIN_PRICE']['CURRENCY']) : '',
			'PRINT_DISCOUNT_DIFF_EX' => !empty($arResult['PROPERTIES']['OLD_PRICE']['VALUE']) ? FormatCurrency(floatval($arResult['PROPERTIES']['OLD_PRICE']['VALUE']) - $arResult['MIN_PRICE']['VALUE'], $arResult['MIN_PRICE']['CURRENCY']) : '',
			'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
			'SLIDER' => $arResult['MORE_PHOTO'],
			'CAN_BUY' => $arResult['CAN_BUY'],
			'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
			'QUANTITY_FLOAT' => is_double($arResult['CATALOG_MEASURE_RATIO']),
			'MAX_QUANTITY' => $arResult['CATALOG_QUANTITY'],
			'STEP_QUANTITY' => $arResult['CATALOG_MEASURE_RATIO'],
			'BUY_URL' => $arResult['~BUY_URL'],
		),
		'BASKET' => array(
			'ADD_PROPS' => ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET']),
			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
			'EMPTY_PROPS' => $emptyProductProperties,
			'BASKET_URL' => $arParams['BASKET_URL']
		)
	);
	unset($emptyProductProperties);
}
?>
<script type="text/javascript">
var <? echo $strObName; ?> = new JCCatalogElement(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
BX.message({
	MESS_BTN_BUY: '<? echo ('' != $arParams['MESS_BTN_BUY'] ? CUtil::JSEscape($arParams['MESS_BTN_BUY']) : GetMessageJS('CT_BCE_CATALOG_BUY')); ?>',
	MESS_BTN_ADD_TO_BASKET: '<? echo ('' != $arParams['MESS_BTN_ADD_TO_BASKET'] ? CUtil::JSEscape($arParams['MESS_BTN_ADD_TO_BASKET']) : GetMessageJS('CT_BCE_CATALOG_ADD')); ?>',
	MESS_NOT_AVAILABLE: '<? echo ('' != $arParams['MESS_NOT_AVAILABLE'] ? CUtil::JSEscape($arParams['MESS_NOT_AVAILABLE']) : GetMessageJS('CT_BCE_CATALOG_NOT_AVAILABLE')); ?>',
	TITLE_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR') ?>',
	TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS') ?>',
	BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
	BTN_SEND_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS'); ?>',
	BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE') ?>',
	SITE_ID: '<? echo SITE_ID; ?>'
});
</script>
<?$this->SetViewTarget("product_info");?>
<a name="product_tabs"></a>
<div class="params">
	<div class="tabs" id="product_tabs">
		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
			<li class="active"><a href="#obzor" role="tab" data-toggle="tab">Обзор</a></li>
			<li><a href="#descr" role="tab" data-toggle="tab">Описание</a></li>
			<li><a href="#teh" role="tab" data-toggle="tab">Технические характеристики</a></li>
			<li><a href="#review" role="tab" data-toggle="tab">Отзывы</a></li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">
			<div class="tab-pane active" id="obzor">
<?
			$preview = trim($arResult['PREVIEW_TEXT']);
			if (!strlen($preview))
				$preview = substr(strip_tags($arResult['DETAIL_TEXT']) , 0, 450);
			if (!strlen($preview))
				$preview = 'Нет описания';
				
?>			
				<div class="inner">
					<div class="left-part">
						<h3>Описание</h3>
						<div class="closed-info">
							<h4><?=$arResult['NAME']?></h4>
							<?=$preview?>
						</div>
						<a href="#product_tabs" class="watch-more watch-more-info" title="">Посмотреть все</a>
						<div class="closed-info closed-docs">
							<h4>Документы</h4>
<?
							if (!empty($arResult['PROPERTIES']['DOCS']['VALUE']))
							{
								$index = 0;
?>							
							<ul class="documents">
<?
								foreach ($arResult['PROPERTIES']['DOCS']['VALUE'] as $key => $docID)
								{
									$index++;
									if ($index > 2) break;
									$arFile = CFile::GetFileArray($docID);
									
?>							
								<li class="pdf"><a target="_blank" href="<?=$arFile['SRC']?>" title=""><?=$arResult['PROPERTIES']['DOCS']['DESCRIPTION'][$key]?> <span>(<?=CFile::FormatSize($arFile['FILE_SIZE'])?>)</span></a></li>
<?
								}
?>							
							</ul>
<?							
							}
							else
							{
?>							
							<p>Нет документов</p>
<?
							}	
?>	
						</div>
						<a href="#product_tabs" class="watch-more watch-more-docs" title="">Посмотреть все документы</a>
						
						<a href="#" class="red-button grey-button" title="">Посмотреть все</a>
					</div>
					<div class="right-part">
						<h3>Характеристики</h3>
						<div class="closed-info">
							<table cellspacing="0" cellpadding="0" border="0">
<?
							$index = 0;
							foreach ($arResult['DISPLAY_PROPERTIES'] as $pCode => $arProperty)
							{
								if (in_array($arProperty['ID'], $arResult['SHOWED_PROPERTIES']))
								{
									$index++;
									if ($index > 9) break;
									
									$pValue = $arProperty['VALUE'];
									if ($arProperty['PROPERTY_TYPE'] == 'E')
										$pValue = $arProperty['LINK_ELEMENT_VALUE'][$arProperty['VALUE']]['NAME'];
?>							
								<tr><td><?=$arProperty['NAME']?></td><td><?=$pValue?></td></tr>
<?
								}
							}
?>								
							</table>
						</div>
						<a href="#product_tabs" class="watch-more watch-more-characters" title="">Посмотреть все характеристики</a>
						<a href="#" class="red-button grey-button" title="">Посмотреть все</a>
					</div>
					<div class="clear-all"></div>
				</div>
				<div class="review-part">
					<div class="inner">
						<h3>Отзывы</h3>
						<a href="#" id="review-link-add-ext" onclick="return false;" class="red-button red-review" title="">Оставить отзыв</a>
						<div class="clear-all"></div>
							
						<dl class="review" id="first-review">

						</dl>
						<a href="#product_tabs" class="watch-more watch-more-reviews" title="">Посмотреть все отзывы</a>
						<a href="#" class="red-button grey-button" title="">Посмотреть все</a>
					</div>
				</div>
			</div>
			<div class="tab-pane" id="descr">
				<div class="inner">
					<div class="left-part">
						<h3>Описание</h3>
						<div class="closed-info opened-info">
							<h4><?=$arResult['NAME']?></h4>
							<?=$arResult['DETAIL_TEXT']?>
						</div>
					</div>
					<div class="right-part">
						<div class="closed-info closed-docs">
							<h4>Документы</h4>
<?
							if (!empty($arResult['PROPERTIES']['DOCS']['VALUE']))
							{
?>							
							<ul class="documents">
<?
								foreach ($arResult['PROPERTIES']['DOCS']['VALUE'] as $key => $docID)
								{
									$arFile = CFile::GetFileArray($docID);
									
?>							
								<li class="pdf"><a target="_blank" href="<?=$arFile['SRC']?>" title=""><?=$arResult['PROPERTIES']['DOCS']['DESCRIPTION'][$key]?> <span>(<?=CFile::FormatSize($arFile['FILE_SIZE'])?>)</span></a></li>
<?
								}
?>							
							</ul>
<?							
							}
							else
							{
?>							
							<p>Нет документов</p>
<?
							}	
?>								
						</div>
					</div>
					<div class="clear-all"></div>
				</div>
			</div>
			<div class="tab-pane" id="teh">
				<div class="inner table-char">
					<h3>Характеристики</h3>
					<table cellspacing="0" cellpadding="0" border="0">
<?
						foreach ($arResult['DISPLAY_PROPERTIES'] as $pCode => $arProperty)
						{
							if (in_array($arProperty['ID'], $arResult['SHOWED_PROPERTIES']))
							{
								$pValue = $arProperty['VALUE'];
								if ($arProperty['PROPERTY_TYPE'] == 'E')
									$pValue = $arProperty['LINK_ELEMENT_VALUE'][$arProperty['VALUE']]['NAME'];
?>							
							<tr><td><?=$arProperty['NAME']?></td><td><?=$pValue?></td></tr>
<?
							}
						}
?>	
				</table>
				</div>
			</div>
			<div class="tab-pane" id="review">
				<div class="review-part review-tab">
					<?$APPLICATION->IncludeComponent(
						"bitrix:catalog.comments",
						"",
						array(
							"ELEMENT_ID" => $arResult['ID'],
							"IBLOCK_ID" => $arParams['IBLOCK_ID'],
							"COMMENTS_COUNT" => "5",
							"BLOG_USE" => 'Y',
							"CACHE_TYPE" => 'A',
							"CACHE_TIME" => 86400,
							"BLOG_TITLE" => "",
							"BLOG_URL" => '',
							"PATH_TO_SMILE" => "",
							"EMAIL_NOTIFY" => "N",
							"AJAX_POST" => "Y",
							"SHOW_SPAM" => "Y",
							"SHOW_RATING" => "N",
						),
						$component,
						array("HIDE_ICONS" => "Y")
					);?>
				</div>
			</div>
		</div>
	</div>
</div>
<?$this->EndViewTarget();?>
<?$this->SetViewTarget("catalog_item_accessories");?>
    <?if (!empty($arResult['PROPERTIES']['ACCESSORIES']['VALUE'])):?>
        <h3>Аксессуары</h3>
        <div id="carousel5" class="carousel acsesorise">
            <!--ol class="carousel-indicators">
                <li data-target="#carousel2" data-slide-to="0" class="active"></li>
                <li data-target="#carousel2" data-slide-to="1"></li>
                <li data-target="#carousel2" data-slide-to="2"></li>
                <li data-target="#carousel2" data-slide-to="3"></li>
                <li data-target="#carousel2" data-slide-to="4"></li>
                <li data-target="#carousel2" data-slide-to="5"></li>
            </ol-->
            <?
            global $arAccessoryFilter;
            $arAccessoryFilter = array('ID' => $arResult['PROPERTIES']['ACCESSORIES']['VALUE']);
            $APPLICATION->IncludeComponent(
                "bitrix:catalog.section",
                "slider",
                $arParams['ACCESSORIES_PARAMS'],
                $component
            );
            ?>
            <a class="left carousel-control" href="#carousel5" role="button" data-slide="prev"><span></span></a>
            <a class="right carousel-control" href="#carousel5" role="button" data-slide="next"><span></span></a>
        </div>
    <?endif;?>
<?$this->EndViewTarget();?>