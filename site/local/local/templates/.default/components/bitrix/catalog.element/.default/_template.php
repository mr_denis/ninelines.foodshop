<section class="inner-catalog inner-product">
    <div class="decription">
        <div class="inner">
            <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
					"START_FROM" => "0",
					"PATH" => "",
				),
				false,
				Array('HIDE_ICONS' => 'Y')
			);?>
            <div class="product-common">
                <div class="photo-part">
                    <div class="big-img">
                        <a href="#" title="">
                            <img src="images/product.jpg" alt="apple" title="apple" class="main_picture" />
                            <span class="lupa">Увеличить</span>
                            <span class="dop-thing hurry-up">Успей <span>купить</span></span>
                        </a>
                        <!-- div class="economy">
                            <span class="price"><span>10 490 руб.</span>экономия</span>
                            <div class="percent">
                                <span class="back"><span style="width:30%;"></span></span>
                                уже куплено <span>30</span>% товаров
                            </div>
                        </div -->
                        <div class="product-tile-sale">
                            <div class="count-down-timer" data-init="countDown">
                                <div class="count-down-timer-label">
                                    <i class="ico-timer"></i>
                                </div>
                                <div class="count-down-timer-cell">
                                    <strong class="count-down-timer-hours">25</strong>
                                    <small class="count-down-timer-abbr">час</small>
                                </div>
                                <div class="count-down-timer-separator">:</div>
                                <div class="count-down-timer-cell">
                                    <strong class="count-down-timer-minutes">34</strong>
                                    <small class="count-down-timer-abbr">мин</small>
                                </div>
                                <div class="count-down-timer-separator">:</div>
                                <div class="count-down-timer-cell">
                                    <strong class="count-down-timer-seconds">36</strong>
                                    <small class="count-down-timer-abbr">сек</small>
                                </div>
                                <div class="count-down-timer-amount">
                                    <strong class="count-down-timer-qty">17</strong>
                                    <small class="count-down-timer-abbr">шт</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="carousel4" class="carousel product">
                        <div class="carousel-outer">
                            <ul class="carousel-inner">
                                <li class="item active">
                                    <a href="images/product.jpg" title="">
                                        <img src="images/prod1.jpg" alt="" title="" />
                                    </a>
                                </li>
                                <li class="item">
                                    <a href="images/product1.jpg" title="">
                                        <img src="images/prod1.jpg" alt="" title="" />
                                    </a>
                                </li>
                                <li class="item">
                                    <a href="images/product2.jpg" title="">
                                        <img src="images/prod1.jpg" alt="" title="" />
                                    </a>
                                </li>
                                <li class="item">
                                    <a href="images/product3.jpg" title="">
                                        <img src="images/prod1.jpg" alt="" title="" />
                                    </a>
                                </li>
                                <li class="item">
                                    <a href="images/product4.jpg" title="">
                                        <img src="images/prod1.jpg" alt="" title="" />
                                    </a>
                                </li>
                                <li class="item">
                                    <a href="images/product5.jpg" title="">
                                        <img src="images/prod1.jpg" alt="" title="" />
                                    </a>
                                </li>
                                <li class="item">
                                    <a href="images/product6.jpg" title="">
                                        <img src="images/prod1.jpg" alt="" title="" />
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <a class="left carousel-control" href="#carousel4" role="button" data-slide="prev"><span></span></a>
                        <a class="right carousel-control" href="#carousel4" role="button" data-slide="next"><span></span></a>
                    </div>
                </div>
                <div class="info-part">
                    <h1><?=$arResult['NAME']?></h1>
                    <p class="art">Артикул: 1945812</p>
                    <span class="logo"><img src="images/apple.png" alt="apple" title="apple" /></span>
                    <div class="rating">
                        <a href="#" class="rate5" title="5"></a>
                        <a href="#" class="rate4" title="4"></a>
                        <a href="#" class="rate3 selected" title="3"></a>
                        <a href="#" class="rate2" title="2"></a>
                        <a href="#" class="rate1" title="1"></a>
                    </div>
                    
                    <form method="post">
                        <select class="select-class select-class-model">
                            <option>Модель</option>
                            <option>Apple</option>
                            <option>Microsoft</option>
                            <option>HTC</option>
                        </select>
                        <select class="select-class select-class-link">
                            <option>возможности связи</option>
                            <option>Apple</option>
                            <option>Microsoft</option>
                            <option>HTC</option>
                        </select>
                        <select class="select-class select-class-garant">
                            <option>Гарантия</option>
                            <option>Apple</option>
                            <option>Microsoft</option>
                            <option>HTC</option>
                        </select>
                    </form>
                    <dl class="params">
                        <dt>Выберите цвет:</dt>
                        <dd>
                            <ul class="colors">
                                <li class="selected"><a href="#" style="background-color: #ff7c62;" title=""></a></li>
                                <li><a href="#" style="background-color: #ffbf43;" title=""></a></li>
                                <li><a href="#" style="background-color: #11bf43;" title=""></a></li>
                            </ul>
                        </dd>
                        <dt>Выберите размер:</dt>
                        <dd>
                            <ul class="size">
                                <li><a href="#" title="">Mini</a></li>
                                <li class="selected"><a href="#" title="">Air</a></li>
                            </ul>
                        </dd>
                    </dl>
                    <div class="cart-thing">
                        <div class="col1">
                            <span class="new">18 900 руб.</span>
                            <span class="old">21 900 руб.</span>
                            <span class="eco">Экономия: <span>3 000 руб.</span></span>
                        </div>
                        <div class="col2">
                            <a href="#" class="red-button" title="">В корзину</a>
                            <span class="eco">Наличие: <span>Много</span></span>
                        </div>
                        <div class="col3">
                            <a href="#" class="red-button grey-button" title="">Купить в 1 клик</a>
                            <a href="#" class="buy-credit" title="">Купить в кредит</a>
                        </div>
                    </div>
                    <a href="#" class="add2fav-link" title="">В избранное</a>
                    <a href="#" class="compare-link" title="">Сравнить</a>
                    <div class="soc">
                        <ul>
                            <li class="fb"><a href="#" title=""></a></li>
                            <li class="vk"><a href="#" title=""></a></li>
                            <li class="ok"><a href="#" title=""></a></li>
                            <li class="tw"><a href="#" title=""></a></li>
                            <li class="gp"><a href="#" title=""></a></li>
                        </ul>
                    </div>
                </div>
                <a href="#" class="back2cat-link" title=""></a>
            <div class="clear-all"></div>
            </div>
            <div class="clear-all"></div>
        </div>
    </div>
    <a name="product_tabs"></a>
    <div class="params">
        <div class="tabs" id="product_tabs">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#obzor" role="tab" data-toggle="tab">Обзор</a></li>
                <li><a href="#descr" role="tab" data-toggle="tab">Описание</a></li>
                <li><a href="#teh" role="tab" data-toggle="tab">Технические характеристики</a></li>
                <li><a href="#review" role="tab" data-toggle="tab">Отзывы</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="obzor">
                    <div class="inner">
                        <div class="left-part">
                            <h3>Описание</h3>
                            <div class="closed-info">
                                <h4>Apple iPad Air Wi-Fi 16Gb Silver</h4>
                                <p>iPad Air — невероятно тонкий и лёгкий. Но при этом он гораздо мощнее и функциональнее своих предшественников. Процессор A7, передовые технологии беспроводной связи и отличные приложения для работы и творчества, работающие в связке с iOS 7 — iPad Air даёт вам больше возможностей, чем вы могли представить. Но что действительно удивляет в iPad Air — это возросшая мощь и функционал, уложенные в тончайший корпус.</p>
                            </div>
                            <a href="#" class="watch-more watch-more-info" title="">Посмотреть все</a>
                            <div class="closed-info closed-docs">
                                <h4>Документы</h4>
                                <ul class="documents">
                                    <li class="pdf"><a href="#" title="">Инструкция по эксплуатации Apple iPad Air <span>(1,6 Mb)</span></a></li>
                                    <li class="pdf"><a href="#" title="">Полная документация и сертификат соответствия <span>(341 Rb)</span></a></li>
                                </ul>
                            </div>
                            <a href="#product_tabs" class="watch-more watch-more-docs" title="">Посмотреть все документы</a>
                            
                            <a href="#" class="red-button grey-button" title="">Посмотреть все</a>
                        </div>
                        <div class="right-part">
                            <h3>Характеристики</h3>
                            <div class="closed-info">
                                <table cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                        <tr><td>Гарантия</td><td>2 года</td></tr>
                                        <tr><td>Страна производитель</td><td>Китай</td></tr>
                                        <tr><td>Встроенная память</td><td>16 Gb</td></tr>
                                        <tr><td>Диагональ</td><td>9,7” (2048x1536 пикселей)</td></tr>
                                        <tr><td>Операционная система</td><td>iOs</td></tr>
                                        <tr><td>Время автономной работы</td><td>10 часов</td></tr>
                                        <tr><td>Поддержка Wi-Fi</td><td>Да</td></tr>
                                        <tr><td>Разрешение камеры</td><td>5 Мегапикселей</td></tr>
                                        <tr><td>Вес</td><td>467 грамм</td></tr>
                                    </tbody>
                                </table>
                            </div>
                            <a href="#product_tabs" class="watch-more watch-more-characters" title="">Посмотреть все характеристики</a>
                            <a href="#" class="red-button grey-button" title="">Посмотреть все</a>
                        </div>
                        <div class="clear-all"></div>
                    </div>
                    <div class="review-part">
                        <div class="inner">
                            <h3>Отзывы</h3>
                            <a href="#" class="red-button red-review" title="">Оставить отзыв</a>
                            <div class="clear-all"></div>
                            <dl class="review">
                                <dt>
                                    <img src="images/rev1.jpg" alt="" title="" />
                                    <p class="pre-info">Иван Петров <span>22.06.2014</span></p>
                                    <div class="rating">
                                        <a href="#" class="rate5" title="5"></a>
                                        <a href="#" class="rate4" title="4"></a>
                                        <a href="#" class="rate3" title="3"></a>
                                        <a href="#" class="rate2" title="2"></a>
                                        <a href="#" class="rate1" title="1"></a>
                                    </div>
                                </dt>
                                <dd>
                                    <p>Купил пару месяцев назад, это мой первый продукт от Apple. Могу сказать только одно - на андроид больше не вернусь никогда) Стильный внешний вид, очень быстрая скорость работы, ни малейшего зависания, только изредка вылетает сафари. Довольно легкий, очень хорошая камера, долгая работа батарейки, пользуясь им очень много - заряжаю раз в два дня, андроида хватит чуть больше, чем на полдня такой работы; если и нагревается, то совсем чуть-чуть и после довольно длительного использования. Всякие самсунги просто не идут ни в какое сравнение (пользовался некоторое время их планшетом.</p>
                                </dd>
                            </dl>
                            <a href="#product_tabs" class="watch-more watch-more-reviews" title="">Посмотреть все отзывы</a>
                            <a href="#" class="red-button grey-button" title="">Посмотреть все</a>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="descr">
                    <div class="inner">
                        <div class="left-part">
                            <h3>Описание</h3>
                            <div class="closed-info opened-info">
                                <h4>Apple iPad Air Wi-Fi 16Gb Silver</h4>
                                <p>iPad Air — невероятно тонкий и лёгкий. Но при этом он гораздо мощнее и функциональнее своих предшественников. Процессор A7, передовые технологии беспроводной связи и отличные приложения для работы и творчества, работающие в связке с iOS 7 — iPad Air даёт вам больше возможностей, чем вы могли представить. Но что действительно удивляет в iPad Air — это возросшая мощь и функционал, уложенные в тончайший корпус.</p>
                                <p>iPad Air создан мощным, но не прожорливым. Хоть он и стал на 20 процентов тоньше и до двух раз быстрее, чем iPad предыдущего поколения, он по-прежнему работает все те же невероятные 10 часов без подзарядки. А всё потому, что процессор A7 стал не только мощнее, но и эффективнее. Куда бы вы ни отправились вместе с iPad Air, у вас всегда хватит заряда аккумулятора, чтобы пойти ещё дальше.</p>
                                <p>На iPad Air скорость подключения к сетям Wi-Fi стала до двух раз выше благодаря двум антеннам и технологии MIMO (несколько входов — несколько выходов). С двухдиапазонным Wi-Fi 802.11n (2,4 ГГц и 5 ГГц) с поддержкой MIMO ваша скорость загрузки может достигнуть 300 Мбит/с — вдвое выше, чем на iPad предыдущего поколения. Теперь, когда вы захотите загрузить фильм, отправить документы или фотографии через AirDrop или просто открыть веб-сайт, ваше соединение будет ещё быстрее, чем прежде.</p>
                                <p>iPad Air с поддержкой Wi-Fi + Cellular позволяет вам подключаться к Интернету даже там, где нет сигнала Wi-Fi. iPad Air поддерживает ещё больше диапазонов и ещё больше сотовых сетей передачи данных по всему миру. Теперь в большинстве мест вы можете подключаться на действительно высоких скоростях — иногда даже быстрее, чем с широкополосным соединением. А благодаря гибким тарифным планам вам не нужно подписывать долгосрочный контракт.</p>
                                <p>Система iOS 7 создана для iPad Air, поэтому она не только выглядит великолепно, она и работает великолепно. В iOS 7 появились новые отличные функции, например улучшенная многозадачность, AirDrop и Центр управления. </p>
                            </div>
                        </div>
                        <div class="right-part">
                            <div class="closed-info closed-docs">
                                <h4>Документы</h4>
                                <ul class="documents">
                                    <li class="pdf"><a href="#" title="">Инструкция по эксплуатации Apple iPad Air <span>(1,6 Mb)</span></a></li>
                                    <li class="pdf"><a href="#" title="">Полная документация и сертификат соответствия <span>(341 Rb)</span></a></li>
                                    <li class="pdf"><a href="#" title="">Инструкция по эксплуатации Apple iPad Air <span>(1,6 Mb)</span></a></li>
                                    <li class="pdf"><a href="#" title="">Полная документация и сертификат соответствия <span>(341 Rb)</span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="clear-all"></div>
                    </div>
                </div>
                <div class="tab-pane" id="teh">
                    <div class="inner table-char">
                        <h3>Характеристики</h3>
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr><td>Гарантия</td><td>2 года</td></tr>
                                <tr><td>Страна производитель</td><td>Китай</td></tr>
                                <tr><td>Встроенная память</td><td>16 Gb</td></tr>
                                <tr><td>Диагональ</td><td>9,7” (2048x1536 пикселей)</td></tr>
                                <tr><td>Операционная система</td><td>iOs</td></tr>
                                <tr><td>Время автономной работы</td><td>10 часов</td></tr>
                                <tr><td>Поддержка Wi-Fi</td><td>Да</td></tr>
                                <tr><td>Разрешение камеры</td><td>5 Мегапикселей</td></tr>
                                <tr><td>Вес</td><td>467 грамм</td></tr>
                                <tr><td>Гарантия</td><td>2 года</td></tr>
                                <tr><td>Страна производитель</td><td>Китай</td></tr>
                                <tr><td>Встроенная память</td><td>16 Gb</td></tr>
                                <tr><td>Диагональ</td><td>9,7” (2048x1536 пикселей)</td></tr>
                                <tr><td>Операционная система</td><td>iOs</td></tr>
                                <tr><td>Время автономной работы</td><td>10 часов</td></tr>
                                <tr><td>Поддержка Wi-Fi</td><td>Да</td></tr>
                                <tr><td>Разрешение камеры</td><td>5 Мегапикселей</td></tr>
                                <tr><td>Вес</td><td>467 грамм</td></tr>
                            </tbody>
                    </table>
                    </div>
                </div>
                <div class="tab-pane" id="review">
                    <div class="review-part review-tab">
                        <div class="inner">
                            <h3>Отзывы111</h3>
                            <a href="#" class="red-button red-review" title="">Оставить отзыв</a>
                            <div class="clear-all"></div>
                            <dl class="review">
                                <dt>
                                    <img src="images/rev1.jpg" alt="" title="" />
                                    <p class="pre-info">Иван Петров <span>22.06.2014</span></p>
                                    <div class="rating">
                                        <a href="#" class="rate5" title="5"></a>
                                        <a href="#" class="rate4" title="4"></a>
                                        <a href="#" class="rate3" title="3"></a>
                                        <a href="#" class="rate2" title="2"></a>
                                        <a href="#" class="rate1" title="1"></a>
                                    </div>
                                </dt>
                                <dd>
                                    <p>Купил пару месяцев назад, это мой первый продукт от Apple. Могу сказать только одно - на андроид больше не вернусь никогда) Стильный внешний вид, очень быстрая скорость работы, ни малейшего зависания, только изредка вылетает сафари. Довольно легкий, очень хорошая камера, долгая работа батарейки, пользуясь им очень много - заряжаю раз в два дня, андроида хватит чуть больше, чем на полдня такой работы; если и нагревается, то совсем чуть-чуть и после довольно длительного использования. Всякие самсунги просто не идут ни в какое сравнение (пользовался некоторое время их планшетом.</p>
                                </dd>
                            </dl>
                            <dl class="review">
                                <dt>
                                    <img src="images/rev1.jpg" alt="" title="" />
                                    <p class="pre-info">Иван Петров <span>22.06.2014</span></p>
                                    <div class="rating">
                                        <a href="#" class="rate5" title="5"></a>
                                        <a href="#" class="rate4" title="4"></a>
                                        <a href="#" class="rate3" title="3"></a>
                                        <a href="#" class="rate2" title="2"></a>
                                        <a href="#" class="rate1" title="1"></a>
                                    </div>
                                </dt>
                                <dd>
                                    <p>Купил пару месяцев назад, это мой первый продукт от Apple. Могу сказать только одно - на андроид больше не вернусь никогда) Стильный внешний вид, очень быстрая скорость работы, ни малейшего зависания, только изредка вылетает сафари. Довольно легкий, очень хорошая камера, долгая работа батарейки, пользуясь им очень много - заряжаю раз в два дня, андроида хватит чуть больше, чем на полдня такой работы; если и нагревается, то совсем чуть-чуть и после довольно длительного использования. Всякие самсунги просто не идут ни в какое сравнение (пользовался некоторое время их планшетом.</p>
                                </dd>
                            </dl>
                            <dl class="review">
                                <dt>
                                    <img src="images/rev1.jpg" alt="" title="" />
                                    <p class="pre-info">Иван Петров <span>22.06.2014</span></p>
                                    <div class="rating">
                                        <a href="#" class="rate5" title="5"></a>
                                        <a href="#" class="rate4" title="4"></a>
                                        <a href="#" class="rate3" title="3"></a>
                                        <a href="#" class="rate2" title="2"></a>
                                        <a href="#" class="rate1" title="1"></a>
                                    </div>
                                </dt>
                                <dd>
                                    <p>Купил пару месяцев назад, это мой первый продукт от Apple. Могу сказать только одно - на андроид больше не вернусь никогда) Стильный внешний вид, очень быстрая скорость работы, ни малейшего зависания, только изредка вылетает сафари. Довольно легкий, очень хорошая камера, долгая работа батарейки, пользуясь им очень много - заряжаю раз в два дня, андроида хватит чуть больше, чем на полдня такой работы; если и нагревается, то совсем чуть-чуть и после довольно длительного использования. Всякие самсунги просто не идут ни в какое сравнение (пользовался некоторое время их планшетом.</p>
                                </dd>
                            </dl>
                            <dl class="review">
                                <dt>
                                    <img src="images/rev1.jpg" alt="" title="" />
                                    <p class="pre-info">Иван Петров <span>22.06.2014</span></p>
                                    <div class="rating">
                                        <a href="#" class="rate5" title="5"></a>
                                        <a href="#" class="rate4" title="4"></a>
                                        <a href="#" class="rate3" title="3"></a>
                                        <a href="#" class="rate2" title="2"></a>
                                        <a href="#" class="rate1" title="1"></a>
                                    </div>
                                </dt>
                                <dd>
                                    <p>Купил пару месяцев назад, это мой первый продукт от Apple. Могу сказать только одно - на андроид больше не вернусь никогда) Стильный внешний вид, очень быстрая скорость работы, ни малейшего зависания, только изредка вылетает сафари. Довольно легкий, очень хорошая камера, долгая работа батарейки, пользуясь им очень много - заряжаю раз в два дня, андроида хватит чуть больше, чем на полдня такой работы; если и нагревается, то совсем чуть-чуть и после довольно длительного использования. Всякие самсунги просто не идут ни в какое сравнение (пользовался некоторое время их планшетом.</p>
                                </dd>
                            </dl>
                        </div>
                        
                        <ol class="nums">
                            <li class="left"><a href="#" title="">&nbsp;</a></li>
                            <li class="selected"><a href="#" title="">1</a></li>
                            <li><a href="#" title="">2</a></li>
                            <li><a href="#" title="">3</a></li>
                            <li><a href="#" title="">4</a></li>
                            <li>&hellip;</li>
                            <li><a href="#" title="">10</a></li>
                            <li class="right"><a href="#" title="">&nbsp;</a></li>
                        </ol> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="acsesorises">
        <div class="inner">
            <h3>Аксессуары</h3>
            <div id="carousel5" class="carousel acsesorise">
                <!--ol class="carousel-indicators">
                    <li data-target="#carousel2" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel2" data-slide-to="1"></li>
                    <li data-target="#carousel2" data-slide-to="2"></li>
                    <li data-target="#carousel2" data-slide-to="3"></li>
                    <li data-target="#carousel2" data-slide-to="4"></li>
                    <li data-target="#carousel2" data-slide-to="5"></li>
                </ol-->
                <div class="carousel-outer catalog-inner">
                    <ul class="carousel-inner">
                        <li class="item">
                            <div class="item element">
                                <a href="#" title="">
                                    <span class="img-inside"><img src="images/cat1.jpg" alt="" title="" /></span>
                                    <span class="name">Фотоаппарат Canon EOS 600D KIT 15-185 IS</span>
                                    <span class="price">19 500 руб.</span>
                                    <span class="dop-thing new">Новинка</span>
                                </a>
                                <a href="#" class="add2cart red-button" title="">Добавить в корзину</a>
                            </div>
                        </li>
                        <li class="item">
                            <div class="item element">
                                <a href="#" title="">
                                    <span class="img-inside"><img src="images/cat1.jpg" alt="" title="" /></span>
                                    <span class="name">Фотоаппарат Canon EOS 600D KIT 15-185 IS</span>
                                    <span class="price">19 500 руб.</span>
                                    <span class="dop-thing new">Новинка</span>
                                </a>
                                <a href="#" class="add2cart red-button" title="">Добавить в корзину</a>
                            </div>
                        </li>
                        <li class="item">
                            <div class="item element">
                                <a href="#" title="">
                                    <span class="img-inside"><img src="images/cat1.jpg" alt="" title="" /></span>
                                    <span class="name">Фотоаппарат Canon EOS 600D KIT 15-185 IS</span>
                                    <span class="price">19 500 руб.</span>
                                    <span class="dop-thing new">Новинка</span>
                                </a>
                                <a href="#" class="add2cart red-button" title="">Добавить в корзину</a>
                            </div>
                        </li>
                        <li class="item">
                            <div class="item element">
                                <a href="#" title="">
                                    <span class="img-inside"><img src="images/cat1.jpg" alt="" title="" /></span>
                                    <span class="name">Фотоаппарат Canon EOS 600D KIT 15-185 IS</span>
                                    <span class="price">19 500 руб.</span>
                                    <span class="dop-thing new">Новинка</span>
                                </a>
                                <a href="#" class="add2cart red-button" title="">Добавить в корзину</a>
                            </div>
                        </li>
                        <li class="item">
                            <div class="item element">
                                <a href="#" title="">
                                    <span class="img-inside"><img src="images/cat1.jpg" alt="" title="" /></span>
                                    <span class="name">Фотоаппарат Canon EOS 600D KIT 15-185 IS</span>
                                    <span class="price">19 500 руб.</span>
                                    <span class="dop-thing new">Новинка</span>
                                </a>
                                <a href="#" class="add2cart red-button" title="">Добавить в корзину</a>
                            </div>
                        </li>
                        <li class="item">
                            <div class="item element">
                                <a href="#" title="">
                                    <span class="img-inside"><img src="images/cat1.jpg" alt="" title="" /></span>
                                    <span class="name">Фотоаппарат Canon EOS 600D KIT 15-185 IS</span>
                                    <span class="price">19 500 руб.</span>
                                    <span class="dop-thing new">Новинка</span>
                                </a>
                                <a href="#" class="add2cart red-button" title="">Добавить в корзину</a>
                            </div>
                        </li>
                        <li class="item">
                            <div class="item element">
                                <a href="#" title="">
                                    <span class="img-inside"><img src="images/cat1.jpg" alt="" title="" /></span>
                                    <span class="name">Фотоаппарат Canon EOS 600D KIT 15-185 IS</span>
                                    <span class="price">19 500 руб.</span>
                                    <span class="dop-thing new">Новинка</span>
                                </a>
                                <a href="#" class="add2cart red-button" title="">Добавить в корзину</a>
                            </div>
                        </li>
                        <li class="item">
                            <div class="item element">
                                <a href="#" title="">
                                    <span class="img-inside"><img src="images/cat1.jpg" alt="" title="" /></span>
                                    <span class="name">Фотоаппарат Canon EOS 600D KIT 15-185 IS</span>
                                    <span class="price">19 500 руб.</span>
                                    <span class="dop-thing new">Новинка</span>
                                </a>
                                <a href="#" class="add2cart red-button" title="">Добавить в корзину</a>
                            </div>
                        </li>
                    </ul>
                </div>
                <a class="left carousel-control" href="#carousel5" role="button" data-slide="prev"><span></span></a>
                <a class="right carousel-control" href="#carousel5" role="button" data-slide="next"><span></span></a>
            </div>
            <h3>Похожие товары</h3>
            <div id="carousel6" class="carousel acsesorise" data-ride="carousel">
                <!--ol class="carousel-indicators">
                    <li data-target="#carousel2" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel2" data-slide-to="1"></li>
                    <li data-target="#carousel2" data-slide-to="2"></li>
                    <li data-target="#carousel2" data-slide-to="3"></li>
                    <li data-target="#carousel2" data-slide-to="4"></li>
                    <li data-target="#carousel2" data-slide-to="5"></li>
                </ol-->
                <div class="carousel-outer catalog-inner">
                    <ul class="carousel-inner">
                        <li class="item active">
                            <div class="item element">
                                <a href="#" title="">
                                    <span class="img-inside"><img src="images/cat1.jpg" alt="" title="" /></span>
                                    <span class="name">Фотоаппарат Canon EOS 600D KIT 15-185 IS</span>
                                    <span class="price">19 500 руб.</span>
                                    <span class="dop-thing new">Новинка</span>
                                </a>
                                <a href="#" class="add2cart red-button" title="">Добавить в корзину</a>
                            </div>
                        </li>
                        <li class="item">
                            <div class="item element">
                                <a href="#" title="">
                                    <span class="img-inside"><img src="images/cat1.jpg" alt="" title="" /></span>
                                    <span class="name">Фотоаппарат Canon EOS 600D KIT 15-185 IS</span>
                                    <span class="price">19 500 руб.</span>
                                    <span class="dop-thing new">Новинка</span>
                                </a>
                                <a href="#" class="add2cart red-button" title="">Добавить в корзину</a>
                            </div>
                        </li>
                        <li class="item">
                            <div class="item element">
                                <a href="#" title="">
                                    <span class="img-inside"><img src="images/cat1.jpg" alt="" title="" /></span>
                                    <span class="name">Фотоаппарат Canon EOS 600D KIT 15-185 IS</span>
                                    <span class="price">19 500 руб.</span>
                                    <span class="dop-thing new">Новинка</span>
                                </a>
                                <a href="#" class="add2cart red-button" title="">Добавить в корзину</a>
                            </div>
                        </li>
                        <li class="item">
                            <div class="item element">
                                <a href="#" title="">
                                    <span class="img-inside"><img src="images/cat1.jpg" alt="" title="" /></span>
                                    <span class="name">Фотоаппарат Canon EOS 600D KIT 15-185 IS</span>
                                    <span class="price">19 500 руб.</span>
                                    <span class="dop-thing new">Новинка</span>
                                </a>
                                <a href="#" class="add2cart red-button" title="">Добавить в корзину</a>
                            </div>
                        </li>
                        <li class="item">
                            <div class="item element">
                                <a href="#" title="">
                                    <span class="img-inside"><img src="images/cat1.jpg" alt="" title="" /></span>
                                    <span class="name">Фотоаппарат Canon EOS 600D KIT 15-185 IS</span>
                                    <span class="price">19 500 руб.</span>
                                    <span class="dop-thing new">Новинка</span>
                                </a>
                                <a href="#" class="add2cart red-button" title="">Добавить в корзину</a>
                            </div>
                        </li>
                        <li class="item">
                            <div class="item element">
                                <a href="#" title="">
                                    <span class="img-inside"><img src="images/cat1.jpg" alt="" title="" /></span>
                                    <span class="name">Фотоаппарат Canon EOS 600D KIT 15-185 IS</span>
                                    <span class="price">19 500 руб.</span>
                                    <span class="dop-thing new">Новинка</span>
                                </a>
                                <a href="#" class="add2cart red-button" title="">Добавить в корзину</a>
                            </div>
                        </li>
                        <li class="item">
                            <div class="item element">
                                <a href="#" title="">
                                    <span class="img-inside"><img src="images/cat1.jpg" alt="" title="" /></span>
                                    <span class="name">Фотоаппарат Canon EOS 600D KIT 15-185 IS</span>
                                    <span class="price">19 500 руб.</span>
                                    <span class="dop-thing new">Новинка</span>
                                </a>
                                <a href="#" class="add2cart red-button" title="">Добавить в корзину</a>
                            </div>
                        </li>
                        <li class="item">
                            <div class="item element">
                                <a href="#" title="">
                                    <span class="img-inside"><img src="images/cat1.jpg" alt="" title="" /></span>
                                    <span class="name">Фотоаппарат Canon EOS 600D KIT 15-185 IS</span>
                                    <span class="price">19 500 руб.</span>
                                    <span class="dop-thing new">Новинка</span>
                                </a>
                                <a href="#" class="add2cart red-button" title="">Добавить в корзину</a>
                            </div>
                        </li>
                    </ul>
                </div>
                <a class="left carousel-control" href="#carousel6" role="button" data-slide="prev"><span></span></a>
                <a class="right carousel-control" href="#carousel6" role="button" data-slide="next"><span></span></a>
            </div>
        </div>
    </div>
</section>