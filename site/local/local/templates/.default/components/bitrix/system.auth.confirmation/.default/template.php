<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetDirProperty("section_class", "inner-catalog forms-page");
$APPLICATION->SetDirProperty("content_class", "decription auth-forms");
$APPLICATION->SetDirProperty("header_style", "display:none");
?>
<div class="windows-form windows-register">
	<a class="close" title="Закрыть" href="#"></a>
	<div class="caption">Подтверждение регистрации</div>
	<div class="slide-div">
		<p><?echo $arResult["MESSAGE_TEXT"]?></p>

	<?//here you can place your own messages
	switch($arResult["MESSAGE_CODE"])
	{
	case "E01":
		?><? //When user not found
		break;
	case "E02":
		?><? //User was successfully authorized after confirmation
		break;
	case "E03":
		?><? //User already confirm his registration
		break;
	case "E04":
		?><? //Missed confirmation code
		break;
	case "E05":
		?><? //Confirmation code provided does not match stored one
		break;
	case "E06":
		?><? //Confirmation was successfull
		break;
	case "E07":
		?><? //Some error occured during confirmation
		break;
	}
	?>
	<?if($arResult["SHOW_FORM"]):?>

		<?
		ShowMessage($arParams["~AUTH_RESULT"]);
		ShowMessage($arResult['ERROR_MESSAGE']);
		?>
		<form name="bform" method="post" id="confirm_form" target="_top"  action="<?echo $arResult["FORM_ACTION"]?>">
			<?if (strlen($arResult["BACKURL"]) > 0):?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
			<?endif;?>
			<input type="hidden" name="AUTH_FORM" value="Y">
			<input type="hidden" name="TYPE" value="SEND_PWD">
			<input type="hidden" name="send_account_info" value="Y" />
			<input type="hidden" name="<?echo $arParams["USER_ID"]?>" value="<?echo $arResult["USER_ID"]?>" />

			<div class="input-box">
				<input type="text" class="first" placeholder="E-Mail" name="<?echo $arParams["LOGIN"]?>" maxlength="50" value="<?echo (strlen($arResult["LOGIN"]) > 0? $arResult["LOGIN"]: $arResult["USER"]["LOGIN"])?>" />
			</div>

			<div class="input-box" <?=(!empty($arResult["CONFIRM_CODE"]) ? "style=\"dispaly: none;\"" : '')?>>
				<input type="text"  class="last" placeholder="<?echo GetMessage("CT_BSAC_CONFIRM_CODE")?>" name="<?echo $arParams["CONFIRM_CODE"]?>" maxlength="50" value="<?echo $arResult["CONFIRM_CODE"]?>" />
			</div>

			<a href="#" onclick="$('#confirm_form').submit(); return false;" class="button" title=""><?echo GetMessage("CT_BSAC_CONFIRM")?></a>
		</form>

	<?endif?>
		</div>
	</div>