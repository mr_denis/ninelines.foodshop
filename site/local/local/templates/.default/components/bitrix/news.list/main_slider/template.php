<?
if (!empty($arResult['ITEMS'])):
?>
<div id="carousel1" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
<?
	$index = -1;
	foreach ($arResult['ITEMS'] as $arItem):
		if (intval($arItem['PREVIEW_PICTURE'])):
			$index++;
?>	
        <li data-target="#carousel1" data-slide-to="<?=$index?>" class="<?=$index == 0 ? 'active' : ''?>"></li>
<?
		endif;
	endforeach;
?>		
    </ol>
    <div class="carousel-inner">
<?
	$index = -1;
	foreach ($arResult['ITEMS'] as  $arItem):
		if (intval($arItem['PREVIEW_PICTURE'])):
			$index++;
			$pic1 = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 1900, 'height' => 574), BX_RESIZE_IMAGE_EXACT, true);
			$pic2 = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 1200, 'height' => 485), BX_RESIZE_IMAGE_EXACT, true);
			$pic3 = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 960, 'height' => 348), BX_RESIZE_IMAGE_EXACT, true);
			$pic4 = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 720, 'height' => 241), BX_RESIZE_IMAGE_EXACT, true);
			$pic5 = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 480, 'height' => 192), BX_RESIZE_IMAGE_EXACT, true);
		
?>	
        <div class="item <?=$index == 0 ? 'active' : ''?> <?=($arItem['PROPERTIES']['STYLE']['VALUE'] == 'black') ? 'white' : 'black'?>">
            <a href="#" title="">
                <img class="slider-img slider-img1" src="<?=$pic1['src']?>" alt="<?=$arItem['NAME']?>" title="<?=$arItem['NAME']?>" />
                <img class="slider-img slider-img2" src="<?=$pic2['src']?>" alt="<?=$arItem['NAME']?>" title="<?=$arItem['NAME']?>" />
                <img class="slider-img slider-img3" src="<?=$pic3['src']?>" alt="<?=$arItem['NAME']?>" title="<?=$arItem['NAME']?>" />
                <img class="slider-img slider-img4" src="<?=$pic4['src']?>" alt="<?=$arItem['NAME']?>" title="<?=$arItem['NAME']?>" />
                <img class="slider-img slider-img5" src="<?=$pic5['src']?>" alt="<?=$arItem['NAME']?>" title="<?=$arItem['NAME']?>" />
            </a>
            <a href="<?=$arResult['RELATED_ITEMS'][$arItem['PROPERTIES']['RELATED_ITEM']['VALUE']]['DETAIL_PAGE_URL']?>" title="<?=$arItem['NAME']?>" div class="carousel-caption">
                <p class="header"><?=$arItem['NAME']?></p>
                <div class="text"><?=$arItem['PREVIEW_TEXT']?></div>
            </a>
            <a href="<?=$arResult['RELATED_ITEMS'][$arItem['PROPERTIES']['RELATED_ITEM']['VALUE']]['DETAIL_PAGE_URL']?>" class="red-button" title="Подробнее">Подробнее</a>
        </div>
<?
		endif;
	endforeach;
?>
    </div>
    <a class="left carousel-control" href="#carousel1" role="button" data-slide="prev"><span></span></a>
    <a class="right carousel-control" href="#carousel1" role="button" data-slide="next"><span></span></a>
</div>
<?
endif;
?>