<?
if (!empty($arResult['ITEMS'])):
?>

<div class="news main-news">
	<h2><?=$arParams['PAGER_TITLE']?></h2>
	<div id="carousel3" class="carousel news">
		<ol class="carousel-indicators">
			<!-- Сразу делай столько, сколько элементов в карусели!! -->
<?
		$index = -1;
		foreach ($arResult['ITEMS'] as $arItem):	
			if (intval($arItem['PREVIEW_PICTURE'])):
				$index++;
?>			
			<li data-target="#carousel3" data-slide-to="<?=$index?>" class="<?=$index == 0 ? 'active' : 'none'?>"></li>
<?
			endif;
		endforeach;
?>			
		</ol>
		<div class="carousel-outer">
			<ul class="carousel-inner news-inner">
<?
			$index = -1;
			foreach ($arResult['ITEMS'] as $index => $arItem):	
				if (intval($arItem['PREVIEW_PICTURE'])):
					$index++;
					$pic = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 283, 'height' => 220), BX_RESIZE_IMAGE_EXACT, true);
?>			
				<li class="item <?=$index == 0 ? 'active' : ''?>">
					<a href="<?=$arItem['DETAIL_PAGE_URL']?>" title="<?=$arItem['NAME']?>">
						<img src="<?=$pic['src']?>" alt="<?=$arItem['NAME']?>" title="<?=$arItem['NAME']?>" />
						<span class="color-white">
							<span class="first-line"> <?=$arItem['ACTIVE_FROM']?></span>
							<span class="second-line"><?=$arItem['NAME']?></span>
							<span class="description-line">
								<?=$arItem['PREVIEW_TEXT']?>
							</span>
						</span>
					</a>
				</li>
<?
				endif;
			endforeach;
?>				
			</ul>
		</div>
		<a class="left carousel-control" href="#carousel2" role="button" data-slide="prev"><span></span></a>
		<a class="right carousel-control" href="#carousel2" role="button" data-slide="next"><span></span></a>
	</div>
</div>
<?
endif;
?>