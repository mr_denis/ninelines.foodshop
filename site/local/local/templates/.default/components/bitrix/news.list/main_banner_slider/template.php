<?
if (!empty($arResult['ITEMS'])):
?>
<div id="carousel2" class="carousel">
	<div class="carousel-outer">
		<ul class="carousel-inner">
			<li class="item active">
<?
	$index = -1;
	foreach ($arResult['ITEMS'] as $arItem):
		if (intval($arItem['PREVIEW_PICTURE'])):
			$index++;
			$pic = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 385, 'height' => 210), BX_RESIZE_IMAGE_EXACT, true);
?>			
			<a href="<?=$arItem['DETAIL_PAGE_URL']?>"><span><img src="<?=$pic['src']?>" alt="<?=$arItem['NAME']?>" title="<?=$arItem['NAME']?>" /></span></a>
<?
		endif;
	endforeach;
?>				
			</li>
		</ul>
	</div>
</div>
<div id="carousel2-1" class="carousel">
	<div class="carousel-outer">
		<ul class="carousel-inner">
<?
	$index = -1;
	foreach ($arResult['ITEMS'] as $arItem):
		if (intval($arItem['PREVIEW_PICTURE'])):
			$index++;
			$pic = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 385, 'height' => 210), BX_RESIZE_IMAGE_EXACT, true);
?>		
			<li class="item <?=$index == 0 ? 'active' : ''?>">
				<img src="<?=$pic['src']?>" alt="<?=$arItem['NAME']?>" title="<?=$arItem['NAME']?>" />
			</li>
<?
		endif;
	endforeach;
?>			
		</ul>
	</div>
	<ol class="carousel-indicators">
<?
	$index = -1;
	foreach ($arResult['ITEMS'] as $arItem):
		if (intval($arItem['PREVIEW_PICTURE'])):
			$index++;
?>	
			<li data-target="#carousel2-1" class="<?=$index == 0 ? 'active' : ''?>"></li>
<?
		endif;
	endforeach;
?>		
	</ol>
</div>
<?
endif;
?>