<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();//dump($arResult);?>
<?if(strlen($arResult["ERROR_MESSAGE"])):?>
    <?=ShowError($arResult["ERROR_MESSAGE"]);?>
<?else:?>
<div class="caption">Заказ <span>№<?=$arResult["ID"]?></span></div>
<div class="caption-date"><?=$arResult["DATE_INSERT_FORMATED"]?></div>
<div class="slide-div">
    <p class="description"><span class="title">Статус:</span> <span><?=$arResult["STATUS"]["NAME"]?></span></p>
    <p class="description"><span class="title">Сумма заказа:</span> <span><?=$arResult["PRODUCT_SUM_FORMATTED"]?></span></p>
    <p class="description"><span class="title">Способ доставки:</span> <span><?=$arResult["DELIVERY"]["NAME"]?></span></p>
    <p class="description"><span class="title">Способ оплаты:</span> <span><?=$arResult["PAY_SYSTEM"]["NAME"]?></span></p>
</div>
<div class="slide-div">
    <div class="bx_searche">
        <h4>Товары</h4>
        <?foreach($arResult["BASKET"] as $prod):
            $hasLink = !empty($prod["DETAIL_PAGE_URL"]);
        ?>
        <div class="bx_item_block">
            <div class="bx_img_element">
            <?if($hasLink):?>
                <a href="<?=$prod["DETAIL_PAGE_URL"]?>" target="_blank">
            <?endif?>
                    <?if($prod['PICTURE']['SRC']):?>
                        <div style="background-image: url('<?=$prod['PICTURE']['SRC']?>'); background-size: cover; background-repeat: no-repeat; background-position: center center;" class="bx_image"></div>
                    <?endif?>

            <?if($hasLink):?>
                </a>
            <?endif?>
            </div>
            <div class="bx_item_element">
            <?if($hasLink):?>
                <a href="<?=$prod["DETAIL_PAGE_URL"]?>" target="_blank">
            <?endif?>
                <?=$prod["NAME"]?>
                    <span class="number ">Количество: <?=$prod["QUANTITY"]?>шт. Цена: <?=$prod["PRICE_FORMATED"]?></span>
                    <span><?=FormatCurrency($prod["PRICE"] * $prod["QUANTITY"], $prod["CURRENCY"])?></span>
            <?if($hasLink):?>
                </a>
            <?endif?>
            </div>
            <div style="clear:both;"></div>
        </div>
        <?endforeach;?>
    </div>
</div>
<?endif;?>
