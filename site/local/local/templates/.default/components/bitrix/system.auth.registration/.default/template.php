<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetDirProperty("section_class", "inner-catalog forms-page");
$APPLICATION->SetDirProperty("content_class", "decription auth-forms");
$APPLICATION->SetDirProperty("header_style", "display:none");
?>
        <div class="windows-form windows-register">
            <a class="close" title="Закрыть" href="#"></a>
            <div class="caption">Регистрация</div>
            <div class="slide-div">
                <?
                ShowMessage($arParams["~AUTH_RESULT"]);
                ?>
                <?if($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && is_array($arParams["AUTH_RESULT"]) &&  $arParams["AUTH_RESULT"]["TYPE"] === "OK"):?>
                    <p><?echo GetMessage("AUTH_EMAIL_SENT")?></p>
                <?else:?>
                    <?if($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):?>
                        <p><?echo GetMessage("AUTH_EMAIL_WILL_BE_SENT")?></p>
                    <?endif?>
                    <form method="post" action="<?=$arResult["AUTH_URL"]?>" name="bform" id="reg_form">
                        <input type="hidden" name="AUTH_FORM" value="Y" />
                        <input type="hidden" name="TYPE" value="REGISTRATION" />

                        <div class="input-box">
                            <input type="text" class="first" name="USER_NAME" placeholder="Фамилия, Имя"  maxlength="50" value="<?=$arResult["USER_NAME"]?>" />
                        </div>
                        <div class="input-box">
                            <input type="text" name="USER_EMAIL" maxlength="255" placeholder="Email" value="<?=$arResult["USER_EMAIL"]?>" />
                        </div>
                        <div class="input-box">
                            <input type="password" name="USER_PASSWORD" placeholder="Пароль" maxlength="50" value="<?=$arResult["USER_PASSWORD"]?>" />
                        </div>
                        <div class="input-box">
                            <input type="password"  class="last" name="USER_CONFIRM_PASSWORD" placeholder="Повторите пароль" maxlength="50" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" />
                        </div>
                        <a href="#" onclick="$('#reg_form').submit(); return false;" class="button" title="">Зарегистрироваться</a>
                    </form>
                <?endif;?>
            </div>
            <div class="slide-div slide-div-last">
                <p>У вас уже есть аккаунт?</p>
                <a href="<?=$arResult["AUTH_AUTH_URL"]?>" class="button button-grey" title="">Войти</a>
            </div>
        </div>