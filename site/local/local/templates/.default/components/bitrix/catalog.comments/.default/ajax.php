<?
define("NO_KEEP_STATISTIC", true);
define('NO_AGENT_CHECK', true);
define("NO_AGENT_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define('DisableEventsCheck', true);

if (isset($_REQUEST['SITE_ID']) && !empty($_REQUEST['SITE_ID']))
{
	$strSiteID = substr((string)$_REQUEST['SITE_ID'], 0, 2);
	if ($strSiteID != '')
		define('SITE_ID', $strSiteID);
}
else
{
	die();
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(
	isset($_REQUEST["IBLOCK_ID"])
	&& isset($_REQUEST["ELEMENT_ID"])
)
{
	$APPLICATION->IncludeComponent(
		"bitrix:catalog.comments",
		"",
		array(
			"ELEMENT_ID" => $_REQUEST['ELEMENT_ID'],
			"IBLOCK_ID" => $_REQUEST['IBLOCK_ID'],
			"URL_TO_COMMENT" => "",
			"WIDTH" => 0,
			"BLOG_FROM_AJAX" => "Y",
			"COMMENTS_COUNT" => "5",
			"BLOG_USE" => 'Y',
			"FB_USE" => "Y",
			"VK_USE" => "Y",
			"CACHE_TYPE" => 'N',
			"CACHE_TIME" => 0,
			"BLOG_TITLE" => "",
			"BLOG_URL" => 'catalog_comments',
			"PATH_TO_SMILE" => "",
			"EMAIL_NOTIFY" => "N",
			"AJAX_POST" => "Y",
			"SHOW_SPAM" => "Y",
			"SHOW_RATING" => "N",
			"TEMPLATE_THEME" => "site",
		),
		false
	);
}
die();