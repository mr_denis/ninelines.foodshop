<?
/** @var CMain $APPLICATION */
/** @var CUser $USER */
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponent $component */
/** @var string $templateFolder */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$rating = 0;

if (isset($_POST['act']) && $_POST['act'] == 'add') {
	if (empty($arResult["MESSAGE"]) && !empty($arResult["ajax_comment"]))
		$arResult["MESSAGE"] = 'Отзыв добавлен';

	$rating = intval($_POST['UF_RATING']);

	$result = array(
		'message' => $arResult["MESSAGE"],
		'error' => $arResult["COMMENT_ERROR"],
		'success' => !empty($arResult["ajax_comment"])
	);

	echo json_encode($result);
	die();
}
?>
	<form action="<?=$_SERVER['REQUEST_URI']?>" id="form-review" method="post" enctype="multipart/form-data">
		<?=bitrix_sessid_post()?>
		<div class="caption">Оставить отзыв</div>
		<div id="review_messages"></div>
		<div class="slide-div">
			<p>Ваша оценка:</p>
			<div class="rating">
				<input type="hidden" value="0" name="UF_RATING">
				<a class="rate5 <?=($rating > 1 ? 'active' : '')?>" data-value="5" title="5" href="#"></a>
				<a class="rate4 <?=($rating > 1 ? 'active' : '')?>" data-value="4" title="4" href="#"></a>
				<a class="rate3 <?=($rating > 1 ? 'active' : '')?>" data-value="3" title="3" href="#"></a>
				<a class="rate2 <?=($rating > 1 ? 'active' : '')?>" data-value="2" title="2" href="#"></a>
				<a class="rate1 <?=($rating > 1 ? 'active' : '')?>" data-value="1" title="1" href="#"></a>
			</div>
			<div class="clear-all"></div>
			<? if (!$USER->IsAuthorized()):?>
				<div class="input-box">
					<input type="text" placeholder="Фамилия, Имя" name="user_name"  value="<?=htmlspecialcharsEx($_SESSION["blog_user_name"])?>"/>
				</div>
			<? endif;?>
			<div class="input-box">
				<textarea placeholder="Ваш отзыв" name="comment"><?=(isset($_POST['comment']) ? $_POST['comment'] : '')?></textarea>
			</div>
			<input type="file" id="comment-file-avatar" name="UF_AVATAR" accept="image/*">
			<span id="comment-file-avatar-name"></span>
			<a href="#" class="download" title="">Загрузить фото</a>
			<input type="submit" value="Отправить" class="button" style="border: none;">
			<input type="hidden" name="post" value="review_add">
			<input type="hidden" name="act" id="act" value="add">
			<input type="hidden" name="post" value="Y">
			<?
			if(isset($_REQUEST["IBLOCK_ID"]))
			{
				?><input type="hidden" name="IBLOCK_ID" value="<?=(int)$_REQUEST["IBLOCK_ID"]; ?>"><?
			}
			if(isset($_REQUEST["ELEMENT_ID"]))
			{
				?><input type="hidden" name="ELEMENT_ID" value="<?=(int)$_REQUEST["ELEMENT_ID"]; ?>"><?
			}
			if(isset($_REQUEST["SITE_ID"]))
			{
				?><input type="hidden" name="SITE_ID" value="<?=htmlspecialcharsbx($_REQUEST["SITE_ID"]); ?>"><?
			}
			?>
		</div>
	</form>
	<script type="text/javascript">
		(function(){
			var $form = $('#form-review');

			top.BX.unbindAll($form.get(0));

			$form.on('submit', function(){
				var w = top.BX.showWait('form-review'),
					formData = $form.serializeArray(),
					data = new FormData(),
					$file = $('#comment-file-avatar'),
					$messages = $('#review_messages'),
					files = $file.prop('files');

				data.append($file.attr('name'), files[0]);

				$.each(formData, function(index, item) {
					data.append(item.name, item.value);
				});

				$messages.empty();

				$.ajax({
					url: $form.attr('action'),
					data: data,
					dataType: 'json',
					cache: false,
					type: 'post',
					processData: false,
					contentType: false
				}).done(function(data){
					if (data.error && data.error.length) {
						$messages.append('<div style="color:#ff1627;">'+data.error+'</div>');
					}
					if (data.message && data.message.length) {
						$messages.append('<div style="color:#16b900;">'+data.message+'</div>');
					}

					if (data.success) {
						$('#review-reload').trigger('click');
					}
				}).always(function(){
					top.BX.closeWait(w);
				}).error(function(){
					alert('Произошла ошибка, попробуйте позже');
				});

				return false;
			});

			var $rating = $form.find('.rating');

			$rating.find('a').on('click', function(){
				var val = $(this).data('value'),
					currentVal = $rating.data('value');

				if (val == currentVal) {
					val = 0;
				}
				$rating.data('value', val);
				$rating.find('a.active').removeClass('active');

				for (var i = 0; i <= val; i++) {
					$rating.find('a.rate'+i).addClass('active');
				}

				$rating.find('input').val(val);

				return false;
			});

			$form.find('#comment-file-avatar').on('change', function(){
				$form.find('#comment-file-avatar-name').text($(this).val());
			});
		})();
	</script>