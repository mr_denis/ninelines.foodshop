<?
/** @var CMain $APPLICATION */
/** @var CUser $USER */
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponent $component */
/** @var string $templateFolder */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

if (isset($_POST['act']) && $_POST['act'] == 'add') {
	include __DIR__.'/form.php';
	die();
}

$dbResult = new CDBResult;
$dbResult->InitFromArray($arResult['ITEMS']);
$dbResult->NavStart($arParams["COMMENTS_COUNT"]);
$navString = $dbResult->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);

function __showComment($comment, $arParams)
{
	$comment["urlToAuthor"] = "";
	$comment["urlToBlog"] = "";

	$aditStyle = "";
	if($arParams["is_ajax_post"] == "Y" || $comment["NEW"] == "Y")
		$aditStyle .= " blog-comment-new";
	if($comment["AuthorIsAdmin"] == "Y")
		$aditStyle = " blog-comment-admin";
	if(IntVal($comment["AUTHOR_ID"]) > 0)
		$aditStyle .= " blog-comment-user-".IntVal($comment["AUTHOR_ID"]);
	if($comment["AuthorIsPostAuthor"] == "Y")
		$aditStyle .= " blog-comment-author";
	if($comment["PUBLISH_STATUS"] != BLOG_PUBLISH_STATUS_PUBLISH && $comment["ID"] != "preview")
		$aditStyle .= " blog-comment-hidden";
	if($comment["ID"] == "preview")
		$aditStyle .= " blog-comment-preview";

	$avatar = null;
	if (!empty($comment['COMMENT_PROPERTIES']['DATA']['UF_AVATAR']['VALUE']))
		$avatar = CFile::ResizeImageGet(
			$comment['COMMENT_PROPERTIES']['DATA']['UF_AVATAR']['VALUE'],
			array('width' => 63, 'height' => 63),
			BX_RESIZE_IMAGE_EXACT
		);

	if (!$avatar)
	{
		$theme_value = COption::GetOptionString("main", "theme_value".SITE_DIR, "");
		$avatar = array('src' => SITE_TEMPLATE_PATH.'/themes/'.$theme_value.'/images/background-download.png');
	}
		
	$rating = 0;
	if (!empty($comment['COMMENT_PROPERTIES']['DATA']['UF_RATING']['VALUE']))
		$rating = intval($comment['COMMENT_PROPERTIES']['DATA']['UF_RATING']['VALUE']);
	?>
	<a name="<?=$comment["ID"]?>"></a>
	<dl class="review <?=$aditStyle?>" id="blg-comment-<?=$comment["ID"]?>">
		<dt>
			<img src="<?=$avatar['src']?>" alt="" title="" />
		<p class="pre-info"><?=$comment["AuthorName"]?> <span><?=date('d.m.Y', strtotime($comment["DateFormated"]))?></span></p>
		<div class="rating">
			<div class="rating-value" style="width: <?=($rating/5 * 100)?>%;"></div>
			<a href="#" class="rate5" title="5"></a>
			<a href="#" class="rate4" title="4"></a>
			<a href="#" class="rate3" title="3"></a>
			<a href="#" class="rate2" title="2"></a>
			<a href="#" class="rate1" title="1"></a>
		</div>
		</dt>
		<dd>
			<div style="margin-bottom: 10px;"><?=$comment["TextFormated"]?></div>
		</dd>
	</dl>
<?
}
?>
<div class="inner" id="review-inner">
	<h3>Отзывы</h3>
	<a href="#" id="review-link-add" onclick="return false;" class="red-button red-review" title="">Оставить отзыв</a>
	<? if($arResult["is_ajax_post"] != "Y" && $arResult["CanUserComment"] && isset($_POST['first_load'])):?>
	<div class="windows-form windows-reviews" id="review-form-add" style="display: none">
		<a class="close" title="Закрыть" href="#"></a>
		<? include __DIR__.'/form.php'?>
	</div>
	<? endif;?>
	<div class="clear-all"></div>
<?
if (!empty($arResult['ITEMS'])):
foreach ($dbResult->arResult as $comment):
	__showComment($comment, $arParams);
endforeach;
else:
?>
	<p>Нет отзывов</p>
<?
endif;
?>
	<a id="review-reload" href="<?=$APPLICATION->GetCurPageParam()?>"></a>
</div>
<script type="text/javascript">
	<? if($arResult["is_ajax_post"] != "Y"):?>
	$(function(){
		var $formContainer = $('#review-form-add');

		<?
			if (!empty($arResult['ITEMS'])) {
				reset($arResult['ITEMS']);
				$comment = current($arResult['ITEMS']);
				ob_start();
				__showComment($comment, $arParams);
				$firstReview = ob_get_clean();
			}
			else
				$firstReview = '<p id="first-review">Нет отзывов</p>';
		?>

		$('#first-review').replaceWith(<?=json_encode($firstReview)?>);

		<? if (isset($_POST['first_load'])):?>
		$('.window-inside').append($formContainer);
		<? endif;?>

		open_window($('#review-link-add'), $formContainer);
		open_window($('#review-link-add-ext'), $formContainer);
		$('#review .nums').find('a').attr('href', '#');
	});
	<? endif;?>
</script>
<?
echo $navString;
?>
<?
if ($arResult["is_ajax_post"] != "Y")
	include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/script.php");
else
	die();