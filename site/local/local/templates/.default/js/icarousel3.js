jQuery.fn.carousel3 = function(previous, next, options){
    var sliderList3 = jQuery(this).children()[0];
    
    if (sliderList3) {
        var increment3 = '240'
        elmnts3 = jQuery(sliderList3).children(),
        numElmts3 = elmnts3.length,
        sizeFirstElmnt3 = increment3,
        shownInViewport3 = Math.round(jQuery(this).width() / sizeFirstElmnt3),
        firstElementOnViewPort3 = 1,
        isAnimating3 = false;
        
        for (i = 0; i < shownInViewport3; i++) {
            jQuery(sliderList3).css('width',(numElmts3+shownInViewport3)*increment3 + increment3 + "px");
            jQuery(sliderList3).append(jQuery(elmnts3[i]).clone());
        }
        
        jQuery(previous).click(function(event){
            if (!isAnimating3) {
                if (firstElementOnViewPort3 == 1) {
                    jQuery(sliderList3).css('left', "-" + numElmts3 * sizeFirstElmnt3 + "px");
                    firstElementOnViewPort3 = numElmts3;
                }
                else {
                    firstElementOnViewPort3--;
                }
                
                jQuery(sliderList3).animate({
                    left: "+=" + increment3,
                    y: 0,
                    queue: true
                }, "swing", function(){isAnimating3 = false;});
                isAnimating3 = true;
            }
            return false;
        });
        
        jQuery(next).click(function(event){
            if (!isAnimating3) {
                if (firstElementOnViewPort3 > numElmts3) {
                    firstElementOnViewPort3 = 2;
                    jQuery(sliderList3).css('left', "0px");
                }
                else {
                    firstElementOnViewPort3++;
                }
                jQuery(sliderList3).animate({
                    left: "-=" + increment3,
                    y: 0,
                    queue: true
                }, "swing", function(){isAnimating3 = false;});
                isAnimating3 = true;
            }
            return false;
        });
    }
};