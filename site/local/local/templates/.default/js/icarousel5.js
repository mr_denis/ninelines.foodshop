jQuery.fn.carousel5 = function(previous, next, options){
    var sliderList5 = jQuery(this).children()[0];
    
    if (sliderList5) {
        var increment5          = '198',
        elmnts5                 = jQuery(sliderList5).children(),
        numElmts5               = elmnts5.length,
        sizeFirstElmnt5         = increment5,
        shownInViewport5        = Math.round(jQuery(this).width() / sizeFirstElmnt5),
        firstElementOnViewPort5 = 1,
        isAnimating5            = false;
        
        for (i = 0; i < shownInViewport5; i++) {
            jQuery(sliderList5).css('width',(numElmts5 + shownInViewport5) * increment5 + increment5 + "px");
            jQuery(sliderList5).append(jQuery(elmnts5[i]).clone());
        }
        
        jQuery(previous).click(function(event){
            if (!isAnimating5) {
                if (firstElementOnViewPort5 == 1) {
                    jQuery(sliderList5).css('left', "-" + numElmts5 * sizeFirstElmnt5 + "px");
                    firstElementOnViewPort5 = numElmts5;
                }
                else {
                    firstElementOnViewPort5--;
                }
                
                jQuery(sliderList5).animate({
                    left: "+=" + increment5,
                    y: 0,
                    queue: true
                }, "swing", function(){isAnimating5 = false;});
                isAnimating5 = true;
            }
            return false;
        });
        
        jQuery(next).click(function(event){
            if (!isAnimating5) {
                if (firstElementOnViewPort5 > numElmts5) {
                    firstElementOnViewPort5 = 2;
                    jQuery(sliderList5).css('left', "0px");
                }
                else {
                    firstElementOnViewPort5++;
                }
                jQuery(sliderList5).animate({
                    left: "-=" + increment5,
                    y: 0,
                    queue: true
                }, "swing", function(){isAnimating5 = false;});
                isAnimating5 = true;
            }
            return false;
        });
    }
};