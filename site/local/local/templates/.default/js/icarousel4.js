jQuery.fn.carousel4 = function(previous, next, options){
    var sliderList4 = jQuery(this).children()[0];
    
    if (sliderList4) {
        var increment4 = '240'
        elmnts4 = jQuery(sliderList4).children(),
        numElmts4 = elmnts4.length,
        sizeFirstElmnt4 = increment4,
        shownInViewport4 = Math.round(jQuery(this).width() / sizeFirstElmnt4),
        firstElementOnViewPort4 = 1,
        isAnimating4 = false;
        
        for (i = 0; i < shownInViewport4; i++) {
            jQuery(sliderList4).css('width',(numElmts4 + shownInViewport4) * increment4 + increment4 + "px");
            jQuery(sliderList4).append(jQuery(elmnts4[i]).clone());
        }
        
        jQuery(previous).click(function(event){
            if (!isAnimating4) {
                if (firstElementOnViewPort4 == 1) {
                    jQuery(sliderList4).css('left', "-" + numElmts4 * sizeFirstElmnt4 + "px");
                    firstElementOnViewPort4 = numElmts4;
                } else {
                    firstElementOnViewPort4--;
                }
                
                jQuery(sliderList4).animate({
                    left: "+=" + increment4,
                    y: 0,
                    queue: true
                }, "swing", function(){isAnimating4 = false;});
                isAnimating4 = true;
            }
            return false;
        });
        
        jQuery(next).click(function(event){
            if (!isAnimating4) {
                if (firstElementOnViewPort4 > numElmts4) {
                    firstElementOnViewPort4 = 2;
                    jQuery(sliderList4).css('left', "0px");
                } else {
                    firstElementOnViewPort4++;
                }
                jQuery(sliderList4).animate({
                    left: "-=" + increment4,
                    y: 0,
                    queue: true
                }, "swing", function(){isAnimating4 = false;});
                isAnimating4 = true;
            }
            return false;
        });
    }
};