jQuery.fn.carousel = function(previous, next, options){
    var sliderList = jQuery(this).children()[0];
    
    if (sliderList) {
        var increment = '305'//jQuery(sliderList).children().width(),
        elmnts = jQuery(sliderList).children(),
        numElmts = elmnts.length,
        sizeFirstElmnt = increment,
        shownInViewport = Math.round(jQuery(this).width() / sizeFirstElmnt),
        firstElementOnViewPort = 1,
        isAnimating = false;
        
        jQuery(previous).click(function(event){
            if (!isAnimating) {
                if (firstElementOnViewPort == 1) {
                    firstElementOnViewPort = 1;
                } else {
                    firstElementOnViewPort--;
                    jQuery(sliderList).animate({
                        left: "+=" + increment,
                        y: 0,
                        queue: true
                    }, "swing", function(){isAnimating = false;});
                    isAnimating = true;
                }
                
            }
            $("#carousel3 .carousel-indicators li").removeClass("active");
            $("#carousel3 .carousel-indicators li:eq(" + (Math.ceil(firstElementOnViewPort / carousel3_count) - 1) + ")").addClass("active");

            return false;
        });
        
        jQuery(next).click(function(event){
            if (!isAnimating) {
                if (firstElementOnViewPort == numElmts) {
                    firstElementOnViewPort = numElmts;
                } else {
                    firstElementOnViewPort++;
                    jQuery(sliderList).animate({
                        left: "-=" + increment,
                        y: 0,
                        queue: true
                    }, "swing", function(){isAnimating = false;});
                    isAnimating = true;
                }
            }
            $("#carousel3 .carousel-indicators li").removeClass("active");
            $("#carousel3 .carousel-indicators li:eq(" + (Math.ceil(firstElementOnViewPort / carousel3_count) - 1) + ")").addClass("active");
            
            return false;
        });
    }
};