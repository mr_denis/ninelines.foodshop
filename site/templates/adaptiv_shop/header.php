<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/local/templates/".SITE_TEMPLATE_ID."/header.php");
?>
<!doctype html>
<html>
<head>
	<?echo '<meta http-equiv="Content-Type" content="text/html; charset='.LANG_CHARSET.'" />'."\n";?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?$APPLICATION->ShowTitle();?></title>
	<?$APPLICATION->ShowMeta("keywords")?>
	<?$APPLICATION->ShowMeta("description")?>
	
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/normalize.css" />
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/main.css" />
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/jquery.ui.datepicker.css" />
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/jquery.formstyler.css" />
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/magicsuggest-1.3.1.css" />
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/jquery.mCustomScrollbar.min.css" />
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap.css" />

	<script type="text/javascript">
		less = { env: 'development' };
	</script>
	<!--[if lt IE 9]>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/css3-mediaqueries.js"></script>
	<![endif]-->
    <?
    $arThemes = array(
        1 => 'indigo_red',
        2 => 'green_red',
        3 => 'blue_red',
        4 => 'black_yellow',
        5 => 'brown_red',
        6 => 'indigo_orange',
    );

    if (isset($_GET['theme']) && in_array($_GET['theme'], $arThemes)) {
		COption::SetOptionString("main","theme_value".SITE_DIR,$_GET['theme']);
        LocalRedirect($APPLICATION->GetCurPageParam('', array('theme')));
    }

	$theme_value = COption::GetOptionString("main", "theme_value".SITE_DIR, "");
    if (!empty($theme_value)) {
        $colorTheme = $theme_value;
    } else {
        $colorTheme = $APPLICATION->GetProperty("default_theme");
		if (!in_array($colorTheme, $arThemes)) 
			$colorTheme = current($arThemes);
		COption::SetOptionString("main","theme_value".SITE_DIR, $colorTheme);
    }
    ?>
    <link rel="stylesheet/less" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/themes/<?=$colorTheme?>/less/common.less" />
    <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/themes/<?=$colorTheme?>/css/media-retina.css" />
    <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/media1024<?if ( strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') ) echo '_fix'?>.css" />
    <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/media960<?if ( strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') ) echo '_fix'?>.css" />
    <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/media720<?if ( strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') ) echo '_fix'?>.css" />
    <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/themes/<?=$colorTheme?>/css/media720<?if ( strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') ) echo '_fix'?>.css" />
    <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/media480<?if ( strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') ) echo '_fix'?>.css" />
    <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/media320<?if ( strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') ) echo '_fix'?>.css" />
    <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/custom.css" />
    <!-- <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/fix_media_css3.css"/> -->

	<script src="<?=SITE_TEMPLATE_PATH?>/js/vendor/modernizr-2.6.2.min.js" type="text/javascript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/less.js" type="text/javascript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.last.js" type="text/javascript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.migrate.js" type="text/javascript"></script>
    <!--script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.1.1.min.js"></script-->
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.mobile.custom.js" type="text/javascript"></script>

	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.ui.core.js" type="text/javascript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.ui.datepicker.js" type="text/javascript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.ui.widget.js" type="text/javascript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.ui.mouse.js" type="text/javascript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.ui.slider.js" type="text/javascript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.isotope.min.js" type="text/javascript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.infinitescroll.min.js" type="text/javascript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.formstyler.min.js" type="text/javascript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/magicsuggest-1.3.1-min.js" type="text/javascript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.mCustomScrollbar.min.js" type="text/javascript"></script>
    
	<script src="<?=SITE_TEMPLATE_PATH?>/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/bootstrap-touch-carousel.js" type="text/javascript"></script>
    
    <script src="<?=SITE_TEMPLATE_PATH?>/js/icarousel.js" type="text/javascript"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/icarousel2.js" type="text/javascript"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/icarousel3.js" type="text/javascript"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/icarousel4.js" type="text/javascript"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/icarousel5.js" type="text/javascript"></script>
    
	<script src="<?=SITE_TEMPLATE_PATH?>/js/main.js" type="text/javascript"></script>
	<script src="<?=SITE_DIR?>js/custom.js" type="text/javascript"></script>
	
	<script src="<?=SITE_TEMPLATE_PATH?>/js/county.js" type="text/javascript"></script>
	<?$APPLICATION->ShowHead();?>
</head>
<body>
<?
$rsSites = CSite::GetByID(SITE_ID);
$arCurrentSite = $rsSites-> Fetch();
?>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<div class="wrapper-all">
	<?$APPLICATION->IncludeComponent(
		"bitrix:menu", 
		"left_site_sub", 
		array(
			"ROOT_MENU_TYPE" => "site",
			"MENU_CACHE_TYPE" => "Y",
			"MENU_CACHE_TIME" => "36000000",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"MENU_CACHE_GET_VARS" => array(
			),
			"MAX_LEVEL" => "2",
			"USE_EXT" => "Y",
			"ALLOW_MULTI_SELECT" => "N",
			"CHILD_MENU_TYPE" => "bottom",
			"DELAY" => "N",
		),
		false
	);?>

	<?$APPLICATION->IncludeComponent('bitrix:menu', "left_catalog", array(
			"ROOT_MENU_TYPE" => "main",
			"MENU_CACHE_TYPE" => "Y",
			"MENU_CACHE_TIME" => "36000000",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"MENU_CACHE_GET_VARS" => array(),
			"MAX_LEVEL" => "3",
			"USE_EXT" => "Y",
			"ALLOW_MULTI_SELECT" => "N"
		)
	);?>
	<div class="right-slider-block right-slider-block-main menu1 opened">
		<ul class="links">
			<li class="personal">
				<a href="<?=SITE_DIR?>personal/" class="login" title="Личный кабинет">Личный кабинет</a>
			</li>
		</ul>
		<?$APPLICATION->IncludeComponent('bitrix:menu', "left_catalog", array(
				"ROOT_MENU_TYPE" => "main",
				"MENU_CACHE_TYPE" => "Y",
				"MENU_CACHE_TIME" => "36000000",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"MENU_CACHE_GET_VARS" => array(),
				"MAX_LEVEL" => "2",
				"USE_EXT" => "Y",
				"ALLOW_MULTI_SELECT" => "N"
			)
		);?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:menu", 
			"left_site", 
			array(
				"ROOT_MENU_TYPE" => "site",
				"MENU_CACHE_TYPE" => "Y",
				"MENU_CACHE_TIME" => "36000000",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"MENU_CACHE_GET_VARS" => array(
				),
				"MAX_LEVEL" => "2",
				"USE_EXT" => "Y",
				"ALLOW_MULTI_SELECT" => "N",
				"CHILD_MENU_TYPE" => "bottom",
				"DELAY" => "N",
			),
			false
		);?>
		<div class="form-search">
			<form method="get" action="<?=SITE_DIR?>catalog/search/">
				<input type="text" value="Поиск по каталогу" name="q"/>
				<span></span>
			</form>
		</div>
	</div>
	<!-- div class="right-slider-block right-slider-block-filtr opened" -->
	<div class="right-slider-block right-slider-block-filtr opened"></div>
	<!-- div class="wrapper main-menu right-position" -->
	<div class="wrapper main-menu">
		<header>
			<?
			global $USER;
			if ($USER->IsAdmin()) {
			?>
            <a href="#" class="pick-color-open">Выбрать цветовую гамму</a>
            <div class="color-picker">
                <span class="pick-color">Выбрать цветовую гамму</span>
                <a href="#" class="pick-color-close"></a>
                <ul class="colors-list">
                <?foreach ($arThemes as $key => $theme):?>
                    <li><input type="radio" name="radio-color" onclick="location.href='<?=$APPLICATION->GetCurPageParam('theme='.$theme, array('theme'))?>'" id="pick-color<?=$key?>" <?if ($theme == $colorTheme) echo 'checked="checked"'?>  /><label for="pick-color<?=$key?>" class="color color<?=$key?>"></label></li>
                <?endforeach;?>
                </ul>
                <div style="clear: both"></div>
            </div>
			<?}?>
			<div class="top-blue-line">
				<div class="top-blue-line-inner">
					<nav>
						<?$APPLICATION->IncludeComponent('bitrix:menu', "top", array(
								"ROOT_MENU_TYPE" => "top",
								"MENU_CACHE_TYPE" => "Y",
								"MENU_CACHE_TIME" => "36000000",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"MENU_CACHE_GET_VARS" => array(),
								"MAX_LEVEL" => "1",
								"USE_EXT" => "N",
								"ALLOW_MULTI_SELECT" => "N"
							)
						);?>
					</nav>
					<ul class="links">
						<li class="favorite"><a id="favorites-link" class="favorites-link" href="<?=SITE_DIR?>favorites/" title="Избранное">Избранное</a></li>
						<li class="personal">
							<a href="<?=SITE_DIR?>personal/" class="login" title="Личный кабинет">Личный кабинет</a>
							<?if ($USER->IsAuthorized()):?>
							<div class="user">
								<a href="<?=SITE_DIR?>personal/" title="">Личный кабинет</a>
								<a href="<?=$APPLICATION->GetCurPageParam('logout=yes', array('logout'));?>" title="">Выйти</a>
							</div>
							<?else:?>
                                <div class="user">
                                    <a href="<?=SITE_DIR?>personal/" title="">Личный кабинет</a>
                                    <a class="login-link" href="<?=$APPLICATION->GetCurPageParam('login=yes', array('login'));?>" title="">Войти</a>
                                </div>
							<?endif;?>
						</li>
					</ul>
				</div>
			</div>
			<div class="top-info-line">
				<a href="#" class="phone-menu" title="Открыть меню"></a>
				<a href="<?=SITE_DIR?>" class="logo" title="<?=$arCurrentSite['NAME']?>"><img src="<?=SITE_TEMPLATE_PATH?>/themes/<?=$colorTheme?>/images/logo.png" alt="<?=$arCurrentSite['NAME']?>" title="<?=$arCurrentSite['NAME']?>"/></a>
					<?$APPLICATION->IncludeComponent(
					"bitrix:search.title", 
					"catalog", 
					array(
						"NUM_CATEGORIES" => "3",
						"TOP_COUNT" => "3",
						"ORDER" => "rank",
						"USE_LANGUAGE_GUESS" => "Y",
						"CHECK_DATES" => "N",
						"SHOW_OTHERS" => "N",
						"PAGE" => SITE_DIR."/catalog/search/",
						"SHOW_INPUT" => "Y",
						"INPUT_ID" => "title-search-input",
						"CONTAINER_ID" => "title-search",
						"CATEGORY_0_TITLE" => "Новости",
						"CATEGORY_0" => array(
							0 => "iblock_news",
						),
						"PRICE_CODE" => array(
							0 => "BASE",
						),
						"PRICE_VAT_INCLUDE" => "Y",
						"PREVIEW_TRUNCATE_LEN" => "",
						"SHOW_PREVIEW" => "Y",
						"CONVERT_CURRENCY" => "Y",
						"PREVIEW_WIDTH" => "52",
						"PREVIEW_HEIGHT" => "50",
						"CATEGORY_1_TITLE" => "Акции",
						"CATEGORY_1" => array(
							0 => "iblock_news",
						),
						"CATEGORY_2_TITLE" => "Товары",
						"CATEGORY_2" => array(
							0 => "iblock_catalog",
						),
						"CATEGORY_0_iblock_news" => array(
							0 => '#NEWS_IBLOCK_ID#',
						),
						"CATEGORY_1_iblock_news" => array(
							0 => '#ACTIONS#',
						),
						"CATEGORY_2_iblock_catalog" => array(
							0 => '#CATALOG_IBLOCK_ID#',
						),
						"CURRENCY_ID" => "RUB"
					),
					false
				);?>
				<div class="phone-block">
					<span><?=SITE_PHONE?></span>
					<a href="#" id="order-call-link" title="Заказать обратный звонок">Заказать обратный звонок</a>
				</div>
				<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "", array(
						"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
						"PATH_TO_PERSONAL" => SITE_DIR."personal/",
						"SHOW_PERSONAL_LINK" => "N",
						"SHOW_NUM_PRODUCTS" => "Y",
						"SHOW_TOTAL_PRICE" => "Y",
						"SHOW_PRODUCTS" => "N",
						"POSITION_FIXED" =>"N"
					),
					false,
					array()
				);?>
			</div>
			<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"main", 
	array(
		"ROOT_MENU_TYPE" => "main",
		"MENU_CACHE_TYPE" => "Y",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "3",
		"USE_EXT" => "Y",
		"ALLOW_MULTI_SELECT" => "N",
		"ALL_MENU_ITEMS_LINK" => SITE_DIR."/catalog/",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MENU_SHOWED_ITEMS_COUNT" => "7"
	),
	false
);?>
		</header>
        <?if (($APPLICATION->GetCurPage() != SITE_DIR.'index.php') && ($APPLICATION->GetCurPage() != SITE_DIR)):?>
        <section class="<?=$APPLICATION->ShowProperty("section_class");?>">
            <?if ($APPLICATION->GetProperty("custom_breadcrumbs") == 'Y' && ($APPLICATION->GetCurPage() != SITE_DIR.'catalog/search/')):?>
                <div class="decription add_desc_section">
                    <div class="inner">
                        <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
                                "START_FROM" => "0",
                                "PATH" => "",
                            ),
                            false,
                            Array('HIDE_ICONS' => 'Y')
                        );?>
                        <?$APPLICATION->ShowViewContent('catalog_description');?>
                    </div>
                </div>
            <?endif;?>
            <?$APPLICATION->ShowViewContent('add_content');?>
            <div class="<?=$APPLICATION->ShowProperty("content_class");?>">
            <?if ($APPLICATION->GetProperty("custom_breadcrumbs") != 'Y'){?>
                <?if ($APPLICATION->GetProperty("breadcrumbs") == 'Y'){?>
                    <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
                            "START_FROM" => "0",
                            "PATH" => "",
                        ),
                        false,
                        Array('HIDE_ICONS' => 'Y')
                    );?>
                <?}?>
                 <?if ($APPLICATION->GetProperty("showed_title") != 'N'){?>
                <h1 style="<?=$APPLICATION->ShowProperty("header_style");?>"><?$APPLICATION->ShowTitle(false)?></h1>
                <?}?>
            <?}?>
        <?endif;?>