<?if ($APPLICATION->GetCurPage() != '/'):?>
        </section>
    </div>
<?endif;?>
<div class="window-inside">
    <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
        array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_DIR."include/popup/login.php"
        ),
    false);?>
    <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
        array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_DIR."include/popup/registration.php"
        ),
        false);?>
    <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
        array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_DIR."include/popup/forgot_password.php"
        ),
        false);?>
	<?$APPLICATION->IncludeComponent("bitrix:main.include", "", 
		array(
			"AREA_FILE_SHOW" => "file", 
			"PATH" => SITE_DIR."include/popup/request_call.php"
		),
	false);?>

    <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
        array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_DIR."include/popup/buy1click.php"
        ),
        false);?>

    <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
        array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_DIR."include/popup/profile_edit.php"
        ),
        false);?>

    <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
        array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_DIR."include/popup/password_edit.php"
        ),
        false);?>
    <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
        array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_DIR."include/popup/favorites.php"
        ),
        false);?>

    <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
        array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_DIR."include/popup/history.php"
        ),
        false);?>

    <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
        array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_DIR."include/popup/history_order.php"
        ),
        false);?>

    <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
        array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_DIR."include/popup/edit_address.php"
        ),
        false);?>

    <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
        array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_DIR."include/popup/delete_address.php"
        ),
        false);?>

    <div class="windows-form windows-done">
        <a class="close" title="Закрыть" href="#"></a>
        <div class="slide-div">
            <p>Ваш заказ №123131 оформлен.</p>
            <p class="details">Информация отправлена на Ваш email. Так же вы можете посмотреть подробности в личном кабинете</p>
            <a href="#" class="button" title="">продолжить</a>
        </div>
    </div>
</div>
<footer>
    <div class="footer-inner-top-line">
		<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
        array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_DIR."include/social.php"
        ),
        false);?>
	
		<?/*$APPLICATION->IncludeComponent(
			"custom:highloadblock.list", 
			"social_links", 
			array(
				"BLOCK_ID" => "3",
				"DETAIL_URL" => "",
				"ROWS_PER_PAGE"   => 6,
				'NAV_TEMPLATE' => '.default'
			), 
		false);*/?>
        
        <?$APPLICATION->IncludeComponent("custom:subscribe.form", "", array(), false);?>
        <div class="money">
            Способы оплаты
            <ul>
                <li class="visa"><a href="JavaScript:void(0)" title=""></a></li>
                <li class="mc"><a href="JavaScript:void(0)" title=""></a></li>
                <li class="ya"><a href="JavaScript:void(0)" title=""></a></li>
                <li class="qiwi"><a href="JavaScript:void(0)" title=""></a></li>
            </ul>
        </div>
    </div>
    <div class="nav-block"> 
        <div class="nav-block-inner">
            <div class="contacts">
                <?
                if (isset($_SESSION['SUTE_THEME'])) {
                    $colorTheme = $_SESSION['SUTE_THEME'];
                } else {
                    $colorTheme = 'indigo_red';
                }
                ?>
                <a href="<?=SITE_DIR?>" class="logo" title="<?=$arCurrentSite['NAME']?>"><img src="<?=SITE_TEMPLATE_PATH?>/theme/<?=$colorTheme?>/images/logo.png" alt="<?=$arCurrentSite['NAME']?>" title="<?=$arCurrentSite['NAME']?>"/></a>

                <div class="phone-block">
                    <span><?=SITE_PHONE?></span>
                    <a href="JavaScript:void(0);" onclick="$('#order-call-link').click()" title="Заказать обратный звонок">Заказать обратный звонок</a>
                </div>
                <a href="mailto:<?=COption::GetOptionString("sale", "order_email")?>" class="email" title="<?=COption::GetOptionString("sale", "order_email")?>"><?=COption::GetOptionString("sale", "order_email")?></a>
            </div>
			<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"footer", 
	array(
		"ROOT_MENU_TYPE" => "main_bottom",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "2",
		"USE_EXT" => "Y",
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "bottom",
		"DELAY" => "N"
	),
	false
);?>
        </div>
    </div>
    <div class="copyright">
        <span class="madeby">Разработано в <a href="http://gadget.bitlate.ru/" title="eShop">gadget.bitlate.ru</a></span>
        &copy; 2015 Gadget Store. Все права защищены
    </div>
</footer>
</div>
</div>
</body>
</html>