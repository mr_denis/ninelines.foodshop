jQuery.fn.carousel2 = function(previous, next, options){
    var sliderList2 = jQuery(this).children()[0];
    
    if (sliderList2) {
        var increment2 = '73'//jQuery(sliderList).children().width(),
        elmnts2 = jQuery(sliderList2).children(),
        numElmts2 = elmnts2.length,
        sizeFirstElmnt2 = increment2,
        shownInViewport2 = Math.round(jQuery(this).width() / sizeFirstElmnt2),
        firstElementOnViewPort2 = 1,
        isAnimating2 = false;
        
        for (i = 0; i < shownInViewport2; i++) {
            jQuery(sliderList2).css('width',(numElmts2+shownInViewport2)*increment2 + increment2 + "px");
            jQuery(sliderList2).append(jQuery(elmnts2[i]).clone());
        }
        
        jQuery(previous).click(function(event){
            if (!isAnimating2) {
                if (firstElementOnViewPort2 == 1) {
                    jQuery(sliderList2).css('left', "-" + numElmts2 * sizeFirstElmnt2 + "px");
                    firstElementOnViewPort2 = numElmts2;
                }
                else {
                    firstElementOnViewPort2--;
                }
                
                jQuery(sliderList2).animate({
                    left: "+=" + increment2,
                    y: 0,
                    queue: true
                }, "swing", function(){isAnimating2 = false;});
                isAnimating2 = true;
            }
            return false;
        });
        
        jQuery(next).click(function(event){
            if (!isAnimating2) {
                if (firstElementOnViewPort2 > numElmts2) {
                    firstElementOnViewPort2 = 2;
                    jQuery(sliderList2).css('left', "0px");
                }
                else {
                    firstElementOnViewPort2++;
                }
                jQuery(sliderList2).animate({
                    left: "-=" + increment2,
                    y: 0,
                    queue: true
                }, "swing", function(){isAnimating2 = false;});
                isAnimating2 = true;
            }
            return false;
        });
    }
};