function open_window(link, windows_div) {
    if ($(windows_div).length) {
    	link.bind("click", function () {
            $('.window-inside .windows-form').hide();
    		$(".window-inside").show();
    		$("html>body").css({overflow: "hidden"});
    		windows_div.show();
    		windows_div.animate({opacity: "1"});

    		if ((windows_div.outerHeight() + 100) > $(window).outerHeight()) {
				windows_div.css({top: "50px"});
    		} else {
				windows_div.css({marginTop: "-" + Math.ceil(windows_div.outerHeight() / 2) + "px"});
    		}
    		
    		return false;
    	});
    }	
}

function open_window_photo(link, windows_div) {
    if ($(windows_div).length) {
        link.bind("click", function () {
            $(".window-inside2").show();
            $("html>body").css({overflow: "hidden"});
            windows_div.show();
            windows_div.animate({opacity: "1"});

            if ((windows_div.outerHeight() + 100) > $(window).outerHeight()) {
                windows_div.css({top: "50px"});
            } else {
                windows_div.css({marginTop: "-" + Math.ceil(windows_div.outerHeight() / 2) + "px"});
        }

        return false;
    });
}
}

function isMobile() {
	try {
		document.createEvent("TouchEvent");
		return true;
	} catch (e) {
		return false;
	}
}

var carousel3_count = 4

var menu_hide = true;

function hide_slide() {
	if (!menu_hide) {
		$(".nav-dop").delay(200).slideUp(10);
		$("nav.main-catalog a").removeClass("selected");
	}
}

var animationOptionsDefault = {
	duration: 10,
	easing: 'linear',
	queue: false
}

function isotope_settings(container_width, container_count) {
	return {
		masonry: {
			columnWidth: container_width / container_count
		},
		animationOptions: animationOptionsDefault,
        sortBy: 'original-order'
	}
}

var container_count = 0

$(document).ready(function () {
/*
    if ($(".container .element").length) {
        var container = $('.container');
    }

    if ($(".container .element").length) {
        if ($(window).width() > 1200) {
            container_count = 5;
        } else if ($(window).width() > 960) {
            container_count = 4;
        } else if ($(window).width() > 720) {
            container_count = 3;
        } else if ($(window).width() > 480) {
            container_count = 2;
        } else {
            container_count = 1;
        }

        container.isotope(isotope_settings(container.width(), container_count));

        $(window).resize(function() {
            if ($(window).width() > 1200) {
                container_count = 5;
            } else if ($(window).width() > 960) {
                container_count = 4;
            } else if ($(window).width() > 720) {
                container_count = 3;
            } else if ($(window).width() > 480) {
                container_count = 2;
            } else {
                container_count = 1;
            }

            container.isotope(isotope_settings(container.width(), container_count));
        });
    }*/

    /*if ($(".catalog-inner .container .element").length) {
        if ($(window).width() > 1200) {
            container_count = 4;
        } else if ($(window).width() > 960) {
            container_count = 3;
        } else if ($(window).width() > 720) {
            container_count = 3;
        } else if ($(window).width() > 480) {
            container_count = 2;
        } else {
            container_count = 1;
        }

        container.isotope(isotope_settings(container.width(), container_count));

        $(window).resize(function() {
            if ($(window).width() > 1200) {
                container_count = 4;
            } else if ($(window).width() > 960) {
                container_count = 3;
            } else if ($(window).width() > 720) {
                container_count = 3;
            } else if ($(window).width() > 480) {
                container_count = 2;
            } else {
                container_count = 1;
            }

            container.isotope(isotope_settings(container.width(), container_count));
        });
    }*/

    if ($(".inner_profile .container .element").length) {
        var container = $('.container');
        container_count = 3;

        if ($(window).width() > 720) {
            container_count = 3;
        } else {
            container_count = 1;
        }

        container.isotope(isotope_settings(container.width(), container_count));

        $(window).resize(function() {
            if ($(window).width() > 720) {
                container_count = 3;
            } else {
                container_count = 1;
            }

            container.isotope(isotope_settings(container.width(), container_count));
        });
    }

    if ($('select.select-class').length) {
        $('select.select-class').styler();
    }

    if ($("#slider-range").length) {
        $("#slider-range").slider({
            range: true,
            min: 0,
            max: 155500,
            values: [7500, 30000]
            , create: function( event, ui ) {
                $(this).find("a:first").addClass("first-a");
                $(this).find("a:last").addClass("last-a");
                $("#value1").val(7500);
                $("#value2").val(30000);
            }
            , slide: function( event, ui ) {
                if ($(ui.handle).hasClass("first-a")) {
                    $("#value1").val(ui.value);
                }
                if ($(ui.handle).hasClass("last-a")) {
                    $("#value2").val(ui.value);
                }
            }
        });
    }

    if ($("#carousel1").length) {
        $("#carousel1 a.left, #carousel1 a.right").css({width: ($("#carousel1").outerWidth() - $("#carousel1 .carousel-caption").outerWidth()) / 2 + "px"})
    }

    if ($(".inner-catalog>.inner").length) {
        if ($(window).width() > 1200) {
            if (!$(".inner-catalog article").length) {
                $(".inner-catalog>.inner").prepend($(".right-slider-block-filtr article.filter"));
            }
        } else if ($(window).width() > 960) {
            if (!$(".inner-catalog article").length) {
                $(".inner-catalog>.inner").prepend($(".right-slider-block-filtr article.filter"));
            }
        } else if ($(window).width() > 720) {
            if (!$(".right-slider-block-filtr article").length) {
                $(".right-slider-block-filtr").append($(".inner-catalog article.filter"));
            }
        } else {
            if (!$(".right-slider-block-filtr article").length) {
                $(".right-slider-block-filtr").append($(".inner-catalog article.filter"));
            }
        }
    }

    if ($("#carousel3").length) {
        $("#carousel2-1 .carousel-indicators li").css({display: "none"});
        if ($(window).width() > 1200) {
            carousel3_count = 4;
        } else if ($(window).width() > 960) {
            carousel3_count = 3;
        } else if ($(window).width() > 720) {
            carousel3_count = 2;
            $("#carousel2-1 .carousel-indicators li:lt(2)").css({display: "inline-block"});
        } else {
            carousel3_count = 1;
            $("#carousel2-1 .carousel-indicators li:lt(3)").css({display: "inline-block"});
        }

        var carousel3_length = $("#carousel3 .carousel-inner li").length;

        var indicator_count = Math.ceil(carousel3_length / carousel3_count);

        $("#carousel3 .carousel-indicators li:lt(" + indicator_count + ")").css({display: "inline-block"});

        $("#carousel3 a.left, #carousel3 a.right").css({width: ($(".main-news").outerWidth() - $("#carousel3 .carousel-outer").outerWidth()) / 2 + "px"})

        $("#carousel3 .carousel-outer").carousel("#carousel3 a.left", "#carousel3 a.right");

        $("#carousel3 .carousel-indicators li").bind("click", function () {
            $("#carousel3 .carousel-indicators li").removeClass("active");
            $(this).addClass("active");
            firstElementOnViewPort = carousel3_count * parseInt($(this).attr("data-slide-to")) + 1;

            $("#carousel3 .carousel-inner").animate({left: "-" + 305 * carousel3_count * parseInt($(this).attr("data-slide-to")) + "px"});

            return false;
        });

        $("#carousel2-1 .carousel-indicators li:eq(0)").bind("click", function () {
            $("#carousel2-1 .carousel-inner").animate({left: "0px"});
            $("#carousel2-1 .carousel-indicators li").removeClass("active");
            $(this).addClass("active");

            return false;
        });

        $("#carousel2-1 .carousel-indicators li:eq(1)").bind("click", function () {
            if ($(window).width() > 720) {
                $("#carousel2-1 .carousel-inner").animate({left: "-370px"});
                $("#carousel2-1 .carousel-indicators li").removeClass("active");
                $(this).addClass("active");
            } else {
                $("#carousel2-1 .carousel-inner").animate({left: "-407px"});
                $("#carousel2-1 .carousel-indicators li").removeClass("active");
                $(this).addClass("active");
            }

            return false;
        });

        $("#carousel2-1 .carousel-indicators li:eq(2)").bind("click", function () {
            $("#carousel2-1 .carousel-inner").animate({left: "-814px"});
            $("#carousel2-1 .carousel-indicators li").removeClass("active");
            $(this).addClass("active");

            return false;
        });

        $("#carousel2-1").bind("swipeleft", function () {
            if ($(window).width() > 720) {
                if ($("#carousel2-1 .carousel-inner").css("left") != "-370px") {
                    $("#carousel2-1 .carousel-inner").animate({left: "-=370px"});
                    $("#carousel2-1 .carousel-indicators li").removeClass("active");
                    $("#carousel2-1 .carousel-indicators li:eq(1)").addClass("active");
                }
            } else {
                if ($("#carousel2-1 .carousel-inner").css("left") != "-814px") {
                    $("#carousel2-1 .carousel-inner").animate({left: "-=407px"});
                    $("#carousel2-1 .carousel-indicators li").removeClass("active");
                    if ($("#carousel2-1 .carousel-inner").css("left") != "-407px") {
                        $("#carousel2-1 .carousel-indicators li:eq(1)").addClass("active");
                    } else {
                        $("#carousel2-1 .carousel-indicators li:eq(2)").addClass("active");
                    }
                }
            }

            return false;
        });

        $("#carousel2-1").bind("swiperight", function () {
            if ($(window).width() < 961) {
                if ($(window).width() > 720) {
                    if ($("#carousel2-1 .carousel-inner").css("left") != "0px") {
                        $("#carousel2-1 .carousel-inner").animate({left: "+=370px"});
                        $("#carousel2-1 .carousel-indicators li").removeClass("active");
                        $("#carousel2-1 .carousel-indicators li:eq(0)").addClass("active");
                    }
                } else {
                    if ($("#carousel2-1 .carousel-inner").css("left") != "0px") {
                        $("#carousel2-1 .carousel-inner").animate({left: "+=407px"});
                        $("#carousel2-1 .carousel-indicators li").removeClass("active");
                        if ($("#carousel2-1 .carousel-inner").css("left") != "-407px") {
                            $("#carousel2-1 .carousel-indicators li:eq(1)").addClass("active");
                        } else {
                            $("#carousel2-1 .carousel-indicators li:eq(0)").addClass("active");
                        }
                    }
                }
            }

            return false;
        });

        /*		$("#carousel1 .carousel-inner").bind("swiperight", function () {
         $("#carousel1").carousel('prev');

         return false;
         });

         $("#carousel1 .carousel-inner").bind("swipeleft", function () {
         $("#carousel1").carousel('next');

         return false;
         });*/

        $("#carousel3 .carousel-outer").bind("swiperight", function () {
            $("#carousel3 .left").click();

            return false;
        });

        $("#carousel3 .carousel-outer").bind("swipeleft", function () {
            $("#carousel3 .right").click();

            return false;
        });
    }

    $(window).resize(function() {

        if ($("#carousel1").length) {
            $("#carousel1 a.left, #carousel1 a.right").css({width: ($("#carousel1").outerWidth() - $("#carousel1 .carousel-caption").outerWidth()) / 2 + "px"})
        }

        if ($("#carousel3").length) {
            $("#carousel3 a.left, #carousel3 a.right").css({width: ($(".main-news").outerWidth() - $("#carousel3 .carousel-outer").outerWidth()) / 2 + "px"})

            $("#carousel2-1 .carousel-indicators li").css({display: "none"});
            if ($(window).width() > 1200) {
                carousel3_count = 4;
            } else if ($(window).width() > 960) {
                carousel3_count = 3;
            } else if ($(window).width() > 720) {
                carousel3_count = 2;
                $("#carousel2-1 .carousel-indicators li:lt(2)").css({display: "inline-block"});
            } else {
                carousel3_count = 1;
                $("#carousel2-1 .carousel-indicators li:lt(3)").css({display: "inline-block"});
            }

            var indicator_count = Math.ceil(carousel3_length / carousel3_count);
            $("#carousel3 .carousel-indicators li").css({display: "none"});
            $("#carousel3 .carousel-indicators li:lt(" + indicator_count + ")").css({display: "inline-block"});
        }

        if ($(window).width() > 1200) {
            if (!$(".inner-catalog article").length) {
                $(".inner-catalog>.inner").prepend($(".right-slider-block-filtr article.filter"));
            }
        } else if ($(window).width() > 960) {
            if (!$(".inner-catalog article").length) {
                $(".inner-catalog>.inner").prepend($(".right-slider-block-filtr article.filter"));
            }
        } else if ($(window).width() > 720) {
            if (!$(".right-slider-block-filtr article").length) {
                $(".right-slider-block-filtr").append($(".inner-catalog article.filter"));
            }
        } else {
            if (!$(".right-slider-block-filtr article").length) {
                $(".right-slider-block-filtr").append($(".inner-catalog article.filter"));
            }
        }

        if (!isMobile()) {
            $(".right-slider-block.opened").css({left: "-260px"});
            $(".wrapper.main-menu").css({left: "0px"}).removeClass("right-position");
        }
    });

	$("header .phone-menu").bind("click", function () {
		if ($(".wrapper.main-menu").hasClass("open-filter")) {
			$(".right-slider-block.right-slider-block-filtr").animate({left: "-220px"});
			$(".wrapper.main-menu").animate({left: "0px"}, function () {
				$(this).removeClass("right-position").removeClass("open-filter");

				if ($(".wrapper.main-menu").hasClass("right-position")) {
					$(".right-slider-block.right-slider-block-main").animate({left: "-260px"});
					$(".wrapper.main-menu").animate({left: "0px"}, function () {
						$(this).removeClass("right-position").removeClass("open-menu");
					});
				} else {
					$(".right-slider-block.menu1").animate({left: "0px"});
					$(".wrapper.main-menu").animate({left: "260px"}).addClass("right-position").addClass("open-menu");
				}
			});
		} else {
			if ($(".wrapper.main-menu").hasClass("right-position")) {
				$(".right-slider-block.right-slider-block-main").animate({left: "-260px"});
				$(".wrapper.main-menu").animate({left: "0px"}, function () {
					$(this).removeClass("right-position").removeClass("open-menu");
				});
			} else {
				$(".right-slider-block.menu1").animate({left: "0px"});
				$(".wrapper.main-menu").animate({left: "260px"}).addClass("right-position").addClass("open-menu");
			}
		}

		return false;
	});

	$(".right-slider-block.opened .main-menu a").bind("click", function () {
        if ($(".right-slider-block.sub" + $(this)[0].className).length) {
            $(this).removeClass("ui-link");
            $(".right-slider-block.sub" + $(this)[0].className).animate({left: "0px"});
            $("html, body").animate({"scrollTop": 0});

            return false;
        }
        return true;
	});

	$(".right-slider-block .back2cat").bind("click", function () {
		$(this).parent().animate({left: "-260px"});

		return false;
	});

	$(".inner-catalog .filtr-button").bind("click", function () {
		if ($(".wrapper.main-menu").hasClass("open-menu")) {
			$(".right-slider-block.menu1").animate({left: "-260px"});
			$(".wrapper.main-menu").animate({left: "0px"}, function () {
				$(this).removeClass("right-position").removeClass("open-menu");
				if ($(".wrapper.main-menu").hasClass("right-position")) {
					$(".right-slider-block.right-slider-block-filtr").animate({left: "-220px"});
					$(".wrapper.main-menu").animate({left: "0px"}, function () {
						$(this).removeClass("right-position").removeClass("open-filter");
					});
				} else {
					$(".right-slider-block.right-slider-block-filtr").animate({left: "0px"});
					$(".wrapper.main-menu").animate({left: "220px"}).addClass("right-position").addClass("open-filter");
				}
			});
		} else {
			if ($(".wrapper.main-menu").hasClass("right-position")) {
				$(".right-slider-block.right-slider-block-filtr").animate({left: "-220px"});
				$(".wrapper.main-menu").animate({left: "0px"}, function () {
					$(this).removeClass("right-position").removeClass("open-filter");
				});
			} else {
				$(".right-slider-block.right-slider-block-filtr").animate({left: "0px"});
				$(".wrapper.main-menu").animate({left: "220px"}).addClass("right-position").addClass("open-filter");
			}
		}

		return false;
	});

	/*$("nav.main-catalog a").bind("click", function () {
		if ($(this).hasClass("selected")) {
			menu_hide = false;
			setTimeout(function () {
				hide_slide();
			}, 500);
		} else {
			$(".nav-dop").slideUp();
			var current_menu = $(this);
			$("#" + $(this)[0].className).slideDown(10, function () {
				$("nav.main-catalog a").removeClass("selected");
				current_menu.addClass("selected");
			});
			menu_hide = true;
		}

		return false;
	});*/

	var menuTimer = null;
	
	$("nav.main-catalog a").hover(function () {
		$(".nav-dop").slideUp(10);
		var current_menu = $(this);
		
		clearTimeout(menuTimer);
		menuTimer = setTimeout(function(){
			$("#" + current_menu[0].className).slideDown(10, function () {
				$("nav.main-catalog a").removeClass("selected");
				current_menu.addClass("selected");
			});
		}, 400);
		menu_hide = true;
	}, function () {
		clearTimeout(menuTimer);
		menu_hide = false;
		setTimeout(function () {
			hide_slide();
		}, 500);
	});

	$(".nav-dop").bind('mousemove', function () {
		menu_hide = true;
	});

	$(".nav-dop").bind('mouseleave', function () {
		menu_hide = false;
		setTimeout(function () {
			hide_slide();
		}, 500);
	});

    $("form input[type=text], form input[type=password], form textarea").bind("focus", function () {
        if ($(this).val() == $(this)[0].defaultValue) {
            $(this).val("");
        }
    }).bind("blur", function () {
        if (!$(this).val()) {
            $(this).val($(this)[0].defaultValue);
        }
    });

    if ($(".inner-catalog .filter").length) {
    	$(".inner-catalog .filter dl dt a").bind("click", function () {
    		var dd = $(this).parent().parent().find("dd");
    		if (dd.is(":hidden")) {
    			dd.slideDown(function () {
    				$(this).parent().removeClass("closed").addClass("open");
    			});
    		} else {
				dd.slideUp(function () {
    				$(this).parent().removeClass("open").addClass("closed");
    			});
    		}

    		return false;
    	});

    	$(".inner-catalog .filter .colors a").bind("click", function () {
    		var li = $(this).parent();
    		if (li.hasClass("selected")) {
    			li.removeClass("selected");
    		} else {
				li.addClass("selected");
    		}

    		return false;
    	});
    }

    if ($(".inner-product .colors").length) {
    	$(".inner-product .colors a").bind("click", function () {
    		$(this).parent().parent().find("li").removeClass("selected");
			$(this).parent().addClass("selected");

    		return false;
    	});

    	$(".inner-product .size a").bind("click", function () {
    		$(this).parent().parent().find("li").removeClass("selected");
			$(this).parent().addClass("selected");

    		return false;
    	});
    }

    if ($("#carousel4 .carousel-outer").length) {
    	$("#carousel4 .carousel-outer").carousel2("#carousel4 .left", "#carousel4 .right");

    	$("#carousel4 .carousel-outer a").bind("click", function () {
    		$(".main_picture").attr({src: $(this)[0].href});
    		$("#carousel4 .carousel-outer li").removeClass("active");
    		$(this).parent().addClass("active");

    		return false;
    	});

		$("#carousel4 .carousel-outer").bind("swiperight", function () {
			$("#carousel4 .left").click();

			return false;
		});

		$("#carousel4 .carousel-outer").bind("swipeleft", function () {
			$("#carousel4 .right").click();

			return false;
		});
    }

    if ($("#carousel5 .carousel-outer").length) {
    	$("#carousel5 .carousel-outer").carousel3("#carousel5 .left", "#carousel5 .right");

		$("#carousel5 .carousel-outer").bind("swiperight", function () {
			$("#carousel5 .left").click();

			return false;
		});

		$("#carousel5 .carousel-outer").bind("swipeleft", function () {
			$("#carousel5 .right").click();

			return false;
		});
    }

    if ($("#carousel6 .carousel-outer").length) {
    	$("#carousel6 .carousel-outer").carousel4("#carousel6 .left", "#carousel6 .right");

		$("#carousel6 .carousel-outer").bind("swiperight", function () {
			$("#carousel6 .left").click();

			return false;
		});

		$("#carousel6 .carousel-outer").bind("swipeleft", function () {
			$("#carousel6 .right").click();

			return false;
		});
    }

    if ($("section.inner-product").length) {
    	$("section.inner-product .watch-more-docs").bind("click", function () {
    		$('#product_tabs a[href="#descr"]').tab('show');
    	});

    	$("section.inner-product .watch-more-characters").bind("click", function () {
    		$('#product_tabs a[href="#teh"]').tab('show');
    	});

    	$("section.inner-product .watch-more-reviews").bind("click", function () {
    		$('#product_tabs a[href="#review"]').tab('show');
    	});
    }

    $(".window-inside").on('click', '.close', function () {
    	$(this).parent().hide();
    	$(this).parent().css({opacity: "0"});
    	$(".window-inside").hide();
    	$("html>body").css({overflow: "auto"});

    	return false;
    });

    open_window($("#favorites-link"), $(".windows-favorites"));
    open_window($("#order-call-link"), $(".windows-call"));


    $(".pick-color-open").bind("click", function () {
        $(this).hide();
        $(".color-picker").show();

        return false;
    });

    $(".pick-color-close").bind("click", function () {
        $(this).parent().hide();
        $(".pick-color-open").show();

        return false;
    });

});