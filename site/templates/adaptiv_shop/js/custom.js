(function( $ ){

    var methods = {
        init : function( options ) {
            return this.each(function(){
                var container = $(this);

                if ($(container).find(".element").length) {
                    var container_count2 = 0;

                    if ($(window).width() > 1200) {
                        if (options.catalog)
                            container_count2 = 4;
                        else
                            container_count2 = 5;
                    } else if ($(window).width() > 960) {
                        if (options.catalog)
                            container_count2 = 3;
                        else
                            container_count2 = 4;
                    } else if ($(window).width() > 720) {
                        if (options.catalog)
                            container_count2 = 3;
                        else
                            container_count2 = 3;
                    } else if ($(window).width() > 480) {
                        if (options.catalog)
                            container_count2 = 2;
                        else
                            container_count2 = 2;
                    } else {
                        container_count2 = 1;
                    }
                    container.isotope(isotope_settings(container.width(), container_count2));
                    $(container).attr('data-cont_count', '0');
                    $(container).attr('data-catalog', options.catalog);

                    $(window).resize(function() {

                        if ($(window).width() > 1200) {
                            container_count2 = 5;
                        } else if ($(window).width() > 960) {
                            container_count2 = 4;
                        } else if ($(window).width() > 720) {
                            container_count2 = 3;
                        } else if ($(window).width() > 480) {
                            container_count2 = 2;
                        } else {
                            container_count2 = 1;
                        }

                        if ($(container).attr('data-catalog') == 'true')  {
                            if (container_count2 == 5)
                                container_count2 = 4;
                            else if (container_count2 == 4)
                                container_count2 = 3;
                            else if (container_count2 == 3)
                                container_count2 = 3;
                            else if (container_count2 == 2)
                                container_count2 = 2;
                        }

                        if (parseInt($(container).attr('data-cont_count')) != container_count2) {

                            $(container).attr('data-cont_count', container_count2);
                            container.isotope(isotope_settings(container.width(), container_count2))
                            setTimeout(function(){
                                container.isotope(isotope_settings(container.width(), container_count2))
                            }, 400);
                            setTimeout(function(){
                                container.isotope(isotope_settings(container.width(), container_count2))
                            }, 600);
                            setTimeout(function(){
                                container.isotope(isotope_settings(container.width(), container_count2))
                            }, 800);
                            setTimeout(function(){
                                container.isotope(isotope_settings(container.width(), container_count2))
                            }, 1000);
                        }
                    });
                }

            });
        },
        destroy : function( ) {

            return this.each(function(){
                $(window).unbind('.makeIso');
            })

        }
    };

    $.fn.makeIso = function( method ) {

        if ( methods[method] ) {
            return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метод с именем ' +  method + ' не существует для jQuery.makeCarousel' );
        }

    };

})( jQuery );


$(document).ready(function () {
    $('.main-tab-check').on('click', function(){
        var id = $(this).attr('href');

        setTimeout(function(){
            $(id+'_container').makeIso({catalog:false});
        }, 200);
        setTimeout(function(){
            $(id+'_container').makeIso({catalog:false});
        }, 400);
        setTimeout(function(){
            $(id+'_container').makeIso({catalog:false});
        }, 600);
        setTimeout(function(){
            $(id+'_container').makeIso({catalog:false});
        }, 800);
        setTimeout(function(){
            $(id+'_container').makeIso({catalog:false});
        }, 1000);
    });
});

function setTabWithSelect(obj) {
    var id = $(obj).val();
    $('.main-tab-check[href="'+id+'"]').click();
}

function doSubscribe(selector) {
	if ($(selector).length) {
		var url = $(selector).attr('action');
		var data = $(selector).serializeArray();
		$.post(url, data, function(response){
			if (response.success == 'Y') {
				$(selector).find('input[name=email]').val(response.ok_mess)
			} else {
				$(selector).find('input[name=email]').val(response.error)
			}
			console.log(response);
		}, 'json');
	
	}
	return false;
}

function getCatalogItems(elem, containerName) {
	var pagingVar = $(elem).data('paging-var');
	var pagingNumber = $(elem).data('paging-number');
	var pageCount = $(elem).data('page-count');
	var curPage = $(elem).data('cur-page');
	$.post(curPage+'?'+pagingVar+'='+pagingNumber, {container: containerName, action: 'get_catalog_items'}, function(response){
		$('#'+containerName).append(response);
		$(elem).data('paging-number', pagingNumber+1);
		$('#'+containerName).isotope('destroy').makeIso({catalog:'false'}).isotope('reloadItems');
		$(window).trigger('resize');
		if (pagingNumber >= pageCount)
			$(elem).remove();
	});
}

function getNewsItems(elem, containerName) {
	var pagingVar = $(elem).data('paging-var');
	var pagingNumber = $(elem).data('paging-number');
	var pageCount = $(elem).data('page-count');
	var curPage = $(elem).data('cur-page');
	$.post(curPage+'?'+pagingVar+'='+pagingNumber, {container: containerName, action: 'get_items'}, function(response){
		$('#'+containerName).append(response);
		$(elem).data('paging-number', pagingNumber+1);
		if (pagingNumber >= pageCount)
			$(elem).remove();
	});
}

$(function(){
	if ($(".inner-catalog .filter").length) {
		$(".inner-catalog .filter .colors input").bind("click", function () {
			var li = $(this).parent();
			if (li.hasClass("selected")) {
				li.removeClass("selected");
			} else {
				li.addClass("selected");
			}
		});
	}
})

$(document).ready(function () {
	if ($(".carousel-box .carousel-outer").length) {
		$(".carousel-box .carousel-outer").each(function(){
			var id = $(this).parent().attr('id');
			$("#"+id+" .carousel-outer").carousel2("#"+id+" .left", "#"+id+" .right");
			
			$("#"+id+" .carousel-outer a").bind("click", function () {
				$(".main_picture").attr({src: $(this)[0].href});
                $("#big-photo").attr({'href': $(this)[0].href});
				$("#"+id+" .carousel-outer li").removeClass("active");
				$(this).parent().addClass("active");

				return false;
			});
			
			$("#"+id+" .carousel-outer").bind("swiperight", function () {
				$("#"+id+" .left").click();

				return false;
			});
			
			$("#"+id+" .carousel-outer").bind("swipeleft", function () {
				$("#"+id+" .right").click();

				return false;
			});
		});
		
    }
});

//Добавление в корзину
(function( $ ){

    var methods = {
        init : function( ) {
			var arId = [],
				sessid = BX.bitrix_sessid();
			this.each(function(){
                $(this).bind('click.catalog', methods.add2cart);
				var id = $(this).data('item_id');
				if (id > 0) {
					arId.push(id);
				}	
            });
			if ($(arId).length > 0) {
                $.post('/ajax/cartHandler.php', {'action': 'check_in_cart', 'ID': arId, 'sessid': sessid}, function(response) {
                    if (response.success == true && response.in_basket == true) {

						$.each(response.basket_items, function(index, value){
                            if ($('.add2cart_btn_'+value).attr('data-type') == 'card')
							    $('.add2cart_btn_'+value).html('В корзине');
                            else
							    $('.add2cart_btn_'+value).html('Перейти в корзину');

                            $('.add2cart_btn_'+value).unbind('click.catalog');
						});
                    } 
                }, 'json');
            }
			
            return true;

        },
        destroy : function( ) {

            return this.each(function(){
                $(window).unbind('.catalog');
            })

        },
        add2cart : function( ) {
            var button = $(this);
            var id = button.attr('data-item_id'),
				is_offer = button.attr('data-s_offer'),
				in_cart = button.attr('data-in_cart'),
				sessid = BX.bitrix_sessid(),
				action = 'add2cart';

            if (id && action) {
				button.parent().addPreloader();
                $.post('/ajax/cartHandler.php', {'action': action, 'ID': id, 'IS_OFFER': is_offer, 'sessid': sessid}, function(response) {

                    if (response.success == true) {
                        if (button.attr('data-type') == 'card')
                            $(button).html('В корзине');
                        else
						    $('.add2cart_btn_'+id).html('Перейти в корзину');

						$('.cart_top_inline_link').html(response.header_total);
						button.unbind('click.catalog');
						
                    }
                    button.parent().removePreloader();    
                }, 'json');
            }
			return false;
        }
    };

    $.fn.catalog = function( method ) {

        if ( methods[method] ) {
            return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метод с именем ' +  method + ' не существует для jQuery.catalog' );
        }

    };

})( jQuery );

$(function(){
    $('.add2cart').catalog();
});

//Обработка корзины
(function( $ ){

    var methods = {
        init : function( ) {
			this.each(function(){
				var that = $(this),
					basket_id = $(this).attr('id');
					
				if (basket_id > 0) {	
					$('#quantity_down_'+basket_id).bind('click.cart', {'item': that, 'basket_id': basket_id}, methods.qt_down);
					$('#quantity_up_'+basket_id).bind('click.cart', {'item': that, 'basket_id': basket_id}, methods.qt_up);
					$('#QUANTITY_INPUT_'+basket_id).bind('change.cart', {'item': that, 'basket_id': basket_id}, methods.qt_set);
					$('#delete_item_'+basket_id).bind('click.cart', {'item': that, 'basket_id': basket_id}, methods.del);
				}	
            });

            $()
            return true;
        },
        destroy : function( ) {

            return this.each(function(){
                $(window).unbind('.cart');
            })
        },
		qt_down : function( e ) {
            var basket_id = e.data.basket_id;
			
			if (basket_id > 0) {	
				var quantity = parseInt($('#QUANTITY_INPUT_'+basket_id).val()) - 1,
					hidden_quantity = $('#QUANTITY_'+basket_id).val();	
	
				if (quantity > 0) {
					$('#QUANTITY_INPUT_'+basket_id).val(quantity);
					$('#QUANTITY_'+basket_id).val(quantity);
					$('#QUANTITY_INPUT_'+basket_id).change();
				}
			}
			return false;
        },
		qt_up : function( e ) {
            var basket_id = e.data.basket_id;

			if (basket_id > 0) {	
				var quantity = parseInt($('#QUANTITY_INPUT_'+basket_id).val()) + 1,
					hidden_quantity = $('#QUANTITY_'+basket_id).val();	
	
				if (quantity > 0) {
					$('#QUANTITY_INPUT_'+basket_id).val(quantity);
					$('#QUANTITY_'+basket_id).val(quantity);
					$('#QUANTITY_INPUT_'+basket_id).change();
				}
			}
			return false;
        },
        qt_set : function( e ) {
            var basket_id = e.data.basket_id,
				sessid = BX.bitrix_sessid(),
				that = e.data.item;

			if (basket_id > 0) {	
				that.addPreloader();
				var quantity = parseInt($('#QUANTITY_INPUT_'+basket_id).val()),
					hidden_quantity = $('#QUANTITY_'+basket_id).val();	
	
				if (quantity > 0) {
					$.post('/ajax/cartHandler.php', {'action': 'set_qt', 'basket_id': basket_id, 'qt': quantity, 'sessid': sessid}, function(response) {
						if (response.success == true) {
							$('#sum_'+basket_id).html(response.full_price);
							$('#total-price').html(response.total);
							$('#header-total').html(response.header_total);
							$('.cart_top_inline_link').html(response.header_total);
						}	
						that.removePreloader();
					}, 'json');
				}
			}

			return false;
        },
		del : function( e ) {
            var basket_id = e.data.basket_id,
				sessid = BX.bitrix_sessid(),
				that = e.data.item;

			if (basket_id > 0) {	
				that.addPreloader();
				$.post('/ajax/cartHandler.php', {'action': 'del', 'basket_id': basket_id, 'sessid': sessid}, function(response) {
					if (response.success == true) {
						$(that).remove();
						$('#total-price').html(response.total);
						$('#header-total').html(response.header_total);
						$('.cart_top_inline_link').html(response.header_total);
						if (response.total == 0)
							window.location.href = "/personal/cart/";
					}	
					that.removePreloader();
				}, 'json');
			}

			return false;
        }
    };

    $.fn.cart = function( method ) {

        if ( methods[method] ) {
            return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метод с именем ' +  method + ' не существует для jQuery.cart' );
        }

    };

})( jQuery );

$(function(){
    $('#basket_items .cart-item').cart();
});

(function($) { // Функция добавляющая прелоадер
	$.fn.addPreloader = function() {
		return this.each(function() {
			var obj = $(this);
			var hg = $(this).outerHeight(true);
			var wd = $(this).outerWidth(true);
			var preloader = '<div class="preloader_bg"></div>'
			var loading = '<img src="/local/templates/.default/images/loading.gif" class="preloader_progress">'
			obj.children().eq(0).before(preloader);
			preloader = $(this).children().eq(0);
			preloader.css({
				'height':hg + 'px',
				'width':wd + 'px'
			});
			
			preloader.html(loading);
			
			loading = preloader.children('img');
			
			loading.css({
				'padding-top': ((preloader.height() - 26) / 2) + 'px',
				'padding-left': ((preloader.width() - 26) / 2) + 'px',
				'width': '26px'
			});
			
			
			
		});
	};
})(jQuery);

(function($) { // Функция удаляющая прелоадер
	$.fn.removePreloader = function() {
		return this.each(function() {
			obj = $(this).children().eq(0).remove();
		});
	};
})(jQuery);

//Добавление в избранное
(function( $ ){

    var methods = {
        init : function( ) {
			var arId = [],
				sessid = BX.bitrix_sessid();
			this.each(function(){
                $(this).bind('click.favorite', methods.handle);
				var id = $(this).data('item_id');
				if (id > 0) {
					arId.push(id);
				}	
            });
			
			if ($(arId).length > 0) {
                $.post('/ajax/favoriteHandler.php', {'action': 'check_in_favorite', 'ID': arId, 'sessid': sessid}, function(response) {
                    if (response.success == true) {
						$.each(response.favorite_items, function(index, value){
							$('.favorite_'+value).html('<span class="favorites-icon"></span> Убрать из избранных');
						});
                    } 
                }, 'json');
            }
			
            return true;
			
		
            return this.each(function(){
                $(this).bind('click.favorite', methods.handle);
            });
        },
        destroy : function( ) {

            return this.each(function(){
                $(window).unbind('.favorite');
            })

        },
        handle : function( ) {
            var link = $(this);
            var id = link.data('item_id'),
                offer_id = link.data('offer_id'),
				sessid = BX.bitrix_sessid(),
				action = 'check';

            if (id && action) {
                $.post('/ajax/favoriteHandler.php', {'action': action, 'id': id, 'offer_id': offer_id, 'sessid': sessid}, function(response) {

                    if (response.success == true) {
						if (response.action == 'set') {
							if (link.hasClass('add2fav-link'))
								link.html('Убрать из избранных');
							else	
								link.html('<span class="favorites-icon"></span> Убрать из избранных');
						}	
						if (response.action == 'unset') {
							if (link.hasClass('add2fav-link'))
								link.html('В избранное');
							else
								link.html('<span class="favorites-icon"></span> В избранное');
						}	
                    }  
                }, 'json');
            }
			return true;
        },
        init_remove : function( ) {
            this.each(function(){
                $(this).bind('click.favorite', methods.remove);
            });
        },
        remove : function( ) {
            var link = $(this);
            var id = link.attr('data-item_id'),
                offer_id = link.attr('data-offer_id'),
                sessid = BX.bitrix_sessid(),
                action = 'check';
            $(link).addPreloader();
            if (id && action) {
                $.post('/ajax/favoriteHandler.php', {'action': action, 'id': id, 'offer_id': offer_id, 'sessid': sessid}, function(response) {

                    if (response.success == true) {
                        $(link).parent().remove();
                        if ($('.favorite-items .cart-item').length <= 0) {
                            $('.favorite-items').hide();
                            $('.is-empty-fav').show();
                        }

                    }
                }, 'json');
            }
            return true;
        }
    };

    $.fn.favorite = function( method ) {

        if ( methods[method] ) {
            return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метод с именем ' +  method + ' не существует для jQuery.favorite' );
        }

    };

})( jQuery );



//Добавление в сравнение
(function( $ ){

	var methods = {
		init : function() {
			var productIds = [],
				items = this,
				sessid = BX.bitrix_sessid();

			items.each(function(){
				var id = $(this).data('item_id');
				$(this).fadeTo("fast" , 0.2);

				if (id > 0) {
					productIds.push(id);
				}

				$(this).on('click.compare', function(){
					var $this = $(this);

					if ($this.hasClass('product-compare-inited')) {
						$(this).fadeTo("fast" , 0.2);

						if ($this.hasClass('product-compare-active')) {	
							window.location.href = "/catalog/compare/";
							/*$.ajax({
								url: '/ajax/compareHandler.php',
								data: {
									action: 'DELETE_FROM_COMPARE_LIST',
									ID: id,
									sessid: sessid
								},
								dataType: 'json',
								type: 'post'
							}).done(function(data){
								if (typeof data.success != 'undefined' && data.success) {
									$this.removeClass('product-compare-active').text($this.data('messageAddCompare'));
									
								}
							}).always(function(){
								$this.fadeTo("fast" , 1);
							});*/
							
						} else {
							console.log('2');
							$.ajax({
								url: '/ajax/compareHandler.php',
								data: {
									action: 'ADD_TO_COMPARE_LIST',
									ID: id,
									sessid: sessid
								},
								dataType: 'json',
								type: 'post'
							}).done(function(data){
								if (typeof data.success != 'undefined' && data.success) {
									$this.addClass('product-compare-active').text($this.data('messageDeleteCompare'));
								}
							}).always(function(){
								$this.fadeTo("fast" , 1);
							});
						}
						
					}
				});
			});
			//console.log(productIds.length);
			if (productIds.length) {
				console.log('3');
				$.ajax({
					url: '/ajax/compareHandler.php',
					data: {
						action: 'GET_COMPARE_IDS',
						sessid: sessid
					},
					dataType: 'json',
					type: 'post'
				}).done(function(data){
					if (typeof data.success != 'undefined' && data.success) {
						items.each(function(){
							var id = $(this).data('item_id');

							if ($.inArray(id, data.ids) >= 0) {
								$(this).addClass('product-compare-active').text($(this).data('messageDeleteCompare'));
							}

							$(this).fadeTo("fast" , 1).addClass('product-compare-inited');
						});
					}
				}).always(function(){
				});
			}

			return this;
		}
	};

	$.fn.productCompare = function( method ) {
		if ( methods[method] ) {
			return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Метод с именем ' +  method + ' не существует для jQuery.productCompare' );
		}
	};
})( jQuery );

$(function(){
    $('.favorites').favorite();
    $('.compare-link').productCompare();
});
$(document).ready(function () {
	if ($("section.inner-product").length) {
		$("section.inner-product .watch-more-info").bind("click", function () {
			$('#product_tabs a[href="#descr"]').tab('show');
		});
	}
});

(function( $ ){

    var methods = {
        init : function( ) {
            return this.each(function(){
                //$(this).bind('click.makeCarousel', methods.handle);
                var id = $(this).data('id'),
                    carousel = $(this);
                if ($(carousel).find(".carousel-outer").length) {
                    $(carousel).find(".carousel-outer").carousel2(".carousel-left-"+id, ".carousel-right-"+id);

                    $(carousel).find(".carousel-outer a").bind("click", function () {
                        $(".main-picture-"+id).attr({src: $(this)[0].href});
                        $(carousel).find(".carousel-outer li").removeClass("active");
                        $(this).parent().addClass("active");

                        return false;
                    });

                    $(carousel).find(".carousel-outer").bind("swiperight", function () {
                        $(".carousel-left-"+id).click();

                        return false;
                    });

                    $(carousel).find(".carousel-outer").bind("swipeleft", function () {
                        $(".carousel-right-"+id).click();

                        return false;
                    });
                }
            });
        },
        destroy : function( ) {

            return this.each(function(){
                $(window).unbind('.makeCarousel');
            })

        }
    };

    $.fn.makeCarousel = function( method ) {

        if ( methods[method] ) {
            return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метод с именем ' +  method + ' не существует для jQuery.makeCarousel' );
        }

    };

})( jQuery );

function open_window_photo_ex(link, windows_div) {
    link.bind("click", function () {
        var id = $(link).attr('data-id');console.log(id);
        $(".window-inside-bigphoto-"+id).show();
        $("html>body").css({overflow: "hidden"});
        windows_div = $(".window-inside-bigphoto-"+id+" .windows-photo");
        if ($(windows_div).length) {
            $(".window-inside-bigphoto-"+id+" .main-picture-"+id).attr({'src': $(link).attr('href')});
            windows_div.show();
            windows_div.animate({opacity: "1"});

            if ((windows_div.outerHeight() + 100) > $(window).outerHeight()) {
                windows_div.css({top: "50px"});
            } else {
                windows_div.css({marginTop: "-" + Math.ceil(windows_div.outerHeight() / 2) + "px"});
            }
        }
        return false;
    });
}


$(document).ready(function () {

    $('.carousel-bigphoto').makeCarousel();

    $(".window-inside2").on('click', '.close', function () {
        $(this).parent().hide();
        $(this).parent().css({opacity: "0"});
        $(".window-inside2").hide();
        $("html>body").css({overflow: "auto"});

        return false;
    });


    $('#order-call-link').click(function(){
        $('#request-call-form-note').html('');
        $('#request-call-form .input-box input').removeClass('error').val('');
        $('#request-call-form').show();
    });

    open_window($(".favorites-link"), $(".windows-favorites"));
    $('.favorites-link').click(function(){
        $('#windows-favorites-popup .cart-items').html('');
        $('#windows-favorites-popup .cart-items').addPreloader();
        var data = {
            action: 'get_list',
            sessid: BX.bitrix_sessid()
        }
        $.post("/ajax/favoriteHandler.php", data, function(response) {
            $('#windows-favorites-popup .cart-items-wrap').html(response);
        }, 'html');
    });


    //Авторизация на сайте
    open_window($(".login-link"), $(".window-inside .windows-login"));
    $('.login-link').click(function(){
            $('#login-form-messages').html('');
            $('#login-form-back-url-input').val(window.location.pathname);
            $('#login-form input').removeClass('error');
        }
    );


    //Регистрация на сайте
    open_window($(".registration-link"), $(".windows-register"));
    $('.registration-link').click(function(){
            $('#registration-form-messages').html('');
            $('#registration-form input').removeClass('error');
        }
    );


    //Забыли пароль
    open_window($(".forgot-link"), $(".windows-forgot"));
    $('.forgot-link').click(function(){
            $('#forgot-form-messages').html('');
            $('#forgot-form input').removeClass('error');
        }
    );

    //Изменить данные профиля
    open_window($(".profile-edit-link"), $(".windows-edit"));
    $('.profile-edit-link').click(function(){
            $('#profile-edit-form-messages').html('');
            $('#profile-edit-form input').removeClass('error');
            $('#profile-edit-form-input-name').val($('#profile-edit-data-name').val());
            $('#profile-edit-form-input-last_name').val($('#profile-edit-data-last_name').val());
            $('#profile-edit-form-input-email').val($('#profile-edit-data-email').val());
            $('#profile-edit-form-input-phone').val($('#profile-edit-data-phone').val());
        }
    );


    //Изменить пароль
    open_window($(".edit-password-link"), $(".windows-edit-password"));
    $('.edit-password-link').click(function(){
            $('#edit-password-form-messages').html('');

            $('#edit-password-form-input-old_password').val('');
            $('#edit-password-form-input-new_password').val('');
            $('#edit-password-form-input-confirm_password').val('');
            $('#edit-password-form input').removeClass('error');
        }
    );


    //Список заказов
    open_window($(".history-link"), $(".windows-history"));
    $('.history-link').click(function(){
        var data = {
            action: 'get_list',
            sessid: BX.bitrix_sessid()
        }
        $('#windows-history-content').addPreloader();
        $.post("/ajax/orderHandler.php", data, function(response) {
            $('#windows-history-content .slide-div').html(response);
            $('#windows-history-content').removePreloader();
        }, 'html');
    });

    //Заказ
    open_window($(".history-order-link"), $(".windows-history-order"));
    $('.history-order-link').click(function(){
            var data = {
                action: 'get_order',
                id: $(this).data('id'),
                sessid: BX.bitrix_sessid()
            }
            $('#windows-history-order-content').addPreloader();
            $.post("/ajax/orderHandler.php", data, function(response) {
                $('#windows-history-order-content').html(response);
            }, 'html');
        }
    );



    //Редатирование алреса доставки
    open_window($(".edit-address-link"), $(".windows-edit-address"));
    $('.edit-address-link').click(function(){
            var data = {
                action: 'edit_address',
                id: $(this).data('id'),
                sessid: BX.bitrix_sessid()
            }
            $('#windows-edit-address-content').addPreloader();
            $.post("/ajax/orderHandler.php", data, function(response) {
                $('#windows-edit-address-content').html(response);
            }, 'html');
        }
    );


    //Удалить адрес доставки
    open_window($(".del-address-link"), $(".windows-delete"));
    $('.del-address-link').click(function(){
        $('#delete-form-messages').html('');
        $('#delete-form-profile-id').val($(this).data('id'));
    });



    //Купить в 1 клик
    open_window($(".click-buy-link"), $(".windows-buy1click"));
    $('.click-buy-link').click(function(){
        $('#buy1click-input-id').val($(this).data('id'));
        $('#buy1click-input-offer_id').val($(this).data('offer_id'));
        $('#buy1click-input-price').val($(this).data('price'));
    });


    //Купить в 1 клик из корзины
    open_window($(".click-buy-cart-link"), $(".windows-buy1click"));
    $('.click-buy-cart-link').click(function(){
        $('#buy1click-input-cart').val('Y');
    });


    var curdate = new Date();
    curdate.setDate(curdate.getDate()+1);

    var yearend = parseInt(curdate.getFullYear()),
        monthend = parseInt(curdate.getMonth()),
        dayend = parseInt(curdate.getDate());

    $('.count-down-timer-content').county({ endDateTime: new Date(yearend, monthend, dayend), reflection: false, animation: 'fade', theme: 'black' });

    open_window_photo_ex($("#big-photo"), $(".windows-photo"));
//open_window_photo($("#big-photo"), $(".windows-photo"));
});




