<?
//Подключим объявленные константы для сайта
define('IBLOCK_ID_CATALOG', '#CATALOG_IBLOCK_ID#');
define('IBLOCK_ID_OFFERS', '3');
define('IBLOCK_ID_BANNERS', '#SLIDER_IBLOCK_ID#');
define('IBLOCK_ID_MAIN_SLIDER', '#SLIDER_IBLOCK_ID#');
define('IBLOCK_ID_NEWS', '#NEWS_IBLOCK_ID#');
define('IBLOCK_ID_ACTIONS', '#MAIN_BANNER_IBLOCK_ID#');

define('SITE_PHONE', '8 (495) 225 63 50');

define('SOCSERVICE_LINK_FB', 'http://facebook.com');
define('SOCSERVICE_LINK_VK', 'http://vk.com');
define('SOCSERVICE_LINK_OK', 'http://ok.com');
define('SOCSERVICE_LINK_TWITTER', 'http://twitter.com/');
define('SOCSERVICE_LINK_G', 'http://plus.google.com/');

define('REQUEST_CALL_FORM_ID', '#REQUEST_CALL_FORM_ID#');
define('PRODUCT_DEFAULT_RATING', 3);