<?
define("NOT_CHECK_PERMISSIONS", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
__IncludeLang($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . "/lang/".LANGUAGE_ID."/ajax.php", false, true);

$arResult = array();
$arResult['success'] = 'N';
if ($_SERVER["REQUEST_METHOD"]=="POST" && ($_POST["action"] == 'buy1click') && check_bitrix_sessid())
{
    $postData = $_POST;
    $name = trim(htmlspecialchars($postData["name"]));
    $phone = trim(htmlspecialchars($postData["phone"]));
	if (!strlen($phone))
		$arResult['error']['phone'] = GetMessage('T_B1C_ERROR_EMPTY_PHONE');
	if (!strlen($name))
		$arResult['error']['name'] = GetMessage('T_B1C_ERROR_EMPTY_NAME');

    if (empty($arResult['error']) && CModule::IncludeModule('form') && CModule::IncludeModule('iblock')) {
        if (isset($postData['cart']) && ($postData['cart'] == 'Y')) {
            if (CModule::IncludeModule('sale')) {
                $arFields = array(
                    'USER_NAME' => $postData["name"],
                    'USER_PHONE' => $postData["phone"],
                );

                $dbBasketItems = CSaleBasket::GetList(
                    array(
                        "NAME" => "ASC",
                        "ID" => "ASC"
                    ),
                    array(
                        "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                        "LID" => SITE_ID,
                        "ORDER_ID" => "NULL"
                    ),
                    false,
                    false,
                    array()
                );
                if ($dbBasketItems->SelectedRowsCount() > 0) {
                    $tmpCart = '<table border="1" width="100%">';
                    $tmpCart .= '<tr>';
                    $tmpCart .= '<td>ID</td>';
                    $tmpCart .= '<td>Имя</td>';
                    $tmpCart .= '<td>Количество</td>';
                    $tmpCart .= '<td>Цена</td>';
                    $tmpCart .= '<td>Сумма</td>';
                    $tmpCart .= '</tr>';
                    $total = 0;
                    $currency = 0;
                    while ($arItem = $dbBasketItems->Fetch())
                    {
                        $dbRes = CIBlockElement::GetByID($arItem['PRODUCT_ID']);
                        if ($obRes = $dbRes->GetNextElement()) {
                            $arRes = $obRes->GetFields();
                            $arRes['PROPERTIES'] = $obRes->GetProperties();
                            $initProduct = true;
                            if ($arRes['IBLOCK_ID'] == IBLOCK_ID_OFFERS) {
                                $initProduct = false;
                                if (intval($arRes['PROPERTIES']['CML2_LINK']['VALUE'])) {
                                    $dbTmpRes = CIBlockElement::GetByID($arRes['PROPERTIES']['CML2_LINK']['VALUE']);
                                    if ($arTmpRes = $dbTmpRes->GetNext()) {
                                        $arRes['DETAIL_PAGE_URL'] = $arTmpRes['DETAIL_PAGE_URL'];
                                        $initProduct = true;
                                    }
                                }
                            }
                            if ($initProduct) {
                                $tmpCart .= '<tr>';
                                $tmpCart .= '<td>';
                                $tmpCart .= $arRes['ID'];
                                $tmpCart .= '</td>';
                                $tmpCart .= '<td>';
                                $tmpCart .= '<a href="http://'.$_SERVER['HTTP_HOST'].$arRes['DETAIL_PAGE_URL'].'">';
                                $tmpCart .= $arRes['NAME'];
                                $tmpCart .= '</td>';
                                $tmpCart .= '<td>';
                                $tmpCart .= intval($arItem['QUANTITY']);
                                $tmpCart .= '</td>';
                                $tmpCart .= '<td>';
                                $tmpCart .= CurrencyFormat($arItem['PRICE'], $arItem['CURRENCY']);
                                $tmpCart .= '</td>';
                                $tmpCart .= '<td>';
                                $tmpCart .= CurrencyFormat($arItem['PRICE']*$arItem['QUANTITY'], $arItem['CURRENCY']);
                                $tmpCart .= '</td>';
                                $tmpCart .= '</tr>';

                                $total += $arItem['PRICE']*$arItem['QUANTITY'];
                                $currency = $arItem['CURRENCY'];
                            }
                        }
                        $tmpCart .= '</tr>';
                    }

                    $tmpCart .= '<tr>';
                    $tmpCart .= '<td colspan="4"><b>Всего</b></td>';
                    $tmpCart .= '<td>'.CurrencyFormat($total, $currency).'</td>';
                    $tmpCart .= '</tr>';

                    $tmpCart .= '</table>';
                    $arFields['CART_ITEMS'] = $tmpCart;

                    if ($total > 0) {
                        if (!CEvent::Send('BUY1CLICK_CART',SITE_ID, $arFields, "Y"))
                            $arResult['error']['add'] = GetMessage('T_B1C_ERROR_ADD');
                    } else $arResult['error']['add'] = GetMessage('T_B1C_ERROR_ADD');

                } else $arResult['error']['add'] = GetMessage('T_B1C_ERROR_ADD');
            }
        } elseif (intval($postData['id'])) {
            $arFields = array(
                'USER_NAME' => $postData["name"],
                'USER_PHONE' => $postData["phone"],
            );
            $dbRes = CIBlockElement::GetByID($postData['id']);
            if ($arRes = $dbRes->GetNext()) {
                $arFields['ID'] = $arRes['ID'];
                $arFields['IBLOCK_ID'] = $arRes['IBLOCK_ID'];
                $arFields['NAME'] = $arRes['NAME'];
                $arFields['DETAIL_PAGE_URL'] = $arRes['DETAIL_PAGE_URL'];
            }

            if (intval($postData['offer_id'])) {
                $dbRes = CIBlockElement::GetByID($postData['offer_id']);
                if ($arRes = $dbRes->Fetch()) {
                    $arFields['OFFER_ID'] = $arRes['ID'];
                    $arFields['OFFER_NAME'] = $arRes['NAME'];
                }
            }

            if (!empty($postData['price']))
                $arFields['PRICE'] = $postData['price'];

            if (!CEvent::Send('BUY1CLICK',SITE_ID, $arFields, "Y"))
                $arResult['error']['add'] = GetMessage('T_B1C_ERROR_ADD');
        }
    }
	if (empty($arResult['error']))
	{
		$arResult['success'] = 'Y';
		$arResult['message'] = GetMessage('T_B1C_SUCCESS_ADD');
	}
}
else
{
    $arResult['error']['add'] = GetMessage('T_B1C_ERROR_ADD');
}
echo json_encode($arResult);
die();