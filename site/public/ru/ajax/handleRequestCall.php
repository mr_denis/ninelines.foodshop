<?
define("NOT_CHECK_PERMISSIONS", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
__IncludeLang($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . "/lang/".LANGUAGE_ID."/ajax.php", false, true);

$arResult = array();
$arResult['success'] = 'N';
if ($_SERVER["REQUEST_METHOD"]=="POST" && ($_POST["action"] == 'request_call') && check_bitrix_sessid())
{
    $name = trim(htmlspecialchars($_POST["name"]));
    $phone = trim(htmlspecialchars($_POST["phone"]));
	if (!strlen($phone))
		$arResult['error']['phone'] = GetMessage('T_RC_ERROR_EMPTY_PHONE');
	if (!strlen($name))
		$arResult['error']['name'] = GetMessage('T_RC_ERROR_EMPTY_NAME');

    if (empty($arResult['error']) && CModule::IncludeModule('form')) {
        $arValues = array (
            "#NAME#" => $name,
            "#TELEPHONE#" => $phone,
        );

        if ($result_id = CFormResult::Add(REQUEST_CALL_FORM_ID, $arValues)) {
            CFormResult::Mail($result_id);
        }
        else
        {
            $arResult['error']['add'] = GetMessage('T_RC_ERROR_ADD');
            global $strError;
            $arResult['error']['debug'] = $strError;
        }
    }
	if (empty($arResult['error']))
	{
		$arResult['success'] = 'Y';
		$arResult['message'] = GetMessage('T_RC_SUCCESS_ADD');
	}
}
else
{
    $arResult['error']['add'] = GetMessage('T_RC_ERROR_ADD');
}
echo json_encode($arResult);
die();