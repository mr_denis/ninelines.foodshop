<?
define("NOT_CHECK_PERMISSIONS", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$arResult = array();
$arResult['success'] = false;

if ($_SERVER["REQUEST_METHOD"]=="POST" && check_bitrix_sessid())
{
	switch ($_POST["action"])
	{
		case 'add2cart':
			if (intval($_POST["ID"]))
			{
				$arResult['success'] = true;
				if (\Bitrix\Main\Loader::includeModule("sale") && \Bitrix\Main\Loader::includeModule("catalog"))
				{
					$productID = $_POST["ID"];
					$QUANTITY = 0;
					$product_properties = array();
					$intProductIBlockID = intval(CIBlockElement::GetIBlockByID($productID));
					
					
					if (0 < $intProductIBlockID)
					{
						$arRes = CCatalogProduct::GetByIDEx($productID);
						$arProps = array();
						foreach ($arRes['PROPERTIES'] as $code => $arProp)
							if ($code != 'CML2_LINK' && $code != 'MORE_PHOTO')
								$arProps[] = $code;
								
						if ($intProductIBlockID == IBLOCK_ID_OFFERS)
						{
							$product_properties = CIBlockPriceTools::GetOfferProperties(
								$productID,
								IBLOCK_ID_CATALOG,
								$arProps,
								array()
							);
						}
						
						$QUANTITY = 1;
					}
					else
					{
						$arResult['error'][] = 'element_not_found';
						$arResult['success'] = false;
					}

					if ($arResult['success'])
					{
						if(!Add2BasketByProductID($productID, $QUANTITY, array("LID" => SITE_ID), $product_properties))
						{
							if ($ex = $APPLICATION->GetException())
								$arResult['error'][] = $ex->GetString();
							else
								$arResult['error'][] = 'add_to_basket';
							$arResult['success'] = false;
						}
						else
						{
							$arResult['item_count'] = 0;
							$total = 0;
							$currency = '';
							$dbRes = CSaleBasket::GetList(array(), array("FUSER_ID" => CSaleBasket::GetBasketUserID(), "ORDER_ID" => "NULL", "LID" => SITE_ID));
							while ($arRes = $dbRes->Fetch()) 
							{
								$arResult['item_count']++;
								$total += $arRes['PRICE']*$arRes['QUANTITY'];
								$currency = $arRes['CURRENCY'];
							}
							$total = SaleFormatCurrency(intval($total), $currency);
							$arResult['header_total'] = $arResult['item_count'].' '.sklonenie($arResult['item_count'], 'товар', 'товара', 'товаров').' на '.$total;
						}
					}
				}
				else
					$arResult['error'][] = 'module_include';
			}		
			break;
		case 'check_in_cart':
			if (\Bitrix\Main\Loader::includeModule("sale"))
			{
				$arResult['success'] = true;
				$arResult['in_basket'] = false;
				if (!empty($_POST['ID']) && is_array($_POST['ID']))
				{
					$dbBasketRes = CSaleBasket::GetList(array(), array("PRODUCT_ID" => $_POST['ID'], "FUSER_ID" => CSaleBasket::GetBasketUserID(), "ORDER_ID" => "NULL", "LID" => SITE_ID));
					while ($arBasketRes = $dbBasketRes->Fetch()) {
						$arResult['basket_items'][] = $arBasketRes['PRODUCT_ID'];
						$arResult['in_basket'] = true;
					}	
				}
			} 
			else
				$arResult['error'][] = 'module_include';
			break;
		case 'set_qt':
			if (\Bitrix\Main\Loader::includeModule("sale"))
			{
				if (!empty($_POST['qt']) && intval($_POST['qt'])&& intval($_POST['basket_id']))
				{
					$dbBasketRes = CSaleBasket::GetList(array(), array("ID" => $_POST['basket_id'], "FUSER_ID" => CSaleBasket::GetBasketUserID(), "ORDER_ID" => "NULL", "LID" => SITE_ID));
					if ($arBasketRes = $dbBasketRes->Fetch()) 
					{				
						if (CSaleBasket::Update($arBasketRes["ID"], array('QUANTITY' => intval($_POST['qt']))))
						{
							$arResult['success'] = true;
							$arResult['full_price'] = SaleFormatCurrency(intval($arBasketRes['PRICE'] * $_POST['qt']), $arBasketRes['CURRENCY']);
							
							$arResult['item_count'] = 0;
							$total = 0;
							$currency = '';
							$dbRes = CSaleBasket::GetList(array(), array("FUSER_ID" => CSaleBasket::GetBasketUserID(), "ORDER_ID" => "NULL", "LID" => SITE_ID));
							while ($arRes = $dbRes->Fetch()) 
							{
								$arResult['item_count']++;
								$total += $arRes['PRICE']*$arRes['QUANTITY'];
								$currency = $arRes['CURRENCY'];
							}
							$arResult['total'] = SaleFormatCurrency(intval($total), $currency);
							$arResult['header_total'] = $arResult['item_count'].' '.sklonenie($arResult['item_count'], 'товар', 'товара', 'товаров').' на '.$arResult['total'];
						}	
					}	
				}
			} 
			else
				$arResult['error'][] = 'module_include';
			break;
		case 'del':
			if (\Bitrix\Main\Loader::includeModule("sale"))
			{
				if (intval($_POST['basket_id']))
				{
					$dbBasketRes = CSaleBasket::GetList(array(), array("ID" => $_POST['basket_id'], "FUSER_ID" => CSaleBasket::GetBasketUserID(), "ORDER_ID" => "NULL", "LID" => SITE_ID));
					if ($arBasketRes = $dbBasketRes->Fetch()) 
					{				
						if (CSaleBasket::Delete($arBasketRes["ID"]))
						{
							$arResult['success'] = true;
							
							$arResult['item_count'] = 0;
							$total = 0;
							$currency = '';
							$dbRes = CSaleBasket::GetList(array(), array("FUSER_ID" => CSaleBasket::GetBasketUserID(), "ORDER_ID" => "NULL", "LID" => SITE_ID));
							while ($arRes = $dbRes->Fetch()) 
							{
								$arResult['item_count']++;
								$total += $arRes['PRICE']*$arRes['QUANTITY'];
								$currency = $arRes['CURRENCY'];
							}
							$arResult['total'] = SaleFormatCurrency(intval($total), $currency);
							$arResult['header_total'] = $arResult['item_count'].' '.sklonenie($arResult['item_count'], 'товар', 'товара', 'товаров').' на '.$arResult['total'];
						}	
					}	
				}
			} 
			else
				$arResult['error'][] = 'module_include';
			break;	
	}
}
echo json_encode($arResult);
die();