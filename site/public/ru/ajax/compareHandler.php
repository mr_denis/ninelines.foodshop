<?
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

define("NOT_CHECK_PERMISSIONS", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$arResult = array();
$arResult['success'] = false;

if ($_SERVER["REQUEST_METHOD"]=="POST" && check_bitrix_sessid())
{
	switch ($_REQUEST["action"]) {
		case 'ADD_TO_COMPARE_LIST':
			$id = intval($_REQUEST['ID']);
			if (!array_key_exists($id, $_SESSION['CATALOG_COMPARE_LIST'][IBLOCK_ID_CATALOG]["ITEMS"]))
			{
				CModule::IncludeModule("iblock");
				CModule::IncludeModule("cattalog");
				//SELECT
				$arSelect = array(
					"ID",
					"IBLOCK_ID",
					"IBLOCK_SECTION_ID",
					"NAME",
					"DETAIL_PAGE_URL",
				);
				//WHERE
				$arFilter = array(
					"ID" => $id,
					"IBLOCK_LID" => SITE_ID,
					"IBLOCK_ACTIVE" => "Y",
					"ACTIVE_DATE" => "Y",
					"ACTIVE" => "Y",
					"CHECK_PERMISSIONS" => "Y",
				);

				$arOffers = CIBlockPriceTools::GetOffersIBlock(IBLOCK_ID_CATALOG);
				$OFFERS_IBLOCK_ID = $arOffers? $arOffers["OFFERS_IBLOCK_ID"]: 0;

				if($arOffers)
					$arFilter["IBLOCK_ID"] = array(IBLOCK_ID_CATALOG, $arOffers["OFFERS_IBLOCK_ID"]);
				else
					$arFilter["IBLOCK_ID"] = IBLOCK_ID_CATALOG;

				$rsElement = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
				$rsElement->SetUrlTemplates($arParams["DETAIL_URL"]);
				$arElement = $rsElement->GetNext();

				$arMaster = false;
				if($arElement && $arElement["IBLOCK_ID"] == $OFFERS_IBLOCK_ID)
				{
					$rsMasterProperty = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement["ID"], array(), array("ID" => $arOffers["OFFERS_PROPERTY_ID"], "EMPTY" => "N"));
					if($arMasterProperty = $rsMasterProperty->Fetch())
					{
						$rsMaster = CIBlockElement::GetList(
							array()
							,array(
								"ID" => $arMasterProperty["VALUE"],
								"IBLOCK_ID" => $arMasterProperty["LINK_IBLOCK_ID"],
								"ACTIVE" => "Y",
							)
						,false, false, $arSelect);
						$rsMaster->SetUrlTemplates($arParams["DETAIL_URL"]);
						$arMaster = $rsMaster->GetNext();
					}
				}

				if($arMaster)
				{
					$arMaster["NAME"] = $arElement["NAME"];
					$_SESSION['CATALOG_COMPARE_LIST'][IBLOCK_ID_CATALOG]["ITEMS"][$id] = $arMaster;
				}
				elseif($arElement)
				{
					$_SESSION['CATALOG_COMPARE_LIST'][IBLOCK_ID_CATALOG]["ITEMS"][$id] = $arElement;
				}

				$arResult['success'] = true;
			}

			break;
		case "DELETE_FROM_COMPARE_LIST":
			$id = intval($_REQUEST['ID']);
			unset($_SESSION['CATALOG_COMPARE_LIST'][IBLOCK_ID_CATALOG]["ITEMS"][$id]);
			$arResult['success'] = true;
			break;
		case "GET_COMPARE_IDS":
			if (
				isset($_SESSION['CATALOG_COMPARE_LIST'][IBLOCK_ID_CATALOG]["ITEMS"])
				&& is_array($_SESSION['CATALOG_COMPARE_LIST'][IBLOCK_ID_CATALOG]["ITEMS"])
			) {
				$arResult['ids'] = array_keys($_SESSION['CATALOG_COMPARE_LIST'][IBLOCK_ID_CATALOG]["ITEMS"]);
			} else {
				$arResult['ids'] = array();
			}
			$arResult['success'] = true;
			break;
	}
}
echo json_encode($arResult);
die();