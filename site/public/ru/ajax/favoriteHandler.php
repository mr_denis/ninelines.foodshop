<?
define("NOT_CHECK_PERMISSIONS", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
__IncludeLang($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . "/lang/".LANGUAGE_ID."/ajax.php", false, true);

$arResult = array();
$arResult['success'] = false;

if ($_SERVER["REQUEST_METHOD"]=="POST" && check_bitrix_sessid())
{
	switch ($_POST["action"])
	{
		case 'get_list':
            if (!empty($_SESSION['USER_FAVORITES']))
            {
                global $favFilter;
                $favFilter = array('ID' => $_SESSION['USER_FAVORITES']);

                $APPLICATION->IncludeComponent(
                    "bitrix:catalog.section",
                    "favorites",
                    array(
                        "USER_FAVORITES" => $_SESSION['USER_FAVORITES'],
                        "BY_LINK" => "Y",
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => "#CATALOG_IBLOCK_ID#",
                        "ELEMENT_SORT_FIELD" => "sort",
                        "ELEMENT_SORT_ORDER" => "asc",
                        "ELEMENT_SORT_FIELD2" => "active_from",
                        "ELEMENT_SORT_ORDER2" => "asc",
                        "PROPERTY_CODE" => array(
                            0 => "BRAND_REF",
                            1 => "NEWPRODUCT",
                            2 => "SALELEADER",
                            3 => "SPECIALOFFER",
                            4 => "ARTNUMBER",
                            5 => "MANUFACTURER",
                            6 => "MATERIAL",
                            7 => "COLOR",
                            8 => "RECOMMEND",
                            9 => "MINIMUM_PRICE",
                            10 => "MAXIMUM_PRICE",
                            11 => "DISCOUNT",
                            12 => "PRODUCT_OF_DAY",
                            13 => "PRODUCT_ACTION",
                            14 => "OLD_PRICE",
                        ),
                        "META_KEYWORDS" => "-",
                        "META_DESCRIPTION" => "-",
                        "BROWSER_TITLE" => "-",
                        "INCLUDE_SUBSECTIONS" => "A",
                        "BASKET_URL" => "",
                        "ACTION_VARIABLE" => "",
                        "PRODUCT_ID_VARIABLE" => "",
                        "SECTION_ID_VARIABLE" => "",
                        "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                        "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
                        "FILTER_NAME" => 'favFilter',
                        "CACHE_TYPE" => "N",
                        "CACHE_TIME" =>  0,
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "N",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "DISPLAY_COMPARE" => "N",
                        "PAGE_ELEMENT_COUNT" => "15",
                        "LINE_ELEMENT_COUNT" => "",
                        "PRICE_CODE" => array(
                            0 => "BASE",
                        ),
                        "USE_PRICE_COUNT" => "N",
                        "SHOW_PRICE_COUNT" => "",
                        "PRICE_VAT_INCLUDE" => "N",
                        "USE_PRODUCT_QUANTITY" => "N",
                        "ADD_PROPERTIES_TO_BASKET" => "N",
                        "PARTIAL_PRODUCT_PROPERTIES" => "N",
                        "PRODUCT_PROPERTIES" => array(
                        ),
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "",
                        "PAGER_SHOW_ALL" => "N",
                        "OFFERS_CART_PROPERTIES" => array(
                        ),
                        "OFFERS_FIELD_CODE" => array(
                        ),
                        "OFFERS_PROPERTY_CODE" => array(
                            0 => "ARTNUMBER",
                            1 => "COLOR_REF",
                        ),
                        "LIST_OFFERS_PROPERTY_CODE" => array(
                            2 => "COLOR_REF",
                            4 => "ARTNUMBER",
                            5 => "",
                        ),
                        "OFFERS_SORT_FIELD" => "name",
                        "OFFERS_SORT_ORDER" => "asc",
                        "OFFERS_SORT_FIELD2" => "sort",
                        "OFFERS_SORT_ORDER2" => "asc",
                        "OFFERS_LIMIT" => "0",
                        "SECTION_ID" => "",
                        "SECTION_CODE" => "",
                        "SECTION_URL" => "",
                        "DETAIL_URL" => "",
                        "CONVERT_CURRENCY" => "N",
                        "CURRENCY_ID" => "RUB",
                        "HIDE_NOT_AVAILABLE" => "N",
                        "LABEL_PROP" => "-",
                        "ADD_PICT_PROP" => "-",
                        "PRODUCT_DISPLAY_MODE" => "Y",
                        "OFFER_ADD_PICT_PROP" => "",
                        "OFFER_TREE_PROPS" => array(
                            2 => "COLOR_REF",
                            3 => "",
                        ),
                        "PRODUCT_SUBSCRIPTION" => "N",
                        "SHOW_DISCOUNT_PERCENT" => "N",
                        "SHOW_OLD_PRICE" => "Y",
                        "MESS_BTN_BUY" => "",
                        "MESS_BTN_ADD_TO_BASKET" => "",
                        "MESS_BTN_SUBSCRIBE" => "",
                        "MESS_BTN_DETAIL" => "",
                        "MESS_NOT_AVAILABLE" => "",
                        "TEMPLATE_THEME" => "",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "SECTION_USER_FIELDS" => array(
                            0 => "",
                            1 => "",
                        ),
                        "SHOW_ALL_WO_SECTION" => "Y",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "N",
                        "AJAX_OPTION_HISTORY" => "N",
                        "SET_META_KEYWORDS" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                    ),
                    false
                );
                die();
            }
            else
            {?>
                <div class="cart-items is-empty-fav">
                    <p class="cart-is-empty fav-is-empty "><?=GetMessage('T_FAVORITE_LIST_EMPTY')?></p>
                </div>
            <?}
            die();
            break;
		case 'check_in_favorite':
			if (!empty($_POST['ID']) && is_array($_POST['ID']))
			{
				foreach ($_POST['ID'] as $id)
					if (in_array($id, $_SESSION['USER_FAVORITES']))
					{
						$arResult['favorite_items'][] = $id;
						$arResult['success'] = true;
					}	
			}
			break;
		case 'check':
			if (intval($_POST["id"]) && \Bitrix\Main\Loader::includeModule("catalog"))
			{
				$productID = intval($_POST["id"]);
				$offerID = intval($_POST["offer_id"]);
				if (empty($offerID)) $offerID = $productID;

				if ($productID)
				{
					$arResult['success'] = true;
					global $_SESSION;
					if (isset($_SESSION['USER_FAVORITES'][$offerID]))
					{
						unset($_SESSION['USER_FAVORITES'][$offerID]);
						$arResult['action'] = 'unset';
					}
					else 
					{
						$_SESSION['USER_FAVORITES'][$offerID] = $productID;
						$arResult['action'] = 'set';
					}
				}
			}		
			break;
	}
}
echo json_encode($arResult);
die();