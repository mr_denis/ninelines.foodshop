<?
define("NOT_CHECK_PERMISSIONS", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$arResult['success'] = 'N';
if ($_SERVER["REQUEST_METHOD"]=="POST" && ($_POST["action"] == 'subscribe') && check_bitrix_sessid())
{
	if (!CModule::IncludeModule("subscribe"))
		$arResult['error'] = "Модуль подписки не найден";
	elseif (check_email($_POST["email"])) 
	{
		$arFields = Array(
			"USER_ID" => ($USER->IsAuthorized()? $USER->GetID():false),
			"FORMAT" => "text",
			"EMAIL" => $_POST["email"],
			"ACTIVE" => "Y",
			"RUB_ID" => array(1, 2),
		);
		$subscr = new CSubscription;

		//can add without authorization
		$ID = $subscr->Add($arFields);
		if($ID>0)
		{
			CSubscription::Authorize($ID);
			$arResult['ok_mess'] = 'Вы подписались на рассылку';
			$arResult['success'] = 'Y';
		}	
		else
			$arResult['error'] = "Не удалось подписаться на рассылку";	
	}
	else
		$arResult['error'] = "Указан некорректный E-Mail";
}
echo json_encode($arResult);
die();