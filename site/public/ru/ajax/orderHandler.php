<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 23.12.14
 * Time: 15:31
 */

define("NOT_CHECK_PERMISSIONS", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
__IncludeLang($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . "/lang/".LANGUAGE_ID."/ajax.php", false, true);
$arResult = array(
    'success' => 'N',
    'errors' => array(),
    'message' => ''
);
if ($_SERVER['REQUEST_METHOD'] == 'POST' && check_bitrix_sessid()) {
    $postData = $_REQUEST;
    global $USER;
    switch ($postData['action']) {
        case 'get_list':
            $APPLICATION->IncludeComponent(
                "bitrix:sale.personal.order.list",
                ".default",
                array(
                    "POPUP" => "Y",
                    "ORDERS_PER_PAGE" => "30",
                    "PATH_TO_PAYMENT" => SITE_DIR."/personal/order/payment/",
                    "PATH_TO_BASKET" => SITE_DIR."/personal/cart/",
                    "SET_TITLE" => "N",
                    "SAVE_IN_SESSION" => "N",
                    "NAV_TEMPLATE" => "",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y H:i",
                    "HISTORIC_STATUSES" => array(
                    ),
                    "CACHE_TYPE" => "Y",
                    "CACHE_TIME" => "0",
                    "CACHE_GROUPS" => "Y",
                    "PATH_TO_DETAIL" => "",
                    "PATH_TO_COPY" => "",
                    "PATH_TO_CANCEL" => "",
                ),
                false
            );
            die();
            break;
        case 'get_order':
            if (intval($postData['id'])) {
                $APPLICATION->IncludeComponent(
                    "bitrix:sale.personal.order.detail",
                    "",
                    array(
                        "ACTIVE_DATE_FORMAT" => "d.m.Y H:i",
                        "HISTORIC_STATUSES" => array(
                        ),
                        "CACHE_TYPE" => "Y",
                        "CACHE_TIME" => "3600",
                        "CACHE_GROUPS" => "Y",
                        "ID" => $postData['id']
                    ),
                    false
                );
            }
            die();
            break;
        case 'edit_address':
            $APPLICATION->IncludeComponent(
                "custom:sale.personal.profile.detail",
                "popup",
                array(
                    "PATH_TO_LIST" => '',
                    "PATH_TO_DETAIL" => '',
                    "SET_TITLE" => 'N',
                    "USE_AJAX_LOCATIONS" => 'Y',
                    "ID" => intval($postData['id']) ? $postData['id'] : $postData['ID'],
                ),
                false
            );
            die();
            break;
        case 'delete_address':
            if (intval($postData['id'])) {
                if (CModule::IncludeModule("sale")) {
                    $dbUserProps = CSaleOrderUserProps::GetList(
                        array(),
                        array(
                            "ID" => $postData['id'],
                            "USER_ID" => IntVal($USER->GetID())
                        )
                    );
                    if ($arUserProps = $dbUserProps->Fetch())
                    {
                        if (CSaleOrderUserProps::Delete($arUserProps["ID"]))
                        {
                            $arResult['success'] = "Y";
                            $arResult['id'] = $postData['id'];
                            $arResult['message'] = GetMessage('T_DELETE_ADDRESS_SUCCESS');
                        }
                    }
                }
            }
            if ($arResult['success'] != "Y") {
                $arResult['error']['all'] = GetMessage('T_DELETE_ADDRESS_ERROR');
            }
            break;
    }
}

echo json_encode($arResult);
die();
