<?
define("NOT_CHECK_PERMISSIONS", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if ($_SERVER["REQUEST_METHOD"]=="POST" && strlen($_POST["basketChange"])>0 && check_bitrix_sessid())
{
	if (isset($_POST["site_id"]))
		$site_id = $_POST["site_id"];

	if(isset($_SESSION["SALE_BASKET_NUM_PRODUCTS"][$site_id]))
	{
		$num_products = $_SESSION["SALE_BASKET_NUM_PRODUCTS"][$site_id];
		$sum_products = $_SESSION["SALE_BASKET_SUM_PRODUCTS"][$site_id];
	}
	else
	{
		if(!CModule::IncludeModule("sale"))
		{
			return;
		}
		$fUserID = CSaleBasket::GetBasketUserID(True);
		$fUserID = IntVal($fUserID);
		$num_products = 0;
		$sum_products = 0;
		
		if ($fUserID > 0)
		{
			$dbRes = CSaleBasket::GetList(
				array(),
				array(
					"FUSER_ID" => $fUserID,
					"LID" => $site_id,
					"ORDER_ID" => "NULL",
					"CAN_BUY" => "Y",
					"DELAY" => "N",
					"SUBSCRIBE" => "N"
				)
			);
			while ($arItem = $dbRes->GetNext())
			{
				if (!CSaleBasketHelper::isSetItem($arItem)) 
				{
					$num_products++;
                    $sum_products += $arItem['PRICE']*$arItem['QUANTITY'];
				}	
			}
		}
		$_SESSION["SALE_BASKET_NUM_PRODUCTS"][$site_id] = intval($num_products);
		$_SESSION["SALE_BASKET_SUM_PRODUCTS"][$site_id] = intval($sum_products);
	}

	$APPLICATION->RestartBuffer();
	echo json_encode(array('NUM' => $num_products, 'PROD_TITLE' => sklonenie($num_products, 'товар', 'товара', 'товаров'), 'TOTAL' => $sum_products));
	die();
}

$arResult = array('success' => 'N');
if ($_SERVER["REQUEST_METHOD"]=="POST" && ($_POST["action"] == 'set_coupon') && ($_POST["coupon"] != '') && check_bitrix_sessid())
{

    if (CModule::IncludeModule('catalog') && CCatalogDiscountCoupon::SetCoupon($_POST["coupon"])) {
        $arResult['success'] = 'Y';
    } else
        CCatalogDiscountCoupon::ClearCoupon();
}
echo json_encode($arResult);
die();
?>