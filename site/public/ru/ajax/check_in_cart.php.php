<?
define("NOT_CHECK_PERMISSIONS", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$arResult = array();
$arResult['success'] = false;

if ($_SERVER["REQUEST_METHOD"]=="POST" && ($_POST["action"] == 'add2cart') && intval($_POST["ID"]) && check_bitrix_sessid())
{
	$arResult['success'] = true;
	if (\Bitrix\Main\Loader::includeModule("sale") && \Bitrix\Main\Loader::includeModule("catalog"))
	{
		$productID = $_POST["ID"];
		$QUANTITY = 0;
		$product_properties = array();
		$intProductIBlockID = intval(CIBlockElement::GetIBlockByID($productID));
		
		
		if (0 < $intProductIBlockID)
		{
			$QUANTITY = 1;
		}
		else
		{
			$arResult['error'][] = 'element_not_found';
			$arResult['success'] = false;
		}

		if ($arResult['success'])
		{
			if(!Add2BasketByProductID($productID, $QUANTITY, array(), $product_properties))
			{
				if ($ex = $APPLICATION->GetException())
					$arResult['error'][] = $ex->GetString();
				else
					$arResult['error'][] = 'add_to_basket';
				$arResult['success'] = false;
			}
		}
	}
	else
		$arResult['error'][] = 'module_include';
	
}
echo json_encode($arResult);
die();