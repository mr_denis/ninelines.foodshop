<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 23.12.14
 * Time: 15:31
 */

define("NOT_CHECK_PERMISSIONS", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
__IncludeLang($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . "/lang/".LANGUAGE_ID."/ajax.php", false, true);

$arResult = array(
    'success' => 'N',
    'errors' => array(),
    'message' => ''
);

if ($_SERVER['REQUEST_METHOD'] == 'POST' && check_bitrix_sessid()) {
    $postData = $_REQUEST;
    global $USER;
    switch ($postData['action']) {
        case 'login':
            if ($USER->IsAuthorized()) {
                $arResult['success'] = 'N';
                $arResult['errors']['all'] = GetMessage('T_LOGIN_ALREADY');
            } else {
                $arAvailableFields = array(
                    'login',
                    'password'
                );
                foreach ($arAvailableFields as $field) {
                    $postData[$field] = trim($postData[$field]);
                    if (!strlen($postData[$field]))
                        $arResult['errors'][$field] = GetMessage('T_LOGIN_EMPTY_'.strtoupper($field));
                }

                if (empty($arResult['errors'])) {
                    $arAuthResult = $USER->Login($postData['login'], $postData['password'], "Y");
                    if (!empty($arAuthResult) && !is_array($arAuthResult)) {
                        $arResult['success'] = 'Y';
                        $arResult['message'] = GetMessage('T_LOGIN_SUCCESS');
                    } else
                        $arResult['errors']['all'] = $arAuthResult['MESSAGE'];
                }
            }
            break;
        case 'registration':
            if ($USER->IsAuthorized()) {
                $arResult['success'] = 'N';
                $arResult['errors']['all'] = GetMessage('T_REGISTRATION_ALREADY');
            } else {
                $arAvailableFields = array(
                    'name',
                    'email',
                    'password',
                    'confirm_password',
                );

                foreach ($arAvailableFields as $field) {
                    $postData[$field] = trim($postData[$field]);
                    if (!strlen($postData[$field]))
                        $arResult['errors'][$field] = GetMessage('T_REGISTRATION_EMPTY_'.strtoupper($field));

                    if ($field == 'email' && !isset($arResult['errors'][$field]) && !check_email($postData[$field]))
                        $arResult['errors'][$field] = GetMessage('T_REGISTRATION_ERROR_'.strtoupper($field));
                }

                if (empty($arResult['errors']) && ($postData['password'] != $postData['confirm_password'])) {
                    $arResult['errors']['password'] = GetMessage('T_REGISTRATION_ERROR_PASSWORD');
                    $arResult['errors']['confirm_password'] = GetMessage('T_REGISTRATION_ERROR_CONFIRM_PASSWORD');
                }

                if (empty($arResult['errors'])) {
                    $arName = explode(' ', $postData['name']);

                    $arRes = $USER->Register($postData['email'], $arName[1], $arName[0], $postData['password'], $postData['confirm_password'], $postData['email'], SITE_ID);

                    if (is_array($arRes) && ($arRes['TYPE'] == 'ERROR')) {
                        $arResult['errors']['all'] = $arRes['MESSAGE'];
                    } else if (is_array($arRes) && ($arRes['TYPE'] == 'OK')) {
                        $arResult['success'] = 'Y';
                        $arResult['message'] = $arRes['MESSAGE'].'<br>'.GetMessage('T_REGISTER_OK');
                    }
					$USER->Logout();
                }
            }
            break;
        case 'forgot':
            if ($USER->IsAuthorized()) {
                $arResult['success'] = 'N';
                $arResult['errors']['all'] = GetMessage('T_FORGOT_ALREADY');
            } else {
                $arAvailableFields = array(
                    'email',
                );
                foreach ($arAvailableFields as $field) {
                    $postData[$field] = trim($postData[$field]);
                    if (!strlen($postData[$field]))
                        $arResult['errors'][$field] = GetMessage('T_FORGOT_EMPTY_'.strtoupper($field));

                    if ($field == 'email' && !isset($arResult['errors'][$field]) && !check_email($postData[$field]))
                        $arResult['errors'][$field] = GetMessage('T_FORGOT_ERROR_'.strtoupper($field));
                }

                if (empty($arResult['errors'])) {
                    $sendResult = $USER->SendPassword('', $postData['email']);
                    if ($sendResult['TYPE'] == 'OK') {
                        $arResult['success'] = 'Y';
                        $arResult['message'] = $sendResult['MESSAGE'];
                    } else {
                        $arResult['errors']['all'] = $sendResult['MESSAGE'];
                    }
                }
            }
            break;
        case 'profile_edit':
            if ($USER->IsAuthorized()) {
                $arAvailableFields = array(
                    'name',
                    'last_name',
                    'email',
                    'phone',
                );

                foreach ($arAvailableFields as $field) {
                    $postData[$field] = trim($postData[$field]);
                    if (!strlen($postData[$field]))
                        $arResult['errors'][$field] = GetMessage('T_PROFILE_EDIT_EMPTY_'.strtoupper($field));

                    if ($field == 'email' && !isset($arResult['errors'][$field]) && !check_email($postData[$field]))
                        $arResult['errors'][$field] = GetMessage('T_PROFILE_EDIT_ERROR_'.strtoupper($field));
                }

                if (empty($arResult['errors'])) {
                    $arFields = array(
                        'NAME' => $postData['name'],
                        'LAST_NAME' => $postData['last_name'],
                        'EMAIL' => $postData['email'],
                        'PERSONAL_PHONE' => $postData['phone'],
                    );
                    $user = new CUser;
                    if ($user->Update($USER->GetID(), $arFields)) {
                        $arResult['success'] = 'Y';
                        $arResult['message'] = GetMessage('T_PROFILE_EDIT_OK');
                    } else {
                        $arResult['errors']['all'] = $user->LAST_ERROR;
                    }
                }
            } else {
                $arResult['success'] = 'N';
                $arResult['errors']['all'] = GetMessage('T_PROFILE_EDIT_ERROR_AUTH');
            }
            break;
        case 'edit_password':
            if ($USER->IsAuthorized()) {
                $arAvailableFields = array(
                    'old_password',
                    'new_password',
                    'confirm_password',
                );

                foreach ($arAvailableFields as $field) {
                    $postData[$field] = trim($postData[$field]);
                    if (!strlen($postData[$field]))
                        $arResult['errors'][$field] = GetMessage('T_EDIT_PASSWORD_EMPTY_'.strtoupper($field));
                }

                if (empty($arResult['errors'])) {
                    if ($postData['new_password'] != $postData['confirm_password']) {
                        $arResult['errors']['new_password'] = GetMessage('T_EDIT_PASSWORD_ERROR_PASSWORD_CONFIRMATION');
                        $arResult['errors']['confirm_password'] = GetMessage('T_EDIT_PASSWORD_ERROR_PASSWORD_CONFIRMATION');
                        $arResult['errors']['all'] = GetMessage('T_EDIT_PASSWORD_ERROR_PASSWORD_CONFIRMATION');
                    }

                    if (!$USER->Login($USER->GetLogin(), $postData['old_password'], "Y")) {
                        $arResult['errors']['old_password'] = GetMessage('T_EDIT_PASSWORD_ERROR_OLD_PASSWORD');
                        $arResult['errors']['all'] = GetMessage('T_EDIT_PASSWORD_ERROR_OLD_PASSWORD');
                    }
                }

                if (empty($arResult['errors'])) {
                    $user = new CUser;
                    $arFields = array(
                        'PASSWORD' => $postData['new_password'],
                        'CONFIRM_PASSWORD' => $postData['confirm_password'],
                    );
                    if ($user->Update($USER->GetID(), $arFields)) {
                        $arResult['success'] = 'Y';
                        $arResult['message'] = GetMessage('T_EDIT_PASSWORD_OK');
                    } else {
                        $arResult['errors']['all'] = $user->LAST_ERROR;
                    }
                }
            } else {
                $arResult['errors']['all'] = GetMessage('T_EDIT_PASSWORD_ERROR_AUTH');
            }
            break;
    }
} else {
    $arResult['errors']['all'] = GetMessage('T_ERROR_'.strtoupper($postData['action']));
}

echo json_encode($arResult);
die();
