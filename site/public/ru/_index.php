<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Интернет-магазин \"Инструменты\"");
?>
<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "main_slider",
    array(
        "IBLOCK_TYPE" => "services",
        "IBLOCK_ID" => '#SLIDER_IBLOCK_ID#',
        "NEWS_COUNT" => "10",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "",
        "FIELD_CODE" => array(
            0 => "NAME",
            1 => "PREVIEW_TEXT",
            2 => "",
        ),
        "PROPERTY_CODE" => array(
            0 => "RELATED_ITEM",
            1 => "STYLE",
        ),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "INCLUDE_SUBSECTIONS" => "Y",
        "PAGER_TEMPLATE" => ".default",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "AJAX_OPTION_ADDITIONAL" => ""
    ),
    false
);?>

    <section class="content-main">
    <?$APPLICATION->IncludeComponent(
        "bitrix:news.list",
        "main_banner_slider",
        array(
            "IBLOCK_TYPE" => "news",
            "IBLOCK_ID" => '#MAIN_BANNER_IBLOCK_ID#',
            "NEWS_COUNT" => "3",
            "SORT_BY1" => "ACTIVE_FROM",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "",
            "FIELD_CODE" => array(
                0 => "NAME",
                1 => "PREVIEW_TEXT",
                2 => "",
            ),
            "PROPERTY_CODE" => array(
                0 => "",
                1 => "RELATED_ITEM",
                2 => "STYLE",
                3 => "",
            ),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "PREVIEW_TRUNCATE_LEN" => "",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_STATUS_404" => "N",
            "SET_TITLE" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "INCLUDE_SUBSECTIONS" => "Y",
            "PAGER_TEMPLATE" => ".default",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "Y",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "AJAX_OPTION_ADDITIONAL" => ""
        ),
        false
    );?>

    <div class="catalog">
        <div class="phone-nav-tabs">
            <form method="post">
                <select class="select-class" class="main-tab-select" onchange="setTabWithSelect(this)">
                    <option value="#recomend">Мы рекомендуем</option>
                    <option value="#news">Новинки</option>
                    <option value="#hits">Хиты продаж</option>
                    <option value="#discount">Скидки</option>
                </select>
            </form>
        </div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li class="active"><a href="#recomend" class="main-tab-check" role="tab" data-toggle="tab">Мы рекомендуем</a></li>
            <li><a href="#news" role="tab" class="main-tab-check"  data-toggle="tab">Новинки</a></li>
            <li><a href="#hits" role="tab" class="main-tab-check"  data-toggle="tab">Хиты продаж</a></li>
            <li><a href="#discount" role="tab" class="main-tab-check"  data-toggle="tab">Скидки</a></li>
        </ul>
        <script>
            $(function(){
                    $('.nav.nav-tabs li:first-child').removeClass('active');
                    $('.nav.nav-tabs li:first-child a').click();}
            );
        </script>
        <!-- Tab panes -->
        <div class="tab-content">
            <?
            $arTabs = array(
                'recomend' => array(
                    'FILTER_NAME' => 'arrRecomendFilter',
                    'FILTER_VALUE' => array('!PROPERTY_RECOMEND' => false),
				
			),
			'news' => array(
				'FILTER_NAME' => 'arrNewFilter',
				'FILTER_VALUE' => array('!PROPERTY_NEWPRODUCT' => false),
				
			),
			'hits' => array(
				'FILTER_NAME' => 'arrHitFilter',
				'FILTER_VALUE' => array('!PROPERTY_SALELEADER' => false),
				
			),
			'discount' => array(
				'FILTER_NAME' => 'arrDiscountFilter',
				'FILTER_VALUE' => array('!PROPERTY_DISCOUNT' => false),
				
			)
		);
		foreach ($arTabs as $type => $arTab)
		{
?>		
			<div class="tab-pane active" id="<?=$type?>">
				<?
				if (!isset($_POST['action']) || ($_POST['action'] == 'get_catalog_items'))
				{
					global ${$arTab['FILTER_NAME']};
					${$arTab['FILTER_NAME']} = $arTab['FILTER_VALUE'];
					
					
					$APPLICATION->IncludeComponent(
						"bitrix:catalog.section", 
						"", 
						array(
							"AJAX_LOAD" => "Y",
							"BY_LINK" => "Y",
							"IBLOCK_TYPE" => "catalog",
							"IBLOCK_ID" => '#CATALOG_IBLOCK_ID#',
							"ELEMENT_SORT_FIELD" => "sort",
							"ELEMENT_SORT_ORDER" => "asc",
							"ELEMENT_SORT_FIELD2" => "active_from",
							"ELEMENT_SORT_ORDER2" => "asc",
							"PROPERTY_CODE" => array(
								0 => "BRAND_REF",
								1 => "NEWPRODUCT",
								2 => "SALELEADER",
								3 => "SPECIALOFFER",
								4 => "ARTNUMBER",
								5 => "MANUFACTURER",
								6 => "MATERIAL",
								7 => "COLOR",
								8 => "RECOMMEND",
								9 => "MINIMUM_PRICE",
								10 => "MAXIMUM_PRICE",
								11 => "DISCOUNT",
								12 => "PRODUCT_OF_DAY",
								13 => "PRODUCT_ACTION",
								14 => "OLD_PRICE",
							),
							"META_KEYWORDS" => "-",
							"META_DESCRIPTION" => "-",
							"BROWSER_TITLE" => "-",
							"INCLUDE_SUBSECTIONS" => "A",
							"BASKET_URL" => "",
							"ACTION_VARIABLE" => "",
							"PRODUCT_ID_VARIABLE" => "",
							"SECTION_ID_VARIABLE" => "",
							"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
							"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
							"FILTER_NAME" => $arTab['FILTER_NAME'],
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => ($_POST['container'] == $type.'_container') ? 0 : "36000",
							"CACHE_FILTER" => "Y",
							"CACHE_GROUPS" => "Y",
							"SET_TITLE" => "N",
							"SET_STATUS_404" => "N",
							"DISPLAY_COMPARE" => "N",
							"PAGE_ELEMENT_COUNT" => "15",
							"LINE_ELEMENT_COUNT" => "",
							"PRICE_CODE" => array(
								0 => "BASE",
							),
							"USE_PRICE_COUNT" => "N",
							"SHOW_PRICE_COUNT" => "",
							"PRICE_VAT_INCLUDE" => "N",
							"USE_PRODUCT_QUANTITY" => "N",
							"ADD_PROPERTIES_TO_BASKET" => "N",
							"PARTIAL_PRODUCT_PROPERTIES" => "N",
							"PRODUCT_PROPERTIES" => array(
							),
							"DISPLAY_TOP_PAGER" => "N",
							"DISPLAY_BOTTOM_PAGER" => "N",
							"PAGER_TITLE" => "",
							"PAGER_SHOW_ALWAYS" => "N",
							"PAGER_TEMPLATE" => ".default",
							"PAGER_DESC_NUMBERING" => "N",
							"PAGER_DESC_NUMBERING_CACHE_TIME" => "",
							"PAGER_SHOW_ALL" => "N",
							"OFFERS_CART_PROPERTIES" => array(
							),
							"OFFERS_FIELD_CODE" => array(
								0 => "",
								1 => $arParams["LIST_OFFERS_FIELD_CODE"],
								2 => "",
							),
							"OFFERS_PROPERTY_CODE" => array(
								0 => "ARTNUMBER",
								1 => "COLOR_REF",
								2 => "MORE_PHOTO",
								3 => "",
								4 => "",
							),
							"OFFERS_SORT_FIELD" => "name",
							"OFFERS_SORT_ORDER" => "asc",
							"OFFERS_SORT_FIELD2" => "sort",
							"OFFERS_SORT_ORDER2" => "asc",
							"OFFERS_LIMIT" => "0",
							"SECTION_ID" => "",
							"SECTION_CODE" => "",
							"SECTION_URL" => "",
							"DETAIL_URL" => "",
							"CONVERT_CURRENCY" => "N",
							"CURRENCY_ID" => $arParams["CURRENCY_ID"],
							"HIDE_NOT_AVAILABLE" => "N",
							"LABEL_PROP" => "-",
							"ADD_PICT_PROP" => "-",
							"PRODUCT_DISPLAY_MODE" => "N",
							"OFFER_ADD_PICT_PROP" => $arParams["OFFER_ADD_PICT_PROP"],
							"OFFER_TREE_PROPS" => $arParams["OFFER_TREE_PROPS"],
							"PRODUCT_SUBSCRIPTION" => "N",
							"SHOW_DISCOUNT_PERCENT" => "N",
							"SHOW_OLD_PRICE" => "Y",
							"MESS_BTN_BUY" => "",
							"MESS_BTN_ADD_TO_BASKET" => "",
							"MESS_BTN_SUBSCRIBE" => "",
							"MESS_BTN_DETAIL" => "",
							"MESS_NOT_AVAILABLE" => "",
							"TEMPLATE_THEME" => "",
							"ADD_SECTIONS_CHAIN" => "N",
							"SECTION_USER_FIELDS" => array(
								0 => "",
								1 => "",
							),
							"SHOW_ALL_WO_SECTION" => "Y",
							"AJAX_MODE" => "N",
							"AJAX_OPTION_JUMP" => "N",
							"AJAX_OPTION_STYLE" => "N",
							"AJAX_OPTION_HISTORY" => "N",
							"SET_META_KEYWORDS" => "N",
							"SET_META_DESCRIPTION" => "N",
							"AJAX_OPTION_ADDITIONAL" => "",
							"TAB_TYPE" => $type
						),
						false
					);
				}?>
            </div>
<?
		}
?>			
        </div>
    </div>
	<?
	global $arrMainNewsFilter;
	$arrMainNewsFilter = array(
		'!PREVIEW_PICTURE' => false,
	);
	
	$APPLICATION->IncludeComponent(
		"bitrix:news.list", 
		"main_news", 
		array(
			"IBLOCK_TYPE" => "news",
			"IBLOCK_ID" => '#NEWS_IBLOCK_ID#',
			"NEWS_COUNT" => "20",
			"SORT_BY1" => "ACTIVE_FROM",
			"SORT_ORDER1" => "DESC",
			"SORT_BY2" => "SORT",
			"SORT_ORDER2" => "ASC",
			"FILTER_NAME" => "arrMainNewsFilter",
			"FIELD_CODE" => array(
				0 => "NAME",
				1 => "PREVIEW_TEXT",
				2 => "ACTIVE_FROM",
			),
			"PROPERTY_CODE" => array(
			),
			"CHECK_DATES" => "Y",
			"DETAIL_URL" => "",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"AJAX_OPTION_HISTORY" => "N",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"CACHE_FILTER" => "Y",
			"CACHE_GROUPS" => "Y",
			"PREVIEW_TRUNCATE_LEN" => "",
			"ACTIVE_DATE_FORMAT" => "d.m.Y",
			"SET_STATUS_404" => "N",
			"SET_TITLE" => "N",
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
			"ADD_SECTIONS_CHAIN" => "N",
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",
			"PARENT_SECTION" => "",
			"PARENT_SECTION_CODE" => "",
			"INCLUDE_SUBSECTIONS" => "Y",
			"PAGER_TEMPLATE" => ".default",
			"DISPLAY_TOP_PAGER" => "N",
			"DISPLAY_BOTTOM_PAGER" => "N",
			"PAGER_TITLE" => "Новости",
			"PAGER_SHOW_ALWAYS" => "Y",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "N",
			"AJAX_OPTION_ADDITIONAL" => ""
		),
		false
	);?>
    
	<?/*$APPLICATION->IncludeComponent(
			"custom:highloadblock.list", 
			"advantages", 
			array(
				"BLOCK_ID" => "4",
				"DETAIL_URL" => "",
				"ROWS_PER_PAGE"   => 4,
			), 
		false);*/?>
	<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
        array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_DIR."include/advantages.php"
        ),
	false);
	?>	
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>