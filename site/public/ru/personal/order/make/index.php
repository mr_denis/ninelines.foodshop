<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заказы");
?>
		<div class="checkout-progress" id="checkout-progress">
			<a href="<?=SITE_DIR?>personal/cart/" class="select" title="">
				<span class="num">1</span>
				<span class="no-num">Корзина</span>
			</a>
			<a href="javascript:void(0);" id="cart-order-step" class="select" onclick="showOrderStep()" title="">
				<span class="num">2</span>
				<span class="no-num">Контакты и доставка</span>
			</a>
			<a href="javascript:void(0);" title="" id="cart-payment-step" onclick="showPaymentStep()">
				<span class="num">3</span>
				<span class="no-num">Оплата</span>
			</a>
			
			<span class="line line2"></span>
			<span class="line line1 s-line"></span>
		</div>
<?COption::SetOptionString("main","new_user_registration_email_confirmation","N");?>
<?$APPLICATION->IncludeComponent(
	"bitrix:sale.order.ajax",
	".default", 
	array(
		"PAY_FROM_ACCOUNT" => "N",
		"COUNT_DELIVERY_TAX" => "N",
		"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
		"ONLY_FULL_PAY_FROM_ACCOUNT" => "N",
		"ALLOW_AUTO_REGISTER" => "Y",
		"SEND_NEW_USER_NOTIFY" => "Y",
		"DELIVERY_NO_AJAX" => "Y",
		"TEMPLATE_LOCATION" => "popup",
		"PROP_1" => array(
		),
		"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
		"PATH_TO_PERSONAL" => SITE_DIR."personal/order/",
		"PATH_TO_PAYMENT" => SITE_DIR."personal/order/payment/",
		"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
		"SET_TITLE" => "Y",
		"DELIVERY2PAY_SYSTEM" => "",
		"SHOW_ACCOUNT_NUMBER" => "Y",
		"DELIVERY_NO_SESSION" => "Y",
		"DELIVERY_TO_PAYSYSTEM" => "d2p",
		"USE_PREPAYMENT" => "N",
		"ALLOW_NEW_PROFILE" => "Y",
		"SHOW_PAYMENT_SERVICES_NAMES" => "Y",
		"SHOW_STORES_IMAGES" => "N",
		"PATH_TO_AUTH" => SITE_DIR."auth/",
		"DISABLE_BASKET_REDIRECT" => "N",
		"PRODUCT_COLUMNS" => array(
		)
	),
	false
);?>
<?COption::SetOptionString("main","new_user_registration_email_confirmation","Y");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>