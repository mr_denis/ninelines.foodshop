<?
define('NEED_AUTH', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Личный кабинет");
?>

    <div class="col-set container">
        <div class="box element contact-information">
            <div class="box-title">
                <h3>Персональная информация</h3>
            </div>
            <div class="box-content">
                <?
                global $USER;
                $rsUser = CUser::GetByID($USER->GetID());
                $arUser = $rsUser->Fetch();
                ?>
                <p><span>Фамилия, Имя:</span>&nbsp;<?=implode(' ', array($arUser['LAST_NAME'], $arUser['NAME']))?></p>
                <p><span>Email:</span>&nbsp;<?=$arUser['EMAIL']?></p>
                <p><span>Телефон:</span>&nbsp;<?=$arUser['PERSONAL_PHONE']?></p>
                <a href="<?=SITE_DIR?>personal/profile/edit/" class="edit profile-edit-link" title="">Изменить данные</a>
                <a href="<?=SITE_DIR?>personal/profile/edit/"  class="edit edit-password-link" title="">Изменить пароль</a>
                <input type="hidden" id="profile-edit-data-name" value="<?=$arUser['NAME']?>">
                <input type="hidden" id="profile-edit-data-last_name" value="<?=$arUser['LAST_NAME']?>">
                <input type="hidden" id="profile-edit-data-email" value="<?=$arUser['EMAIL']?>">
                <input type="hidden" id="profile-edit-data-phone" value="<?=$arUser['PERSONAL_PHONE']?>">
            </div>
        </div>
        <div class="box element favorites">
            <div class="box-title">
                <h3>Избранное</h3>
            </div>
            <div class="box-content">
            <?if (!empty($_SESSION['USER_FAVORITES']) && is_array($_SESSION['USER_FAVORITES'])):?>
                <p>У Вас в избранном <span><?=count($_SESSION['USER_FAVORITES'])?></span> <?=sklonenie(count($_SESSION['USER_FAVORITES']), 'товар', 'товара', 'товаров')?></p>
                <a href="javascript:void(0)" class="favorites-link edit" title="">Изменить</a>
            <?else:?>
                <p>У Вас нет товаров в избранном</p>
            <?endif;?>
            </div>
        </div>
        <?$APPLICATION->IncludeComponent(
            "bitrix:sale.personal.profile",
            ".default",
            array(
                "SET_TITLE" => "N",
                "SEF_MODE" => "Y",
                "SEF_FOLDER" => SITE_DIR."personal/profile/",
                "PER_PAGE" => "20",
                "USE_AJAX_LOCATIONS" => "N",
                "SEF_URL_TEMPLATES" => array(
                    "list" => "profile_list.php",
                    "detail" => "profile_detail.php?ID=#ID#",
                ),
                "VARIABLE_ALIASES" => array(
                    "detail" => array(
                        "ID" => "ID",
                    ),
                )
            ),
            false
        );?>

        <?$APPLICATION->IncludeComponent(
            "bitrix:subscribe.edit",
            "",
            array(
                "SHOW_HIDDEN" => "N",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "N",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600",
                "SET_TITLE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "ALLOW_ANONYMOUS" => "Y",
                "SHOW_AUTH_LINKS" => "N"
            ),
            false
        );?>

        <?$APPLICATION->IncludeComponent(
            "bitrix:sale.personal.order.list",
            ".default",
            array(
                "ORDERS_PER_PAGE" => "2",
                "PATH_TO_PAYMENT" => SITE_DIR."personal/order/payment/",
                "PATH_TO_BASKET" => SITE_DIR."personal/cart/",
                "SET_TITLE" => "N",
                "SAVE_IN_SESSION" => "N",
                "NAV_TEMPLATE" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y H:i",
                "HISTORIC_STATUSES" => array(
                ),
                "CACHE_TYPE" => "Y",
                "CACHE_TIME" => "3600",
                "CACHE_GROUPS" => "Y",
                "PATH_TO_DETAIL" => "",
                "PATH_TO_COPY" => "",
                "PATH_TO_CANCEL" => "",
                "ID" => $ID
            ),
            false
        );?>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
