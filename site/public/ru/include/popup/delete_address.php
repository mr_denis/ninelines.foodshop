<?php

if ($USER->IsAuthorized()):
?>
<div class="windows-form windows-delete">
    <a class="close" title="Закрыть" href="#"></a>
    <div id="windows-delete-content">
        <div class="slide-div">
            <p id="delete-form-messages"></p>
            <form id="delete-form" method="post" action="/ajax/orderHandler.php">
                <p>Вы уверенны, что Вы хотите удалить данный профиль доставки?</p>
                <a href="javascript:void(0)" onclick="$('.windows-delete .close').click()" class="button button-grey" title="">Отмена</a>
                <a href="javascript:void(0)" id="delete-form-submit" class="button" title="">Удалить</a>
                <input type="hidden" name="id" id="delete-form-profile-id" value=""/>
                <input type="hidden" name="action" value="delete_address">
                <?=bitrix_sessid_post()?>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#delete-form-submit').click(function(){
        $('#windows-delete-content').addPreloader();
        $('#forgot-form-messages').html('');
        $.post($('#delete-form').attr('action'), $('#delete-form').serializeArray(), function(response){

            if (response.success == 'N') {
                if ($(response.errors).length) {
                    $.each(response.errors, function(key, message){
                        if (key == 'all') {
                            $('#delete-form-messages').html(message);
                        }
                    });
                }
            }
            $('#windows-delete-content').removePreloader();

            if (response.success == 'Y') {
                $('#delete-form-messages').html(response.message);
                $('#address-'+response.id).remove();
                if ($('.profile-address').length <= 0)
                    $('#address-default-block').hide();
            }
        }, 'json');
    });
</script>
<?endif;?>