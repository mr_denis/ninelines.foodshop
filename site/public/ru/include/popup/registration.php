<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 23.12.14
 * Time: 16:28
 */
if (!$USER->IsAuthorized()):
?>
<div class="windows-form windows-register">
    <a class="close" title="Закрыть" href="#"></a>
    <div id="windows-registration-content">
        <div class="caption">Регистрация</div>
        <form id="registration-form" action="/personal/profile/">
            <div class="slide-div">
                <div id="registration-form-messages"></div>
                <div class="input-box">
                    <input id="registration-form-input-name" type="text" class="first" name="name" placeholder="Фамилия, Имя" />
                </div>
                <div class="input-box">
                    <input id="registration-form-input-email" type="text" name="email" placeholder="Email" />
                </div>
                <div class="input-box">
                    <input id="registration-form-input-password" type="password" name="password" placeholder="Пароль" />
                </div>
                <div class="input-box">
                    <input id="registration-form-input-confirm_password" type="password" name="confirm_password" class="last" placeholder="Повторите пароль" />
                </div>
                <a  href="javascript:void(0);" id="registration-form-submit" class="button" title="">Зарегистрироваться</a>
            </div>
            <input type="hidden" name="action" value="registration">
            <?=bitrix_sessid_post()?>
        </form> 
        <div class="slide-div slide-div-last">
            <p>У вас уже есть аккаунт?</p>
            <a href="javascript:void(0)" class="login-link button button-grey" title="">Войти</a>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#registration-form-submit').click(function(){
        $('#windows-registration-content').addPreloader();
        $('#registration-form input').removeClass('error');
        $('#registration-form-messages').html('');
        $.post('<?=SITE_DIR?>ajax/userHandler.php', $('#registration-form').serializeArray(), function(response){

            if (response.success == 'N') {
                if ($(response.errors).length) {
                    $.each(response.errors, function(key, message){
                        if (key == 'all') {
                            $('#registration-form-messages').html(message);
                        } else {
                            $('#registration-form-input-'+key).addClass('error');
                        }

                    });
                }
            }
            $('#windows-registration-content').removePreloader();

            if (response.success == 'Y') {
                $('#registration-form-messages').html(response.message);
            }
        }, 'json');
    });
</script>
<?endif;?>