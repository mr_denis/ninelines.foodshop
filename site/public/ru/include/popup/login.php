<?php

global $USER;
if (!$USER->IsAuthorized()):
?>
<div class="windows-form windows-login">
    <a class="close" title="Закрыть" href="javascript:void(0);"></a>
    <div id="windows-login-content">
        <div class="caption">Вход</div>
        <form id="login-form" action="/personal/" method="post">
            <div class="slide-div">
                <div id="login-form-messages"></div>
                <div class="input-box">
                    <input id="login-form-input-login" type="text" class="first" name="login" placeholder="Email" />
                </div>
                <div class="input-box">
                    <input id="login-form-input-password" type="password" class="last" name="password" placeholder="Пароль" /><a href="<?=$APPLICATION->GetCurPageParam('forgot_password=yes', array('forgot_password'));?>" class="password forgot-link forgot-password-link" title="">Забыли?</a>
                </div>
                <a href="javascript:void(0);" id="login-form-submit" class="button" title="">Войти</a>
            </div>
            <input type="hidden" id="login-form-back-url-input" name="backurl" value="/personal/">
            <input type="hidden" name="action" value="login">
            <?=bitrix_sessid_post()?>
        </form>
        <div class="slide-div slide-div-last">
            <p>У вас нет аккаунта?</p>
            <a href="<?=$APPLICATION->GetCurPageParam('register=yes', array('register'));?>" class="registration-link button button-grey register-link" title="">Зарегистрироваться</a>
        </div>
    </div>
</div>
    <script type="text/javascript">
        $('#login-form-submit').click(function(){
            $('#windows-login-content').addPreloader();
            $('#login-form input').removeClass('error');
            $('#login-form-messages').html('');
            $.post('<?=SITE_DIR?>ajax/userHandler.php', $('#login-form').serializeArray(), function(response){

                if (response.success == 'N') {
                    if ($(response.errors).length) {
                        $.each(response.errors, function(key, message){
                            if (key == 'all') {
                                $('#login-form-messages').html(message);
                            } else {
                                $('#login-form-input-'+key).addClass('error');
                            }

                        });
                    }
                }
                $('#windows-login-content').removePreloader();

                if (response.success == 'Y') {
                    $('#login-form-messages').html(response.message);
                    window.location.href = $('#login-form-back-url-input').val();
                }
            }, 'json');
        });
    </script>
<?endif;?>