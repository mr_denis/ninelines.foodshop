<?php

if ($USER->IsAuthorized()):
?>
<div class="windows-form windows-edit">
    <a class="close" title="Закрыть" href="#"></a>
    <div id="windows-profile-edit-content">
        <div class="caption">Изменить данные</div>
        <form id="profile-edit-form" action="/personal/profile/">
            <div class="slide-div">
                <div id="profile-edit-form-messages"></div>
                <div class="input-box">
                    <input placeholder="Фамилия" name="last_name" id="profile-edit-form-input-last_name" type="text" value="" />
                </div>
                <div class="input-box">
                    <input placeholder="Имя" name="name" id="profile-edit-form-input-name" type="text" value="" />
                </div>
                <div class="input-box">
                    <input placeholder="Email" name="email" id="profile-edit-form-input-email" type="text" value="" />
                </div>
                <div class="input-box">
                    <input placeholder="Телефон" name="phone" id="profile-edit-form-input-phone" type="text" value="" />
                </div>
                <a href="javascript:void(0)" id="profile-edit-form-submit"  class="button" title="">сохранить</a>
            </div>
            <input type="hidden" name="action" value="profile_edit">
            <?=bitrix_sessid_post()?>
        </form>
    </div>
</div>
<script type="text/javascript">
    $('#profile-edit-form-submit').click(function(){
        $('#windows-profile-edit-content').addPreloader();
        $('#profile-edit-form input').removeClass('error');
        $('#profile-edit-form-messages').html('');
        $.post('<?=SITE_DIR?>ajax/userHandler.php', $('#profile-edit-form').serializeArray(), function(response){

            if (response.success == 'N') {
                if ($(response.errors).length) {
                    $.each(response.errors, function(key, message){
                        if (key == 'all') {
                            $('#profile-edit-form-messages').html(message);
                        } else {
                            $('#profile-edit-form-input-'+key).addClass('error');
                        }

                    });
                }
            }
            $('#windows-profile-edit-content').removePreloader();

            if (response.success == 'Y') {
                $('#profile-edit-form-messages').html(response.message);
                location.reload();
            }
        }, 'json');
    });
</script>
<?endif;?>