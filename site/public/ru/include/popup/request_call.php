<div id="request-call-windows" class="windows-form windows-call">
    <a class="close" title="Закрыть" href="javascript:void(0)"></a>
    <div id="request-call-windows-content" >
        <div class="caption">Заказать звонок</div>
        <div class="slide-div">
            <form id="request-call-form">
                <?=bitrix_sessid_post()?>
                <div class="input-box">
                    <input type="text" id="request-call-input-name" name="name" placeholder="Имя" />
                </div>
                <div class="input-box">
                    <input type="text" id="request-call-input-phone" name="phone" placeholder="Телефон" />
                </div>
                <input type="hidden" name="action" value="request_call" />
                <a href="JavaScript:void(0);" id="request-call-form-submit"  class="button" title="">Отправить</a>
            </form>
            <div id="request-call-form-note"></div>
        </div>
        <script>
            $('#request-call-form-submit').click(function(){
                $('#request-call-windows-content').addPreloader();
                var data = $('#request-call-form').serializeArray();
                $.post("<?=SITE_DIR?>ajax/handleRequestCall.php", data, function(response) {
                    $('#request-call-form .input-box input').removeClass('error');
                    $('#request-call-form-note').html('');
                    if (response.success == 'Y') {
                        $('#request-call-form').hide();
                        $('#request-call-form-note').html(response.message);
                        $('#request-call-windows-content').removePreloader();
                    } else {
                        if ($(response.error).length > 0) {
                            $.each(response.error, function(key, value){
                                if ($('#request-call-input-'+key).length > 0)
                                    $('#request-call-input-'+key).addClass('error');
                                else
                                    $('#request-call-form-note').append(value);
                            });
                        }

                        $('#request-call-windows-content').removePreloader();
                    }

                }, 'json');
            });
        </script>
    </div>
</div>
