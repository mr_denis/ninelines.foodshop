<?
global $USER;
$name = '';
$phone = '';
if ($USER->IsAuthorized()) {
    $dbUser = CUser::GetByID($USER->GetID());
    if ($arUser = $dbUser->fetch()) {
        $name = implode(' ', array($arUser['LAST_NAME'], $arUser['NAME']));
        $phone = $arUser['PERSONAL_PHONE'];
    }
}
?>

<div id="buy1click-windows" class="windows-form windows-buy1click" >
    <a class="close" title="Закрыть" href="javascript:void(0)"></a>
    <div id="buy1click-windows-windows-content" >
        <div class="caption">Купить в 1 клик</div>
        <div class="slide-div">
            <form id="buy1click-form" method="post" action="/ajax/buy1clickHandler.php">
                <?=bitrix_sessid_post()?>
                <input type="hidden" name="action" value="buy1click"/>
                <input type="hidden" id="buy1click-input-cart" name="cart" value="N"/>
                <input type="hidden" id="buy1click-input-id" name="id" value=""/>
                <input type="hidden" id="buy1click-input-offer_id" name="offer_id" value=""/>
                <input type="hidden" id="buy1click-input-price" name="price" value=""/>
                <p>Оставьте данные и наш оператор свяжется с Вами</p>
                <div class="input-box">
                    <input name="name" id="buy1click-input-name" type="text" value="<?=$name?>" placeholder="Имя" />
                </div>
                <div class="input-box">
                    <input name="phone" id="buy1click-input-phone" type="text" value="<?=$phone?>" placeholder="Телефон" />
                </div>
                <a href="javascript:void(0)" id="buy1click-form-submit" class="button" title="">Отправить</a>
            </form>
            <div id="buy1click-form-note"></div>
        </div>
    </div>
</div>
<script>
    $('#buy1click-form-submit').click(function(){
        $('#buy1click-windows-content').addPreloader();
        $.post($('#buy1click-form').attr('action'), $('#buy1click-form').serializeArray(), function(response) {
            $('#buy1click-form .input-box input').removeClass('error');
            $('#buy1click-form-note').html('');
            if (response.success == 'Y') {
                $('#buy1click-form').hide();
                $('#buy1click-form-note').html(response.message);
                $('#buy1click-windows-content').removePreloader();
            } else {
                if ($(response.error).length > 0) {
                    $.each(response.error, function(key, value){
                        if ($('#buy1click-input-'+key).length > 0)
                            $('#buy1click-input-'+key).addClass('error');
                        else
                            $('#buy1click-form-note').append(value);
                    });
                }

                $('#buy1click-windows-content').removePreloader();
            }

        }, 'json');
    });
</script>
