<?php

if ($USER->IsAuthorized()):
?>
<div class="windows-form windows-edit-address">
    <a class="close" title="Закрыть" href="#"></a>
    <div id="windows-edit-address-content">
        <div class="caption">Адрес доставки</div>
        <div class="slide-div">
            <div class="input-box">
                <input type="text" value="Фамилия, Имя" />
            </div>
            <div class="input-box">
                <input type="text" value="Email" />
            </div>
            <div class="input-box">
                <input type="text" value="Телефон" />
            </div>
            <div class="input-box">
                <input type="text" value="Город" />
            </div>
            <div class="input-box">
                <input type="text" value="Адрес доставки" />
            </div>
            <div class="input-box">
                <input type="text" value="Индекс" />
            </div>
            <a href="#" class="button" title="">сохранить</a>
        </div>
    </div>
</div>
<?endif;?>