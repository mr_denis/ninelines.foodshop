<?php

if ($USER->IsAuthorized()):
?>
<div class="windows-form windows-edit-password">
    <a class="close" title="Закрыть" href="#"></a>
    <div id="windows-edit-password-content">
        <div class="caption">Изменить пароль</div>
        <form id="edit-password-form" action="/personal/profile/edit/">
            <div class="slide-div">
                <div id="edit-password-form-messages"></div>
                <div class="input-box">
                    <input id="edit-password-form-input-old_password" name="old_password" type="password" placeholder="Старый пароль" value="" />
                </div>
                <div class="input-box">
                    <input id="edit-password-form-input-new_password" name="new_password" type="password" placeholder="Новый пароль" value=""/>
                </div>
                <div class="input-box">
                    <input id="edit-password-form-input-confirm_password" name="confirm_password" type="password" placeholder="Повторите новый пароль" value=""/>
                </div>
                <a href="javascript:void(0)"  id="edit-password-form-submit"  class="button" title="">сохранить</a>
            </div>
            <input type="hidden" name="action" value="edit_password">
            <?=bitrix_sessid_post()?>
        </form>
    </div>
</div>
<script type="text/javascript">
    $('#edit-password-form-submit').click(function(){
        $('#windows-edit-password-content').addPreloader();
        $('#edit-password-form input').removeClass('error');
        $('#edit-password-form-messages').html('');
        $.post('<?=SITE_DIR?>ajax/userHandler.php', $('#edit-password-form').serializeArray(), function(response){

            if (response.success == 'N') {
                if ($(response.errors).length) {
                    $.each(response.errors, function(key, message){
                        if (key == 'all') {
                            $('#edit-password-form-messages').html(message);
                        } else {
                            $('#edit-password-form-input-'+key).addClass('error');
                        }
                    });
                }
            }
            $('#windows-edit-password-content').removePreloader();

            if (response.success == 'Y') {
                $('#edit-password-form-messages').html(response.message);
            }
        }, 'json');
    });
</script>
<?endif;?>