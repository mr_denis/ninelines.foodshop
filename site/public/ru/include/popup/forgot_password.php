<?php

if (!$USER->IsAuthorized()):
?>
<div class="windows-form windows-forgot">
    <a class="close" title="Закрыть" href="javascript:void(0)"></a>
    <div id="windows-forgot-content">
        <div class="caption">Восстановить пароль</div>
        <div class="slide-div">
            <div id="forgot-form-messages"></div>
            <form id="forgot-form" action="/personal/profile/">
                <div class="input-box">
                    <input class="first" id="forgot-form-input-email" type="text" name="email" placeholder="Email" />
                </div>
                <a id="forgot-form-submit" href="javascript:void(0)" class="button" title="">Отправить новый пароль</a>
                <input type="hidden" name="action" value="forgot">
                <?=bitrix_sessid_post()?>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#forgot-form-submit').click(function(){
        $('#windows-forgot-content').addPreloader();
        $('#forgot-form input').removeClass('error');
        $('#forgot-form-messages').html('');
        $.post('<?=SITE_DIR?>ajax/userHandler.php', $('#forgot-form').serializeArray(), function(response){

            if (response.success == 'N') {
                if ($(response.errors).length) {
                    $.each(response.errors, function(key, message){
                        if (key == 'all') {
                            $('#forgot-form-messages').html(message);
                        } else {
                            $('#forgot-form-input-'+key).addClass('error');
                        }
                    });
                }
            }
            $('#windows-forgot-content').removePreloader();

            if (response.success == 'Y') {
                $('#forgot-form-messages').html(response.message);
            }
        }, 'json');
    });
</script>
<?endif;?>