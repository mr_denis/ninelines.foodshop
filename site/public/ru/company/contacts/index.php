<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>
<p>Хотя единого стандарта так и не придумано, любители новинок могут окунуться в новый мир сверх реального кино.
    С нашим ресурсом вы будете в курсе всех новинок и событий в мире 3D-телевизоров.
    Отсутствие единого стандарта, который обеспечит приверженность компаний, выпускающих фильмы.</p>

<section class="contacts-block">
    <article class="text">
        <p class="phone"><span>Телефон:</span> 8 (495) 225 63 50</p>
        <p class="addr"><span>Адрес:</span> г. Москва, ул. 1905 года д. 151, стр. 12</p>
        <p class="email"><span>Email:</span> <a href="mailto:info@eshop.ru"> info@eshop.ru</a></p>
        <p class="worktime"><span>Режим работы:</span>
            Будние дни: 9:00&ndash;22:00<br>
            Выходные дни: 9:00&ndash;20:00
        </p>
    </article>
    <article class="map">
        <?$APPLICATION->IncludeComponent(
            "bitrix:map.yandex.view",
            ".default",
            array(
                "INIT_MAP_TYPE" => "MAP",
                "MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:55.73829999999371;s:10:\"yandex_lon\";d:37.59459999999997;s:12:\"yandex_scale\";i:10;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:37.68386391601558;s:3:\"LAT\";d:55.72783908744833;s:4:\"TEXT\";s:14:\"Магазин\";}}}",
                "MAP_WIDTH" => "842",
                "MAP_HEIGHT" => "403",
                "CONTROLS" => array(
                ),
                "OPTIONS" => array(
                    0 => "ENABLE_SCROLL_ZOOM",
                    1 => "ENABLE_DRAGGING",
                ),
                "MAP_ID" => "contact_map"
            ),
            false
        );?>
    </article>
</section>

<?$APPLICATION->IncludeComponent(
	"bitrix:main.feedback", 
	".default", 
	array(
		"USE_CAPTCHA" => "Y",
		"OK_TEXT" => "Спасибо, ваш вопрос принят.",
		"EMAIL_TO" => "sale@e-shop.9-lines.com",
		"REQUIRED_FIELDS" => array(
			0 => "NAME",
			1 => "EMAIL",
			2 => "MESSAGE",
		),
		"EVENT_MESSAGE_ID" => array(
		)
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>