<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

if (isset($_REQUEST["backurl"]) && strlen($_REQUEST["backurl"])>0) 
	LocalRedirect($backurl);

$APPLICATION->SetTitle("Авторизация");
?>
<section class="inner-catalog forms-page">
	<div class="decription">
		<div class="windows-form">
			<div class="caption">Авторизация</div>
			<div class="slide-div">
				<p>Вы зарегистрированы и успешно авторизовались.</p>
				<p><a href="<?=SITE_DIR?>">Вернуться на главную страницу</a></p>
			</div>
		</div>
	</div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>