<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/install/wizard_sol/wizard.php");

class SelectSiteStep extends CSelectSiteWizardStep
{
    function InitStep()
    {
        $wizard =& $this->GetWizard();
        parent::InitStep();

        //$this->SetTitle(GetMessage("WIZ_STEP_SITE_SET"));
        //$this->SetNextCaption(GetMessage("NEXT_BUTTON"));
        //$this->content .= '<div class="wizard-upload-img-block"><div class="wizard-catalog-title">'.GetMessage("SITE").'</div>';
        //$this->content .= '<div class="wizard-upload-img-block"><div class="wizard-catalog-title"><a href = "http://9-lines.com/">'.GetMessage("SITE_DESCRITION").'</a></div>';
        $wizard->solutionName = "adaptiv_shop";


        /*global $SHOWIMAGEFIRST;
        $SHOWIMAGEFIRST = true;

        $this->content .= '<div class="inst-template-list-block">';
        foreach ($arTemplateOrder as $templateID)
        {
            $arTemplate = $arTemplateInfo[$templateID];

            if (!$arTemplate)
                continue;

            $this->content .= '<div class="inst-template-description">';
            $this->content .= $this->ShowRadioField("wizTemplateID", $templateID, Array("id" => $templateID, "class" => "inst-template-list-inp"));

            global $SHOWIMAGEFIRST;
            $SHOWIMAGEFIRST = true;

            if ($arTemplate["SCREENSHOT"] && $arTemplate["PREVIEW"])
                $this->content .= CFile::Show2Images($arTemplate["PREVIEW"], $arTemplate["SCREENSHOT"], 150, 150, ' class="inst-template-list-img"');
            else
                $this->content .= CFile::ShowImage($arTemplate["SCREENSHOT"], 150, 150, ' class="inst-template-list-img"', "", true);

            $this->content .= '<label for="'.$templateID.'" class="inst-template-list-label">'.$arTemplate["NAME"]."</label>";
            $this->content .= "</div>";
        }

        $this->content .= "</div>";*/
    }
}


class SelectTemplateStep extends CSelectTemplateWizardStep
{
}

class SelectThemeStep extends CSelectThemeWizardStep
{
}

class SiteSettingsStep extends CSiteSettingsWizardStep
{
    function InitStep()
    {
        $wizard =& $this->GetWizard();
        $wizard->solutionName = "adaptiv_shop";
        parent::InitStep();

        $templateID = $wizard->GetVar("templateID");

        $this->SetTitle(GetMessage("WIZ_STEP_SITE_SET"));
        $this->SetNextCaption(GetMessage("NEXT_BUTTON"));

        /*$wizard->SetDefaultVars(
            Array(
                "siteCopy" => $this->GetFileContent(WIZARD_SITE_PATH."include/copyright.php", GetMessage("COPYRIGHT_TEXT")),
                "sitePhone" => $this->GetFileContent(WIZARD_SITE_PATH."include/phone_two.php", GetMessage("PHONE_TWO_M")),
            )
        );*/
    }

    function ShowStep()
    {

        $wizard =& $this->GetWizard();

        //сайт
        /*$this->content .= '<div class="wizard-upload-img-block"><div class="wizard-catalog-title">'.GetMessage("WIZ_COPYRIGT").'</div>';
        //$this->content .= $wizard->GetVar("templateID").'1111';
        $this->content .= $this->ShowInputField("textarea", "siteCopy", Array("id" => "site-copy", "class" => "wizard-field", "rows"=>"3"))."</div>";

        $this->content .= '<div class="wizard-upload-img-block"><div class="wizard-catalog-title">'.GetMessage("PHONE_TWO").'</div>';
        $this->content .= $this->ShowInputField("textarea", "sitePhone", Array("id" => "site-phone", "class" => "wizard-field", "rows"=>"3"))."</div>";*/

        //Сайт
        $this->content .= '<div class="wizard-upload-img-block"><div class="wizard-catalog-title">'.GetMessage("SITE").'</div>';

            $this->content .= '
            <div class="wizard-input-form-block">
                <label for="siteName" class="wizard-input-title">'.GetMessage("WIZ_COMPANY_NAME").'</label>
                '.$this->ShowInputField('text', 'siteName', array("id" => "siteName", "class" => "wizard-field")).'
            </div>';

            $this->content .= '
            <div class="wizard-input-form-block">
                <label class="wizard-input-title" for="shopEmail">'.GetMessage("SITE_PHONE_TWO").'</label><br />
                '.$this->ShowInputField("text", "sitePhone", array("id" => "sitePhone", "class" => "wizard-field")).'
            </div>';

            $this->content .= '
            <div class="wizard-input-form-block">
                <label class="wizard-input-title" for="shopEmail">'.GetMessage("SITE_EMAIL").'</label><br />
                '.$this->ShowInputField("text", "siteEmail", array("id" => "siteEmail", "class" => "wizard-field")).'
            </div>
        </div>';

        //Интернет-магазин(продажи) email
        $this->content .= '<div class="wizard-upload-img-block"><div class="wizard-catalog-title">'.GetMessage("WIZ_SHOP").'</div>';
        $this->content .= '
        <div class="wizard-input-form-block">
            <label class="wizard-input-title" for="shopEmail">'.GetMessage("WIZ_SHOP_EMAIL").'</label>
            '.$this->ShowInputField('text', 'shopEmail', array("id" => "shopEmail", "class" => "wizard-field")).'
        </div>';
        $this->content .= GetMessage("SITE_DESCRITION").'<a target = "_blank" href = "http://9-lines.com/">'.'9-lines.com'.'</a>';
    }
    function OnPostForm()
    {
        $wizard =& $this->GetWizard();
    }
}

class DataInstallStep extends CDataInstallWizardStep
{
    function CorrectServices(&$arServices)
    {

        $wizard =& $this->GetWizard();
        if($wizard->GetVar("installDemoData") != "Y")
        {
        }

        //$this->content .= GetMessage("SITE_DESCRITION").'<a href = "http://9-lines.com/">'.'9-lines.com'.'</a>';
    }
}

class FinishStep extends CFinishWizardStep
{
}
?>